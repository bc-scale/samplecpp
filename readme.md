# Grow own

This is the place I grow my own skills on C++, Linux, Docker, Cross-Compiling, FPGA in open space.

## C++
src/sandbox contains study on C++ with various books and ariticles.

## V4L2, Qt
src/SpaceShape contains V4L2 based vision application using Qt.

## FPGA
fpga/eleven contains Verilog code based on Zynq using Xilinx Vivado. 

## Linux Platform
spinning contains Petalinux build for Minized. 


# Build & Run

## create image
```
samplecpp/docker_samplecpp_build$ docker build -f ./Dockerfile.samplecpp_build . -t samplecpp_build:0.1
```

## push image to registry
```
samplecpp/docker_samplecpp_build$ docker image tag samplecpp_build:0.1 registry.gitlab.com/sungwoonam/samplecpp/samplecpp_build:0.1
samplecpp/docker_samplecpp_build$ docker push registry.gitlab.com/sungwoonam/samplecpp/samplecpp_build:0.1
```

## build at docker
```
$ docker run --rm -it -v "$PWD":/du -w /du -u 0 samplecpp_build:0.1
```

### build x86_64
```
/du# rm -rf ./build_x86_64
/du# cmake -S . -B ./build_x86_64
/du# cmake --build ./build_x86_64

/du# rm -rf ./build_x86_64 && cmake -S . -B ./build_x86_64 && cmake --build ./build_x86_64
```

### run x86_64
```
/du/build_x86_64/bin# ./sandbox
/du/build_x86_64/bin# ./testbench
```

### build arm
```
/du# rm -rf ./build_arm
/du# cmake -S . -B ./build_arm -DCMAKE_TOOLCHAIN_FILE=../toolchain_raspberrypi_gcc9.cmake
/du# cmake --build ./build_arm

/du# rm -rf ./build_arm && cmake -S . -B ./build_arm -DCMAKE_TOOLCHAIN_FILE=../toolchain_raspberrypi_gcc9.cmake && cmake --build ./build_arm
```

### run arm
```
samplecpp/build_arm/bin$ sshpass -p 0506 scp -r ./* pi@192.168.1.65:~/samplecpp_build

pi@raspberrypi:~/samplecpp_build $ ./sandbox
pi@raspberrypi:~/samplecpp_build $ ./testbench
```



# vscode
* Auto indent : Ctrl-K + Ctrl-F 

