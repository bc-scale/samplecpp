# Install Linux Distro
## SSL agent setup
```
eval "$(ssh-agent -s)"
cp '/media/sungwoo/TERA/private/key/the.sungwooAtGmail'  .ssh
chmod 400 .ssh/the.sungwooAtGmail
ssh-add .ssh/the.sungwooAtGmail
ssh-add -l
ssh git@gitlab.com
```
##  vcode
* install extension C/C++, Python, GitGraph

## docker

```
$ sudo apt-get update
$ sudo apt-get install ca-certificates curl gnupg lsb-release
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
$ echo \
>   "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
>   $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
$ sudo apt-get install docker-ce docker-ce-cli containerd.io
$ sudo docker run hello-world
```

## install cmake
```
sudo apt-get -y install cmake
sudo apt-get -y install cmake-qt-gui
```

## sudoers
```
cd /etc
gedit sudoers
  add new_user
```

```
$ git config --global user.email "the.sungwoo@gmail.com"
$ gir config --global user.name "Sungwoo Nam"
```


# latest cmake
>sudo apt-get install libssl-dev 
download source of cmake
>cmake .
>make 
>sudo make install


# log4cpp
download log4cpp from sourceforge
>.configure, make, sudo make install
>sudo apt-get install libpthread-stubs0-dev
>sudo ldconfig  <- create the necessary links and cache ...


# googletest
download source
>cmake -S . -B ./build
>cmake --build ./build
>sudo cmake --install ./build

# poco
download source
>cmake -S . -B ./build
>cmake --build ./build
>sudo cmake --install ./build



# Setup Visual Studio Code for Multi-File C++ Projects 
https://dev.to/talhabalaj/setup-visual-studio-code-for-multi-file-c-projects-1jpi


# cmake
https://cmake.org/cmake/help/latest/guide/tutorial/A%20Basic%20Starting%20Point.html


# log4cpp - download from sourceforge
>.configure
>make
>sudo make install

## pthread
>sudo apt-get install libpthread-stubs0-dev

## create the necessary links and cache ...
>sudo ldconfig  

## qtsample
https://vitux.com/compiling-your-first-qt-program-in-debian-10/

## qt tutorial youtube
https://www.youtube.com/watch?v=JtyCM4BTbYo&list=PL2D1942A4688E9D63&index=4



## cross-compile
https://blog.kitware.com/cross-compiling-for-raspberry-pi/
https://www.raspberrypi.org/documentation/computers/linux_kernel.html


#raspberry
'pi' 'raspberry'


$ lsusb
Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 001 Device 004: ID 0408:5300 Quanta Computer, Inc. 
Bus 001 Device 008: ID 046d:c534 Logitech, Inc. Unifying Receiver
Bus 001 Device 006: ID 064f:2af9 WIBU-Systems AG CmStick (HID, article no. 1001-xx-xxx)
Bus 001 Device 005: ID 8087:0aaa Intel Corp. 
Bus 001 Device 013: ID 14cd:1212 Super Top microSD card reader (SY-T18) <- USB
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub


sungwoo@L2020:~$ ping raspberrypi.local
PING raspberrypi.local (192.168.1.46) 56(84) bytes of data.
64 bytes from 192.168.1.46 (192.168.1.46): icmp_seq=1 ttl=64 time=117 ms

sungwoo@L2020:~$ ssh pi@192.168.1.46
pi@raspberrypi:~ $ 

$cat .ssh/the.sungwooAtGmail | ssh pi@192.168.1.46 'cat >> .ssh/the.sungwooAtGmail'

# remote access
https://www.raspberrypi.org/documentation/computers/remote-access.html#introduction-to-remote-access

pi@raspberrypi:~ $  vncserver

# pi & qt
https://www.interelectronix.com/qt-on-the-raspberry-pi-4.html

/etc/apt/sources.list.
deb-src http://raspbian.raspberrypi.org/raspbian/ buster main contrib non-free rpi

$ sudo apt-get update
$ sudo apt-get dist-upgrade
$ sudo reboot
$ sudo rpi-update
$ sudo reboot

$ sudo apt-get build-dep qt5-qmake
$ sudo apt-get build-dep libqt5gui5
$ sudo apt-get build-dep libqt5webengine-data
$ sudo apt-get build-dep libqt5webkit5
$ sudo apt-get install libudev-dev libinput-dev libts-dev libxcb-xinerama0-dev libxcb-xinerama0 gdbserver

$ sudo mkdir /usr/local/RaspberryQt
$ sudo chown -R pi:pi /usr/local/RaspberryQt

$ sudo mkdir /opt/RaspberryQt
$ cd /opt/RaspberryQt/
$ sudo wget http://download.qt.io/archive/qt/5.15/5.15.2/single/qt-everywhere-src-5.15.2.tar.xz


# pi & qt
https://wiki.qt.io/Raspberry_Pi_Beginners_Guide


https://webnautes.tistory.com/957

$ sudo apt-get update
$ sudo apt-get install qt5-default qtbase5-dev qtdeclarative5-dev qt5-qmake qtcreator libqt5gui5  qtscript5-dev qtmultimedia5-dev libqt5multimedia5-plugins qtquickcontrols2-5-dev libqt5network5 cmake build-essential 
$ qmake -version
QMake version 3.1
Using Qt version 5.11.3 in /usr/lib/arm-linux-gnueabihf


#opencv
https://docs.opencv.org/4.5.1/d7/d9f/tutorial_linux_install.html

sudo apt update && sudo apt install -y cmake g++ wget unzip
wget -O opencv.zip https://github.com/opencv/opencv/archive/master.zip
unzip opencv.zip
mkdir -p build && cd build
cmake  ../opencv-master
cmake --build .
sudo make install

#Increase swap at pi ?
https://www.pyimagesearch.com/2019/09/16/install-opencv-4-on-raspberry-pi-4-and-raspbian-buster/

$ sudo nano /etc/dphys-swapfile
# set size to absolute value, leaving empty (default) then uses computed value
#   you most likely don't want this, unless you have an special disk situation
# CONF_SWAPSIZE=100
CONF_SWAPSIZE=2048

$ sudo /etc/init.d/dphys-swapfile stop
$ sudo /etc/init.d/dphys-swapfile start


# debug !!!
set(CMAKE_CXX_FLAGS "-g")


https://www.linuxtv.org/downloads/legacy/video4linux/API/V4L2_API/spec-single/v4l2.html



# check video devices
$ sudo apt-get install v4l-utils
$ v4l2-ctl --list-devices
IPC-HD1 WEBCAM: IPC-HD1 WEBCAM (usb-0000:00:14.0-1):
	/dev/video2
	/dev/video3
	/dev/media1

HP Wide Vision HD Camera: HP Wi (usb-0000:00:14.0-5):
	/dev/video0
	/dev/video1
	/dev/media0



cmake -S . -B ./build
cmake --build ./build

$ sudo apt-get install libv4l-dev


# opencv_highgui library cause below error, just opencv_core is fine
QXcbConnection: Failed to initialize XRandr
qt5ct: using qt5ct plugin
(Unittest:4136): GLib-GObject-WARNING **: 11:03:21.488: cannot register existing type 'GtkWidget'
...
(Unittest:4136): GLib-GObject-CRITICAL **: 11:03:21.489: g_type_register_static: assertion 'parent_type > 0' failed

# ???
Warning: Ignoring XDG_SESSION_TYPE=wayland on Gnome. Use QT_QPA_PLATFORM=wayland to run on Wayland anyway.
 


# shared library : 
# https://www.cprogramming.com/tutorial/shared-libraries-linux-gcc.html

$ gcc -L/home/username/foo -Wall -o test main.c -lfoo

$ ldconfig
That should create a link to our shared library and update the cache so it is available for immediate use. Let us double check:

$ ldconfig -p | grep foo
libfoo.so (libc6) => /usr/lib/libfoo.so

$ ldd test | grep foo
libfoo.so => /usr/lib/libfoo.so (0x00a42000)

# qt5 qml

$ apt list "libqt5*-dev" "qt*-dev"

$ sudo apt-get install qtdeclarative5-dev
$ sudo apt-get install qtquickcontrols2-5-dev
$ sudo apt install qml-module-qtquick-controls2

apt-cache search log4cpp

qml-module-qt-labs-folderlistmodel - Qt 5 folderlistmodel QML module
qml-module-qt-labs-handlers - Qt 5 Qt.labs.handlers QML module
qml-module-qt-labs-settings - Qt 5 settings QML module
qml-module-qt-labs-sharedimage - Qt 5 SharedImage QML module
qml-module-qt-labs-location - Qt Location Labs module
qml-module-qt-labs-calendar - Qt 5 qt.labs.calendar QML module
qml-module-qt-labs-platform - Qt 5 qt.labs.platform QML module


$ sudo apt-get install qml-module-qt-labs-folderlistmodel qml-module-qt-labs-handlers qml-module-qt-labs-settings qml-module-qt-labs-sharedimage qml-module-qt-labs-location qml-module-qt-labs-calendar qml-module-qt-labs-platform



# setup raspberry pi 4

from debian host
host$ ssh-keygen -R "192.168.1.46"
host$ ssh pi@192.168.1.46
pi$ mkdir .ssh
pi$ exit

host$ scp .ssh/the.sungwooAtGmail pi@192.168.1.46:.ssh/

pi$ nano .profile
#add below lines
eval "$(ssh-agent -s)"
ssh-add .ssh/the.sungwooAtGmail

pi$ sudo reboot

pi$ ssh-add -L
2048 SHA256:iSKanhwHs/NdTVirtZBOyTOexb4XUmKkAGmgI18q6/Q the.sungwoo@gmail.com (RSA)

# gitcola
pi$ sudo apt-get install git-cola
git@gitlab.com:SungwooNam/samplecpp.git
set git name and email

# install dependencies

sudo apt-get install v4l-utils libv4l-dev
sudo apt-get install liblog4cpp5-dev
sudo apt-get install libpoco-dev
sudo apt-get install libpthread-stubs0-dev
sudo apt-get install cmake
sudo apt-get install libopencv-dev
sudo apt-get install qt5-default qtbase5-dev qtdeclarative5-dev qt5-qmake qtcreator libqt5gui5  qtscript5-dev qtmultimedia5-dev libqt5multimedia5-plugins qtquickcontrols2-5-dev libqt5network5 cmake build-essential
sudo apt-get install qml-module-qt-labs-folderlistmodel qml-module-qt-labs-handlers qml-module-qt-labs-settings qml-module-qt-labs-sharedimage qml-module-qt-labs-location qml-module-qt-labs-calendar qml-module-qt-labs-platform

# googletest
```
$ sudo apt install libgtest-dev build-essential cmake
$ cd /usr/src/googletest
$ sudo cmake .
$ sudo cmake --build . --target install
```

https://doc.qt.io/archives/qt-5.11/qtqml-index.html


~/Downloads/MVS$ sudo dpkg -i MVS-2.1.0_x86_64_20201228.deb
/opt/MVS/bin$ ./MVS.sh


$ git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com


# yocto
* https://docs.yoctoproject.org/brief-yoctoprojectqs/index.html
```
$ sudo apt install gawk wget git diffstat unzip texinfo gcc build-essential chrpath socat cpio python3 python3-pip python3-pexpect xz-utils debianutils iputils-ping python3-git python3-jinja2 libegl1-mesa libsdl1.2-dev pylint3 xterm python3-subunit mesa-common-dev zstd liblz4-tool

~/depo$ git clone git://git.yoctoproject.org/poky
~/depo$ cd poky/
~/depo/poky$ git checkout -t origin/honister -b my-honister
~/depo/poky$ git pull
Already up to date.
~/depo/poky$ source oe-init-build-env 
~/depo/poky/build$ nano conf/local.conf 

BB_SIGNATURE_HANDLER = "OEEquivHash"
BB_HASHSERVE = "auto"
BB_HASHSERVE_UPSTREAM = "typhoon.yocto.io:8687"
SSTATE_MIRRORS ?= "file://.* https://sstate.yoctoproject.org/3.4.1/PATH;downloadfilename=PATH"

~/depo/poky/build$ bitbake core-image-sato

~/depo/poky/build$ runqemu qemux86-64

```

# docker

```
$ docker run --rm -it -v "$PWD":/home/docker_user -w /home/docker_user -u $(id -u):$(id -g) --name samplecpp_build_run samplecpp_build

as root
$ docker run --rm -it -v "$PWD":/home/docker_user -w /home/docker_user -u 0 --name samplecpp_build_run samplecpp_build

~/depo/samplecpp$ docker image build -t samplecpp_build .

$ docker image tag samplecpp_build:latest registry.gitlab.com/sungwoonam/samplecpp/cpp_build
$ docker push registry.gitlab.com/sungwoonam/samplecpp/cpp_build

```  


# vscode
* Auto indent : Ctrl-K + Ctrl-F 



## debugging

### CMAKE_SYSROOT , CMAKE_FIND_ROOT_PATH both turn me down
Tried to cross-compile with CMAKE_SYSROOT. But gcc can't find libraries - crt1.o , ... - from CMAKE_SYSROOT

```
at toolchain_raspberrypi_gcc9.cmake

CMAKE_SYSROOT

# set(CMAKE_SYSROOT 
#     /opt/raspios-buster-armhf-lite
# )
# set(CMAKE_FIND_ROOT_PATH 
#     /opt/raspios-buster-armhf-lite
#     /opt/raspios-buster-armhf-lite/usr/lib/arm-none-linux-gnueabihf   
# )


# add_compile_options( 
#     -I/opt/raspios-buster-armhf-lite/usr/include 
#     -I/opt/raspios-buster-armhf-lite/usr/include/arm-linux-gnueabihf
#     -L/opt/raspios-buster-armhf-lite/usr/lib 
#     -L/opt/raspios-buster-armhf-lite/usr/lib/arm-linux-gnueabihf 
# )
```

```
root@78ac32f8991a:/home/docker_user# ./buildit.py --clean

...
    Run Build Command(s):/opt/raspios-buster-armhf-lite/usr/bin/make cmTC_2fb92/fast && /opt/raspios-buster-armhf-lite/usr/bin/make: 1: Syntax error: word unexpected (expecting ")")
-> remove /opt/raspios-buster-armhf-lite/usr/bin

    /opt/gcc-arm-9.2-2019.12-x86_64-arm-none-linux-gnueabihf/bin/../lib/gcc/arm-none-linux-gnueabihf/9.2.1/../../../../arm-none-linux-gnueabihf/bin/ld: cannot find crt1.o: No such file or directory   
-> ???
# find -name crt1.o
./usr/lib/x86_64-linux-gnu/crt1.o
./opt/raspios-buster-armhf-lite/usr/lib/arm-linux-gnueabihf/crt1.o
./opt/gcc-arm-9.2-2019.12-x86_64-arm-none-linux-gnueabihf/arm-none-linux-gnueabihf/libc/usr/lib/crt1.o


# set(CMAKE_SYSROOT /opt/raspios-buster-armhf-lite/)
set(CMAKE_FIND_ROOT_PATH 
    /opt/raspios-buster-armhf-lite/
)
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)  <- make will be from host
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)


# this does not work !
# set(CMAKE_SYSROOT /opt/raspios-buster-armhf-lite/)
# set(CMAKE_FIND_ROOT_PATH /opt/raspios-buster-armhf-lite/)

# neither this !
# link_directories(BEFORE
#     /opt/raspios-buster-armhf-lite/usr
#     /opt/raspios-buster-armhf-lite/usr/lib
#     /opt/raspios-buster-armhf-lite/usr/lib/arm-linux-gnueabihf
# )
  
```

### error with c++17 filesystem
```
[ RUN      ] cpp_17_in_detail_part2.filesystem_demo
listing files in the directory: /home/pi/samplecpp_build
./sandbox: symbol lookup error: ./sandbox: undefined symbol: _ZNSt12__shared_ptrINSt10filesystem7__cxx114_DirELN9__gnu_cxx12_Lock_policyE2EEC1EOS5_
```