## installation

* install Xilinx 2020.2, petalinux
```
~/Downloads$ ./Xilinx_Unified_2020.2_1118_1232_Lin64.bin

~/Downloads$ sudo dpkg-reconfigure dash
    -> No
~/Downloads$ sudo apt-get install autoconf libtool gcc-multilib net-tools libncurses5-dev libncursesw5-dev libtinfo5

~/Downloads$ ./petalinux-v2020.2-final-installer.run --dir ~/petalinux
```

* install JTGA Drvier
```
$ cd ~/tools/Xilinx/Vivado/2020.2/data/xicom/cable_drivers/lin64/install_script/install_drivers/
$ sudo ./install_drivers
```

* ttyUSB* group
```
$ sudo usermod -aG dialout sungwoo
```

## minized

```
~/Downloads$ tar -xf minized_sbc_base_2020_2.tar.gz

~/depo$ source ~/petalinux/settings.sh

~/depo$ petalinux-create -t project -s ~/Downloads/minized_sbc_base_2020_2.bsp -n spinning

~/depo/spinning$ petalinux-build -c avnet-image-full

~/depo/spinning$ petalinux-package --boot --fsbl images/linux/zynq_fsbl.elf --u-boot images/linux/u-boot.elf --fpga images/linux/system.bit --force

~/depo/spinning$ ~/tools/Xilinx/Vitis/2020.2/bin/xsct

xsct% exec program_flash -f /home/sungwoo/depo/spinning/images/linux/BOOT.BIN -offset 0 -flash_type qspi-x4-single -fsbl /home/sungwoo/depo/spinning/images/linux/zynq_fsbl.elf -cable type xilinx_tcf url TCP:127.0.0.1:3121

```


## spinning from minized bsp

* NO way
```
~/depo/spinning$ petalinux-config
 changed hostname, firmware, version
~/depo/spinning$ petalinux-build
...
ERROR: petalinux-image-minimal-1.0-r0 do_image_wic: No kickstart files from WKS_FILES were found: petalinux-image-minimal.minized-sbc.wks petalinux-image-minimal.wks. Please set WKS_FILE or WKS_FILES appropriately.

```


## from xpr
* https://www.hackster.io/news/microzed-chronicles-building-petalinux-for-the-minized-48f79880f849

```
~/depo$ petalinux-create -t project -s ~/Downloads/minized_sbc_base_2020_2.bsp -n woopsy

~/depo/woopsy$ petalinux-config --get-hw-description ./hardware/minized_sbc_base_2020_2/

~/depo/woopsy$ petalinux-build
RROR: petalinux-image-minimal-1.0-r0 do_image_wic: No kickstart files from WKS_FILES were found: petalinux-image-minimal.minized-sbc.wks petalinux-image-minimal.wks. Please set WKS_FILE or WKS_FILES appropriately

```
* Starting with PetaLinux 2020.1 Avnet has started using the meta-avnet yocto layer for customizing kernel and rootfs configurations, etc.  As a result it is now necessary to specify on the PetaLinux command line that you are building an OS image for a Avnet SOM or SBC.  For Ultra96-V2 this will be: petalinux-build -c avnet-image-full 

* Refer here point that change doesn't goes to the image : https://support.xilinx.com/s/question/0D52E00006mGSmISAW/petalinux-kernel-modules-lost-with-avnetimageminimal?language=en_US


## minized wifi
* wpa_supplicant.conf first
```
root@plnx_arm:~# vi /run/media/mmcblk1p1/wpa_supplicant.conf 

ctrl_interface=/var/run/wpa_supplicant
ctrl_interface_group=0
update_config=1
network=(
    key_mgmt=WPA-PSK
    ssid="HotelStef"
    psk="74248483"
)

root@plnx_arm:~# cat /usr/local/bin/wifi.sh
root@plnx_arm:~# wifi.sh

``` 

* Wireless software support
The 1DX module is based on the Cypress CYW4343W, which is a Cypress Semiconductor IEEE
802.11 b/g/n MAC/Baseband radio with Bluetooth 4.1. Standalone Zynq projects are not supported,
only Linux using either the bcmdhd or brcmfmac Broadcom drivers as demonstrated in our reference
designs are supported.

* kernel boot log
```
root@mzspi:/usr/bin# dmesg | grep brcmfmac
brcmfmac: brcmf_fw_alloc_request: using brcm/brcmfmac43430-sdio for chip BCM43430/1
brcmfmac mmc1:0001:1: Direct firmware load for brcm/brcmfmac43430-sdio.bin failed with error -2
brcmfmac: brcmf_sdio_htclk: HT Avail timeout (1000000): clkctl 0x50

```

## petalinux on minized

```
$ source ~/petalinux/settings.sh
$ source ~/tools/Xilinx/Vivado/2020.2/settings64.sh

$ petalinux-build -c avnet-image-minimal

$ cp ./pre-built/linux/images/*.sh .
$ nano rebuild_minized_sbc_base.sh
BOOT_METHOD='INITRD'
BOOT_SUFFIX='_MINIMAL'
INITRAMFS_IMAGE="avnet-image-minimal"
configure_boot_method
build_bsp

#BOOT_METHOD='INITRD'
#BOOT_SUFFIX='_FULL'
#INITRAMFS_IMAGE="avnet-image-full"
#configure_boot_method
#build_bsp

#BOOT_METHOD='EXT4'
#unset BOOT_SUFFIX
#unset INITRAMFS_IMAGE
#configure_boot_method
#build_bsp


$ ./rebuild_minized_sbc_base.sh

$ petalinux-package --boot --fsbl ./images/linux/zynq_fsbl.elf --fpga ./images/linux/system.bit --uboot --kernel ./images/linux/image.ub -o BOOT.BIN --force --boot-device flash --add ./images/linux/avnet-boot/avnet_qspi.scr --offset 0xFC0000

$ ~/tools/Xilinx/Vitis/2020.2/bin/xsct
exec program_flash -f ./images/linux/BOOT.BIN -fsbl ./images/linux/zynq_fsbl.elf -flash_type qspi-x4-single

```
* 

## minized bsp xpr

* error message shown up at Vivado
```
 [Board 49-67] The board_part definition was not found for avnet.com:minized:part0:1.3. This can happen sometimes when you use custom board part. You can resolve this issue by setting 'board.repoPaths' parameter, pointing to the location of custom board files. Valid board_part values can be retrieved with the 'get_board_parts' Tcl command.

```
* error when exporting 
```
 [Common 17-53] User Exception: Specified ip cache dir /home/training/Projects/bsp_build_tests/test_tag/minized_sbc_base/hdl/projects/minized_sbc_base_2020_2/minized_sbc_base.cache/ip does not exist. Unable to copy into Shell.

```

* asked upgrade part but got errors and warnings
```
 [Common 17-55] 'get_property' expects at least one object.
Resolution: If [get_<value>] was used to populate the object, check to make sure this command returns at least one valid object.

 [IP_Flow 19-3477] Update of parameter 'PARAM_VALUE.C_ALL_OUTPUTS' failed for IP 'minized_sbc_base_axi_gpio_1_0'. ERROR: [Common 17-55] 'get_property' expects at least one object.
Resolution: If [get_<value>] was used to populate the object, check to make sure this command returns at least one valid object.


 [IP_Flow 19-3419] Update of 'minized_sbc_base_axi_gpio_1_0' to current project options has resulted in an incomplete parameterization. Please review the message log, and recustomize this instance before continuing with your design.

 [Coretcl 2-1279] The upgrade of 'minized_sbc_base_axi_gpio_1_0' has identified issues that may require user intervention. Please review the upgrade log '/home/sungwoo/depo/woopsy/hardware/minized_sbc_base_2020_2/ip_upgrade.log', and verify that the upgraded IP is correctly configured.

```

## minized + modified xpr

```
$ petalinux-config --get-hw-description ./hardware/daisy/

$ petalinux-build -c avnet-image-minimal

Error: /home/sungwoo/depo/woopsy/build/tmp/work/minized_sbc-xilinx-linux-gnueabi/device-tree/xilinx-v2020.2+gitAUTOINC+f725aaecff-r0/system-bsp.dtsi:113.1-16 Label or path bluetooth_uart not found
Error: /home/sungwoo/depo/woopsy/build/tmp/work/minized_sbc-xilinx-linux-gnueabi/device-tree/xilinx-v2020.2+gitAUTOINC+f725aaecff-r0/system-bsp.dtsi:120.1-11 Label or path axi_iic_0 not found
FATAL ERROR: Syntax error parsing input tree

```

* commented out bluetooth_uart, axi_iic_0 at system-bsp.dtsi

```
$ nano ./project-spec/meta-avnet/recipes-bsp/device-tree/files/minized-sbc/system-bsp.dtsi

$ petalinux-build -c avnet-image-minimal

$ ./rebuild_minized_sbc_base.sh

$ petalinux-package --boot --fsbl ./images/linux/zynq_fsbl.elf --fpga ./images/linux/system.bit --uboot --kernel ./images/linux/image.ub -o BOOT_daisy.BIN --force --boot-device flash --add ./images/linux/avnet-boot/avnet_qspi.scr --offset 0xFC0000

$ ~/tools/Xilinx/Vitis/2020.2/bin/xsct
exec program_flash -f ./images/linux/BOOT_daisy.BIN -fsbl ./images/linux/zynq_fsbl.elf -flash_type qspi-x4-single

```

* boot failed with emmc interrupt error : refer daisy_boot.log

```
mmc1: Got command interrupt 0x00030000 even though no command operation was in progress.

```

* open Vivado and copy the minized_sbc design to daisy 

* commented out only axi_iic_0 at system-bsp.dtsi

* add bram and check 
* https://www.hackster.io/news/microzed-chronicles-building-petalinux-for-the-minized-48f79880f849

```
root@daisy4:~# devmem 0x40000000 32
8<--- cut here ---
Unhandled fault: external abort on non-linefetch (0x018) at 0xb6f65000
pgd = ac2bfefd
[b6f65000] *pgd=1d407831, *pte=40000783, *ppte=40000e33
Bus error


root@daisy4:/# devmem 0x40000000 32
0x00000000
root@daisy4:/# devmem 0x40000000 32 0x0000cafe
root@daisy4:/# devmem 0x40000000
0x0000CAFE
root@daisy4:/#
```


* apply Vivado design directly
```
$ petalinux-package --boot --fsbl ./images/linux/zynq_fsbl.elf --fpga ./hardware/daisy/daisy.runs/impl_1/design_1_wrapper.bit --uboot --kernel ./images/linux/image.ub -o BOOT_daisy.BIN --force --boot-device flash --add ./images/linux/avnet-boot/avnet_qspi.scr --offset 0xFC0000
```

## minized + full
* modify rebuild_minized_sbc_base.sh : avnet-image-full

```
$ ./rebuild_minized_sbc_base.sh
 
$ petalinux-package --boot --fsbl ./images/linux/zynq_fsbl.elf --fpga ./images/linux/system.bit --uboot --kernel ./images/linux/image.ub -o BOOT_daisy_full.BIN --force --boot-device flash --add ./images/linux/avnet-boot/avnet_qspi.scr --offset 0xFC0000


// full
-rw-rw-r-- 1 sungwoo sungwoo   93664620  3월  6 23:16 BOOT_daisy_full.BIN
-rw-r--r-- 1 sungwoo sungwoo   90518548  3월  6 23:10 image.ub



exec program_flash -f ./images/linux/BOOT_daisy_full.BIN -fsbl ./images/linux/zynq_fsbl.elf -flash_type qspi-x4-single


// minimal
-rw-rw-r-- 1 sungwoo sungwoo   16515372  3월  6 11:08 BOOT_daisy.BIN
-rw-r--r-- 1 sungwoo sungwoo   13025460  3월  6 11:00 image.ub

```
* image is too big to fit in the eMMC 8GB ? 


## install factory bin
* https://www.avnet.com/wps/portal/us/products/avnet-boards/avnet-board-families/minized/
* Restore QSPI and eMMC Factory Images
* Download and extract flash_fallback_7007S.bin

```
exec program_flash -f ./factory/flash_fallback_7007S.bin -fsbl ./factory/zynq_fsbl.elf -flash_type qspi-x4-single

...
PetaLinux 2017.4 MiniZed /dev/ttyPS0
MiniZed login: 

```

* testing wifi

```
root@MiniZed:/usr/local/bin# cat /mnt/emmc/wpa_supplicant.conf
ctrl_interface=/var/run/wpa_supplicant
ctrl_interface_group=0
update_config=1

network={
        key_mgmt=WPA-PSK
        ssid="SWSY2"
        psk="01071368692"
}

network={
        key_mgmt=WPA-PSK
        ssid="Intellian"
        psk="satflower."
}

root@MiniZed:/usr/local/bin# ./wifi.sh
Successfully initialized wpa_supplicant
rfkill: Cannot open RFKILL control device
IPv6: ADDRCONF(NETDEV_UP): wlan0: link is not ready
rfkill: Cannot get wiphy information
ifconfig: SIOCGIFFLAGS: No such device
udhcpc (v1.24.1) started
Sending discover...
Sending discover...
Sending discover...
IPv6: ADDRCONF(NETDEV_CHANGE): wlan0: link becomes ready
Sending discover...
Sending discover...
Sending select for 10.1.105.51...
Lease of 10.1.105.51 obtained, lease time 86400
/etc/udhcpc.d/50default: Adding DNS 10.1.200.7
/etc/udhcpc.d/50default: Adding DNS 10.1.200.14
root@MiniZed:/usr/local/bin#
root@MiniZed:/usr/local/bin# ifconfig
lo        Link encap:Local Loopback
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1%1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1
          RX bytes:0 (0.0 B)  TX HWaddrLTICAST  MTU:1500  Metric:1
          RX packets:386 errors:0 dropped:20 overruns:0 frame:0
          TX packets:37 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:54481 (53.2 KiB)  TX bytes:7091 (6.9 KiB)

```

* refer boot at factory_fallback_dmesg.log

## update board from wic file & from USB

* rebuild_minized_sbc_base.sh -> uncomment EXT4

```
root@mzspi:/usr# dd if=/run/media/sda1/rootfs.wic of=/dev/mmcblk0

 mmcblk0: p1 p2
5852798+0 records in
5852798+0 records out
root@mzspi:/usr#
root@mzspi:/usr# EXT4-fs (mmcblk0p2): mounted filesystem with ordered data mode. Opts: (null)

root@mzspi:/usr# flash_erase /dev/mtd2 0 0
Erasing 4 Kibyte @ 3f000 -- 100 % complete
root@mzspi:/usr# cat /proc/mtd
dev:    size   erasesize  name
mtd0: 00300000 00001000 "boot"
mtd1: 00cc0000 00001000 "kernel"
mtd2: 00040000 00001000 "bootenv"
mtd3: 00000000 00001000 "spare"
root@mzspi:/usr# reboot

Zynq> sf probe 0 0 0
Zynq> sf read 0x04000000 0x300000 0xcc0000
Zynq> bootm 0x04000000

root@mzspi:/usr# mkdir /mnt/ext4
root@mzspi:/usr# mount /run/media/sda1/rootfs.ext4 /mnt/ext4/ -o loop
EXT4-fs (loop0): mounted filesystem with ordered data mode. Opts: (null)
ext4 filesystem being mounted at /mnt/ext4 supports timestamps until 2038 (0x7fffffff)
root@mzspi:/usr# rm -r /media/sd-mmcblk0p2/*
root@mzspi:/usr# cp -rf /mnt/ext4/* /media/sd-mmcblk0p2/
root@mzspi:/usr# cp /run/media/sda1/image.ub /media/sd-mmcblk0p1/fitImage

root@mzspi:/usr# cd /media/sd-mmcblk0p1/avnet-boot
root@mzspi:/media/sd-mmcblk0p1/avnet-boot# cp avnet_emmc.scr ../boot.scr
root@mzspi:/media/sd-mmcblk0p1/avnet-boot# cd ..
root@mzspi:/media/sd-mmcblk0p1# ls
avnet-boot  boot.scr    fitImage

root@mzspi:/usr# sync
root@mzspi:/usr# reboot

root@mzspi:~# df -h
Filesystem      Size  Used Avail Use% Mounted on
/dev/root       1.8G  394M  1.3G  25% /
devtmpfs        240M  4.0K  240M   1% /dev
tmpfs           248M  188K  248M   1% /run
tmpfs           248M   56K  248M   1% /var/volatile
/dev/mmcblk0p1 1022M  4.7M 1018M   1% /media/sd-mmcblk0p1


root@mzspi:~# cd /usr/local/bin
root@mzspi:/usr/local/bin# cat ./wifi.sh
#!/bin/sh

cp -f /usr/local/bin/wpa_supplicant.conf /etc/.

echo "Bringup the WiFi interface."
wpa_supplicant -Dnl80211 -iwlan0 -c/etc/wpa_supplicant.conf -B

echo "Fetch an IP address from the DHCP server."
udhcpc -i wlan0

root@mzspi:/usr/local/bin# cat wpa_supplicant.conf
ctrl_interface=/var/run/wpa_supplicant
ctrl_interface_group=0
update_config=1
network={
        key_mgmt=WPA-PSK
        ssid="Intellian"
        psk="satflower."
}
root@mzspi:/usr/local/bin# ./wifi.sh
Bringup the WiFi interface.
Successfully initialized wpa_supplicant
rfkill: Cannot open RFKILL control device
Fetch an IP address from the DHCP server.
udhcpc: started, v1.31.0
udhcpc: sending discover
udhcpc: sending discover
udhcpc: sending select for 10.1.105.51
udhcpc: lease of 10.1.105.51 obtained, lease time 86400
/etc/udhcpc.d/50default: Adding DNS 10.1.200.7
/etc/udhcpc.d/50default: Adding DNS 10.1.200.14
root@mzspi:/usr/local/bin#

```


```
...

Starting OpenBSD Secure Shell server: sshd
done.
Starting bluetooth: bluetoothd.
Starting HOSTAP Daemon: Configuration file: /etc/hostapd.conf
Could not read interface wlan0 flags: No such device
nl80211: Driver does not support authentication/association or connect commands
nl80211: deinit ifname=wlan0 disabled_11b_rates=0
Could not read interface wlan0 flags: No such device
nl80211 driver initialization failed.
wlan0: interface state UNINITIALIZED->DISABLED
wlan0: AP-DISABLED
wlan0: CTRL-EVENT-TERMINATING
hostapd_free_hapd_data: Interface wlan0 wasn't started
hwclock: Cannot access the Hardware Clock via any known method.
hwclock: Use the --verbose option to see the details of our search for an access method.
Starting internet superserver: inetd.
Starting syslogd/klogd: done
Starting tcf-agent: OK

PetaLinux 2020.2 mzspi ttyPS0

mzspi login:

```


```
Starting OpenBSD Secure Shell server: sshd
  generating ssh RSA host key...
haveged: haveged: ver: 1.9.5; arch: generic; vend: ; build: (gcc 9.2.0 CTV); collect: 128K

haveged: haveged: cpu: (VC); data: 16K (D); inst: 16K (D); idx: 12/40; sz: 15006/57790

haveged: haveged: tot tests(BA8): A:1/1 B:1/1 continuous tests(B):  last entropy estimate 8.00051

haveged: haveged: fills: 0, generated: 0

  generating ssh ECDSA host key...
  generating ssh ED25519 host key...
done.
Starting bluetooth: bluetoothd.
Starting HOSTAP Daemon: Configuration file: /etc/hostapd.conf
rfkill: Cannot open RFKILL control device
wlan0: Could not connect to kernel driver
Using interface wlan0 with hwaddr 48:eb:62:c0:91:b1 and ssid "test"
IPv6: ADDRCONF(NETDEV_CHANGE): wlan0: link becomes ready
wlan0: interface state UNINITIALIZED->ENABLED
wlan0: AP-ENABLED
hostapd.
hwclock: Cannot access the Hardware Clock via any known method.
hwclock: Use the --verbose option to see the details of our search for an access method.
Starting internet superserver: inetd.
Starting syslogd/klogd: done
Starting tcf-agent: OK

PetaLinux 2020.2 minized-sbc-base-2020-2 ttyPS0

minized-sbc-base-2020-2 login:


root@minized-sbc-base-2020-2:# nano /etc/ssh/sshd_config
...
PermitRootLogin yes
PasswordAuthentication yes
ChallengeResponseAuthentication no
...

```


* how to write bit file ?

```
$ nano bit2bin.bif
all:
{
 ./hardware/daisy/daisy.runs/impl_1/design_1_wrapper.bit
}

$ bootgen -image bit2bin.bif -arch zynq -process_bitstream bin -w on
$ cp ./hardware/daisy/daisy.runs/impl_1/design_1_wrapper.bit.bin ./fpga.bin
$ scp fpga.bin root@192.168.1.58:/mnt/sd-mmcblk0p1/


Zynq> fpga info
Xilinx Device
Descriptor @ 0x1ffdbe84
Family:         Zynq PL
Interface type: Device configuration interface (Zynq)
Device Size:    2083740 bytes
Cookie:         0x0 (0)
Device name:    7z007s
No Device Function Table.


root@daisy4:/mnt/sd-mmcblk0p1# cat boot.scr
'Vut▒Ub a▒Py▒Boot script▒# This is a boot script for U-Boot
# Generate boot.scr:
# mkimage -c none -A arm -T script -d boot.cmd.default boot.scr
#
################

fatload mmc 1:1 0x4000000 fitImage
bootm 0x04000000

tftpb 0x10000000 fpga.bin
fpga load 0 0x10000000 ${filesize}

```


* create HDL Wrapper fails...

```
 [BD 41-1665] Unable to generate top-level wrapper HDL for the block design 'minized_sbc_base.bd' is locked. Locked reason(s):
* Block design contains locked IPs. Please run report_ip_status for more details and recommendations on how to fix this issue. 
List of locked IPs: 
minized_sbc_base_pdm_filt_0_0


```

* removed pdm_filt, generate bit and download at Vivado

```
Unhandled fault: external abort on non-linefetch (0x018) at 0xb6f65000
pgd = ac2bfefd
[b6f65000] *pgd=1d407831, *pte=40000783, *ppte=40000e33
Bus error
root@daisy4:~# brcmfmac: brcmf_sdio_bus_sleep: error while changing bus sleep state -110
brcmfmac: brcmf_sdio_txfail: sdio error, abort command and terminate frame

root@daisy4:~# devmem 0x40000000 32
0x00000000



root@daisy4:~# brcmfmac: brcmf_sdio_bus_sleep: error while changing bus sleep state -110
brcmfmac: brcmf_sdio_txfail: sdio error, abort command and terminate frame

Starting OpenBSD Secure Shell server: sshd
done.
Starting bluetooth: bluetoothd.
Starting HOSTAP Daemon: Configuration file: /etc/hostapd.conf
Could not read interface wlan0 flags: No such device
nl80211: Driver does not support authentication/association or connect commands
nl80211: deinit ifname=wlan0 disabled_11b_rates=0
Could not read interface wlan0 flags: No such device
nl80211 driver initialization failed.
wlan0: interface state UNINITIALIZED->DISABLED
wlan0: AP-DISABLED


```

* update FPGA and program to the QSPI Flash

```
$ petalinux-config --get-hw-description ./hardware/minized_sbc_base_2020_2/

BOOT_METHOD='INITRD'
BOOT_SUFFIX='_MINIMAL'
INITRAMFS_IMAGE="avnet-image-minimal"
configure_boot_method
build_bsp

$ ./rebuild_minized_sbc_base.sh 

$ petalinux-package --boot --fsbl ./images/linux/zynq_fsbl.elf --fpga ./images/linux/system.bit --uboot --kernel ./images/linux/image.ub -o BOOT_INITRD_MINIMAL.BIN --force --boot-device flash --add ./images/linux/avnet-boot/avnet_qspi.scr --offset 0xFC0000

$ ~/tools/Xilinx/Vitis/2020.2/bin/xsct
exec program_flash -f ./images/linux/BOOT_INITRD_MINIMAL.BIN -fsbl ./images/linux/zynq_fsbl.elf -flash_type qspi-x4-single

reboot

# mkdir /mnt/ext4
# mount /run/media/sda1/rootfs.ext4 /mnt/ext4/ -o loop
# rm -r /media/sd-mmcblk0p2/*
# cp -rf /mnt/ext4/* /media/sd-mmcblk0p2/
# cp /run/media/sda1/image.ub /media/sd-mmcblk0p1/fitImage

# flash_erase /dev/mtd2 0 0

# sync
# reboot

# devmem 0x40000000
0x00000000

turn off PL_LED_R
# devmem 0x41200000 32 0x00000000    

turn on PL_LED_R
# devmem 0x41200000 32 0x0000000f

move PL_SW off
# devmem 0x41200008
0x00000000

move PL_SW on
# devmem 0x41200008
0x00000001

```


* simple axi-gpio access driver :
* ref : https://en.ica123.com/writing-a-simple-device-driver-and-adding-the-fpga-register/

```
$ petalinux-create -t modules --name fii-driver

$ petalinux-config -c rootfs
enable fii-driver

$ petalinux-build -c kernel

$ petalinux-build -c fii-driver

$ find -name 'fii-driver.ko'
./build/tmp/sysroots-components/minized_sbc/fii-driver/lib/modules/5.4.0-xilinx-v2020.2/extra/fii-driver.ko

cp fii-driver.ko to USB stick and put to minized and boot


# cp /run/media/sda1/fii-driver.ko . 

# insmod fii-driver.ko

[  571.678641] <1>Hello module world.
[  571.680740] <1>Module parameters were (0xdeadbeef) and "default"

* dmesg
[  590.959561] Created device fii-module with 244.0
root@mzspi2:/run/media/sda1# mknod /dev/fii c 244 0


```

# application
* app 

```
sungwoo@L2020:~/depo/woopsy$ petalinux-create -t apps --template c++ --name fiiapp
INFO: Create apps: fiiapp
INFO: New apps successfully created in /home/sungwoo/depo/woopsy/project-spec/meta-user/recipes-apps/fiiapp

sungwoo@L2020:~/depo/woopsy$ petalinux-build -c fiiapp -x build
sungwoo@L2020:~/depo/woopsy$ petalinux-build -c fiiapp -x install


sungwoo@L2020:~/depo/woopsy$ file ./build/tmp/work/cortexa9t2hf-neon-xilinx-linux-gnueabi/fiiapp/1.0-r0/image/usr/bin/fiiapp
./build/tmp/work/cortexa9t2hf-neon-xilinx-linux-gnueabi/fiiapp/1.0-r0/image/usr/bin/fiiapp: ELF 32-bit LSB shared object, ARM, EABI5 version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux-armhf.so.3, BuildID[sha1]=25562bf5a041368288da330d789dc9a058f8e75d, for GNU/Linux 3.2.0, with debug_info, not stripped

sungwoo@L2020:~/depo/woopsy$ cp ./build/tmp/work/cortexa9t2hf-neon-xilinx-linux-gnueabi/fiiapp/1.0-r0/image/usr/bin/fiiapp /media/sungwoo/33E9-3B21/

root@daisy6:/run/media/sda1# ./fiiapp wr 0
kernel: ioremap(0x41200000)=0xe0823000
kernel: gpio_data address = 0xe0823000
kernel: gpio_dir  address = 0xe0823004

 +----------------------------------+
 |     fii-driver functiokernel: swled_write start.... size = 7
n          |
 +----------------------------------+
  fii-drivekernel: val[0] = 0x00000077
r Opened Successfully
value = 0
kernel: gpio_data = 0x00000000
root@daisy6:/run/media/sda1# ./fiiapp wr 1
kernel: ioremap(0x41200000)=0xe0825000
kernel: gpio_data address = 0xe0825000
kernel: gpio_dir  address = 0xe0825004

 +----------------------------------+
 |     fii-driver functionkernel: swled_write start.... size = 7
          |
 +----------------------------------+
  fii-driverkernel: val[0] = 0x00000077
 Opened Successfully
value = 1
kernel: gpio_data = 0x00000001


```

* axi fifo

```
$ gedit ./components/plnx_workspace/device-tree/device-tree/pl.dtsi

axi_fifo_mm_s_0  0x43c2_0000


./components/plnx_workspace/device-tree/device-tree/pl.dtsi

		axi_fifo_mm_s_0: axi_fifo_mm_s@43c20000 {
			clock-names = "s_axi_aclk";
			clocks = <&misc_clk_0>;
			compatible = "xlnx,axi-fifo-mm-s-4.2";
			interrupt-names = "interrupt";
			interrupt-parent = <&intc>;
			interrupts = <0 33 4>;
			reg = <0x43c20000 0x10000>;
			xlnx,axi-str-rxd-protocol = "XIL_AXI_STREAM_ETH_DATA";
			xlnx,axi-str-rxd-tdata-width = <0x20>;
			xlnx,axi-str-txc-protocol = "XIL_AXI_STREAM_ETH_CTRL";
			xlnx,axi-str-txc-tdata-width = <0x20>;
			xlnx,axi-str-txd-protocol = "XIL_AXI_STREAM_ETH_DATA";
			xlnx,axi-str-txd-tdata-width = <0x20>;
			xlnx,axis-tdest-width = <0x4>;
			xlnx,axis-tid-width = <0x4>;
			xlnx,axis-tuser-width = <0x4>;
			xlnx,data-interface-type = <0x0>;
			xlnx,has-axis-tdest = <0x0>;
			xlnx,has-axis-tid = <0x0>;
			xlnx,has-axis-tkeep = <0x0>;
			xlnx,has-axis-tstrb = <0x0>;
			xlnx,has-axis-tuser = <0x0>;
			xlnx,rx-cascade-height = <0x0>;
			xlnx,rx-fifo-depth = <0x200>;
			xlnx,rx-fifo-pe-threshold = <0x5>;
			xlnx,rx-fifo-pf-threshold = <0x1fb>;
			xlnx,s-axi-id-width = <0x4>;
			xlnx,s-axi4-data-width = <0x20>;
			xlnx,select-xpm = <0x0>;
			xlnx,tx-cascade-height = <0x0>;
			xlnx,tx-fifo-depth = <0x200>;
			xlnx,tx-fifo-pe-threshold = <0x5>;
			xlnx,tx-fifo-pf-threshold = <0x1fb>;
			xlnx,use-rx-cut-through = <0x0>;
			xlnx,use-rx-data = <0x1>;
			xlnx,use-tx-ctrl = <0x0>;
			xlnx,use-tx-cut-through = <0x0>;
			xlnx,use-tx-data = <0x1>;
		};


/components/yocto/workspace/sources/linux-xlnx/drivers/staging/axis-fifo/axis-fifo.c


root@daisy8:~# insmod /run/media/sda1/cmdqueue.ko
cmdqueue: loading out-of-tree module taints kernel.
axis-fifo driver loaded with parameters read_timeout = 1000, write_timeout = 1000
probing cmdqueue_dev
cmdqueue 43c20000.axi_fifo_mm_s: couldn't read IP dts property 'xlnx,tx-max-pkt-size'
cmdqueue: probe of 43c20000.axi_fifo_mm_s failed with error -22
root@daisy8:~#

root@daisy8:~# /run/media/sda1/cqapp push 1
/run/media/sda1/cqapp: error while loading shared libraries: libstdc++.so.6: cannot open shared object file: No such file or directory
root@daisy8:~#

/run/media/sda1/cqapp: error while loading shared libraries: libgcc_s.so.1: cannot open shared object file: No such file or directory

root@daisy8:/run/media/sda1# ./cqapp push 301fcafe 301fbeef
root@daisy8:/run/media/sda1# ./cqapp push 40000001
root@daisy8:/run/media/sda1# ./cqapp pop 2
pop 401fcafe 401fbeef


```

* remote debugging in VSCode ?



