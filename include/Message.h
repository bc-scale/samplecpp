#ifndef __MESSAGE_H__
#define __MESSAGE_H__

#include <vector>
#include <string>

class Message
{
public:
    std::vector<std::string> GetMessage();
};

#endif