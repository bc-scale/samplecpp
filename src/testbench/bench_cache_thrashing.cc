#include <benchmark/benchmark.h>
#include <array>


int increment_matrix_element_no_cache_thrasing( std::vector<int>& m, size_t stride, int value )
{
    for( auto i = 0; i < m.size()/stride; ++i ) {
        for( auto j = 0; j < stride; ++j ) {
            m[ i * stride + j ] = value++;
        }
    }

    return value;
}

static void BM_no_cache_thrashing(benchmark::State& state)
{
    int stride = state.range(0)/sizeof(int);
    int dim = stride*stride; 
    std::vector<int> matrix( dim, 0); 

    for (auto _ : state) {
        benchmark::DoNotOptimize( increment_matrix_element_no_cache_thrasing( matrix, stride, 0));
    }
}

int increment_matrix_element_cache_thrasing( std::vector<int>& m, size_t stride, int value )
{
    for( auto j = 0; j < stride; ++j ) {
        for( auto i = 0; i < m.size()/stride; ++i ) {
            m[ i * stride + j ] = value++;
        }
    }

    return value;
}

static void BM_cache_thrashing(benchmark::State& state)
{
    int stride = state.range(0)/sizeof(int);
    int dim = stride*stride; 
    std::vector<int> matrix( dim, 0); 

    for (auto _ : state) {
        benchmark::DoNotOptimize( increment_matrix_element_cache_thrasing( matrix, stride, 0));
    }
}

BENCHMARK(BM_no_cache_thrashing)->Arg(2 << 11)->Arg(2 << 12)->Arg(2 << 13);
BENCHMARK(BM_cache_thrashing)->Arg(2 << 11)->Arg(2 << 12)->Arg(2 << 13);
