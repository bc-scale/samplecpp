#include <benchmark/benchmark.h>
#include <functional>
#include <any>

static void escape( void *p) {
    asm volatile( "" : : "g"(p) : "memory" );
}

static void clobber() {
    asm volatile( "" : : : "memory" );
}

class Foo42 {
public:
    using OnAnswerFunction = std::function< int( std::string question)>;
    void RegisterFunction(OnAnswerFunction on_answer) { on_answer_ = on_answer; }
    
    int AnswerFunction( std::string question) { 
        return on_answer_( std::move(question) ); 
    }

    using OnAnswerFnPtr = int(*)(std::string question);
    void RegisterFnPtr(OnAnswerFnPtr on_answer) { on_answer_fn_ptr_ = on_answer; }
    
    int AnswerFnPtr(std::string question) { 
        return on_answer_fn_ptr_( std::move(question) ); 
    }

    using OnAnswerFnPtrWithArg = int(*)( std::string question, std::any arg);
    void RegisterFnPtrWithArg(OnAnswerFnPtrWithArg on_answer, std::any arg) { 
        on_answer_fn_arg_ptr_ = on_answer; 
        arg_ = arg; 
    }

    int AnswerFnPtrWithArg( std::string question) { 
        return on_answer_fn_arg_ptr_( std::move(question), arg_ ); 
    }


private:
    OnAnswerFunction on_answer_;
    OnAnswerFnPtr on_answer_fn_ptr_;
    OnAnswerFnPtrWithArg on_answer_fn_arg_ptr_;
    std::any arg_;
};

static void Bench_call_std_function_without_state(benchmark::State& state)
{
    Foo42 foo;
    foo.RegisterFunction( []( std::string question){ 
        if( question == "hello") return 0;
        else if( question == "ultimate") return 42;
        
        return 1;
    });

    for (auto _ : state) {
        int answer = foo.AnswerFunction( "ulitmate");
    }   
}
BENCHMARK(Bench_call_std_function_without_state);


static void Bench_call_std_function_with_state(benchmark::State& state)
{
    Foo42 foo;
    int intervene = 43;
    foo.RegisterFunction( [&]( std::string question){ 
        if( question == "hello") return 0;
        else if( question == "ultimate") return intervene;
        
        return 1;
    });

    for (auto _ : state) {
        int answer = foo.AnswerFunction( "ulitmate");
    }
    
}
BENCHMARK(Bench_call_std_function_with_state);



static void Bench_call_fn_ptr(benchmark::State& state)
{
    Foo42 foo;
    foo.RegisterFnPtr( []( std::string question ){ 
        if( question == "hello") return 0;
        else if( question == "ultimate") return 42;
        
        return 1;
    });

    for (auto _ : state) {
        int answer = foo.AnswerFnPtr( "ulitmate");
    }
    
}
BENCHMARK(Bench_call_fn_ptr);


static void Bench_call_fn_ptr_with_arg(benchmark::State& state)
{
    Foo42 foo;
    int intervene = 43;
    foo.RegisterFnPtrWithArg( []( std::string question, std::any arg){ 
        if( question == "hello") return 0;
        else if( question == "ultimate") return std::any_cast<int>(arg);
        
        return 1;
    }, std::any{intervene});

    for (auto _ : state) {
        int answer = foo.AnswerFnPtrWithArg( "ulitmate");
    }
}
BENCHMARK(Bench_call_fn_ptr_with_arg);
