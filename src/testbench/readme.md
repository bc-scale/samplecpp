## perf

```
sudo perf record -g ./testbench --benchmark_filter=Bench_vector*
sudo perf report -g
sudo perf report -g 'graph,0.5,caller'

```

## valgrind

```
$ valgrind --tool=callgrind ./testbench --benchmark_filter=Bench_vector*
$ ls
callgrind.out.188236  ...
$ callgrind_annotate callgrind.out.188236 --inclusive=yes --tree=both | less
...
```

## std::function overhead

How much overhead on calling std::function compared to just a function pointer ? 

What option do we have if we want to store lambda with state ? 

```
Run on (12 X 4500 MHz CPU s)
CPU Caches:
  L1 Data 32K (x6)
  L1 Instruction 32K (x6)
  L2 Unified 256K (x6)
  L3 Unified 12288K (x1)
Load Average: 0.64, 0.43, 0.64
***WARNING*** CPU scaling is enabled, the benchmark real time measurements may be noisy and will incur extra overhead.
--------------------------------------------------------------------------------
Benchmark                                      Time             CPU   Iterations
--------------------------------------------------------------------------------
Bench_call_std_function_without_state       17.2 ns         17.2 ns     40395565
Bench_call_std_function_with_state          17.7 ns         17.7 ns     39329429
Bench_call_fn_ptr                           15.1 ns         15.1 ns     44958593
Bench_call_fn_ptr_with_arg                  20.0 ns         20.0 ns     34312438
```
