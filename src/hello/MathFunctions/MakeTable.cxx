#include <iostream>
#include <string>
#include <fstream>
#include <exception>
using namespace std;

int main( int argc, char** argv)
{
    if( argc != 2 )
    {
        throw std::logic_error("Should have 2 args");
    }

    cout << "writing " << argv[1] << endl;

    ofstream f(argv[1]);
    f << "double sqrtTable[] { 0, 1, 1.414, 1.7, 2, };" << endl;
    return 0;
}