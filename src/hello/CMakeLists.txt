set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED True)

option(USE_MYMATH "Use tutorial provided math implementation" ON)

configure_file(HelloWorldConfig.h.in HelloWorldConfig.h)

if( USE_MYMATH)
    add_subdirectory(MathFunctions)
    list(APPEND EXTRA_LIBS MathFunctions)
endif()

add_executable(HelloWorld 
  hello.cpp 
  Message.cpp
)

list(APPEND EXTRA_LIBS 
  log4cpp 
  pthread
)

target_link_libraries(HelloWorld PUBLIC ${EXTRA_LIBS})

target_include_directories(HelloWorld PUBLIC 
    "${PROJECT_BINARY_DIR}"
)

include_directories( ../../include)

file(COPY log4cpp.properties DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

install( TARGETS HelloWorld DESTINATION bin)
install( FILES "${PROJECT_BINARY_DIR}/HelloWorldConfig.h"
    DESTINATION include 
)
