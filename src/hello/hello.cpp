#include "HelloWorldConfig.h"
#include <vector>
#include <string>
#include <iostream>
#include "Message.h"
#include <math.h>
#include "log4cpp/Category.hh"
#include "log4cpp/PropertyConfigurator.hh"

#ifdef USE_MYMATH
#  include "../MathFunctions/MathFunctions.h"
#endif

using namespace std;

int main( int argc, char** argv)
{
    log4cpp::PropertyConfigurator::configure("log4cpp.properties"); 

    if (argc <2 )
    {
        // cout << "Usage: " << argv[0] << " Version " << HelloWorld_VERSION_MAJOR << "."
        //    << HelloWorld_VERSION_MINOR << " number" << endl;
        return 0;
    }

    double v = atoi( argv[1] );
#ifdef USE_MYMATH
    const double outputValue = mysquare(v);
#else
    const double outputValue = pow(v,2);
#endif
    cout << "square(" << v << ")=" << outputValue << endl;

    if ( argc == 3)
    {
        Message message;
        auto words = message.GetMessage();
        for( auto& w : words )
        {
            cout << w << " ";
        }

        cout << "sqrt(" << v << ")=" << mysqrt( atoi(argv[2])) << endl;

       	log4cpp::Category& root = log4cpp::Category::getRoot();

        log4cpp::Category& sub1 = 
            log4cpp::Category::getInstance(std::string("sub1"));

        log4cpp::Category& sub2 = 
            log4cpp::Category::getInstance(std::string("sub1.sub2"));

        root.warn("Storm is coming");

        sub1.debug("Received storm warning");
        sub1.info("Closing all hatches");

        sub2.debug("Hiding solar panels");
        sub2.error("Solar panels are blocked");
        sub2.debug("Applying protective shield");
        sub2.warn("Unfolding protective shield");
        sub2.info("Solar panels are shielded");

        sub1.info("All hatches closed");

        root.info("Ready for storm.");
    }
    
    log4cpp::Category::shutdown(); 
    return 0;
}