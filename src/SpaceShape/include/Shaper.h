#ifndef __SPACE_SHAPE_SHAPER__
#define __SPACE_SHAPE_SHAPER__

#include "ImageScene.h"
#include <QLabel>
#include <QVariant>
#include <QLabel>
#include <QPushButton>

namespace SpaceShape {

class Shaper : public QObject
{
    Q_OBJECT

public:
    explicit Shaper( QWidget* parent = nullptr );
    ~Shaper();

    Q_INVOKABLE SpaceShape::Shaper* beginTabs( const QString& name = "" );
    Q_INVOKABLE SpaceShape::Shaper* beginGrid( const QString& name, 
        const QVariantList& rows, const QVariantList& cols );
    Q_INVOKABLE SpaceShape::Shaper* end();
    Q_INVOKABLE SpaceShape::Shaper* add( const QString& name, QWidget* w );
    Q_INVOKABLE SpaceShape::Shaper* at( const QPoint& pos );
    Q_INVOKABLE SpaceShape::Shaper* atRect( const QRect& r );
    Q_INVOKABLE QWidget* find( const QString& path );
        
    static void registerTypes( const QString& libName, int major, int minor );

private:
    struct Impl;
    Impl *impl_;
};

#ifdef SPACESHAPE_INTERNAL
    #include "Shaper.moc"
#endif

}

#endif