#ifndef __IMAGE_SCENE_H__
#define __IMAGE_SCENE_H__

#include <QGraphicsView>
#include <QEvent>

namespace SpaceShape { 

class ImageScene : public QGraphicsView 
{
    Q_OBJECT
public:
    ImageScene(); 
    virtual ~ImageScene();

    void setImage( QImage m );
    QImage image();
    
protected:
    void resizeEvent(QResizeEvent *ev) override;
    void wheelEvent(QWheelEvent *ev) override;
    void contextMenuEvent(QContextMenuEvent *ev) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseDoubleClickEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override; 

private:
    struct Impl;
    Impl *impl_;
};

#ifdef SPACESHAPE_INTERNAL
    #include "ImageScene.moc"
#endif

}

#endif