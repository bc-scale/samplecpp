#ifndef __SPACE_SHAPE_GRAB_CONTROLLER_H__
#define __SPACE_SHAPE_GRAB_CONTROLLER_H__

#include <QQuickWidget>
#include "FrameGrabber.h"
#include "FramesScene.h"

namespace SpaceShape {

class GrabController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString device READ device WRITE setDevice)
    Q_PROPERTY(QSize frameSize READ frameSize WRITE setFrameSize)
    Q_PROPERTY(int frameBPP READ frameBPP WRITE setFrameBPP)
    Q_PROPERTY(int bufferCount READ bufferCount WRITE setBufferCount)

public:
    GrabController();
    ~GrabController(); 

    QString device();
    void setDevice( const QString& name );
    QSize frameSize();
    void setFrameSize( const QSize& frameSize );
    int frameBPP();
    void setFrameBPP( int frameBPP );
    int bufferCount();
    void setBufferCount( int bufferCount );

    Q_SIGNAL void frameGrabbed( const Frame& frame );    

    Q_INVOKABLE void init();
    Q_INVOKABLE void transmitTo( FramesScene* view );
    Q_INVOKABLE void grab( int count );

    static void registerTypes( const QString& libName, int major, int minor );
 
private:
    struct Impl;
    Impl *impl;
};

#ifdef SPACESHAPE_INTERNAL
    #include "GrabController.moc"
#endif

}

Q_DECLARE_METATYPE( SpaceShape::Frame )

#endif