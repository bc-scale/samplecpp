#ifndef __V4L2_FRAME_GRABBER_H__
#define __V4L2_FRAME_GRABBER_H__

#include <functional>
#include <string>
#include <memory>
#include <vector>

namespace SpaceShape {

struct Frame
{
    int index;
    int count;
    int width;
    int height;
    int bpp;
    uint8_t* image;
    size_t imageLength;
};

class FrameGrabber
{
public:
    enum Status
    {
        idle,
        grabbing,
    };

    FrameGrabber( const char* deviceName );
    virtual ~FrameGrabber();

    void setFormat( int width, int height, int bpp );
    void setBuffers( int bufferCount );
    std::vector<Frame> getBuffers() const;

    using GrabCallbackFn = std::function<void(FrameGrabber*,Frame&)>;
    void setGrabCallback( GrabCallbackFn&& fn );

    void grab( int count );
    void stop();

    int deviceID();
    const std::string& deviceName() const;

    Status status();
    std::string description() const; 

private:
    struct Impl;
    Impl *impl_;
};

}

#endif