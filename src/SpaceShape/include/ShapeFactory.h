#ifndef __SHAPE_FACTORY_H__
#define __SHAPE_FACTORY_H__

#include "ImageScene.h"
#include "FramesScene.h"
#include <QLabel>
#include <QPushButton>

namespace SpaceShape {

class ShapeFactory : public QObject
{
    Q_OBJECT

public:
    explicit ShapeFactory( QObject* parent = nullptr );
    ~ShapeFactory();

    static void registerTypes( const QString& libName, int major, int minor );

    Q_INVOKABLE SpaceShape::ImageScene *imagine( const QString& filePath );
    Q_INVOKABLE SpaceShape::ImageScene *imageScene( const QString& filePath );
    Q_INVOKABLE SpaceShape::FramesScene *framesScene();
    Q_INVOKABLE QLabel *label( const QString& text );
    Q_INVOKABLE QPushButton *pushButton( const QString& text );

private:
    struct Impl;
    Impl *impl_;
};

#ifdef SPACESHAPE_INTERNAL
    #include "ShapeFactory.moc"
#endif

}

#endif