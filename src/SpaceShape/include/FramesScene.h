#ifndef __FRAMES_SCENE_H__
#define __FRAMES_SCENE_H__

#include <QGraphicsView>
#include <QEvent>
#include "FrameGrabber.h"

namespace SpaceShape {

class FramesScene : public QGraphicsView 
{
    Q_OBJECT
public:
    FramesScene(); 
    virtual ~FramesScene();

    void setFrames( const std::vector<Frame>& frames );
    Q_SLOT void updateCurrentFrame( const Frame& frame );

protected:
    void resizeEvent(QResizeEvent *ev) override;
    void wheelEvent(QWheelEvent *ev) override;
    void contextMenuEvent(QContextMenuEvent *ev) override;

private:
    struct Impl;
    Impl *impl_;
};

#ifdef SPACESHAPE_INTERNAL
    #include "FramesScene.moc"
#endif

}

#endif