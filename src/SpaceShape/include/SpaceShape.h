#ifndef __SPACE_SHAPE_H__
#define __SPACE_SHAPE_H__

#include <string>

namespace SpaceShape {

class SpaceShapeFoo
{
public:
    virtual ~SpaceShapeFoo();

    int shape( const std::string& arg );
};

}

#endif