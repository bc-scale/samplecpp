#include "GrabController.h"
#include <QQmlEngine>
#include <QQmlContext>

namespace SpaceShape {

struct GrabController::Impl
{
    Impl()
    {
        frameSize = QSize( 640, 480 );
        frameBPP = 24;
        bufferCount = 4;
    }

    ~Impl()
    {
        delete grabber;
    }

    void createFrameGrabber()
    {
        delete grabber;

        grabber = new FrameGrabber( devName.toStdString().c_str() ); 
        grabber->setFormat( frameSize.width(), frameSize.height(), frameBPP);
        grabber->setBuffers( bufferCount );
        grabber->setGrabCallback( [this]( FrameGrabber*sender, Frame& frame){
            emit owner->frameGrabbed( frame );
        });
    }

    QString devName;
    FrameGrabber* grabber = nullptr;
    GrabController* owner = nullptr;
    QSize frameSize;
    int frameBPP;
    int bufferCount;
};

GrabController::GrabController() : impl( new Impl()) 
{
    impl->owner = this;
}

GrabController::~GrabController() 
{ 
    delete impl;
}

QString GrabController::device()
{
    return impl->devName;
}

void GrabController::setDevice( const QString& name )
{
    impl->devName = name;
}

QSize GrabController::frameSize()
{
    return impl->frameSize;
}

void GrabController::setFrameSize( const QSize& frameSize )
{
    impl->frameSize = frameSize;
}

int GrabController::frameBPP()
{
    return impl->frameBPP;
}

void GrabController::setFrameBPP( int frameBPP )
{
    impl->frameBPP = frameBPP;
}

int GrabController::bufferCount()
{
    return impl->bufferCount;
}

void GrabController::setBufferCount( int bufferCount )
{
    impl->bufferCount = bufferCount;
}

Q_SIGNAL void frameGrabbed( const Frame& frame );    

Q_INVOKABLE void GrabController::init()
{
    impl->createFrameGrabber();
}

Q_INVOKABLE void GrabController::transmitTo( FramesScene* view )
{
    view->setFrames( impl->grabber->getBuffers() );
    connect( this, &GrabController::frameGrabbed, view, &FramesScene::updateCurrentFrame );
}

Q_INVOKABLE void GrabController::grab( int count )
{
    impl->grabber->grab( count );
}

void GrabController::registerTypes( const QString& libName, int major, int minor )
{
    auto slib = libName.toStdString();
    qRegisterMetaType<SpaceShape::Frame>( "Frame" );
    qmlRegisterType<SpaceShape::GrabController>( slib.c_str(), 1, 0, "GrabController");
}

}