#ifndef __REGION_RECT_HPP__
#define __REGION_RECT_HPP__

#include <QGraphicsItem>
#include "SnapPoint.hpp"

namespace SpaceShape {

class RegionRect : public QGraphicsRectItem
{
public:
    explicit RegionRect(const QRectF &rect, QGraphicsItem *parent = nullptr)
        : QGraphicsRectItem( rect, parent )
    {
        topLeft_ = new SnapPoint( rect.topLeft(), 2, this );
        bottomRight_ = new SnapPoint( rect.bottomRight(), 2, this ); 

        topLeft_->setOnMove( [this](const QPointF& p){
            setRect( QRectF( p, this->rect().bottomRight()));
        });
        bottomRight_->setOnMove( [this](const QPointF& p){
            setRect( QRectF( this->rect().topLeft(), p));
        });
    }

    void setBottomRight( const QPointF& p )
    {
        setRect( QRectF( rect().topLeft(), p));
        bottomRight_->setPoint( p );
    }

private:
    SnapPoint* topLeft_;
    SnapPoint* bottomRight_;
}; 

}

#endif