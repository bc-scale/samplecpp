#include "Shaper.h"
#include <QTabWidget>
#include <QQmlEngine>
#include <QQmlContext>
#include <QVBoxLayout>
#include <QGridLayout>
#include <stack>
#include <QDebug>

using namespace std;

namespace SpaceShape {

struct Shaper::Impl
{
    Shaper* owner_;
    std::stack<QObject*> stack_;
    bool isContainerBegin = false;

    void beginContainer( QWidget* w )
    {
        if( dynamic_cast<QTabWidget*>( stack_.top() ) != nullptr )
        {
            ((QTabWidget*)stack_.top())->addTab( w, w->objectName() );
        }
        else if( dynamic_cast<QVBoxLayout*>( stack_.top() ) != nullptr )
        {
            ((QVBoxLayout *)stack_.top())->addWidget(w);
        }
        else if( dynamic_cast<QGridLayout*>( stack_.top() ) != nullptr )
        {
            stack_.push(w);
        }
        else
        {
            throw std::runtime_error("Not supporting other layout yet");
        }

        isContainerBegin = true;
    }

    void push( QObject* e )
    {
        stack_.push( e );
    }

    QObject* pop( )
    {
        QObject* e = stack_.top(); 
        stack_.pop();
        return e;
    }

    QObject* top()
    {
        return stack_.top();
    }

};

Shaper::Shaper( QWidget* parent ) : impl_(new Impl()), QObject( parent ) 
{
    impl_->owner_ = this;
    parent->setLayout( new QVBoxLayout() );
    impl_->stack_.push( parent->layout() );
}

Shaper::~Shaper()
{
    delete impl_;
}

Shaper* Shaper::beginTabs( const QString& name )
{
    QTabWidget* tabs = new QTabWidget();
    tabs->setObjectName( name );
    tabs->setLayout( new QVBoxLayout() );

    impl_->beginContainer( tabs );

    impl_->push( tabs );
    return this;
}

Shaper* Shaper::beginGrid( const QString& name, 
    const QVariantList& rows, const QVariantList& cols )
{
    QWidget* gridw = new QWidget();
    gridw->setObjectName( name );

    auto* grid = new QGridLayout();
    for( int i = 0; i < rows.size(); ++i )
    {
        grid->setRowStretch( i, rows[i].toInt());
    }
    for( int i = 0; i < cols.size(); ++i )
    {
        grid->setColumnStretch( i, cols[i].toInt());
    }
    gridw->setLayout( grid );

    impl_->beginContainer( gridw );

    impl_->push( gridw->layout() );
    return this;
}    

Shaper* Shaper::end( )
{
    impl_->pop();
    return this;
}    

Shaper* Shaper::add( const QString& name, QWidget* w )
{
    w->setObjectName( name );

    if( dynamic_cast<QTabWidget*>( impl_->top() ) != nullptr )
    {
        ((QTabWidget*)impl_->top())->addTab( w, w->objectName() );
    }
    else if( dynamic_cast<QGridLayout*>( impl_->top() ) != nullptr )
    {
        impl_->push(w);
    }
    else
    {
        throw std::runtime_error( "Not supporting add");
    }

    impl_->isContainerBegin = false;
    return this;
}

Shaper* Shaper::at( const QPoint& pos )
{
    return atRect( QRect(pos.x(),pos.y(),1,1));
}    

Shaper* Shaper::atRect( const QRect& r )
{
    QObject* containerBegin = nullptr;
    if( impl_->isContainerBegin )
    {
        containerBegin = impl_->pop();
    }

    if( dynamic_cast<QWidget*>(impl_->top()) == nullptr )
    {
        throw std::runtime_error( "at() should have QWidget on stack top");
    }
    QWidget* w = (QWidget*)impl_->pop();
    w->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    if( dynamic_cast<QGridLayout*>(impl_->top()) == nullptr )
    {
        throw std::runtime_error( "at() should have QGridLayout on second top");
    }
    
    ((QGridLayout*)impl_->top())->addWidget( w, 
        r.top(), r.left(), r.height(), r.width() );
    
    if( impl_->isContainerBegin )
    {
        impl_->stack_.push(containerBegin);
    }
    return this;
}        

QWidget* Shaper::find( const QString& path )
{
    QWidget* child = (QWidget*)parent();
    for( auto& t : path.split( '/' ) )
    {
        child = child->findChild<QWidget*>( t );
    }
    return child;
}

void Shaper::registerTypes( const QString& libName, int major, int minor )
{
    auto slib = libName.toStdString();
    qmlRegisterType<SpaceShape::Shaper>( slib.c_str(), major, minor, "Shaper");
}

}