#include "ImageScene.h"
#include <QtWidgets>
#include <QImage>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <iostream>
#include "useful.hpp"
#include "SceneGestureBase.hpp"
#include "PanZoom.hpp"
#include "DrawRectangle.hpp"
#include "SelectAdjust.hpp"
#include <memory>

using namespace std;

namespace SpaceShape {

class ViewImageItem : public QGraphicsItem
{
public:
    ViewImageItem() {}
    ~ViewImageItem() {}

    QImage image() { return image_; }
    void setImage( QImage m) 
    { 
        image_ = m; 
    }

    QRectF boundingRect() const override
    {
        return scene()->sceneRect();        
    }

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0) override
    {   
        if( !image_.isNull() )
        {
            painter->drawImage( scene()->sceneRect(), image_, image_.rect() );
        }
    }

private:
    QImage image_;
};

struct ImageScene::Impl
{
    using Gesture = SceneGestureBase<QGraphicsView>;

    Impl( ImageScene* owner) : 
        owner_( owner ) 
        ,scene_( QRectF(0,0,1,1)) 
    {
        panZoom_ = new QAction("&Pan Zoom", owner_);
        owner->connect(panZoom_, &QAction::triggered, [this](){
            switchGesture(  gestures_[0].get() );
        });

        rectangle_ = new QAction("Rectangle", owner_);
        owner->connect(rectangle_, &QAction::triggered, [this](){
            switchGesture(  gestures_[1].get() );
        });

        adjust_ = new QAction("Adjust", owner_);
        owner->connect(adjust_, &QAction::triggered, [this](){
            switchGesture(  gestures_[2].get() );
        });

        gestures_.push_back( unique_ptr<Gesture>( new PanZoom(owner)) );
        gestures_.push_back( unique_ptr<Gesture>( new DrawRectangle(owner)) );
        gestures_.push_back( unique_ptr<Gesture>( new SelectAdjust(owner)) );
        gesture_ = gestures_[0].get();
        gesture_->enter(nullptr);
    }

    ~Impl()
    {
        delete panZoom_;
        delete rectangle_;
    }

    void switchGesture( Gesture* next )
    {
        Gesture* temp = next;
        gesture_->exit(temp);        
        std::swap( temp, gesture_ );
        gesture_->enter(temp);
    }

    ImageScene* owner_;
    Gesture* gesture_;
    vector<unique_ptr<Gesture>> gestures_;
    QAction *panZoom_;
    QAction *rectangle_;
    QAction *adjust_;
    QGraphicsScene scene_;
    ViewImageItem *imageItem_;
};

ImageScene::ImageScene() : 
    impl_( new Impl(this) )
{
    setScene(&impl_->scene_);
    scene()->addItem( impl_->imageItem_ = new ViewImageItem());
}

ImageScene::~ImageScene()
{
    delete impl_;
}

void ImageScene::setImage( QImage m )
{
    if( m.isNull() ) 
        throw std::runtime_error("null image is not allowed");
    
    auto prev = impl_->imageItem_->image();
    if( prev.isNull() 
        || prev.size() != m.size())
    {
       scene()->setSceneRect( QRectF(m.rect()));
    }

    impl_->imageItem_->setImage( m );
}

void ImageScene::resizeEvent(QResizeEvent *ev)
{
    QGraphicsView::resizeEvent(ev);

    impl_->gesture_->resizeEvent(ev);
    if( ev->isAccepted() ) return;
    
    auto sr = scene()->sceneRect();
    auto sx = (double) ev->size().width() / sr.width(); 
    auto sy = (double) ev->size().height() / sr.height(); 
    QTransform tx( 
        sx, 0,  
        0, sy, 
        0, 0 );
    setTransform( tx );
}

void ImageScene::wheelEvent(QWheelEvent *event)
{
    impl_->gesture_->wheelEvent(event);
}

void ImageScene::mousePressEvent(QMouseEvent *event)
{
    QGraphicsView::mousePressEvent(event);
    impl_->gesture_->mousePressEvent(event);
}

void ImageScene::mouseReleaseEvent(QMouseEvent *event)
{
    QGraphicsView::mouseReleaseEvent(event);
    impl_->gesture_->mouseReleaseEvent(event);
}

void ImageScene::mouseDoubleClickEvent(QMouseEvent *event)
{
    QGraphicsView::mouseDoubleClickEvent(event);
    impl_->gesture_->mouseDoubleClickEvent(event);
}

void ImageScene::mouseMoveEvent(QMouseEvent *event) 
{
    QGraphicsView::mouseMoveEvent(event);
    impl_->gesture_->mouseMoveEvent(event);
}

void ImageScene::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu menu(this);
    menu.addAction( impl_->panZoom_);
    menu.addAction( impl_->rectangle_);
    menu.addAction( impl_->adjust_);
    menu.exec(event->globalPos());
}

QImage ImageScene::image()
{
    return impl_->imageItem_->image();
}

}