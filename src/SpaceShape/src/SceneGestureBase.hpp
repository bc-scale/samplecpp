#ifndef __SCENE_GESTURE_BASE_HPP__
#define __SCENE_GESTURE_BASE_HPP__

#include <QEvent>

namespace SpaceShape {

template<typename TOwner>
class SceneGestureBase 
{
public:
    SceneGestureBase(TOwner *owner ) : owner_(owner) {}

    virtual TOwner *owner() { return owner_; };

    virtual void enter( SceneGestureBase<TOwner>* prev ){}
    virtual void exit( SceneGestureBase<TOwner>* next ){};

    virtual void resizeEvent(QResizeEvent *ev){};
    virtual void wheelEvent(QWheelEvent *event){}
    virtual void mousePressEvent(QMouseEvent *event){}
    virtual void mouseReleaseEvent(QMouseEvent *event){};
    virtual void mouseDoubleClickEvent(QMouseEvent *event) {};
    virtual void mouseMoveEvent(QMouseEvent *event) {}; 

private:
    TOwner *owner_;
};

}

#endif