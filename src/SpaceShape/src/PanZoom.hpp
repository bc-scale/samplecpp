#ifndef __PAN_ZOOM_HPP
#define __PAN_ZOOM_HPP

#include <QGraphicsView>
#include "SceneGestureBase.hpp"

namespace SpaceShape {

class PanZoom : public SceneGestureBase<QGraphicsView>
{
public:
    PanZoom(QGraphicsView *owner ) : SceneGestureBase(owner) {}

    void enter( SceneGestureBase<QGraphicsView>* prev ) override
    {
        owner()->setDragMode( QGraphicsView::DragMode::ScrollHandDrag );
    }

    void exit( SceneGestureBase<QGraphicsView>* next ) override
    {
        owner()->setDragMode( QGraphicsView::DragMode::NoDrag );
    }

    void resizeEvent(QResizeEvent *ev) override
    {
        if( isWheelEvent_ )
        {
            isWheelEvent_ = false;
            ev->accept();
        }
    }

    void wheelEvent(QWheelEvent *event) override 
    {
        auto s = event->angleDelta().y() > 0 ? 1.1 : 0.9;
        owner()->setTransform(QTransform::fromScale(s, s), true);
        isWheelEvent_ = true;
        event->accept();
    }

    bool isWheelEvent_ = false;
};

}

#endif