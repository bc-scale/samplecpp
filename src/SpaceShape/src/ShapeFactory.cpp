#include "ShapeFactory.h"
#include <QQmlEngine>
#include <QQmlContext>

using namespace std;

namespace SpaceShape {

struct ShapeFactory::Impl
{
    ShapeFactory* owner_;
};

ShapeFactory::ShapeFactory( QObject* parent ) : impl_(new Impl()), QObject( parent ) 
{
    impl_->owner_ = this;
}

ShapeFactory::~ShapeFactory()
{
    delete impl_;
}

ImageScene *ShapeFactory::imagine( const QString& filePath )
{
    auto s = new SpaceShape::ImageScene();
    s->setImage( QImage(filePath) );
    return s;
}

QLabel *ShapeFactory::label( const QString& text )
{
    auto w = new QLabel( text);
    return w;
}    

QPushButton *ShapeFactory::pushButton( const QString& text )
{
    auto w = new QPushButton( text);
    return w;
}    

ImageScene *ShapeFactory::imageScene( const QString& filePath )
{
    auto s = new SpaceShape::ImageScene();
    s->setImage( QImage(filePath) );
    return s;
}

FramesScene *ShapeFactory::framesScene()
{
    auto s = new SpaceShape::FramesScene();
    return s;
}


void ShapeFactory::registerTypes( const QString& libName, int major, int minor )
{
    auto slib = libName.toStdString();
    qmlRegisterType<QWidget>( slib.c_str(), major, minor, "QWidget");
    qmlRegisterType<QLabel>( slib.c_str(), major, minor, "QLabel");
    qmlRegisterType<QPushButton>( slib.c_str(), major, minor, "QPushButton");
    qmlRegisterType<SpaceShape::ImageScene>( slib.c_str(), major, minor, "ImageScene");
    qmlRegisterType<SpaceShape::FramesScene>( slib.c_str(), major, minor, "FramesScene");
    qmlRegisterType<SpaceShape::ShapeFactory>( slib.c_str(), major, minor, "ShapeFactory");
}

}