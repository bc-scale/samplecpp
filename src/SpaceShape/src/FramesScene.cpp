#include "FramesScene.h"
#include <QtWidgets>
#include <QImage>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <iostream>
#include "useful.hpp"

using namespace std;

namespace SpaceShape {

class FramesItem : public QGraphicsItem
{
public:
    FramesItem() {}
    ~FramesItem() {}

    QRectF boundingRect() const override
    {
        return scene()->sceneRect();        
    }

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0) override
    {   
        if( current_ != nullptr)
        {
            QImage * m = current_;        
            painter->drawImage( scene()->sceneRect(), *m, m->rect() );
        }
    }

    void setFrames( const std::vector<Frame>& frames )
    {
        frameImages_.clear();

        for( auto& f : frames )
        {
            QImage img( f.image, f.width, f.height, QImage::Format_RGB888 );
            frameImages_[ f.image ] = img;
        }

        for( auto& fm : frameImages_ )
        {
            auto r = fm.second.rect();
            cout << string_format("%d %d %d %d\n", r.left(), r.top(), r.width(), r.height());
            scene()->setSceneRect( QRectF( fm.second.rect()));
            break;
        }
    }

    void setCurrentFrame( const Frame& frame )
    {
        auto I = frameImages_.find( frame.image );
        if( I != frameImages_.end())
        {
            current_ = &I->second;
        }
    }

private:
    QImage * current_ = nullptr;
    std::map< uint8_t*, QImage> frameImages_;
};

struct FramesScene::Impl
{
    Impl( FramesScene* owner) : 
        owner_( owner ) 
        ,scene_( QRectF(0,0,1,1)) 
    {
        panZoom_ = new QAction("&Pan Zoom", owner_);
        owner->connect(panZoom_, &QAction::triggered, [](){

        });

        polyLine_ = new QAction("Polyline", owner_);
        owner->connect(polyLine_, &QAction::triggered, [](){

        });
    }

    ~Impl()
    {
        delete panZoom_;
        delete polyLine_;
    }

    FramesScene* owner_;
    QAction *panZoom_;
    QAction *polyLine_;
    bool isWheelEvent_ = false;
    QGraphicsScene scene_;
    FramesItem *framesItem_;
};

FramesScene::FramesScene() : 
    impl_( new Impl(this) )
{
    setScene(&impl_->scene_);
    scene()->addItem( impl_->framesItem_ = new FramesItem());

    setDragMode( DragMode::ScrollHandDrag );
}

FramesScene::~FramesScene()
{
    delete impl_;
}

void FramesScene::setFrames( const std::vector<Frame>& frames )
{
    impl_->framesItem_->setFrames( frames );
}

void FramesScene::updateCurrentFrame( const Frame& frame )
{
    impl_->framesItem_->setCurrentFrame(frame);
    impl_->framesItem_->update();
}

void FramesScene::resizeEvent(QResizeEvent *ev)
{
    QGraphicsView::resizeEvent(ev);

    if( impl_->isWheelEvent_ )
    {
        impl_->isWheelEvent_ = false;
        return;
    }
    
    auto sr = scene()->sceneRect();
    auto sx = (double) ev->size().width() / sr.width(); 
    auto sy = (double) ev->size().height() / sr.height(); 
    QMatrix tx( 
        sx, 0,  
        0, sy, 
        0, 0 );
    setMatrix( tx );
}

void FramesScene::wheelEvent(QWheelEvent *event)
{
    auto s = event->angleDelta().y() > 0 ? 1.1 : 0.9;
    scale( s, s );
    impl_->isWheelEvent_ = true;

    event->accept();
}

void FramesScene::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu menu(this);
    menu.addAction( impl_->panZoom_);
    menu.addAction( impl_->polyLine_);
    menu.exec(event->globalPos());
}

}