#ifndef __SELECT_ADJUST_HPP__
#define __SELECT_ADJUST_HPP__

#include <QGraphicsView>
#include "SceneGestureBase.hpp"
#include "SnapPoint.hpp"

namespace SpaceShape {

class SelectAdjust : public SceneGestureBase<QGraphicsView>
{
public:
    SelectAdjust(QGraphicsView *owner ) : SceneGestureBase(owner) 
    {
    }

    void enter( SceneGestureBase<QGraphicsView>* prev ) override
    {
        for( auto* e : owner()->scene()->items() )
        {
            if( dynamic_cast<QAbstractGraphicsShapeItem*>(e) != nullptr )
            {
                e->setFlag(QGraphicsItem::ItemIsMovable);
                if( dynamic_cast<SnapPoint*>(e) != nullptr )
                {
                    e->show();
                }
            }
        }
    }

    void exit( SceneGestureBase<QGraphicsView>* next ) override
    {
        for( auto* e : owner()->scene()->items() )
        {
            if( dynamic_cast<QAbstractGraphicsShapeItem*>(e) != nullptr )
            {
                e->setFlag(QGraphicsItem::ItemIsMovable,false);
                if( dynamic_cast<SnapPoint*>(e) != nullptr )
                {
                    e->hide();
                }                
            }            
        }
    }
};

}

#endif