#ifndef __DRAW_RECTANGLE_HPP__
#define __DRAW_RECTANGLE_HPP__

#include <QGraphicsView>
#include "SceneGestureBase.hpp"
#include "SnapPoint.hpp"
#include "RegionRect.hpp"
#include <memory>

namespace SpaceShape {

class DrawRectangle : public SceneGestureBase<QGraphicsView>
{
private:
    using RectDrawGesture =  SceneGestureBase<DrawRectangle>;

    QPointF map( QPointF mousePos )
    {
        QTransform tx = owner()->viewportTransform().inverted();
        return tx.map( mousePos );
    }

    class WaitForStart : public RectDrawGesture
    {
    public:
        WaitForStart(DrawRectangle* owner) : RectDrawGesture(owner) {}

        void mousePressEvent(QMouseEvent *event) override 
        {
            if( event->button() == Qt::MouseButton::LeftButton )
            {
                DrawRectangle* dr = owner();
                QPointF mp = dr->map( event->localPos());
                dr->rect_ = new RegionRect(QRectF(mp,mp)); 
                dr->owner()->scene()->addItem(dr->rect_);
                dr->owner()->scene()->update();

                dr->switchTo( dr->dragToEnd_.get() );
                event->accept();
            }
        }
    };

    class DragToEnd : public RectDrawGesture
    {
    public:
        DragToEnd(DrawRectangle* owner) : RectDrawGesture(owner) {}

        void mouseMoveEvent(QMouseEvent *event) override 
        {
            DrawRectangle* dr = owner();
            auto r = dr->rect_->rect();
            QPointF mp = dr->map( event->localPos() );            
            dr->rect_->setBottomRight( mp );
            dr->owner()->scene()->update();
            event->accept();
        }   

        void mouseReleaseEvent(QMouseEvent *event) override 
        {
            if( event->button() == Qt::MouseButton::LeftButton )
            {
                owner()->switchTo( owner()->waitForStart_.get() );
                event->accept();
            }            
        }
    };    

    std::unique_ptr<RectDrawGesture> waitForStart_;
    std::unique_ptr<RectDrawGesture> dragToEnd_;    
    RectDrawGesture* current_ = nullptr;
    RegionRect *rect_ = nullptr;

public:
    DrawRectangle(QGraphicsView *owner ) : SceneGestureBase(owner) 
    {
        waitForStart_ = std::unique_ptr<RectDrawGesture>( new WaitForStart(this));
        dragToEnd_ = std::unique_ptr<RectDrawGesture>( new DragToEnd(this));
        current_ = waitForStart_.get();
    }

    void enter( SceneGestureBase<QGraphicsView>* prev ) override
    {
        current_ = waitForStart_.get();
        current_->enter(nullptr);
    }

    void exit( SceneGestureBase<QGraphicsView>* next ) override
    {
        current_->exit(nullptr);
    }

    void switchTo( RectDrawGesture* next )
    {
        RectDrawGesture* temp = next;
        current_->exit(temp);        
        std::swap( temp, current_ );
        current_->enter(temp);
    }     

    void mousePressEvent(QMouseEvent *event) override 
    {
        current_->mousePressEvent(event);
    }
    
    void mouseReleaseEvent(QMouseEvent *event) override 
    {
        current_->mouseReleaseEvent(event);
    }

    void mouseMoveEvent(QMouseEvent *event) override 
    {
        current_->mouseMoveEvent(event);
    }     
};

}

#endif