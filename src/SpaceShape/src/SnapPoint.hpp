#ifndef __SNAP_POINT_HPP__
#define __SNAP_POINT_HPP__

#include <QGraphicsView>
#include <memory>
#include <functional>

namespace SpaceShape {

class SnapPoint : public QGraphicsRectItem
{
public:
    SnapPoint( const QPointF& p, int delta, QGraphicsItem *parent ) 
        : QGraphicsRectItem( QRectF( p - QPointF(delta,delta), p + QPointF(delta,delta)), parent )
        , delta_( delta, delta )
    {
        setFlag(ItemSendsGeometryChanges);
        hide();
    }

    QPointF point() const 
    {
        return rect().center();
    }

    void setPoint( const QPointF& p )
    {
        setRect( QRectF( p - delta_, p + delta_) );
    }

    using OnMoveFn = std::function< void(const QPointF&) >;
    void setOnMove( OnMoveFn fn ) { onMoveFn_ = fn; }

    QVariant itemChange(GraphicsItemChange change, const QVariant &value) override
    {
        if( change == ItemPositionHasChanged)
        {
            auto p = point() + pos();
            if( onMoveFn_ != nullptr) 
            {
                onMoveFn_( p );
            }
        }

        return QGraphicsItem::itemChange(change, value);
    }    

    QPointF delta_;

private:
    OnMoveFn onMoveFn_;
};

}

#endif