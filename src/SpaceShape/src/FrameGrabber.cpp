#include "FrameGrabber.h"
#include "useful.hpp"

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <asm/types.h>		/* for videodev2.h */
#include <linux/videodev2.h>
#include "libv4lconvert.h"
#include <functional>
#include <mutex>
#include <iostream>
#include <thread>
#include <vector>
#include <sstream>
#include <cstring>

namespace SpaceShape {

int xioctl(int fd, int request, void *arg)
{
    int r;

    do {
        r = ioctl(fd, request, arg);
    } while (r < 0 && EINTR == errno);
    return r;
} 

std::string FourCCAsString( uint32_t v) 
{
    return string_format( "%c%c%c%c",
        (0xFF & v), (0xFF & (v >> 8)), (0xFF & (v>>16)), (0xFF & (v>>24)) );
}

struct CaptureBuffer
{   
    struct v4l2_buffer buffer;
    void *mmapData;
    uint8_t *converted;
};

enum { 
    PIPE_MSG_SIZE = 16 
};

template< typename F>
void enumFormats( int fd, F fn )
{
    struct v4l2_fmtdesc fmtdesc;
    fmtdesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    for( int i = 0; ;++i)
    {
        fmtdesc.index = i;
        if( xioctl( fd, VIDIOC_ENUM_FMT, &fmtdesc ) < 0) break;
        fn( fmtdesc );
    }     
}

template< typename F>
void enumSizes( int fd, uint32_t pixelFormat, F fn )
{
    struct v4l2_frmsizeenum frmsize;
    frmsize.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    frmsize.pixel_format = pixelFormat;

    for( int i = 0; ;++i)
    {
        frmsize.index = i;
        if( xioctl( fd, VIDIOC_ENUM_FRAMESIZES, &frmsize ) < 0) break;
        fn( frmsize );
    }     
} 

struct FrameGrabber::Impl
{
    Impl( FrameGrabber* owner, const char * deviceName ) : owner_(owner) 
    {
        deviceName_ = deviceName;

        struct stat st;
        if (stat(deviceName, &st) < 0) {
            throw std::runtime_error( string_format(
                "Cannot identify '%s': %d, %s",  deviceName, errno, strerror(errno)));
        }

        if (!S_ISCHR(st.st_mode)) {
            throw std::runtime_error( string_format( "%s is no device", deviceName));
        }
        
        devfd_ = open(deviceName, O_RDWR | O_NONBLOCK, 0);
        if( devfd_ < 0) {
            throw std::runtime_error( string_format( 
                "Cannot open '%s': %d, %s",  errno, strerror(errno)));
        }

        convert_ = v4lconvert_create(devfd_); 

        if( pipe( pipefd_) < 0 ) {
            throw std::runtime_error( string_format( 
                "Faile to create pipe : %d, %s",  errno, strerror(errno)));
        }
        
        threadSelect_ = std::thread( &FrameGrabber::Impl::loopSelect, this );           
    }

    ~Impl()
    {
        pipeMessage( "quit" );
        threadSelect_.join();
        close(pipefd_[0]);
        close(pipefd_[1]);

        clearRawBufers();
        v4lconvert_destroy(convert_);
        close(devfd_);
    }

    void setFormat( int width, int height, int bpp )
    {
        std::vector<struct v4l2_frmsizeenum> fsizes;
        enumFormats( devfd_, [&]( const struct v4l2_fmtdesc& fmt )
        {
            enumSizes(  devfd_, fmt.pixelformat, [&]( const struct v4l2_frmsizeenum& fsize )
            {
                fsizes.push_back( fsize );
            });         
        }); 

        capability_ = queryCap();
        setupFormat( width, height, bpp );
    }

    std::vector<Frame> getBuffers() const 
    {
        int i = 0;
        std::vector<Frame> frames;
        for( auto& cb : captureBuffers_)
        {
            Frame f;            
            f.index = i++;
            f.count = captureBuffers_.size();
            f.width = captureFormat_.fmt.pix.width;
            f.height = captureFormat_.fmt.pix.height;
            f.bpp = bitsPerPixel_;
            f.image = cb.converted;
            f.imageLength = captureFormat_.fmt.pix.sizeimage;
            frames.push_back(f);
        }

        return std::move(frames);
    }

    void grab( int count )
    {
        std::string error = popAsyncError();
        if( !error.empty() )
        {
            throw std::runtime_error( error.c_str());
        }

        if( currentFrameIndex_ < grabFrameCount_)
        {
            throw std::runtime_error( string_format( 
                "%s is already grabbing", deviceName_.c_str() ));
        }

        currentFrameIndex_ = 0;
        grabFrameCount_ = count;

        queueRawBuffers( captureBuffers_ );

        enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        if (xioctl(devfd_, VIDIOC_STREAMON, &type) < 0)
        {
            throw std::runtime_error( string_format(
                "%s failed at VIDIOC_STREAMON : %d, %s",  deviceName_, errno, strerror(errno)));
        }

        pipeMessage( "select" );
    }    

    void stop()
    {
        enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        if (xioctl(devfd_, VIDIOC_STREAMOFF, &type) < 0)
        {
            throw std::runtime_error( string_format(
                "%s failed at VIDIOC_STREAMOFF : %d, %s",  deviceName_, errno, strerror(errno)));
        }

        currentFrameIndex_ = grabFrameCount_ = 0;
        pipeMessage( "stop" );

        std::string error = popAsyncError();
        if( !error.empty() )
        {
            throw std::runtime_error( error.c_str());
        }
    }


    v4l2_capability queryCap()
    {
        struct v4l2_capability cap;
        memset( &cap, 0, sizeof(cap));

        if (xioctl( devfd_, VIDIOC_QUERYCAP, &cap) < 0) {
            throw std::runtime_error( string_format(
                "%s failed at VIDIOC_QUERYCAP : %d, %s", deviceName_.c_str(), errno, strerror(errno)));
        }

        if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
            throw std::runtime_error( string_format(
                "%s is no video capture device", deviceName_.c_str()));
        }

        if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
            throw std::runtime_error( string_format(
                "%s does not support streaming i/o", deviceName_.c_str()));
        }
        return cap;
    }

    std::string description() const 
    {
        std::stringstream ss;
        ss << string_format( "driver : %s", capability_.driver) << std::endl;
        ss << string_format( "card : %s", capability_.card) << std::endl;
        ss << string_format( "bus_info : %s", capability_.bus_info) << std::endl;
        ss << string_format( "version : %08X", capability_.version) << std::endl;
        ss << string_format( "capabilities : %08X", capability_.capabilities) << std::endl;
        ss << string_format( "device_caps : %08X", capability_.device_caps) << std::endl;
        ss << string_format( "dimension : %dx%dx%d", 
            captureFormat_.fmt.pix.width, captureFormat_.fmt.pix.height, bitsPerPixel_ ) << std::endl;
        ss << string_format( "pixel format : %s->%s", 
            FourCCAsString(captureFormatRaw_.fmt.pix.pixelformat).c_str(),
            FourCCAsString(captureFormat_.fmt.pix.pixelformat).c_str()
            ) << std::endl;
        return ss.str();
    }

    void setupFormat( int width, int height, int bpp)
    {
        struct v4l2_format dst, raw;
        memset( &dst, 0, sizeof(dst));
        memset( &raw, 0, sizeof(raw));

        dst.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        dst.fmt.pix.width = width;
        dst.fmt.pix.height = height;
        dst.fmt.pix.pixelformat = 
            bpp == 24 ? V4L2_PIX_FMT_RGB24 
            : bpp == 8 ? V4L2_PIX_FMT_GREY
            : V4L2_PIX_FMT_RGB24;
        dst.fmt.pix.field = V4L2_FIELD_NONE;
        if (v4lconvert_try_format( convert_, &dst, &raw) != 0)
        {
            throw std::runtime_error( string_format(
                "%s failed at v4lconvert_try_format", deviceName_.c_str()
            ));
        }
        if (xioctl(devfd_, VIDIOC_S_FMT, &raw) < 0) {
            throw std::runtime_error( string_format(
                "%s failed at VIDIOC_S_FMT : %d, %s", 
                deviceName_.c_str(), errno, strerror(errno)));
        }
        captureFormat_ = dst;
        captureFormatRaw_ = raw;
        bitsPerPixel_ = bpp;
    }

    std::vector<CaptureBuffer> createRawBuffers( int bufferCount )
    {
        struct v4l2_requestbuffers req;
        memset( &req, 0, sizeof(req));
        req.count = bufferCount;
        req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        req.memory = V4L2_MEMORY_MMAP;
        if (xioctl(devfd_, VIDIOC_REQBUFS, &req) < 0) {
            throw std::runtime_error( string_format(
                "%s failed at VIDIOC_REQBUFS : %d, %s", deviceName_.c_str(), errno, strerror(errno)));
        }

        std::vector<CaptureBuffer> rawBufs;
        for( int i = 0; i < bufferCount; ++i )
        {
            CaptureBuffer capture;
            capture.buffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            capture.buffer.memory = V4L2_MEMORY_MMAP;
            capture.buffer.index = i;
            capture.converted = nullptr;

            if (xioctl(devfd_, VIDIOC_QUERYBUF, &capture.buffer) < 0)
            {
                throw std::runtime_error( string_format(
                    "%s failed at VIDIOC_QUERYBUF : %d, %s", deviceName_.c_str(), errno, strerror(errno)));
            }
            capture.mmapData = mmap( NULL, capture.buffer.length, PROT_READ | PROT_WRITE, MAP_SHARED, 
                devfd_, capture.buffer.m.offset);

            if (MAP_FAILED == capture.mmapData)
            {
                throw std::runtime_error( string_format(
                    "%s failed at mmap : %d, %s", deviceName_.c_str(), errno, strerror(errno)));
            }

            if( captureFormat_.fmt.pix.pixelformat != captureFormatRaw_.fmt.pix.pixelformat )
            {
                capture.converted = new uint8_t[ captureFormat_.fmt.pix.sizeimage ];
            }

            rawBufs.push_back( capture ); 
        }

        return std::move( rawBufs );
    }

    void queueRawBuffers( std::vector<CaptureBuffer>& bufs )
    {
        for( auto i = 0; i < bufs.size(); ++i )
        {
            auto& buf = bufs[i].buffer;
            if (xioctl( devfd_, VIDIOC_QBUF, &buf) < 0)
            {
                throw std::runtime_error( string_format(
                    "%s failed at VIDIOC_QBUF : %d, %s", deviceName_.c_str(), errno, strerror(errno)));
            }
        }
    }

    void clearRawBufers()
    {
        for( auto& capture : captureBuffers_ )
        {
            if (munmap(capture.mmapData, capture.buffer.length) < 0)
            {
                throw std::runtime_error( string_format(
                    "%s failed at munmap : %d, %s", deviceName_.c_str(), errno, strerror(errno)));
            }

            delete capture.converted;
        }

        captureBuffers_.clear();
    }


    void loopSelect()
    {
        for( ;; )
        {
            fd_set fds;
            FD_ZERO(&fds);
            FD_SET(pipefd_[0], &fds);
            if( currentFrameIndex_ < grabFrameCount_ )
            {
                FD_SET(devfd_, &fds);
            }

            struct timeval tv { 10000, 0 };
            int nfds = std::max( devfd_, pipefd_[0] );
            int ready = select( nfds+1, &fds, NULL, NULL, &tv);
            if (ready < 0) {
                pushAsyncError( string_format( "%s failed at select : %d %s",  
                    deviceName_.c_str(), errno, strerror(errno)));
                continue;
            }
            else if (ready == 0) {
                pushAsyncError( string_format( "%s timeout : %d.%d",  
                    deviceName_.c_str(), tv.tv_sec, tv.tv_usec));
                continue;
            }

            if( FD_ISSET( pipefd_[0], &fds) )
            {
                char msg[ PIPE_MSG_SIZE ];
                if( read( pipefd_[0], msg, PIPE_MSG_SIZE ) < 0 ) {
                    pushAsyncError( string_format( "%s failed at read pipe : %d %s",  
                        deviceName_.c_str(), errno, strerror(errno)));
                    continue;
                }

                std::string smsg{ msg };
                if( smsg == "quit")
                    break;
                else if ( smsg == "select" || smsg == "stop")
                    continue;
                else
                {
                    pushAsyncError( string_format( "%s is not supported", smsg.c_str() ));
                    continue;
                }
            }
            else if ( FD_ISSET( devfd_, &fds) )
            {
                struct v4l2_buffer buf;
                memset( &buf, 0, sizeof(buf));
                buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
                buf.memory = V4L2_MEMORY_MMAP; 
                if (ioctl(devfd_, VIDIOC_DQBUF, &buf) < 0) {
                    
                    if( errno == EAGAIN ) {
                        continue;
                    }
                    else {
                        pushAsyncError( string_format( "%s failed at VIDIOC_DQBUF : %d %s",  
                            deviceName_.c_str(), errno, strerror(errno)));
                        continue;
                    }
                }

                if (xioctl(devfd_, VIDIOC_QBUF, &buf) < 0) {
                    pushAsyncError( string_format( "%s failed at VIDIOC_QBUF : %d %s",  
                        deviceName_.c_str(), errno, strerror(errno)));
                    continue;
                }

                if( currentFrameIndex_+1 >= grabFrameCount_)
                {
                    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
                    if (xioctl(devfd_, VIDIOC_STREAMOFF, &type) < 0)
                    {
                        pushAsyncError( string_format(
                            "%s failed at VIDIOC_STREAMOFF : %d, %s",  deviceName_, errno, strerror(errno)));
                        continue;
                    }
                }

                int frameIndex = currentFrameIndex_++; 
                if(grabCallbackFn_ != nullptr ) 
                {
                    Frame f { 
                        frameIndex, grabFrameCount_, 
                        (int)captureFormat_.fmt.pix.width,
                        (int)captureFormat_.fmt.pix.height,
                        bitsPerPixel_ 
                    };    

                    auto& capture = captureBuffers_[ buf.index ];
                    if( capture.converted != nullptr )
                    {
                        if (v4lconvert_convert(convert_, &captureFormatRaw_, &captureFormat_,
                                (uint8_t*)capture.mmapData, buf.length, 
                                capture.converted, captureFormat_.fmt.pix.sizeimage ) < 0) 
                        {
                            pushAsyncError( string_format( "%s failed at v4lconvert_convert : %d %s",  
                                deviceName_.c_str(), errno, strerror(errno)));
                            continue;
                        }
                        f.image = capture.converted;
                        f.imageLength = captureFormat_.fmt.pix.sizeimage;
                    }
                    else
                    {
                        f.image = (uint8_t*)capture.mmapData;
                        f.imageLength = buf.length;
                    }

                    try
                    {
                        grabCallbackFn_( owner_, f );
                    }
                    catch(const std::exception& e)
                    {
                        pushAsyncError( e.what() );
                        continue;
                    }
                }
            }
        }
    }

    void pipeMessage( const std::string& msg )
    {   
        if( msg.size() > PIPE_MSG_SIZE ) {
            throw std::runtime_error( string_format( 
                "%s can't pipe %s", deviceName_.c_str(), msg.c_str()).c_str());
        }

        char buf[PIPE_MSG_SIZE];
        std::fill( std::copy(msg.begin(), msg.end(), buf), buf+PIPE_MSG_SIZE, 0 );
        write(pipefd_[1], buf, PIPE_MSG_SIZE);
    }

    void pushAsyncError( const std::string& msg )
    {
        std::lock_guard<std::mutex> guard( mutexLock_);
        errorDesc_ = msg;
        grabFrameCount_ = 0;
    }

    std::string popAsyncError()
    {
        std::lock_guard<std::mutex> guard( mutexLock_);
        std::string error;
        error.swap( errorDesc_ );
        return std::move( error );
    }    


    FrameGrabber* owner_;

    int devfd_ = -1;
    struct v4lconvert_data *convert_;
    std::string deviceName_;
    struct v4l2_capability capability_;
    struct v4l2_format captureFormat_;
    struct v4l2_format captureFormatRaw_;
    int bitsPerPixel_;
    std::vector<CaptureBuffer> captureBuffers_;

    std::mutex mutexLock_;
    std::thread threadSelect_;

    int pipefd_[2]; 

    std::string errorDesc_;

    int currentFrameIndex_ = 0;
    int grabFrameCount_ = 0;
    
    GrabCallbackFn grabCallbackFn_ = nullptr;
};

FrameGrabber::FrameGrabber( const char* deviceName )
    : impl_( new Impl(this, deviceName))
{
}

FrameGrabber::~FrameGrabber()
{
    delete impl_;
}

void FrameGrabber::setFormat( int width, int height, int bpp )
{
    impl_->setFormat( width, height, bpp );
}

void FrameGrabber::setBuffers( int bufferCount )
{
    impl_->clearRawBufers();
    impl_->captureBuffers_ = impl_->createRawBuffers( bufferCount );
}

std::vector<Frame> FrameGrabber::getBuffers() const 
{
    return std::move( impl_->getBuffers());
}

void FrameGrabber::setGrabCallback( FrameGrabber::GrabCallbackFn&& fn )
{
    impl_->grabCallbackFn_ = fn;
} 

void FrameGrabber::grab( int count )
{
    impl_->grab( count );
}

void FrameGrabber::stop()
{
    impl_->stop();
}

int FrameGrabber::deviceID() { return impl_->devfd_; }
const std::string& FrameGrabber::deviceName() const { return impl_->deviceName_; }

FrameGrabber::Status FrameGrabber::status() 
{
    return impl_->currentFrameIndex_ < impl_->grabFrameCount_ ? 
        Status::grabbing : Status::idle;
}

std::string FrameGrabber::description() const 
{
    return impl_->description();
}

}