
#include "AppRunner.hpp"
#include <QLabel>
#include <QWidget>
#include <QMainWindow>
#include <iostream>

using namespace std;

int main( int argc, char *argv[] )
{
    AppRunner app( argc, argv );

    for( int i = 0; i < 10; ++ i )
    {
        QMainWindow* w = nullptr;

        cout << "..." << endl;
        app.wait( [&]()
        {
            w = new QMainWindow();
            w->setWindowTitle(std::to_string(i).c_str());
            w->show(); 
        });

        std::this_thread::sleep_for(100ms);
        cout << "..." << endl;
        app.wait( [w]()
        {
            w->close(); 
            delete w;
        }); 
    }

    cout << "posting quit" << endl;
    return 0;    
}