#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <charconv>

// From C++17 In Detail by Bartlomiej Filipek

using namespace std;

TEST(cpp_17_in_detail_part2, char_conv_from_chars)
{
	{
		const std::string str{ "12345" };
		int value = 0;
		const auto res = std::from_chars( str.data(), str.data()+str.size(), value );
		EXPECT_EQ( value, 12345 );
		EXPECT_EQ( res.ec, std::errc() );
		EXPECT_EQ( res.ptr, str.data()+str.size() );
	}

	{
		const std::string str{ "12345678901234" };
		int value = 0;
		const auto res = std::from_chars( str.data(), str.data()+str.size(), value );
		EXPECT_EQ( res.ec, std::errc::result_out_of_range );
	}

	{
		const std::string str{ "abcdefg" };
		int value = 0;
		const auto res = std::from_chars( str.data(), str.data()+str.size(), value );
		EXPECT_EQ( res.ec, std::errc::invalid_argument );
	}

	{
		const std::string str{ "16.78" };
		double value = 0;
		// const auto res = std::from_chars( str.data(), str.data()+str.size(), value );  
		// EXPECT_EQ( res.ec, std::errc() );

		// floating point not supported until gcc 11
	}

}


TEST(cpp_17_in_detail_part2, char_conv_to_chars)
{
	{
		std::string str{ "xxxxxxxx" };
		const auto res = std::to_chars( &str[0], &str[str.size()], 2022 );
		EXPECT_EQ( res.ec, std::errc() );
		EXPECT_EQ( res.ptr, str.data() + 4 );
		EXPECT_EQ( str, "2022xxxx" );
	}

	{
		std::string str{ "xxxxxxxx" };
		const auto res = std::to_chars( &str[0], &str[str.size()], -2022 );
		EXPECT_EQ( res.ec, std::errc() );
		EXPECT_EQ( std::string(str.data(),res.ptr), "-2022" );
	}

	{
		std::string str{ "xxxxxxxx" };
		const auto res = std::to_chars( &str[0], &str[str.size()], 202120222023 );
		EXPECT_EQ( res.ec, std::errc::value_too_large );
	}

	{
		char buf[255];
		const auto res = std::to_chars( buf, buf+sizeof(buf), 0xCAFEBEEF, 16 );
		EXPECT_EQ( res.ec, std::errc() );
		EXPECT_EQ( std::string(buf,res.ptr), "cafebeef" );
	}
}
 

TEST(cpp_17_in_detail_part2, serach)
{
	std::string testString = "Hello Super World";
	std::string needle = "Super";
	const auto it = std::search( std::begin(testString), std::end(testString),
		std::boyer_moore_searcher(std::begin(needle), std::end(needle)));
	EXPECT_EQ( it, testString.begin()+6);
}

struct Nucleotide
{
	enum class Type : uint8_t {
		A = 0,
		C = 1,
		G = 3,
		T = 2
	};
	Type mType;
	friend bool operator==(Nucleotide a, Nucleotide b) noexcept
	{
		return a.mType == b.mType;
	}
	static char ToChar(Nucleotide t);
	static Nucleotide FromChar(char ch);
};

char Nucleotide::ToChar(Nucleotide t)
{
	switch (t.mType)
	{
	case Nucleotide::Type::A: return 'A';
	case Nucleotide::Type::C: return 'C';
	case Nucleotide::Type::G: return 'G';
	case Nucleotide::Type::T: return 'T';
	}
	return 0;
}

Nucleotide Nucleotide::FromChar(char ch)
{
	return Nucleotide{ static_cast<Nucleotide::Type>((ch >> 1) & 0x03) };
}

std::vector<Nucleotide> FromString(const std::string& s)
{
	std::vector<Nucleotide> out;
	out.reserve(s.length());
	std::transform(std::cbegin(s), std::cend(s),

		std::back_inserter(out), Nucleotide::FromChar);

	return out;
}
std::string ToString(const std::vector<Nucleotide>& vec)
{
	std::stringstream ss;
	std::ostream_iterator<char> out_it(ss);
	std::transform(std::cbegin(vec), std::cend(vec), out_it, Nucleotide::ToChar);
	return ss.str();
}

namespace std
{
	template<> struct hash<Nucleotide>
	{
		size_t operator()(Nucleotide n) const noexcept
		{
			return std::hash<Nucleotide::Type>{}(n.mType);
		}
	};
}



TEST(cpp_17_in_detail_part2, search_gene)
{
	const std::vector<Nucleotide> dna = FromString("CTGATGTTAAGTCAACGCTGC");
	std::cout << ToString(dna) << '\n';
	const std::vector<Nucleotide> s = FromString("GCTGC");
	std::cout << ToString(s) << '\n';
	std::boyer_moore_horspool_searcher searcher(std::cbegin(s), std::cend(s));
	const auto it = std::search(std::cbegin(dna), std::cend(dna), searcher);
	if (it == std::cend(dna))
		std::cout << "The pattern " << ToString(s) << " not found\n";
	else
	{
		std::cout << "DNA matched at position: "
			<< std::distance(std::cbegin(dna), it) << '\n';
	}
}
 