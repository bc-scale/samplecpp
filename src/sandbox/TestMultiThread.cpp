#include "stdafx.h"

#include "Fixture.hpp"
#include "Poco/Format.h"
#include <string>
#include <iostream>
#include <thread>
#include <future>
#include <chrono>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <memory>

using namespace std;
using namespace Poco;


class TestMultiThread : public CppUnit::TestCase
{
public:
	TestMultiThread(const string& name) : CppUnit::TestCase(name) {}
	~TestMultiThread() {}

	void setUp() {}
	void tearDown() {}

	static void called_from_static_fn( const string& arg )
	{
		cout << "bar - " << arg << endl;
	}
	
	void called_from_member_fn( const string& arg)
	{
		cout << "bar - " << arg << endl;
	}

	void testThread0()
	{
		std::thread t1(called_from_static_fn, "static");
		//		std::this_thread::sleep_for(chrono::milliseconds(100));
		cout << "foo" << endl;
		t1.join();
	}

	void testThread1()
	{
		thread t1(&TestMultiThread::called_from_member_fn, this, "static");
		cout << "foo" << endl;
		t1.join();
	}

	void testThread2()
	{
		int ret = 0;
		thread t( [&ret](int m, int n) { ret = m + n; }, 2, 4 );
		t.join();
		cout << ret << endl;
	}

	static int twice(int m) { return 2 * m; }

	void testThread3()
	{
		vector<thread> threads;
		for (int i = 0; i < 10; ++i)
		{
			threads.push_back( thread(twice, i));
		}

		for (auto& t : threads)
		{
			t.join();
		}

		// where are the return value ?
	}

	static int DoNotTryAtHome()
	{
		throw exception("I said NO");
	}

#pragma region answer
	static int DoNotTryAtHomeNoThrow()
	{
		try
		{
			throw exception("I said NO");
		}
		catch (exception& ex)
		{
			printf("exception caught : %s", ex.what());
		}

		return 0;
	}
#pragma endregion

	void testThread4()
	{
		thread t1(DoNotTryAtHomeNoThrow);
		cout << "Main Thread" << endl;
		t1.join();
	}

	int m_Bar = 0;

	void AddBarUnsafe( int add )
	{
		int temp = m_Bar;
		this_thread::sleep_for(chrono::milliseconds(10));
		m_Bar = temp + add;
	}

#pragma region answer
	mutex m_LockBar;
	void AddBarSafe(int add)
	{
		lock_guard<mutex> guard(m_LockBar);
		int temp = m_Bar;
		this_thread::sleep_for(chrono::milliseconds(10));
		m_Bar = temp + add;
	}
#pragma endregion

	void testThread5()
	{
		m_Bar = 0;
		vector<thread> ts;
		ts.emplace_back(thread{ &TestMultiThread::AddBarUnsafe, this, 2 });
		ts.emplace_back(thread{ &TestMultiThread::AddBarUnsafe, this, 3 });
		ts.emplace_back(thread{ &TestMultiThread::AddBarUnsafe, this, 4 });

		for (auto& t : ts) {  t.join(); }
		cout << m_Bar << endl;
	}

	struct funcCount
	{
		int &i;
		funcCount(int& i_) : i(i_) {}
		void operator()()
		{
			for (unsigned j = 0; j < 1000; ++j)
			{
				++i;
				this_thread::sleep_for(chrono::milliseconds(1));
			}
		}
	};

	void testThread6()
	{
		int some_local_state = 0;
		funcCount my_func(some_local_state);
		std::thread my_thread(my_func);
		// my_thread.detach();				// undefined behaviour
		my_thread.join();
		cout << "some_local_state = " << some_local_state << endl;
	}

	class thread_guard
	{
		std::thread& t;
	public:
		explicit thread_guard(std::thread& t_) : t(t_) {}
		~thread_guard() 
		{ 
			if (t.joinable()) t.join(); 
		}

		thread_guard(thread_guard const&) = delete;
		thread_guard operator = (thread_guard const &) = delete;
	};

	void do_something_stupid()
	{
		for (int i = 1; i < 10; ++i)
		{
			if (i % 5 == 0)
			{
				throw std::exception("ooops");
			}
		}
	}

	void testThread7()
	{
		int some_local_state = 0;

		try
		{
			funcCount my_func(some_local_state);
			std::thread t(my_func);
			thread_guard g(t);
			do_something_stupid();
		}
		catch (...)
		{
		}

		cout << "some_local_state = " << some_local_state << endl;
	}

#if 0
	struct big_object
	{
		char data[1024];
	};

	static void process_big_object(std::unique_ptr<big_object> p)
	{
		fill( p->data, p->data + 1024, 'a');
		cout << "p->data[0] = " << p->data[0] << endl;
	}

	void testThread8()
	{
		std::unique_ptr<big_object> p(new big_object);
		std::thread t(process_big_object, std::move(p));
		t.join();
	}
#endif

	void testThread9()
	{
		std::thread t1(called_from_static_fn, "static");
		std::thread t2 = std::move(t1);
		t1 = std::thread(&TestMultiThread::called_from_member_fn, this, "static");
		std::thread t3;
		t3 = std::move(t2);
		// t1 = std::move(t3);   //  terminate

		t1.join();
		t3.join();
	}

	void testAsync0()
	{
		future<void> result(async(called_from_static_fn, "static"));
//		this_thread::sleep_for(chrono::milliseconds(100));
		cout << "foo" << endl;
		result.get();
	}

	void testAsync1()
	{
		future<void> result(async(&TestMultiThread::called_from_member_fn, this, "member"));
		cout << "foo" << endl;
		result.get();
	}

	void testAsync2()
	{
		future<int> result(async([](int m, int n) { return m + n; }, 2, 4));
		cout << "foo" << endl;
		cout << result.get() << endl;
	}

	void testAsync3()
	{
		vector<future<int>> futures;
		for (int i = 0; i < 10; ++i)
		{
			futures.push_back(async(twice, i));
		}

		for (auto& e : futures)
		{
			cout << e.get() << endl;
		}
	}

	void testAsync4()
	{
		future<int> result(async(DoNotTryAtHome));
		cout << "foo" << endl;
		
		try
		{
			int ret = result.get();
		}
		catch (exception& ex)
		{
			printf("exception caught : %s", ex.what());
		}
	}

	void testAsync5()
	{
		future<void> f = async( [](){ cout << "bar" << endl; } );
		
		if (f.wait_for(chrono::milliseconds(0)) == future_status::deferred)
		{
			cout << "foo and wait for bar" << endl;
			f.get();
		}
		else
		{
			while (f.wait_for(chrono::milliseconds(10)) != future_status::ready)
			{
				cout << "foo waiting ... " << endl;
			}

			cout << "foo" << endl;
		}
	}

	

	mutex m_LockReadyProcess;
	condition_variable m_Condition;
	string m_Log;
	bool m_Ready = false;
	bool m_Processed = false;

	void worker_thread()
	{
		unique_lock<mutex> lock(m_LockReadyProcess);
		m_Condition.wait(lock, [=]{return m_Ready; });

		cout << "Worker thread is processing data\n";
		m_Log += " after processing";

		m_Processed = true;
		cout << "Worker thread signals data processing completed\n";

		lock.unlock();
		m_Condition.notify_one();
	}

	void testCondtionVariable1()
	{
		thread worker( &TestMultiThread::worker_thread, this);

		m_Log = "Example data";

		{
			lock_guard<mutex> guard(m_LockReadyProcess);
			m_Ready = true;
			cout << "main() signals data ready for processing\n";
		}
		m_Condition.notify_one();

		{
			unique_lock<mutex> lock(m_LockReadyProcess);
			m_Condition.wait(lock, [=]{return m_Processed; });
		}
		cout << "Back in main(), data = " << m_Log << '\n';

		worker.join();
	}

	template<typename T>
	class threadsafe_queue
	{
	private:
		mutable std::mutex mut;
		std::queue< std::shared_ptr<T> > data_queue;
		std::condition_variable data_cond;

	public:
		threadsafe_queue() {}
		void wait_and_pop(T& value)
		{
			std::unique_lock<std::mutex> lk(mut);
			data_cond.wait(lk, [this]{ return !data_queue.empty(); });
			value = std::move(*data_queue.front());
			data_queue.pop();
		}

		bool try_pop(T& value)
		{
			std::lock_guard<std::mutex> lk(mut);
			if (data_queue.empty()) return false;
			value = std::move(&data_queue.front());
			data_queue.pop();
			return true;
		}

		std::shared_ptr<T> wait_and_pop()
		{
			std::unique_lock<std::mutex> lk(mut);
			data_cond.wait(lk, [this]{ return !data_queue.empty(); });
			std::shared_ptr<T> res = data_queue.front();
			data_queue.pop();
			return res;
		}

		std::shared_ptr<T> try_pop()
		{
			std::lock_guard<std::mutex> lk(mut);
			if (data_queue.empty()) return std::shared_ptr<T>();
			std::shared_ptr<T> res = data_queue.front();
			data_queue.pop();
			return res;
		}

		void push(T new_value)
		{
			std::shared_ptr<T> data(std::make_shared<T>(std::move(new_value)));
			std::lock_guard<std::mutex> lk(mut);
			data_queue.push(data);
			data_cond.notify_one();
		}

		bool empty() const
		{
			std::lock_guard<std::mutex> lk(mut);
			return data_queue.empty();
		}
	};

	void test_threadsafe_queue()
	{
		threadsafe_queue<int> queue;
		
		thread writer([this, &queue]{
			for (int i = 0; i < 26; ++i) 
			{ 
				queue.push(i); 
				printf("%c", i+65);
				Sleep(1);
			}
		});

		thread reader([this, &queue]{
			for (int i = 0; i < 26; ++i)
			{ 
				auto v = queue.wait_and_pop();
				int value = *v;
				printf("%c", value+97);
			}
		});

		writer.join();
		reader.join();
	}

	static CppUnit::Test* CreateTest()
	{
		CppUnit::TestSuite* pSuite = new CppUnit::TestSuite("TestAsync");

		CppUnit_addTest(pSuite, TestMultiThread, test_threadsafe_queue);

		CppUnit_addTest(pSuite, TestMultiThread, testThread0);
		CppUnit_addTest(pSuite, TestMultiThread, testThread1);
		CppUnit_addTest(pSuite, TestMultiThread, testThread2);
		CppUnit_addTest(pSuite, TestMultiThread, testThread3);
		CppUnit_addTest(pSuite, TestMultiThread, testThread4);
		CppUnit_addTest(pSuite, TestMultiThread, testThread5);
		CppUnit_addTest(pSuite, TestMultiThread, testThread6);
		CppUnit_addTest(pSuite, TestMultiThread, testThread7);
		CppUnit_addTest(pSuite, TestMultiThread, testThread9);

		CppUnit_addTest(pSuite, TestMultiThread, testAsync0);
		CppUnit_addTest(pSuite, TestMultiThread, testAsync1);
		CppUnit_addTest(pSuite, TestMultiThread, testAsync2);
		CppUnit_addTest(pSuite, TestMultiThread, testAsync3);
		CppUnit_addTest(pSuite, TestMultiThread, testAsync4);
		CppUnit_addTest(pSuite, TestMultiThread, testAsync5);

		CppUnit_addTest(pSuite, TestMultiThread, testCondtionVariable1);


		return pSuite;
	}
};


static AddTestCase<TestMultiThread> s_Test;
