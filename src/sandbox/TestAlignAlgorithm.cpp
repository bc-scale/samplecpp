#include "stdafx.h"

#include "Fixture.hpp"
#include <string>
#include "opencv/cv.hpp"
#include <math.h>
#include <iostream>

using namespace std;
using namespace Poco;
using namespace cv;

class AlignAlgorithmTest : public CppUnit::TestCase
{
public:
	AlignAlgorithmTest(const string& name) : CppUnit::TestCase(name) {}
	~AlignAlgorithmTest() {}

	void setUp() {}

	void tearDown() {}

	Matx33d Orientation( double x, double y, double theta )
	{
		return Matx33d(
			cos(theta), -sin(theta), x,
			sin(theta), cos(theta), y,
			0, 0, 1);
	}

	Matx33d CameraMatrix(double rx, double ry, double W, double H)
	{
		return Matx33d(
			1 / rx, 0, W / 2,
			0, -1 / ry, H / 2,
			0, 0, 1);
	}

	const double ToRadain = 3.14159265358979323846 / 180.0;

	void testMatrixOperations()
	{
		try
		{
			Mat U = Mat::zeros(2, 4, CV_64FC1);
			((Mat)(Mat_<double>(1, 4) << 0, 1, 0, 0)).copyTo(U.row(0));
			U.row(1) = (Mat_<double>(1,4) << 0, 0, 0, 1 ) + 0;

			Dump("U= ", U);


			Mat Ut = U.t();
			assert(Ut.rows == 4 && Ut.cols == 2);
			assert(0.0 == Ut.at<double>(0, 0));
			assert(1.0 == Ut.at<double>(1, 0));
			assert(1.0 == Ut.at<double>(3, 1));

			Mat UtU = Ut * U;
		}
		catch (cv::Exception& ex)
		{
			fail(ex.msg.c_str());
		}
	}

	template<typename T>
	void Dump(const std::string& name, const T& m)
	{
		std::cout << name << endl << " " << m << endl << endl;
	}
	
	void testCameraCalibration1()
	{
		try
		{
			Matx33d Rc2p = CameraMatrix(0.01, 0.01, 800, 600);
			Matx33d Rp2c = Rc2p.inv(DECOMP_LU);

			Matx33d Rca2w = Orientation(190, 90, 0);
			Matx33d Rw2ca = Rca2w.inv(DECOMP_LU);
			Matx33d Rb2s = Orientation(0, 0, 0);

			Dump( "Rb2s = ", Rb2s );

			Point2d M_b(190, 90);
			vector<Point2d> M_p = { Point2d(400 + 100, 300), Point2d(400 - 100, 300) };
			vector<Point2d> S = { Point2d(1, 0), Point2d(-1, 0) };

			int N = (int)M_p.size();
			Mat V(2 * N, 1, CV_64FC1);
			Mat U(2 * N, 4, CV_64FC1);
			for (int i = 0; i < N; ++i)
			{
				Vec3d M_pi(M_p[i].x, M_p[i].y, 1);
				Matx33d Rs2w(Orientation(S[i].x, S[i].y, 0));
				Dump("Rs2w = ", Rs2w);

				Vec3d Vi = Rw2ca * Rs2w * Rb2s * Vec3d(M_b.x, M_b.y, 1);
				V.rowRange(i * 2, i * 2 + 2) = (Mat_<double>(2, 1) << Vi[0], Vi[1]) + 0;

				Vec3d Ui = Rp2c * Vec3d(M_p[i].x, M_p[i].y, 1);
				U.row(i * 2 + 0) = (Mat_<double>(1,4) << 1, 0, -Ui[1], Ui[0]) + 0;
				U.row(i * 2 + 1) = (Mat_<double>(1,4) << 0, 1, Ui[0], Ui[1]) + 0;
			}

			Dump("U=", U);
			Dump("V=", V);

			Mat Ut = U.t();
			Mat UtU = Ut * U;
			Mat X = UtU.inv(DECOMP_LU) * Ut * V;
			Dump("X=", X);

			Vec3d vX((const double*)X.data);
			assertEqualDelta(0.0, vX[0], 1e-6);
			assertEqualDelta(0.0, vX[1], 1e-6);
			assertEqualDelta(0.0, vX[2], 1e-6);

			// error
			double es = 0.0;
			Matx33d Rc2ca = Orientation(vX[0], vX[1], vX[2]);
			for (int i = 0; i < N; ++i)
			{
				Vec3d M_wc = Rca2w * Rc2ca * Rp2c * Vec3d(M_p[i].x, M_p[i].y, 1 );
				Dump("M_wc=", M_wc);

				Matx33d Rs2w(Orientation(S[i].x, S[i].y, 0));
				Vec3d M_wb = Rs2w * Rb2s * Vec3d(M_b.x, M_b.y, 1);
				Dump("M_wb=", M_wb);

				Vec3d ei = (M_wc - M_wb);
				es += sqrt(ei[0] * ei[0] + ei[1] * ei[1]);
			}

			es = es / N;
			assertEqualDelta(0.0, es, 1e-6);
		}
		catch (cv::Exception& ex)
		{
			fail(ex.msg.c_str());
		}
	}

	Vec3d CalibrateCamera(const vector<Point2d>& M_p, const vector<Point2d>& S, double& error )
	{
		Matx33d Rp2c = CameraMatrix(0.01, 0.01, 800, 600).inv(DECOMP_LU);
		Matx33d Rca2w = Orientation(190, 90, 0);
		Matx33d Rb2s = Orientation(0, 0, 0);
		Vec3d M_b(190, 90, 1);

		int N = (int)M_p.size();
		Mat V(2 * N, 1, CV_64FC1);
		Mat U(2 * N, 4, CV_64FC1);
		for (int i = 0; i < N; ++i)
		{
			Matx33d Rs2w(Orientation(S[i].x, S[i].y, 0));
			Vec3d Vi = Rca2w.inv() * Rs2w * Rb2s * M_b;
			V.rowRange(i * 2, i * 2 + 2) = (Mat_<double>(2, 1) << Vi[0], Vi[1]) + 0;

			Vec3d Ui = Rp2c * Vec3d(M_p[i].x, M_p[i].y, 1);
			U.row(i * 2 + 0) = (Mat_<double>(1, 4) << 1, 0, -Ui[1], Ui[0]) + 0;
			U.row(i * 2 + 1) = (Mat_<double>(1, 4) << 0, 1, Ui[0], Ui[1]) + 0;
		}
		Mat X = (U.t() * U).inv(DECOMP_LU) * U.t() * V;

		// error
		Vec3d vX((const double*)X.data);
		Matx33d Rc2ca = Orientation(vX[0], vX[1], vX[2]);

		error = 0.0;
		for (int i = 0; i < N; ++i)
		{
			Vec3d M_wc = Rca2w * Rc2ca * Rp2c * Vec3d(M_p[i].x, M_p[i].y, 1);

			Matx33d Rs2w(Orientation(S[i].x, S[i].y, 0));
			Vec3d M_wb = Rs2w * Rb2s * M_b;

			Vec3d ei = (M_wc - M_wb);
			error += sqrt( ei.dot( ei ));
		}
		error /= N;

		return vX;
	}

	void testCameraCalibration2()
	{
		try
		{
			vector<Point2d> M_p = { Point2d(400 + 200, 300), Point2d(400 - 0, 300) };
			vector<Point2d> S = { Point2d(1, 0), Point2d(-1, 0) };
			
			double error = 0;
			Vec3d vX = CalibrateCamera(M_p, S, error);

			assertEqualDelta(-1.0, vX[0], 1e-6);
			assertEqualDelta(0.0, vX[1], 1e-6);
			assertEqualDelta(0.0, vX[2], 1e-6);
			assertEqualDelta(0.0, error, 1e-6);
		}
		catch (cv::Exception& ex)
		{
			fail(ex.msg.c_str());
		}
	}

	void testCameraCalibration3()
	{
		try
		{
			vector<Point2d> M_p = { Point2d(400 + 200, 300+100), Point2d(400 - 0, 300+100) };
			vector<Point2d> S = { Point2d(1, 0), Point2d(-1, 0) };

			double error = 0;
			Vec3d vX = CalibrateCamera(M_p, S, error);

			assertEqualDelta(-1.0, vX[0], 1e-6);
			assertEqualDelta(1.0, vX[1], 1e-6);
			assertEqualDelta(0.0, vX[2], 1e-6);
			assertEqualDelta(0.0, error, 1e-6);
		}
		catch (cv::Exception& ex)
		{
			fail(ex.msg.c_str());
		}
	}

	void testCameraCalibration4()
	{
		try
		{
			vector<Point2d> M_p = { Point2d(400 + 100, 300+50 ), Point2d(400 - 100, 300-50) };
			vector<Point2d> S = { Point2d(1, 0), Point2d(-1, 0) };

			double error = 0;
			Vec3d vX = CalibrateCamera(M_p, S, error);

			assertEqualDelta(0.0, vX[0], 1e-6);
			assertEqualDelta(0.0, vX[1], 1e-6);
			assertEqualDelta(0.4, vX[2], 1e-6);
			assertEqualDelta(0.135866, error, 1e-6);
		}
		catch (cv::Exception& ex)
		{
			fail(ex.msg.c_str());
		}
	}

	void testCameraCalibration5()
	{
		try
		{
			vector<Point2d> M_p = { Point2d(400 + 200, 300 + 100), Point2d(400 - 0, 300 + 100), Point2d(400 + 100, 300 + 100) };
			vector<Point2d> S = { Point2d(1, 0), Point2d(-1, 0), Point2d(0,0) };

			double error = 0;
			Vec3d vX = CalibrateCamera(M_p, S, error);

			assertEqualDelta(-1.0, vX[0], 1e-6);
			assertEqualDelta(1.0, vX[1], 1e-6);
			assertEqualDelta(0.0, vX[2], 1e-6);
			assertEqualDelta(0.0, error, 1e-6);
		}
		catch (cv::Exception& ex)
		{
			fail(ex.msg.c_str());
		}
	}


	Vec3d CalibrateGlass(const vector<Point2d>& M_p, const vector<Point2d>& M_g, double& error)
	{
		Matx33d Rp2c = CameraMatrix(0.01, 0.01, 800, 600).inv(DECOMP_LU);
		Matx33d Rc2ca = Orientation(0, 0, 0);

		vector<Matx33d> Rca2w = { Orientation(190, 90, 0), Orientation(-190, 90, 0) };

		Matx33d Rb2s = Orientation(0, 0, 0);
		Matx33d Rs2w = Orientation(0, 0, 0);
		Matx33d Rgl2b = Orientation(0, 0, 0);
		Matx33d Rg2gc = Orientation(0, 0, 0);

		int N = (int)M_p.size();
		Mat V(2 * N, 1, CV_64FC1);
		Mat U(2 * N, 4, CV_64FC1);
		for (int i = 0; i < N; ++i)
		{
			Vec3d Vi = (Rs2w * Rb2s * Rgl2b).inv(DECOMP_LU) * Rca2w[i] * Rc2ca * Rp2c * Vec3d(M_p[i].x, M_p[i].y, 1);
			V.rowRange(i * 2, i * 2 + 2) = (Mat_<double>(2, 1) << Vi[0], Vi[1]) + 0;

			Vec3d Ui = Rg2gc * Vec3d(M_g[i].x, M_g[i].y, 1);
			U.row(i * 2 + 0) = (Mat_<double>(1, 4) << 1, 0, -Ui[1], Ui[0]) + 0;
			U.row(i * 2 + 1) = (Mat_<double>(1, 4) << 0, 1, Ui[0], Ui[1]) + 0;
		}
		Mat X = (U.t() * U).inv(DECOMP_LU) * U.t() * V;

		// error
		Vec3d vX((const double*)X.data);
		Matx33d Rgc2gl = Orientation(vX[0], vX[1], vX[2]);
		error = 0.0;
		for (int i = 0; i < N; ++i)
		{
			Vec3d M_wc = Rca2w[i] * Rc2ca * Rp2c * Vec3d(M_p[i].x, M_p[i].y, 1);
			Vec3d M_wb = Rs2w * Rb2s * Rgl2b * Rgc2gl * Rg2gc * Vec3d(M_g[i].x, M_g[i].y, 1);

			Vec3d ei = (M_wc - M_wb);
			error += sqrt(ei.dot(ei));
		}
		error /= N;
		return vX;
	}

	void testGlassCalibration1()
	{
		try
		{
			vector<Point2d> M_p = { Point2d(400, 300), Point2d(400, 300) };
			vector<Point2d> M_g = { Point2d(190, 90), Point2d(-190, 90) };

			double error = 0;
			Vec3d vX = CalibrateGlass(M_p, M_g, error);

			assertEqualDelta(0.0, vX[0], 1e-6);
			assertEqualDelta(0.0, vX[1], 1e-6);
			assertEqualDelta(0.0, vX[2], 1e-6);
			assertEqualDelta(0.0, error, 1e-6);
		}
		catch (cv::Exception& ex)
		{
			fail(ex.msg.c_str());
		}
	}

	void testGlassCalibration2()
	{
		try
		{
			vector<Point2d> M_p = { Point2d(400+200, 300), Point2d(400+200, 300) };
			vector<Point2d> M_g = { Point2d(190, 90), Point2d(-190, 90) };

			double error = 0;
			Vec3d vX = CalibrateGlass(M_p, M_g, error);

			assertEqualDelta(2.0, vX[0], 1e-6);
			assertEqualDelta(0.0, vX[1], 1e-6);
			assertEqualDelta(0.0, vX[2], 1e-6);
			assertEqualDelta(0.0, error, 1e-6);
		}
		catch (cv::Exception& ex)
		{
			fail(ex.msg.c_str());
		}
	}

	void testGlassCalibration3()
	{
		try
		{
			vector<Point2d> M_p = { Point2d(400 + 200, 300-150), Point2d(400 + 200, 300-150) };
			vector<Point2d> M_g = { Point2d(190, 90), Point2d(-190, 90) };

			double error = 0;
			Vec3d vX = CalibrateGlass(M_p, M_g, error);

			assertEqualDelta(2.0, vX[0], 1e-6);
			assertEqualDelta(1.5, vX[1], 1e-6);
			assertEqualDelta(0.0, vX[2], 1e-6);
			assertEqualDelta(0.0, error, 1e-6);
		}
		catch (cv::Exception& ex)
		{
			fail(ex.msg.c_str());
		}
	}

	static CppUnit::Test* CreateTest()
	{
		CppUnit::TestSuite* pSuite = new CppUnit::TestSuite("AlignAlgorithmTest");

		CppUnit_addTest(pSuite, AlignAlgorithmTest, testMatrixOperations);
		CppUnit_addTest(pSuite, AlignAlgorithmTest, testCameraCalibration1);
		CppUnit_addTest(pSuite, AlignAlgorithmTest, testCameraCalibration2);
		CppUnit_addTest(pSuite, AlignAlgorithmTest, testCameraCalibration3);
		CppUnit_addTest(pSuite, AlignAlgorithmTest, testCameraCalibration4);
		CppUnit_addTest(pSuite, AlignAlgorithmTest, testCameraCalibration5);
		CppUnit_addTest(pSuite, AlignAlgorithmTest, testGlassCalibration1);
		CppUnit_addTest(pSuite, AlignAlgorithmTest, testGlassCalibration2);
		CppUnit_addTest(pSuite, AlignAlgorithmTest, testGlassCalibration3);

		return pSuite;
	}
};


static AddTestCase<AlignAlgorithmTest> s_Test;
