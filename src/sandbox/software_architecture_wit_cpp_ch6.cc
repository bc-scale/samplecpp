#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <string>
#include <iostream>
#include <vector>
#include <memory>
#include <chrono>
#include <variant>
#include <algorithm>

// From Software Architecture with C++ - Adrian Ostrowski, Piotr Gaczkowski

using namespace std;

struct MyNonCopiableType { 
	MyNonCopiableType() = delete;
	MyNonCopiableType( const MyNonCopiableType &) = delete;
	MyNonCopiableType& operator = ( const MyNonCopiableType &) = delete;
	MyNonCopiableType( MyNonCopiableType &&) = delete;
	MyNonCopiableType& operator = ( MyNonCopiableType &&) = delete;

	MyNonCopiableType( int answer ) : answer( answer )
	{
	};

	int answer;
};

TEST(software_architecture_wit_cpp_ch6, verbose_no_copiable )
{
	MyNonCopiableType a{42};
	// MyNonCopiableType b = a;
	// MyNonCopiableType c(a);
	// MyNonCopiableType d(std::move(a));
	// MyNonCopiableType e = std::move(a);
}

struct SimpleNoCopy
{
	SimpleNoCopy( int answer ) : answer(answer) {}

	int answer;
	std::unique_ptr<int> just_to_prevent_copy;
};

TEST(software_architecture_wit_cpp_ch6, simple_no_copiable )
{
	SimpleNoCopy a{42};
	// SimpleNoCopy b = a;
	// SimpleNoCopy c(a);
	// SimpleNoCopy d(std::move(a));
	// SimpleNoCopy e = std::move(a);
}

namespace CRTP {

template< typename ConcreteItem > class GlamorousItem { 
public:
	void appear_in_full_glory() {
		static_cast<ConcreteItem*>(this)->appear_in_full_glory();
	}
};

class PinkHeels : public GlamorousItem< PinkHeels > {
public: 
	void appear_in_full_glory() {
		std::cout << "Pink high heels suddenly appeared in all their beauty\n";
	}
};

class GoldenWatch : public GlamorousItem< GoldenWatch > {
public:
	void appear_in_full_glory() {
		std::cout << "Everyone wanted to watch this watch\n";
	}
};

template <typename... Args>
using PreciousItems = std::tuple<GlamorousItem<Args>...>;

}

TEST( software_architecture_wit_cpp_ch6, CRTP_using_tuple )
{
	using namespace CRTP;

	auto glamorous_items = PreciousItems< PinkHeels, GoldenWatch > { };

	std::apply( 
		[]<typename... T>(GlamorousItem<T>... items) {
			(items.appear_in_full_glory(), ... );
		},
		glamorous_items
	);
}

namespace CRTP{

using GlamorousVariant = std::variant<PinkHeels,GoldenWatch>;

}


TEST( software_architecture_wit_cpp_ch6, CRTP_using_variant )
{
	using namespace CRTP;

	auto glamorous_items = std::array{ 
		GlamorousVariant{ PinkHeels{} }, GlamorousVariant{ GoldenWatch{} } 
	};

	for( auto& elem : glamorous_items) {
		std::visit( []<typename T>(GlamorousItem<T> item){
			item.appear_in_full_glory();
		}, elem);
	}
}

#if 0
// 'requires' is C++20.
class CommonGlamorousItem {
public:
	template< typename T> requires std::is_base_of_v< GlamorousItem<T>, T>
	explicit CommonGlamorousItem(T&& item)
		: item_{ std::forward<T>(item)} 
	{}

	void appear_in_full_glory()
	{
		std::visit(
			[]<typename T>(GlamorousItem<T> item ) { item.appear_in_full_glory(); }, 
			item_ 
		);
	}

private:
	GlamorousVariant item_;
};


TEST( software_architecture_wit_cpp_ch6, CRTP_using_variant_class )
{
	auto glamorous_items = std::array{ 
		CommonGlamorousItem{ PinkHeels{} }, CommonGlamorousItem{ GoldenWatch{} } 
	};

	for( auto& elem : glamorous_items) {
		elem.appear_in_full_glory();
	}
}
#endif


namespace CRTP{

struct TypeErasedItemBase {
	virtual ~TypeErasedItemBase() = default;
	virtual void appear_in_full_glory_impl() = 0;
};

template<typename T>
class TypeErasedItem final : public TypeErasedItemBase {
public:
	explicit TypeErasedItem(T t) : t_{std::move(t)} {}
	void appear_in_full_glory_impl() override { t_.appear_in_full_glory(); }

private:
	T t_;
};

class GlamorousItemTE {
public:
	template< typename T>
	explicit GlamorousItemTE(T t)
		: item_{ std::make_unique<TypeErasedItem<T>>(std::move(t))}
	{}

	void appear_in_full_glory() { item_->appear_in_full_glory_impl(); }

private:
	std::unique_ptr<TypeErasedItemBase> item_;
};

}

TEST( software_architecture_wit_cpp_ch6, CRTP_using_type_erasure )
{
	using namespace CRTP;

	auto glamorous_items = std::array{ 
		GlamorousItemTE{ PinkHeels{} }, GlamorousItemTE{ GoldenWatch{} } 
	};

	for( auto& item : glamorous_items) {
		item.appear_in_full_glory();
	}
}


namespace Factory {

class IDocument {
public:
	virtual ~IDocument() = default;
	virtual void Process() = 0;
};

class DocumentOpener {
public:
	using DocumentType = std::unique_ptr<IDocument>;
	using ConcreteOpener = DocumentType (*) (std::string_view);

	void Register( std::string_view extension, ConcreteOpener opener ) {
		openerByExtensions.emplace( extension, opener);
	}

	DocumentType open( std::string_view path ) {
		if ( auto last_dot = path.find_last_of('.'); last_dot != std::string_view::npos) {
			auto extension = path.substr(last_dot+1);
			return openerByExtensions.at(extension)(path);
		} 
		else {
			throw std::invalid_argument("trying to open file with no extension");
		}
	}

private:
	std::unordered_map<std::string_view, ConcreteOpener> openerByExtensions;
};

class PdfDocument : public IDocument {
public:
	PdfDocument( std::string_view path ) : path_{path} {}
	void Process() override { std::cout << "process pdf file : " << path_ << "\n"; }

	std::string path_;
};

class HtmlDocument : public IDocument {
public:
	HtmlDocument( std::string_view path ) : path_{path} {}
	void Process() override { std::cout << "process html file : " << path_ << "\n"; }

	std::string path_;
};


}

TEST( software_architecture_wit_cpp_ch6, factory_open_to_extension )
{
	using namespace Factory;

	auto document_opener = DocumentOpener{};
	document_opener.Register( "pdf", [](auto path) -> DocumentOpener::DocumentType {
		return std::make_unique<PdfDocument>( path );
	});
	document_opener.Register( "html", [](auto path) -> DocumentOpener::DocumentType {
		return std::make_unique<HtmlDocument>( path );
	});

	document_opener.open( "my.html" )->Process();
	document_opener.open( "my.pdf" )->Process();
}

namespace Builder
{

struct Item {
	std::string name;
	std::optional<std::string> photo_url;
	std::string description;
	std::optional<float> price;
	std::chrono::time_point<std::chrono::system_clock> date_added {};
	bool featured {};
};

template<typename ConcreteBuilder>
class GenericItemBuilder{
public:
	explicit GenericItemBuilder( std::string name )
		: item_ { std::move(name) } 
	{}

	Item build() && {
		item_.date_added = std::chrono::system_clock::now();
		return std::move( item_ );
	}

	ConcreteBuilder &&with_description( std::string desc ) {
		item_.description = std::move(desc);
		return static_cast<ConcreteBuilder &&>(*this);
	}

	ConcreteBuilder &&with_price( float price ) {
		item_.price = price;
		return static_cast<ConcreteBuilder &&>(*this);
	}

	ConcreteBuilder &&marked_as_featured() {
		item_.featured = true;
		return static_cast<ConcreteBuilder &&>(*this);		
	}

protected:
	Item item_;
};

class ItemBuilder final : public GenericItemBuilder< ItemBuilder> {
	using GenericItemBuilder< ItemBuilder>::GenericItemBuilder;
};

}

TEST( software_architecture_wit_cpp_ch6, builder )
{
	using namespace Builder;

	auto directly_loaded_item = ItemBuilder{ "Pot" }
		.with_description( "A decent one")
		.with_price( 100 )
		.build();

	EXPECT_EQ( directly_loaded_item.description, "A decent one" );
	EXPECT_EQ( directly_loaded_item.price, 100.f );
}

namespace Builder
{

class FetchingItemBuilder final : public GenericItemBuilder<FetchingItemBuilder> {
public:
	explicit FetchingItemBuilder(std::string name) 
		: GenericItemBuilder( std::move(name) ) 
	{}

	FetchingItemBuilder&& using_data_from( std::string_view url ) && {
		item_.description = std::string(url) + " fetched";
		return std::move(*this);
	}
};

}

TEST( software_architecture_wit_cpp_ch6, builder_extended )
{
	using namespace Builder;

	auto fetched_item = FetchingItemBuilder( "Linen blouses")
		.using_data_from( "https://example.com/items/linen_blouses" )
		.marked_as_featured()
		.build();

	EXPECT_EQ( fetched_item.description, "https://example.com/items/linen_blouses fetched" );
}

namespace Prototype {

class Map {
public: 
	Map() {}
	Map( std::vector<std::string> locations ) : locations_( std::move(locations) ) {}

	virtual std::unique_ptr<Map> clone() const {
		auto cloned = std::make_unique<Map>();
		std::copy( locations_.begin(), locations_.end(), std::back_inserter(cloned->locations_) );
		return cloned;
	}

	const std::vector<std::string>& get_locations() const { return locations_; }

protected:
	std::vector<std::string> locations_;
};

class MapWithPointsOfInterests : public Map {
public:
	MapWithPointsOfInterests() {}
	MapWithPointsOfInterests( std::vector<std::string> locations, std::vector<std::string> pois ) 
		: Map( std::move(locations) ), pois_(std::move(pois)) 
	{}

	virtual std::unique_ptr<Map> clone() const override {
		auto cloned = std::make_unique<MapWithPointsOfInterests>();
		std::copy( locations_.begin(), locations_.end(), std::back_inserter(cloned->locations_) );
		std::copy( pois_.begin(), pois_.end(), std::back_inserter(cloned->pois_) );
		return cloned;
	}

	const std::vector<std::string>& get_point_of_interests() const { return pois_; }

protected:
	std::vector<std::string> pois_;
};

}

TEST( software_architecture_wit_cpp_ch6, prototype )
{
	using namespace Prototype;

	Map my_map( { "here", "there"} );
    EXPECT_THAT( my_map.get_locations(), testing::ElementsAre("here", "there") );

	auto cloned_map = my_map.clone();
    EXPECT_THAT( cloned_map->get_locations(), testing::ElementsAre("here", "there") );	

	MapWithPointsOfInterests my_pois( { "here", "there"}, {"where"} );
	auto cloned_pois = my_pois.clone();
    EXPECT_THAT( cloned_pois->get_locations(), testing::ElementsAre("here", "there") );	
}


namespace ItemStateMachine {

namespace state
{

struct Depleted {};

struct Available {
	int count;
};

struct Discontinued {};

} // state


namespace event {

struct DeliveryArrived {
	int count;
};

struct Purchased {
	int count;
};

struct Discontinued {};

} // event

using State = std::variant<state::Depleted, state::Available, state::Discontinued>;

State on_event( state::Available available, event::DeliveryArrived delivered ) {
	available.count += delivered.count;
	return available;
}

State on_event(state::Available available, event::Purchased purchased ) {
	available.count -= purchased.count;
	if( available.count > 0 )
		return available;
	return state::Depleted{};
}

template< typename S> 
State on_event( S, event::Discontinued) {
	return state::Discontinued{};	
}

State on_event( state::Depleted depleted, event::DeliveryArrived delivered) {
	return state::Available{ delivered.count };	
}

template<typename TE> 
State on_debug_depleted( state::Depleted depleted, TE ev ) {
	std::cout << "debugging depleted state \n";
	return depleted;
}

template<class... Ts> struct overload : Ts... { using Ts::operator()...; };
template<class... Ts> overload(Ts...) -> overload<Ts ...>;

class StateMachine {
public:
	StateMachine(State initial ) : state_(initial) {}

	template<typename Event> 
	void process_event(Event &&ev) {
		state_ = std::visit( overload {
			[&](const auto& state) -> State {
				return on_event(state, std::forward<Event>(ev));
			},
			[](const auto &unsupported_state) -> State {
				throw std::logic_error("Unsupported state transition");
			}
		},
		state_ );
	}

	template<typename Event> 
	void debug(Event &&ev) {
		std::cout << "just debug \n";
		state_ = std::visit( overload { 
				[&](state::Depleted st) -> State {
					return on_debug_depleted( st, std::forward<Event>(ev) );
				}
				, [](const auto& st ) -> State{
					std::cout << "no supported debugging \n";
					return st;
				}
			}
			, state_ );
	}

private:
	State state_;
};

} // namespace ItemStateMachine


TEST( software_architecture_wit_cpp_ch6, state_machine_incomplete )
{
	using namespace ItemStateMachine; 
	auto fsm = StateMachine( state::Depleted{} );

	fsm.debug( event::DeliveryArrived{3} );
	
	// compile error in here 
	// overload resultion should be like above debug, I guess ???
	// fsm.process_event( event::DeliveryArrived{3});  
}
