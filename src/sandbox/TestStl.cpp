#include <gtest/gtest.h>
#include <string>
#include <unordered_map>
#include <initializer_list>
#include <Poco/JSON/Object.h>

using namespace std;
//using namespace Poco;

struct ScaleDegree {
	int Scale, Degree;

	// for map
	bool operator <( const ScaleDegree& rhs) const
	{
		if (Scale != rhs.Scale)
			return Scale < rhs.Scale;

		return Degree < rhs.Degree;
	}
};

// for unordered_map
bool operator==(const ScaleDegree& lhs, const ScaleDegree& rhs) {
	return lhs.Scale == rhs.Scale && lhs.Degree == rhs.Degree;
}

namespace std
{
	template<> struct hash<ScaleDegree>
	{
		size_t operator()(ScaleDegree const& key) const
		{
			return hash<int>{}(key.Scale) ^ hash<int>{}(key.Degree);
		}
	};
}

TEST(StlTest, testBinaryTreeMap) 
{
	map< ScaleDegree, string > stringMap;
	stringMap[ScaleDegree{ 1, 1 }] = "Scale 1 - Degree 1";
	stringMap[ScaleDegree{ 2, 3 }] = "Scale 2 - Degree 3";
	stringMap[{ 2, 2 }] = "Scale 2 - Degree 2";
	stringMap[{ 3, 4 }] = "Scale 3 - Degree 4";

	string ret = stringMap[{ 2, 3 }];
	assert(ret == "Scale 2 - Degree 3");

	cout << endl;
	for (const auto& e : stringMap)
	{
		cout << e.second << endl;
	}
}

TEST(StlTest, testHashDictionary) 
{
	unordered_map< ScaleDegree, string > stringMap;
	stringMap[{ 1, 1 }] = "Scale 1 - Degree 1";
	stringMap[{ 2, 3 }] = "Scale 2 - Degree 3";
	stringMap[{ 2, 2 }] = "Scale 2 - Degree 2";
	stringMap[{ 3, 4 }] = "Scale 3 - Degree 4";

	string ret = stringMap[{ 2, 3 }];
	assert(ret == "Scale 2 - Degree 3");

	cout << endl;
	for (const auto& e : stringMap)
	{
		cout << e.second << endl;
	}

	cout << "Hash of ScaleDegree(2,3) " << hash<ScaleDegree>{}({ 2, 3 }) << endl;
}


TEST(StlTest, testNameValueTable) 
{
	struct NameValue
	{
		string Name;
		Poco::Dynamic::Var Value;
	};

	class NameValueTable : public Poco::JSON::Object
	{
	public:
		NameValueTable(initializer_list<NameValue> nvl)
		{
			for (auto& nv : nvl)
			{
				set(nv.Name, nv.Value);
			}
		}
	};

	NameValueTable t({ { "Hello", "World" }, { "Answer", 42 }, { "Hello", "How's going?" } });

	EXPECT_EQ(t.get("Hello").toString(), "How's going?");
	EXPECT_EQ(t.get("Answer").convert<int>(), 42);	
}
