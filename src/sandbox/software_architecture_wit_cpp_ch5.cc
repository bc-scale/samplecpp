#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <vector>
#include <memory>
#include <chrono>
#include <variant>
#include <algorithm>

// From Software Architecture with C++ - Adrian Ostrowski, Piotr Gaczkowski

using namespace std;

struct Resource {
	int Foo;
	double Bar;
};

// C API
Resource* acquireResource() { return new Resource{}; }
void releaseResource( Resource* r) { delete r; }

// C++ API
using ResourceRAII = std::unique_ptr<Resource, decltype(&releaseResource)>;
ResourceRAII acquireResourceRAII() { return ResourceRAII( acquireResource(), releaseResource ); }

TEST(software_architecture_wit_cpp_ch5, leveraing_RAII )
{
	{
		ResourceRAII r = acquireResourceRAII();
		r->Foo = 42; r->Bar = 32.1;
	}

	{
		ResourceRAII r = acquireResourceRAII();
		r->Foo = 24; r->Bar = 1.23;
	}
}

TEST(software_architecture_wit_cpp_ch5, class_template_argument_deduction )
{
	auto ints = std::array{ 1, 2, 3 };

	auto legCount = std::unordered_map{ std::pair{"cat", 4}, {"human",2}, {"mushroom",1} };
}

namespace which_of_following_should_be_used {

void A( Resource*);
void B( Resource& );
void B1( const Resource& );
void C( std::unique_ptr<Resource> );  // take ownership
void D( std::unique_ptr<Resource>& ); // no need. use B1
void E( std::shared_ptr<Resource> );  // incrementing reference count. 
void F( std::shared_ptr<Resource>& ); // B1 instead

}


#ifdef NDEBUG
inline namespace release {
#else
inline namespace debug {
#endif

struct EasilyDebuggable {  
//...
#ifndef NDEBUG
	int debug_foo;
	int debug_bar;	
#else
	bool no_debug_available;
#endif
};

}

TEST(software_architecture_wit_cpp_ch5, inline_namespace )
{
	EasilyDebuggable a;   // can see this class without namespace release or debug.
	a.debug_bar = 10;
	a.debug_foo = 20;
	// a.no_debug_available = false;
}

namespace try1 {
	int try_parse( std::string_view maybe_number );  							// -1 if not number ???
}

namespace try2 {
	bool try_parse( std::string_view maybe_number, int &parsed_number );	 	// still you can read parsed_number without checking return bool
}

namespace try3 {
	int* try_parse( std::string_view maybe_number );  							// null pointer if failed ? 
}

namespace try4 {
	std::optional<int> try_parse( std::string_view maybe_number ); 				// beautiful
}


struct PhoneNumber {};

struct UserProfile {
	std::string nickname;
	std::optional<std::string> full_name;
	std::optional<std::string> address;
	std::optional<PhoneNumber> phone;
};

namespace declarative_ignorant {
std::optional<size_t> find_changes( const std::vector<double> temperatures, double diff )
{
	for( size_t i = 0; i < temperatures.size()-1; ++i )
	{
		if( std::abs( temperatures[i]-temperatures[i+1])>diff)
		{
			return std::optional{i};
		}
	}

	return std::nullopt;
}
}

namespace declarative_algo {
std::optional<size_t> find_changes( const std::vector<double> temperatures, double diff )
{
	auto it = std::adjacent_find( temperatures.begin(), temperatures.end(), 
		[diff] (double first, double second ) { return std::abs( first -second) > diff; });
		
	if ( it != std::end(temperatures))
		return std::optional{ std::distance( std::begin(temperatures),it)};

	return std::nullopt;
}
}


TEST(software_architecture_wit_cpp_ch5, declarative_code )
{
	std::vector<double> temperature { -3., -2., 0., 8., -10., -7. };

	auto pos1 = declarative_ignorant::find_changes( temperature, 5.0 );
	auto pos2 = declarative_algo::find_changes( temperature, 5.0 );

	EXPECT_EQ( pos1, pos2 );
}

namespace gallery{

using CustomerId = int;
CustomerId get_current_customer_id() { return 42; }

struct Merchant {
	int id;
};

struct Item {
	std::string name;
	std::optional<std::string> photo_url;
	std::string description;
	std::optional<float> price;
	std::chrono::time_point<std::chrono::system_clock> date_added {};
	bool featured {};
};

std::ostream &operator <<( std::ostream& os, const Item& item) {
	auto stringify_optional = []( const auto& optional ) {
		using optional_value_type = 
			typename std::__remove_cvref_t< decltype(optional) >::value_type;
		if constexpr (std::is_same_v<optional_value_type,std::string>) {
			return optional ? *optional : "missing";
		} 
		else {
			return optional ? std::to_string(*optional) : "missing";
		}
	};

	auto time_added = std::chrono::system_clock::to_time_t( item.date_added );

	os << "name: " << item.name
		<< ", photo_url: " << stringify_optional( item.photo_url )
		<< ", description: " << item.description
		<< ", price: " << std::setprecision(2) << stringify_optional(item.price)
		<< ", date_added: " << std::put_time( std::localtime(&time_added), "%c %Z")
		<< ", featured: " << item.featured;

	return os;
}

enum class Category {
	Food,
	Antique,
	Books,
	Music,
	Photography,
	Handicraft,
	Artist,
};

struct Store {
	const Merchant* owner;
	std::vector<Item> items;
	std::vector<Category> categories;
};

using Stores = std::vector<const Store*>;
Stores get_favorite_stores_for( const CustomerId& customer_id ) 
{
	static const auto merchants = std::vector<Merchant>{ {17}, {29}};
	static const auto stores = std::vector<Store>{
		{ 
			{ &merchants[0] }, 
			{
				{
					"Honey", 
					{},
					"Straight outta Compton's apiary",
					9.99f,
					std::chrono::system_clock::now(),
					false,
				},
				{
					"Potato", 
					{},
					"Straight outta Compton's apiary",
					2.99f,
					std::chrono::system_clock::now(),
					true,
				},				
			},
			{
				Category::Food
			}
		},
		{ 
			{ &merchants[1] }, 
			{
				{
					"Butter", 
					{},
					"Straight outta My's apiary",
					4.99f,
					std::chrono::system_clock::now(),
					false,
				},
			},
			{
				Category::Food
			}
		}
	};

	static auto favourite_store_by_customer = std::unordered_map<CustomerId, Stores> 
	{
		{ 
			42, 
			{ &stores[0], &stores[1] }, 
		},
	}; 

	return favourite_store_by_customer[ customer_id ];
}

using Items = std::vector<const Item*>;

Items get_featured_items_for_store( const Store& store )
{
	auto featured = Items {};
	for( const auto& item : store.items) {
		if ( item.featured ) {
			featured.emplace_back( &item );
		}
	}
	return featured;
}

Items get_all_featured_items( const Stores& stores ) 
{
	auto all_featured = Items();
	for( const auto& store : stores ) {
		const auto featured_in_store = get_featured_items_for_store( *store );
		all_featured.reserve( all_featured.size() + featured_in_store.size() );
		std::copy( std::begin(featured_in_store), std::end(featured_in_store), std::back_inserter(all_featured));
	}

	return all_featured;
}

void order_items_by_date_added( Items& items )
{
	auto date_comparator = [] ( const auto& left, const auto &right) {
		return left->date_added > right->date_added;
	};
	std::sort( std::begin(items), std::end(items), date_comparator );
}

// C++20 template lambda
void order_items_by_date_added_templated_lambda( Items& items )
{
	auto date_comparator = []<typename T>( const T& left, const T& right) {
		return left->date_added > right->date_added;
	};
	std::sort( std::begin(items), std::end(items), date_comparator );
}

void render_item_gallery( const Items& items )
{
	std::copy(
		std::begin(items), std::end(items),
		std::ostream_iterator<const Item*>(std::cout, "\n"));
}

}


TEST(software_architecture_wit_cpp_ch5, item_gallery )
{
	using namespace gallery;

	{
		auto fav_stores = get_favorite_stores_for( get_current_customer_id());
		auto selected_items = get_all_featured_items( fav_stores );
		order_items_by_date_added( selected_items );
		render_item_gallery( selected_items );
	}

	{
		auto fav_stores = get_favorite_stores_for( get_current_customer_id());
		auto selected_items = get_all_featured_items( fav_stores );
		order_items_by_date_added_templated_lambda( selected_items );
		render_item_gallery( selected_items );
	}

}

