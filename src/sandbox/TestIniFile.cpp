#include "stdafx.h"

#include "Fixture.hpp"
#include <string>
#include "..\include\CoreSystem.h"
#include "..\include\CoreIni.hpp"

#include <fstream>

using namespace std;
using namespace enscape;
using namespace enscape::System;

class IniFileTest : public CppUnit::TestCase
{
public:
	IniFileTest(const string& name) : CppUnit::TestCase(name) {}
	~IniFileTest() {}
	
	ISystemFactory* m_Fab;

	void setUp()
	{
		ModuleManager* mm = Fixture::GetGlobal().GetModuleManager();
		mm->Load("Enscape.Core.System.dll");
		m_Fab = mm->CreateClass<ISystemFactory>("enscape::System::SystemFactory");
	}

	void tearDown() {}

	void testCreate()
	{
		{
			ofstream temp("temp.ini", ios_base::out);
			temp << "";
		}

		IIniFile::Ptr ini = m_Fab->CreateIniFile("temp.ini");
	}



	void testRead()
	{
		{
			ofstream temp("temp.ini", ios_base::out);
			temp << 
				"[Dummy]\n"
				"Foo=Hello\n"
				"Bar=123\n"
				"FooBar=3.141592\n"
				;
		}

		IIniFile::Ptr ini = m_Fab->CreateIniFile("temp.ini");
		assert(ini->ReadString("Dummy", "Foo", "World") == "Hello");
		assert(ini->ReadInt("Dummy", "Bar", 0) == 123);
		assert(ini->ReadDouble("Dummy", "FooBar", 0.0) == 3.141592);

		char fullPath[MAX_PATH];
		if (_fullpath(fullPath, "temp.ini", sizeof(fullPath)) == NULL)
		{
			assert(1);
		}
		assert(IniPointer::ReadString(fullPath, "Dummy", "Foo", "World") == "Hello");
		assert(IniPointer::ReadInt(fullPath, "Dummy", "Bar", 0) == 123);
		assert(IniPointer::ReadDouble(fullPath, "Dummy", "FooBar", 0.0) == 3.141592);
	}

	void testWriteRead()
	{
		{
			ofstream temp("temp.ini", ios_base::out);
			temp << "";
		}

		IIniFile::Ptr ini = m_Fab->CreateIniFile("temp.ini");
		ini->WriteString("Dummy", "Foo", "GoodMorning");
		ini->WriteInt("Dummy", "Bar", 9999);
		ini->WriteDouble("Dummy", "FooBar", 1.2345);

		assert(ini->ReadString("Dummy", "Foo", "World") == "GoodMorning");
		assert(ini->ReadInt("Dummy", "Bar", 0) == 9999);
		assert(ini->ReadDouble("Dummy", "FooBar", 0.0) == 1.2345);

		char fullPath[MAX_PATH];
		if (_fullpath(fullPath, "temp2.ini", sizeof(fullPath)) == NULL)
		{
			assert(1);
		}
		IniPointer::WriteString(fullPath, "Dummy", "Foo", "GoodMorning");
		IniPointer::WriteInt(fullPath, "Dummy", "Bar", 9999);
		IniPointer::WriteDouble(fullPath, "Dummy", "FooBar", 1.2345);

		assert(IniPointer::ReadString(fullPath, "Dummy", "Foo", "World") == "GoodMorning");
		assert(IniPointer::ReadInt(fullPath, "Dummy", "Bar", 0) == 9999);
		assert(IniPointer::ReadDouble(fullPath, "Dummy", "FooBar", 0.0) == 1.2345);

	}

	void testDefaultValue()
	{
		{
			ofstream temp("temp.ini", ios_base::out);
			temp << "";
		}

		IIniFile::Ptr ini = m_Fab->CreateIniFile("temp.ini");
		assert(ini->ReadString("Dummy", "Foo", "World") == "World");
		assert(ini->ReadInt("Dummy", "Bar", 1234) == 1234);
		assert(ini->ReadDouble("Dummy", "FooBar", 3.141592) == 3.141592);

		char fullPath[MAX_PATH];
		if (_fullpath(fullPath, "temp.ini", sizeof(fullPath)) == NULL)
		{
			assert(1);
		}
		assert(IniPointer::ReadString(fullPath, "Dummy", "Foo", "World") == "World");
		assert(IniPointer::ReadInt(fullPath, "Dummy", "Bar", 1234) == 1234);
		assert(IniPointer::ReadDouble(fullPath, "Dummy", "FooBar", 3.141592) == 3.141592);
	}

	static CppUnit::Test* CreateTest()
	{
		CppUnit::TestSuite* pSuite = new CppUnit::TestSuite("IniFileTest");
		
		CppUnit_addTest(pSuite, IniFileTest, testCreate);
		CppUnit_addTest(pSuite, IniFileTest, testRead);
		CppUnit_addTest(pSuite, IniFileTest, testWriteRead);
		CppUnit_addTest(pSuite, IniFileTest, testDefaultValue);

		return pSuite;
	}
};

AddTestCase<IniFileTest> s_IniFileTest;
