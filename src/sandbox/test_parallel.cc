#include <gtest/gtest.h>
#include <string>
#if 0   

#include <execution>

TEST(TestParallel, test1) 
{
// std::execution generate compile error unless you have tbb ( intel thread building block library installed )
// And the tbb is only utilize multi-core. No chance on SIMD I presume.
//
// In file included from /usr/include/c++/9/pstl/parallel_backend.h:14,
//                  from /usr/include/c++/9/pstl/algorithm_impl.h:25,
//                  from /usr/include/c++/9/pstl/glue_execution_defs.h:52,
//                  from /usr/include/c++/9/execution:32,
//                  from /home/sungwoo/depo/samplecpp/src/sandbox/test_parallel.cc:3:
// /usr/include/c++/9/pstl/parallel_backend_tbb.h:19:10: fatal error: tbb/blocked_range.h: No such file or directory

    std::vector<float> vx{ 1.0f, 2.0f, 3.0f, 4.0f };
    std::vector<float> vy{ x.size() };
    
    std::transform( std::execution::seq, vx.begin(), vx.end(), vy.begin(), 
        [](float x) { return x * 2.0f; });
}

#endif
