#include <gtest/gtest.h>
#include <fstream>
#include <Poco/SharedMemory.h>
#include <Poco/Stopwatch.h>
#include <Poco/Format.h>
#include <list>
#include <iostream>

using namespace std;
using namespace Poco;

const int MB = 1024 * 1024;
const int GB = 1024 * MB;

TEST(SharedMemoryTest, testFill) 
{
	GTEST_SKIP_("Fail at Gitlab runner");

	int quota = GB;
	SharedMemory sm("temp.dat", quota, SharedMemory::AM_WRITE);

	const int iteration = 4;
	Stopwatch w; w.start();
	for (int i = 0; i < iteration; ++i)
	{
		fill(sm.begin(), sm.end(), (char)i);
	}
	printf("SharedMemory fill %.3f GB/s", 
		(double)iteration * quota / GB / ((double)w.elapsed() / w.resolution()));
}

TEST(SharedMemoryTest, testCopy) 
{
	GTEST_SKIP_("Fail at Gitlab runner");

	try
	{
		int quota = GB;
		SharedMemory to("to.dat", quota, SharedMemory::AM_WRITE);
		SharedMemory from("from.dat", quota, SharedMemory::AM_WRITE);

		const int iteration = 4;
		Stopwatch w; w.start();
		for (int i = 0; i < iteration; ++i)
		{
			// memcpy(to.begin(), from.begin(), quota);
			copy( from.begin(), from.end(), to.begin() );
		}
		printf("SharedMemory copy %.3f GB/s",
			iteration * (double)quota / GB / ((double)w.elapsed() / w.resolution()));
	}
	catch (Poco::Exception& ex)
	{
		printf("Poco::Exception : %s", ex.message().c_str());
	}
}

TEST(SharedMemoryTest, testAllocation) 
{
	GTEST_SKIP_("Fail at Gitlab runner");

	list<SharedMemory> ml;
	int quota = GB;

	try
	{
		for (int i = 0; i < 4; ++i)
		{
			SharedMemory m(format("shared_%d.dat", i), quota, SharedMemory::AM_WRITE);
			// fill(m.begin(), m.end(), (char)i);
			ml.push_back(m);
		}
	}
	catch (Poco::Exception& ex)
	{
		printf("Poco::Exception : %s", ex.message().c_str());
	}

	printf("SharedMemory allocation up to %d GB/s", (int)(ml.size() * quota / GB));
}



#if 0
void testLockFree()
{
	list<SharedMemory> ml;
	int quota = 12*MB;
	int quotaCount = 100;

	SYSTEM_INFO sysInfo;
	GetSystemInfo(&sysInfo);

	SIZE_T oldMinWS, oldMaxWS;
	assertEqual(TRUE, GetProcessWorkingSetSize(GetCurrentProcess(), &oldMinWS, &oldMaxWS));

	SIZE_T minWS = (SIZE_T)(quota * (quotaCount*1.1));
	SIZE_T maxWS = (SIZE_T)(quota * (quotaCount*1.2));

	if (SetProcessWorkingSetSize(GetCurrentProcess(), minWS, maxWS) == FALSE)
	{
		printf("Skip testLockFree as memory is low\n");
	}

	try
	{
		Poco::Stopwatch watch; watch.start();
		for (int i = 0; i < quotaCount; ++i)
		{
			SharedMemory m(format("c:/shared/%d.dat", i), quota, SharedMemory::AM_WRITE);
			ml.push_back(m);

			if (VirtualLock(m.begin(), quota) == FALSE)
			{
				if (VirtualLock(m.begin(), quota) == FALSE)
				{
					throw Poco::Exception(Poco::format("Failed to allocate memory %d Bytes", quota));
				}
			}
			
			for (char* addr = m.begin(); addr < m.end(); addr += sysInfo.dwPageSize)
			{
				*addr = 0;
			}
		}

		printf("Elapsed time %.3f sec for locking %d shared memory ( %.1f GB)\n",
			(double)watch.elapsed() / watch.resolution(), 
			ml.size(), 
			ml.size() * (double)quota / GB );

		watch.restart();
		for (SharedMemory& m : ml)
		{
			assertEqual(TRUE, VirtualLock(m.begin(), quota));
			for (char* addr = m.begin(); addr < m.end(); addr += sysInfo.dwPageSize)
			{
				*addr = 0;
			}
		}
		printf("Elapsed time %.3f sec for second locking %d shared memory ( %.1f GB)\n",
			(double)watch.elapsed() / watch.resolution(),
			ml.size(),
			ml.size() * (double)quota / GB);


		for (SharedMemory& m : ml)
		{
			assertEqual(TRUE, VirtualUnlock(m.begin(), quota));
		}
	}
	catch (Poco::Exception& ex)
	{
		printf("Poco::Exception : %s", ex.message().c_str());
	}

	assertEqual(TRUE, SetProcessWorkingSetSize(GetCurrentProcess(), oldMinWS, oldMaxWS));
}

void testProcessWorkingSetSize()
{
	int quota = 1 * MB;

	SYSTEM_INFO sysInfo;
	GetSystemInfo(&sysInfo);

	SIZE_T oldMinWS, oldMaxWS;
	assertEqual(TRUE, GetProcessWorkingSetSize(GetCurrentProcess(), &oldMinWS, &oldMaxWS));


	SIZE_T minWS = quota * 100;
	SIZE_T maxWS = quota * 200;
	if (SetProcessWorkingSetSize(GetCurrentProcess(), minWS, maxWS) == FALSE)
	{
		printf("Skip testProcessWorkingSetSize as memory is low\n");
	}

	list<uint8_t*> allocated;
	try
	{
		int i = 0;
		for (;;)
		{
			uint8_t* addr = (uint8_t*)VirtualAlloc(NULL, quota, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
			assert(addr != NULL);

			if (VirtualLock(addr, quota) == TRUE)
			{
				allocated.push_back(addr);
				fill(addr, addr + quota, 0);
				cout << "+";
				continue;
			}
			else if (VirtualLock(addr, quota) == TRUE)
			{
				allocated.push_back(addr);
				fill(addr, addr + quota, 0);
				cout << "*";
				continue;
			}

			VirtualFree(addr, 0, MEM_RELEASE);
			break;
		}

		printf("%d x %.1fMB allocated and locked\n", allocated.size(), (double)quota / MB);

		for (uint8_t* addr : allocated)
		{
			assertEqual( TRUE, VirtualUnlock(addr, quota) );
			assertEqual( TRUE, VirtualFree(addr, 0, MEM_RELEASE));
		}

	}
	catch (Poco::Exception& ex)
	{
		printf("Poco::Exception : %s", ex.message().c_str());
	}

	assertEqual(TRUE, SetProcessWorkingSetSize(GetCurrentProcess(), oldMinWS, oldMaxWS));
}

#endif