#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <vector>
#include <complex>
#include <mutex>
#include <variant>

// From C++17 In Detail by Bartlomiej Filipek

using namespace std;

// used to print the currently active type
struct PrintVisitor
{
	void operator()(int i) { cout << "int: " << i << '\n'; }
	void operator()(float f) { cout << "float: " << f << '\n'; }
	void operator()(const string& s) { cout << "str: " << s << '\n'; }
};


TEST(cpp_17_in_detail_part2, variant_basic)
{
	variant<int, float, string> intFloatString;

	intFloatString = 42;

	static_assert(variant_size_v<decltype(intFloatString)> == 3);

	// default initialised to the first alternative, should be 0
	visit(PrintVisitor{}, intFloatString);

	// index will show the currently used 'type'
	cout << "index = " << intFloatString.index() << endl;
	intFloatString = 100.0f;
	cout << "index = " << intFloatString.index() << endl;
	intFloatString = "hello super world";
	cout << "index = " << intFloatString.index() << endl;

	// try with get_if:
	if (const auto intPtr = get_if<int>(&intFloatString))
		cout << "int: " << *intPtr << '\n';
	else if (const auto floatPtr = get_if<float>(&intFloatString))
		cout << "float: " << *floatPtr << '\n';

	if (holds_alternative<int>(intFloatString))
		cout << "the variant holds an int!\n";
	else if (holds_alternative<float>(intFloatString))
		cout << "the variant holds a float\n";
	else if (holds_alternative<string>(intFloatString))
		cout << "the variant holds a string\n";
	// try/catch and bad_variant_access
	try
	{
		auto f = get<float>(intFloatString);
		cout << "float! " << f << '\n';
	}
	catch (bad_variant_access&)
	{
		cout << "our variant doesn't hold float at this moment...\n";
	}
}


class NotSimple
{
public:
	NotSimple(int, float) { }
};

TEST(cpp_17_in_detail_part2, variant_creation)
{
	// default initialisation: (the first type has to have a default ctor)
	std::variant<int, float> intFloat;
	std::cout << intFloat.index() << ", val: " << std::get<int>(intFloat) << '\n';

	// std::variant<NotSimple, int> cannotInit; // error
	std::variant<std::monostate, NotSimple, int> okInit;
	std::cout << okInit.index() << '\n';
	// pass a value:
	std::variant<int, float, std::string> intFloatString{ 10.5f };
	std::cout << intFloatString.index() << ", value " << std::get<float>(intFloatString) << '\n';

	// ambiguity
	// double might convert to float or int, so the compiler cannot decide
	//std::variant<int, float, std::string> intFloatString { 10.5 };
	// ambiguity resolved by in_place
	variant<long, float, std::string> longFloatString{
		std::in_place_index<1>, 7.6 // double!
	};
	std::cout << longFloatString.index() << ", value "
		<< std::get<float>(longFloatString) << '\n';

	// in_place for complex types
	std::variant<std::vector<int>, std::string> vecStr{
		std::in_place_index<0>, { 0, 1, 2, 3 }
	};
	std::cout << vecStr.index() << ", vector size "
		<< std::get<std::vector<int>>(vecStr).size() << '\n';

	// copy-initialize from other variant:
	std::variant<int, float> intFloatSecond{ intFloat };
	std::cout << intFloatSecond.index() << ", value "
		<< std::get<int>(intFloatSecond) << '\n';
}

TEST(cpp_17_in_detail_part2, variant_in_place)
{
	std::variant<int,float> if1 { std::in_place_index<0>, 10.5f};
	std::variant<int,float> if2 { std::in_place_type<int>, 10.5f};
	std::variant<std::vector<int>,std::string> vecStr {
		std::in_place_index<0>, { 0, 1, 2, 3}
	};

	std::variant<int, float, std::string> intFloatString { "Hello" };
	intFloatString = 10; // we're now an int
	intFloatString.emplace<2>(std::string("Hello")); // we're now string again
	// std::get returns a reference, so you can change the value:
	std::get<std::string>(intFloatString) += std::string(" World");
	intFloatString = 10.1f;
	if (auto pFloat = std::get_if<float>(&intFloatString); pFloat)
		*pFloat *= 2.0f;
}

TEST(cpp_17_in_detail_part2, variant_object_lifetime )
{
	std::variant<std::string, int> v { "Hello A Quite Long String" };
	// v allocates some memory for the string
	v = 10; // we call destructor for the string!
	// no memory leak
}

class MyType
{
public:
	MyType() { std::cout << "MyType::MyType\n"; }
	~MyType() { std::cout << "MyType::~MyType\n"; }
};

class OtherType
{
public:
	OtherType() { std::cout << "OtherType::OtherType\n"; }
    OtherType(const OtherType&) { 
        std::cout << "OtherType::OtherType(const OtherType&)\n"; 
    }	
	~OtherType() { std::cout << "OtherType::~OtherType\n"; }
};

TEST(cpp_17_in_detail_part2, variant_object_lifetime_with_class )
{
	{
		std::variant<MyType, OtherType> v;
		v = OtherType();
	}
	{
		std::variant<MyType, OtherType> v;
		v.emplace<OtherType>( OtherType());
	}
}

TEST(cpp_17_in_detail_part2, variant_accessing_stored_value )
{
	std::variant<int, float, std::string> intFloatString;
	try
	{
		auto f = std::get<float>(intFloatString);
		std::cout << "float! " << f << '\n';
	}
	catch (std::bad_variant_access&)
	{
		std::cout << "our variant doesn't hold float at this moment...\n";
	}

	if (const auto intPtr = std::get_if<0>(&intFloatString))
		std::cout << "int!" << *intPtr << '\n';	
}

struct MultiplyVisitor
{
	float mFactor;
	MultiplyVisitor(float factor) : mFactor(factor) { }
	void operator()(int& i) const {  i *= static_cast<int>(mFactor); }
	void operator()(float& f) const {  f *= mFactor; }
	void operator()(std::string&) const {}
};

TEST(cpp_17_in_detail_part2, variant_visitor )
{
	auto PrintVisitor = []( const auto& t) { std::cout << t << '\n'; };
	auto TwiceMoreVisitor = [](auto& t) { t *= 2; };

	std::variant< int, float> intFloat { 20.4f };
	std::visit( PrintVisitor, intFloat );
	std::visit( TwiceMoreVisitor, intFloat );
	std::visit( PrintVisitor, intFloat );
	std::visit( MultiplyVisitor(1.5f), intFloat );
	std::visit( PrintVisitor, intFloat );
}

template<class... Ts> 
struct overload : Ts...
{
	using Ts::operator()...;
};

template<class... Ts> overload(Ts...) -> overload<Ts...>;

TEST(cpp_17_in_detail_part2, variant_overload )
{
	std::variant<int,float,std::string> intFloatString( "Hello");
	std::visit( overload {
		[](int& i) { i *= 2; },
		[](float& f) { f *= 2.0f; },
		[](std::string& s) { s = s + s; },
	}, intFloatString );
}

struct Pizza { };
struct Chocolate { };
struct Salami { };
struct IceCream { };

TEST(cpp_17_in_detail_part2, variant_overload_default )
{
	std::variant<Pizza, Chocolate, Salami, IceCream> firstIngredient{ IceCream() };
	std::variant<Pizza, Chocolate, Salami, IceCream> secondIngredient{ Chocolate() };
	std::visit(overload{
		[](const Pizza& p, const Salami& s) {
			std::cout << "here you have, Pizza with Salami!\n";
		},
		[](const Salami& s, const Pizza& p) {
			std::cout << "here you have, Pizza with Salami!\n";
		},
		[](const Chocolate& c, const IceCream& i) {
			std::cout << "Chocolate with IceCream!\n";
		},
		[](const IceCream& i, const Chocolate& c) {
			std::cout << "IceCream with a bit of Chocolate!\n";
		},
		[](const auto& a, const auto& b) {
			std::cout << "invalid composition...\n";
		},
	}, firstIngredient, secondIngredient);
}

class ThrowingClass
{
public:
	explicit ThrowingClass(int i) { if (i == 0) throw int(10); }
	operator int() { throw int(10); }
};

TEST(cpp_17_in_detail_part2, variant_valueless )
{

	std::variant<int, ThrowingClass> v;
	// change the value:
	try
	{
		v = ThrowingClass(0);
	}
	catch (...)
	{
		std::cout << "catch(...)\n";
		// we keep the old state!
		std::cout << v.valueless_by_exception() << '\n';
		std::cout << std::get<int>(v) << '\n';
	}
	// inside emplace
	try
	{
		v.emplace<0>(ThrowingClass(10)); // calls the operator int
	}
	catch (...)
	{
		std::cout << "catch(...)\n";
		// the old state was destroyed, so we're not in invalid state!
		std::cout << v.valueless_by_exception() << '\n';
	}
}


TEST(cpp_17_in_detail_part2, variant_size )
{
	std::cout << "sizeof string: " << sizeof(std::string) << '\n';

	std::cout << "sizeof variant<int, string>: " << sizeof(std::variant<int, std::string>) << '\n';

	std::cout << "sizeof variant<int, float>: " << sizeof(std::variant<int, float>) << '\n';

	std::cout << "sizeof variant<int, double>: " << sizeof(std::variant<int, double>) << '\n';
}

struct DoorState
{
	struct DoorOpened {};
	struct DoorClosed {};
	struct DoorLocked {};
	using State = std::variant<DoorOpened, DoorClosed, DoorLocked>;

	struct OpenEvent
	{
		State operator()(const DoorOpened&) { return DoorOpened(); }
		State operator()(const DoorClosed&) { return DoorOpened(); }
		// cannot open locked doors
		State operator()(const DoorLocked&) { return DoorLocked(); }
	};
	struct CloseEvent
	{
		State operator()(const DoorOpened&) { return DoorClosed(); }
		State operator()(const DoorClosed&) { return DoorClosed(); }
		State operator()(const DoorLocked&) { return DoorLocked(); }
	};
	struct LockEvent
	{
		// cannot lock opened doors
		State operator()(const DoorOpened&) { return DoorOpened(); }
		State operator()(const DoorClosed&) { return DoorLocked(); }
		State operator()(const DoorLocked&) { return DoorLocked(); }
	};
	struct UnlockEvent
	{
		// cannot unlock opened doors
		State operator()(const DoorOpened&) { return DoorOpened(); }
		State operator()(const DoorClosed&) { return DoorClosed(); }
		// unlock
		State operator()(const DoorLocked&) { return DoorClosed(); }
	};

	void open()
	{
		state = std::visit(OpenEvent{}, state);
	}
	void close()
	{
		state = std::visit(CloseEvent{}, state);
	}
	void lock()
	{
		state = std::visit(LockEvent{}, state);
	}
	void unlock()
	{
		state = std::visit(UnlockEvent{}, state);
	}
	State state;
};


TEST(cpp_17_in_detail_part2, variant_door )
{
	DoorState door;
	EXPECT_TRUE( std::holds_alternative<DoorState::DoorOpened>(door.state) );

	door.open();
	EXPECT_TRUE( std::holds_alternative<DoorState::DoorOpened>(door.state) );

	door.close();
	EXPECT_TRUE( std::holds_alternative<DoorState::DoorClosed>(door.state) );

	door.lock();
	EXPECT_TRUE( std::holds_alternative<DoorState::DoorLocked>(door.state) );

	door.lock();
	EXPECT_TRUE( std::holds_alternative<DoorState::DoorLocked>(door.state) );

	door.open();
	EXPECT_TRUE( std::holds_alternative<DoorState::DoorLocked>(door.state) );

	door.unlock();
	EXPECT_TRUE( std::holds_alternative<DoorState::DoorClosed>(door.state) );

	door.open();
	EXPECT_TRUE( std::holds_alternative<DoorState::DoorOpened>(door.state) );
}