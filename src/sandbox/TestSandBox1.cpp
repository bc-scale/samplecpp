#include <string>
#include <iostream>
#include <thread>
#include <future>
#include <chrono>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <gtest/gtest.h>
#include <Poco/Format.h>

using namespace std;
using namespace Poco;

class TestBench
{
public:
	mutex m_LockImageQueue;
	condition_variable m_ImageQueueSignal;
	queue<int> m_ImageQueue;


	void Grab( int grabCount )
	{
		while (grabCount-- > 0)
		{
			this_thread::sleep_for(chrono::milliseconds(30));

			lock_guard<mutex> lock(m_LockImageQueue);
			m_ImageQueue.push(grabCount);
			m_ImageQueueSignal.notify_one();
			printf("Grabbed %d\n", grabCount );
		}
	}

	void Inspect( int id )
	{
		for (;;)
		{
			int value = 0;

			{
				unique_lock<mutex> lock(m_LockImageQueue);
				if (!m_ImageQueueSignal.wait_for(lock, chrono::milliseconds(200), [=] { return !m_ImageQueue.empty(); }))
					return;

				value = m_ImageQueue.front();
				m_ImageQueue.pop();
			}

			this_thread::sleep_for(chrono::milliseconds(80));
			printf("Thread ID %d inspected %d\n", id, value);
		}
	}
};

TEST(SandBox1, test1) 
{
	TestBench tb;
	thread grabber(&TestBench::Grab, &tb, 10);

	vector<thread> inspectors;
	inspectors.push_back(thread(&TestBench::Inspect, &tb, 0));
	inspectors.push_back(thread(&TestBench::Inspect, &tb, 1));
	inspectors.push_back(thread(&TestBench::Inspect, &tb, 2));

	grabber.join();
	for (auto& t : inspectors) { t.join(); }
}

TEST(SandBox1, testPocoFormat)
{
	uint8_t v = 0xAB;
	string a;
	a = Poco::format("%#x", (uint32_t)v );
}
