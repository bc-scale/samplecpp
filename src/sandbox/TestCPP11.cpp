#include "stdafx.h"

#include "Fixture.hpp"
#include "Poco/Format.h"
#include "Poco/Crypto/DigestEngine.h"
#include <string>

using namespace std;
using namespace Poco;

int TestBar(char a)
{
	return a + 42;
}

int TestToo(char a)
{
	return a + 22;
}

int AddAndMultiply(int a, int b, int& mult)
{
	mult = a * b;
	return a + b;   
}

int g_mult = 0;
int AddAndMultiplyV2(int a, int b)
{
	g_mult = a * b;
	return a + b;
}

int AddAndMultiplyV3(int a, int b, int* mult)
{
	*mult = a * b;
	return a + b;
}

struct AddMult
{
	int Add;
	int Mult;
};

AddMult AddAndMultiplyV3(int a, int b)
{
	AddMult r;
	r.Add = a + b;
	r.Mult = a * b;
	return r;
}

bool TryAdd(int a, int b, int& result)
{
	if (a > 1000000)
		return false;

	result = a + b;
	return true;
}

struct AddData
{
	int A;
	int B;

	mutable int C = 0;

	int AddAll() const
	{
		if (C == 0)
		{
			C = A + B;
		}

		return C;
	}
};

class CPP11Test : public CppUnit::TestCase
{
public:
	CPP11Test(const string& name) : CppUnit::TestCase(name) {}
	~CPP11Test() {}

	void setUp() {}
	void tearDown() {}

	uint32_t DoNotOptimize(uint32_t* pa)
	{
		return *pa;
	}

	struct Foo
	{
		uint32_t a;
		char b;
		uint16_t c;
		bool d;
		const char * e;
	};



	void testPointer0()
	{
		{
			int a = 1234;
			int *pa = &a;
			*pa = 1111;

			int &ra = a;
			ra = 4321;

			int b = 2222;
				
			int mult = 0;
			int sum = AddAndMultiply(1, 2, mult);
			sum = AddAndMultiplyV3(1, 2, &mult);

			AddMult r = AddAndMultiplyV3(1, 2);
			sum = r.Add;
			mult = r.Mult;

			sum = AddAndMultiplyV2(1, 2);
			mult = g_mult;


			const int* cpa = &a;
			// *cpa = 42;
			cpa = &b;

			const int& cra = a;
			// cra = 42;

			int* const pca = &a;
			*pca = 42;
			// *pca = &b;
			
			cpa = pca;
		}

		{
			uint32_t *pa = new uint32_t[4];
			pa[0] = 0x12345678;
			pa[1] = 0x87654321;
			pa[2] = 0x10101010;
			pa[3] = 0xCAFEBEEF;

			*pa = 0xFFFFFFFF;
			*(pa + 1) = 0xEEEEEEEE;

			delete[] pa;
		}

		{
			int a = 1;
			int b = 2;
			int c = a + b;
			int d = a * b;
			c = d;
			a = TestToo('a');
			b = TestBar('b');
		}

		{
			Foo foo;
			Foo* pfoo = &foo;
			foo = { 0x12345678, 'a', 0xCAFE, true, nullptr };
			pfoo->a = 321;
			pfoo->b = 'b';
			pfoo->c = 0xBEEF;
			pfoo->d = false;
			pfoo->e = "Hello World";

			Foo bar = foo;
			bar.a = 0x11223344;
		}

		{
			typedef int(*FooFn)(char);
			FooFn fn = TestBar;
			int ret = fn('a');
			fn = TestToo;
			ret = (*fn)('b');
		}

		{
			using FooFn = std::function<int(char)>;
			FooFn fn = TestBar;
			int ret = fn('a');
			fn = TestToo;
			ret = fn('b');
		}
	}

	void testInitializer0()
	{
		struct Foo {
			int Bar; string Too;
		};

		Foo a = { 1234, "Hello" };
		// Foo b( 1234, "Hello" );   // compile error

		vector<Foo> c = {
			{ 1234, "Hello" },
			{ 4321, "World" },
			{ 1111, "Good Morning" }
		};
	}

	void testInitializer1()
	{
		struct Foo {
			int Bar; string Too;
			Foo(int bar) : Bar(bar) {}
		};

		// Foo a = { 1234, "Hello" };   // compile error
		Foo b{ 1234 };   
		Foo c(1234);
	}
	
	void testInitializer2()
	{
		struct Foo {
			int sum(const std::initializer_list<int>& list) {
				int total = 0;
				for (auto& e : list) {
					total += e;
				}

				return total;
			}
		};
		Foo foo;

		auto list = { 1, 2, 3 };
		assert(foo.sum(list) == 6);
		assert(foo.sum({ 1, 2, 3 }) == 6);
		assert(foo.sum({}) == 0);
	}

	void testLvalueRvalue0()
	{
		int a = 42;
		int b = 43;

		// a and b are both l-values:
		a = b; // ok
		b = a; // ok
		a = a * b; // ok

		// a * b is an rvalue:
		int c = a * b; // ok, rvalue on right hand side of assignment
		// a * b = 42; // error C2106: '=' : left operand must be l-value
	}

	int someValue = { 1212 };
	int& foo() { return someValue; }
	int foobar() { return someValue; }

	void testLvalueRvalue1()
	{
		// lvalues:
		int i = 42;
		i = 43; // ok, i is an lvalue
		int* p = &i; // ok, i is an lvalue
		foo() = 42; // ok, foo() is an lvalue
		int* p1 = &foo(); // ok, foo() is an lvalue

		// rvalues:
		int j = 0;
		j = foobar(); // ok, foobar() is an rvalue
//		int* p2 = &foobar(); // error C2102: '&' requires l-value
		j = 1234; // ok, 42 is an rvalue
	}


	struct FooA { int X = 1234; };
	FooA RunFooA(const FooA& a) { FooA temp = a; temp.X = 4321; return temp; }
	FooA RunFooA(FooA&& a) { a.X = 4321; return a; }

	void testRvalueReference1()
	{
		FooA a;
		FooA b = RunFooA(a);
		FooA c = RunFooA(FooA());
		FooA d = RunFooA(std::move(a));
	}

	void testMove()
	{
		std::unique_ptr<int> p1{ new int };
//		std::unique_ptr<int> p2 = p1; // error -- cannot copy unique pointers
		std::unique_ptr<int> p3 = std::move(p1); // move `p1` into `p2`
												 // now unsafe to dereference object held by `p1`
	}


	void testAuto0()
	{
		auto a = 3.14; // double
		auto b = 1; // int
		auto& c = b; // int&
		auto d = { 0 }; // std::initializer_list<int>
		auto&& e = 1; // int&&
		auto&& f = b; // int&
		auto g = new auto(123); // int*
		delete g;
		const auto h = 1; // const int
		auto i = 1, j = 2, k = 3; // int, int, int
		// auto l = 1, m = true, n = 1.61; // error -- `l` deduced to be int, `m` is bool
		// auto o; // error -- `o` requires initializer
	}

	void testAuto1()
	{
		std::vector<int> v = { 1, 2, 3, 4 };
		std::vector<int>::const_iterator cit1 = v.cbegin();
		auto cit2 = v.cbegin();
	}

	template <typename X, typename Y>
	auto AddAnyTwo(X x, Y y) -> decltype(x + y) {
		return x + y;
	}

	void testAuto2()
	{
		assert(AddAnyTwo(1, 2) == 3);
		assert(AddAnyTwo(1, 2.0) == 3.0);
		assert(AddAnyTwo(1.5, 1.5) == 3.0);
	}

	int LBar = 1;

	void testLambda0()
	{
		auto getLBar = [=]{ return LBar; };
		assert(getLBar() == 1);

		auto addLBar = [=](int y) { return LBar + y; };
		assert(addLBar(1) == 2);

		auto getLBarRef = [&]() -> int& { return LBar; };
		getLBarRef() = 2;
		assert(LBar == 2);
	}

	void testLambda1()
	{
		int x = 1;
		auto f1 = [&x] { x = 2; }; // OK: x is a reference and modifies the original
		f1();
		// auto f2 = [x] { x = 2; }; // ERROR: the lambda can only perform const-operations on the captured value

		auto f3 = [x]() mutable { x = 2; }; // OK: the lambda can perform any operations on the captured value
		f3();
	}

	template <typename T> using Vec = std::vector<T>;

	void testAlias()
	{
		Vec<int> v{}; // std::vector<int>

		using String = std::string;
		String s{ "foo" };
	}

	void testNullPtr()
	{
		struct Foo
		{
			void Bar(int) {}
			void Bar(char*) {}
		};

		Foo foo;
		foo.Bar(NULL);		// should throw error but not with VS2013
		foo.Bar(nullptr);
	}

	void testStrongTypedEnum0()
	{
		enum Week { Monday = 0, Tuesday, Thursday };
		enum Month { January = 0, Feburary,  };

		Month m = January;

		if (m == Monday)
		{
			// danger !!!
			m = Feburary;
		}
	}

	void testStrongTypedEnum1()
	{
		// Specifying underlying type as `unsigned int`
		enum class Color : unsigned int { Red = 0xff0000, Green = 0xff00, Blue = 0xff };
		
		// `Red`/`Green` in `Alert` don't conflict with `Color`
		enum class Alert : bool { Red = false, Green };
		
		Color redColor = Color::Red;
		Alert redAlert = Alert::Red;

		// bool isSame = (redColor == redAlert);	
		// error C2678: binary '==' : no operator found which takes a left-hand operand of type 'CPPTest::testStrongTypedEnum::Color' 
		// (or there is no acceptable conversion)
	}

	void testDelegatingConstructor()
	{
		struct Foo {
			int foo;
			Foo(int foo) : foo(foo) {}
			Foo() : Foo(1) {}
		};

		Foo foo{};
		assert( foo.foo == 1 );
	}

#if 0
	void testUserDefinedLiterals()
	{
		struct Foo {
			long long operator "" _celsius(unsigned long long tempCelsius) {
				return std::llround(tempCelsius * 1.8 + 32);
			}

			void Bar()
			{
				return 24_celsius;
			}
		};

		Foo foo;
		foo.Bar();
	}
#endif

	void testOverride()
	{
		struct A {
			virtual void foo() {}
			void bar() {}
		};

		struct B : A {
			void foo() override {};  // correct -- B::foo overrides A::foo
			// void bar() override {}; // error C3668 : 'CPPTest::testOverrides::B::bar' : method with override specifier 'override' did not override any base class methods
			// void baz() override {}; // error C3668 : 'CPPTest::testOverrides::B::baz' : method with override specifier 'override' did not override any base class methods
		};
	}

	void testFinal0()
	{
		struct A {
			virtual void foo() {}
		};

		struct B : A {
			virtual void foo() final {}
		};

		struct C : B {
			// virtual void foo() {} // error C3248: 'CPPTest::testFinal::B::foo': function declared as 'final' cannot be overridden by 'CPPTest::testFinal::C::foo'
		};
	}

	void testFinal1()
	{
		struct A final {};

		// error C3246: 'CPPTest::testFinal1::B' : cannot inherit from 'CPPTest::testFinal1::A' as it has been declared as 'final'
		// struct B : A {};
	}

	
// warning C4822: 'CPPTest::testDeletedFunction::A::A' : local class member function does not have a body
#pragma warning( push )
#pragma warning( disable : 4822)

	void testDeletedFunction()
	{
		class A {
			int x;

		public:
			A(int x) : x(x) {};
			A(const A&) = delete;
			A& operator=(const A&) = delete;
		};

		A x{ 123 };
		// A y = x; // error C2280: 'CPPTest::testDeletedFunction::A::testDeletedFunction::A(const CPPTest::testDeletedFunction::A &)' : attempting to reference a deleted function
		// y = x; // error -- operator= deleted
	}

#pragma warning( pop )

	void testRangeBasedLoop()
	{
		std::vector<int> a{ 1, 2, 3, 4, 5 };
		for (int& x : a) 
		{ 
			x *= 2; 
		}
		// a == { 2, 4, 6, 8, 10 }

		for (int x : a) 
		{
			x *= 2;
		}
		// a == { 2, 4, 6, 8, 10 }
	}

	void testToString()
	{
		assertEqual("123", std::to_string(123));
		assertEqual("1.200000", std::to_string(1.2));
	}

	void testUniquePointer0()
	{
		{
			std::unique_ptr<int> p1{ new int };
			*p1 = 1234;
			assert(*p1 == 1234);
		}

		{
		int *p1 = new int;
		*p1 = 1234;
		assert(*p1 == 1234);
		delete p1;
	}
	}

	void testUniquePointer1()
	{
		struct Foo { int Bar; string Too; };
		std::unique_ptr<Foo> p1{ new Foo };
		p1->Bar = 1234;
		p1->Too = "hello";

		assert(p1->Bar == 1234);
		assert(p1->Too == "hello");
	}

	void testUniquePointer2()
	{
		struct Foo {
			Foo(int bar, string too) : Bar(bar), Too(too){}
			int Bar; string Too;
		};
		std::unique_ptr<Foo> p1;

		if (p1 == nullptr)
		{
			// p1 = new Foo;  // compile error 
			p1 = std::unique_ptr<Foo>(new Foo(1234, "Hello"));
			p1 = std::make_unique<Foo>(1234, "Hello");
		}
	}

	void testUniquePointer3()
	{
		struct Foo {
			Foo(int bar, string too) : Bar(bar), Too(too){}
			int Bar; string Too;
		};
		vector<std::unique_ptr<Foo>> Foos;
		Foos.push_back(std::unique_ptr<Foo>(new Foo(1234, "Hello")));
		Foos.push_back(std::make_unique<Foo>(4321, "World"));
		Foos.push_back(std::make_unique<Foo>(1111, "Good Morning"));
	}

	void testSharedPtr0()
	{
		struct Foo { int Bar = 1234; string Too = "Hello"; };
		std::shared_ptr<Foo> p1{ new Foo };
		p1->Bar = 4321;

		std::shared_ptr<Foo> p2 = p1;
		p2->Too = "World";

		assertEqual("World", p1->Too);

		auto p3 = make_shared<Foo>();
		assertEqual(1234, p3->Bar);

		p1 = p3;
	}

	void testSharedPtr1()
	{
		struct Foo { int Bar = 1234; string Too = "Hello"; };
		std::unique_ptr<Foo> p1( new Foo );

		// std::shared_ptr<Foo> p2 = p1; 
		// error C2440 : 'initializing' : cannot convert from 'std::unique_ptr<CPPTest::testSharedPtr1::Foo,std::default_delete<_Ty>>' 
		// to 'std::shared_ptr<CPPTest::testSharedPtr1::Foo>'

		std::shared_ptr<Foo> p3 = std::move(p1); 

		// p1 is dangling now
	}

#if 0
	void testChrono()
	{
		std::chrono::time_point<std::chrono::system_clock> start, end;
		start = std::chrono::system_clock::now();
		// Some computations...
		end = std::chrono::system_clock::now();

		std::chrono::duration<double> elapsed_seconds = end - start;

		elapsed_seconds.count(); // t number of seconds, represented as a `double`
	}
#endif

	void testTuples()
	{
		// `playerProfile` has type `std::tuple<int, std::string, std::string>`.
		auto playerProfile = std::make_tuple(51, "Frans Nielsen", "NYI");
		assertEqual( 51, std::get<0>(playerProfile)); 
		assertEqual( "Frans Nielsen", std::get<1>(playerProfile));
		assertEqual( "NYI", std::get<2>(playerProfile)); 
	}

	void testTie()
	{
		// With tuples...
		std::string playerName;
		std::tie(std::ignore, playerName, std::ignore) = std::make_tuple(91, "John Tavares", "NYI");
		assertEqual("John Tavares", playerName);

		// With pairs...
		std::string yes, no;
		std::tie(yes, no) = std::make_pair("yes", "no");
		assertEqual("yes", yes);
		assertEqual("no", no);
	}

#if 0
	void testArray()
	{
		std::array<int, 3> a = { 2, 1, 3 };
		std::sort(a.begin(), a.end()); // a == { 1, 2, 3 }
		for (int& x : a) x *= 2; // a == { 2, 4, 6 }
	}
#endif

	class Character {
	public :
		typedef Poco::SharedPtr<Character> Ptr;
		virtual ~Character() {}

		Character(const std::string& name) { Name = name; }
		void Chase(Character::Ptr a) { ToChase = a; }
		void Backstab(Character::Ptr a) { ToBackstab = a; }
		void Bomb() {};
		void Play()
		{
			if (!ToChase.isNull()) ToChase->Bomb();
			if (!ToBackstab.isNull()) ToBackstab->Bomb();
		}

	private:
		std::string Name;
		Character::Ptr ToChase;
		Character::Ptr ToBackstab;
	};

	void testMemoryLeak()
	{
		Character::Ptr tom(new Character("Tom"));
		Character::Ptr jerry(new Character("Jerry"));

		tom->Chase(jerry);
		// jerry->Backstab(tom);

		tom->Play();
		jerry->Play();
	}


	class LambdaCharacter {
	public:
		typedef Poco::SharedPtr<LambdaCharacter> Ptr;
		virtual ~LambdaCharacter() {}

		LambdaCharacter(const std::string& name) { Name = name; }

		using OnChaseFn = std::function<void()>;
		void SetOnChase(OnChaseFn fn) { OnChase = fn; }
		void Burn() { };
		void Play() { 
			if( OnChase != nullptr) OnChase();
		}

	private:
		std::string Name;
		OnChaseFn OnChase;
	};

	void testMemoryLeakByFn()
	{
		LambdaCharacter::Ptr tom(new LambdaCharacter("Tom"));
		LambdaCharacter::Ptr jerry(new LambdaCharacter("Jerry"));

		tom->SetOnChase([jerry]() mutable {
			jerry->Burn();
		});

//		jerry->SetOnChase([tom]() mutable {
//			tom->Burn();
//		});

		tom->Play();
		jerry->Play();
	}


	class SCharacter {
	public:
		typedef std::shared_ptr<SCharacter> Ptr;
		virtual ~SCharacter() {}

		SCharacter(const std::string& name) { Name = name; }
		void Chase(SCharacter::Ptr a) { ToChase = a; }
		void Backstab(SCharacter::Ptr a) { ToBackstab = a; }
		void Bomb() {};
		void Play()
		{
			if (!ToChase.expired()) ToChase.lock()->Bomb();
			if (!ToBackstab.expired()) ToBackstab.lock()->Bomb();
		}

	private:
		std::string Name;
		typedef std::weak_ptr<SCharacter> WeakPtr;
		SCharacter::WeakPtr ToChase;
		SCharacter::WeakPtr ToBackstab;
	};

	void testNoCircularDependency()
	{
		SCharacter::Ptr tom(new SCharacter("Tom"));
		SCharacter::Ptr jerry(new SCharacter("Jerry"));

		tom->Chase(jerry);
		jerry->Backstab(tom);

		tom->Play();
		jerry->Play();
	}


	static CppUnit::Test* CreateTest()
	{
		CppUnit::TestSuite* pSuite = new CppUnit::TestSuite("CPPTest");

		CppUnit_addTest(pSuite, CPP11Test, testPointer0);

		CppUnit_addTest(pSuite, CPP11Test, testMemoryLeakByFn);
		CppUnit_addTest(pSuite, CPP11Test, testNoCircularDependency);
		CppUnit_addTest(pSuite, CPP11Test, testMemoryLeak);

		CppUnit_addTest(pSuite, CPP11Test, testInitializer0);
		CppUnit_addTest(pSuite, CPP11Test, testInitializer1);
		CppUnit_addTest(pSuite, CPP11Test, testInitializer2);

		CppUnit_addTest(pSuite, CPP11Test, testLvalueRvalue0);
		CppUnit_addTest(pSuite, CPP11Test, testLvalueRvalue1);

		CppUnit_addTest(pSuite, CPP11Test, testRvalueReference1);

		CppUnit_addTest(pSuite, CPP11Test, testMove);

		CppUnit_addTest(pSuite, CPP11Test, testAuto0);
		CppUnit_addTest(pSuite, CPP11Test, testAuto1);
		CppUnit_addTest(pSuite, CPP11Test, testAuto2);

		CppUnit_addTest(pSuite, CPP11Test, testLambda0);
		CppUnit_addTest(pSuite, CPP11Test, testLambda1);

		CppUnit_addTest(pSuite, CPP11Test, testAlias);

		CppUnit_addTest(pSuite, CPP11Test, testNullPtr);

		CppUnit_addTest(pSuite, CPP11Test, testStrongTypedEnum0);
		CppUnit_addTest(pSuite, CPP11Test, testStrongTypedEnum1);

		CppUnit_addTest(pSuite, CPP11Test, testDelegatingConstructor);

		CppUnit_addTest(pSuite, CPP11Test, testOverride);

		CppUnit_addTest(pSuite, CPP11Test, testFinal0);
		CppUnit_addTest(pSuite, CPP11Test, testFinal1);

		CppUnit_addTest(pSuite, CPP11Test, testDeletedFunction);

		CppUnit_addTest(pSuite, CPP11Test, testRangeBasedLoop);

		CppUnit_addTest(pSuite, CPP11Test, testToString);

		CppUnit_addTest(pSuite, CPP11Test, testUniquePointer0);
		CppUnit_addTest(pSuite, CPP11Test, testUniquePointer1);
		CppUnit_addTest(pSuite, CPP11Test, testUniquePointer2);
		CppUnit_addTest(pSuite, CPP11Test, testUniquePointer3);

		CppUnit_addTest(pSuite, CPP11Test, testSharedPtr0);
		CppUnit_addTest(pSuite, CPP11Test, testSharedPtr1);

		CppUnit_addTest(pSuite, CPP11Test, testTuples);

		CppUnit_addTest(pSuite, CPP11Test, testTie);
		return pSuite;
	}
};


static AddTestCase<CPP11Test> s_Test;
