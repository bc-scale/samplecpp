#include "stdafx.h"

#include "Fixture.hpp"
#include "log4cpp\Category.hh"
#include "log4cpp\PropertyConfigurator.hh"
#include "log4cpp\PatternLayout.hh"
#include "log4cpp\LayoutAppender.hh"
#include "log4cpp\AppendersFactory.hh"
#include <fstream>
#include <vector>
#include <functional>
#include <Poco/Timestamp.h>
#include <Poco/DateTimeFormatter.h>
#include <Poco/DateTimeFormat.h>

using namespace std;
using namespace log4cpp;
using namespace enscape;


class TestLog4Cpp : public CppUnit::TestCase
{
public:
	TestLog4Cpp(const string& name) : CppUnit::TestCase(name) {}
	~TestLog4Cpp() {}

	void setUp() {}
	void tearDown() {}

	void testConsoleLog()
	{
		{
			ofstream temp("temp.log.prop", ios_base::out);
			temp << R"(
				log4cpp.rootCategory=WARN,root
				log4cpp.appender.root=ConsoleAppender
				log4cpp.appender.root.layout=BasicLayout
			)";
		}
		PropertyConfigurator::configure("temp.log.prop");

		auto& root = log4cpp::Category::getRoot();
		for (int i = 0; i < 10; ++i)
		{
			root.debug("Foo debug %d", i);
			root.info("Foo %dd", i);
			root.warn("FooBar %d", i);
			//root.error("Bar %d", i);
			//root.alert("Too %d", i);
			//root.crit("ARRRRR !!!! %d", i);
		}
	}

	void testDebugView()
	{
		{
			ofstream temp("temp.log.prop", ios_base::out);
			temp << R"(
				log4cpp.rootCategory = WARN, root
				log4cpp.appender.root = Win32DebugAppender
				log4cpp.appender.root.layout = BasicLayout
			)";
		}
		PropertyConfigurator::configure("temp.log.prop");
			
		auto& root = log4cpp::Category::getRoot();
		for (int i = 0; i < 10; ++i)
		{
			root.error("Foo %d", i);
		}
	}

	void testFile()
	{
		{
			ofstream temp("temp.log.prop", ios_base::out);
			temp << R"(
				log4cpp.rootCategory=INFO,root
				log4cpp.appender.root=FileAppender
				log4cpp.appender.root.fileName=temp.log
				log4cpp.appender.root.layout=PatternLayout
				log4cpp.appender.root.layout.ConversionPattern=testFile : %d{%Y-%m-%d %H:%M:%S:%l}|%c|%p|%m%n
			)";
		}
		PropertyConfigurator::configure("temp.log.prop");

		auto& root = log4cpp::Category::getRoot();
		for (int i = 0; i < 10; ++i)
		{
			root.info("Foo %d", i);
		}
	}

	void testRollingFile()
	{
		{
			ofstream temp("temp.log.prop", ios_base::out);
			temp << R"(
				log4cpp.rootCategory=INFO,root
				log4cpp.appender.root=RollingFileAppender
				log4cpp.appender.root.layout=BasicLayout
				log4cpp.appender.root.maxFileSize=1024000
				log4cpp.appender.root.maxBackupIndex=10
			)";
		}
		PropertyConfigurator::configure("temp.log.prop");

		auto& root = log4cpp::Category::getRoot();
		for (int i = 0; i < 10; ++i)
		{
			root.info("Foo %d", i);
		}
	}

	void testSyslog()
	{
		{
			// https://en.wikipedia.org/wiki/Syslog
			ofstream temp("temp.log.prop", ios_base::out);
			temp << R"(
				log4cpp.rootCategory=INFO,root
				log4cpp.appender.root=SyslogAppender
				log4cpp.appender.root.syslogName=DoesntMatter
				log4cpp.appender.root.syslogHost=127.0.0.1
				log4cpp.appender.root.facility=1
				log4cpp.appender.root.portNumber=514
				log4cpp.appender.root.layout=PatternLayout
				log4cpp.appender.root.layout.ConversionPattern=MyMachine %d{%Y-%m-%dT%H:%M:%S.%l} %m%n
			)";
		}
		PropertyConfigurator::configure("temp.log.prop");

		auto& root = log4cpp::Category::getRoot();
		for (int i = 0; i < 2; ++i)
		{
			root.debug("Foo debug %d", i);
			root.info("Foo %dd", i);
			root.warn("FooBar %d", i);
			//root.error("Bar %d", i);
			//root.alert("Too %d", i);
			//root.crit("ARRRRR !!!! %d", i);
		}
	}

	void testCategory()
	{
		{
			ofstream temp("temp.log.prop", ios_base::out);
			temp << R"(
				log4cpp.rootCategory=INFO,root
				log4cpp.appender.root=ConsoleAppender
				log4cpp.appender.root.layout=BasicLayout

				log4cpp.category.sequence=INFO,seqfile
				log4cpp.appender.seqfile=FileAppender
				log4cpp.appender.seqfile.fileName=temp.seq.log
				log4cpp.appender.seqfile.layout=BasicLayout

				log4cpp.category.process=INFO,processfile
				log4cpp.appender.processfile=FileAppender
				log4cpp.appender.processfile.fileName=temp.proc.log
				log4cpp.appender.processfile.layout=BasicLayout
			)";
		}
		PropertyConfigurator::configure("temp.log.prop");

		auto& root = log4cpp::Category::getRoot();
		auto& seq = log4cpp::Category::getInstance("sequence");
		auto& proc = log4cpp::Category::getInstance("process");
		for (int i = 0; i < 10; ++i)
		{
			root.info("Foo %d", i);
			seq.info("sequence Foo %d", i);
			proc.info("process Foo %d", i);
		}
	}

	class CustomAppender : public log4cpp::LayoutAppender
	{
	private:
		using CallbackFn = std::function< void(Priority::Value, const string&) >;
		CallbackFn m_Callback;

	public:
		CustomAppender(const std::string& name) : log4cpp::LayoutAppender(name), m_Callback(nullptr) { }
		void SetCallBack(CallbackFn fn) { m_Callback = fn; }
		bool reopen() { return true; }
		void close() {}

	protected:
		void _append(const log4cpp::LoggingEvent& event)
		{
			std::string message(_getLayout().format(event));
			if (m_Callback) m_Callback(event.priority, message);
		}
	};

	void testCustomAppender()
	{
		{
			ofstream temp("temp.log.prop", ios_base::out);
			temp << R"(
				log4cpp.rootCategory=INFO,root
				log4cpp.appender.root=FileAppender
				log4cpp.appender.root.fileName=temp.log
				log4cpp.appender.root.layout=BasicLayout
			)";
		}
		PropertyConfigurator::configure("temp.log.prop");
		auto& root = log4cpp::Category::getRoot();

		{
			CustomAppender*	custom = new CustomAppender("custom");
			custom->SetCallBack([](Priority::Value prio, const string& msg)
			{ 
				cout << "custom : " << prio << " : " << msg; 
			});

			root.addAppender(custom);
		}

		for (int i = 0; i < 10; ++i)
		{
			root.info("Foo %d", i);
		}
	}

	class WrapperLog4Cpp {
	private:
		log4cpp::Category& m_Cat;

	public:
		WrapperLog4Cpp(log4cpp::Category& cat) : m_Cat( cat ) {}

		template<typename ...Args>
		void LogWarn(Args... args)
		{
			m_Cat.warn(args...);
		}
	};

	void testWrapper()
	{
		{
			ofstream temp("temp.log.prop", ios_base::out);
			temp << R"(
				log4cpp.rootCategory=WARN,root
				log4cpp.appender.root=ConsoleAppender
				log4cpp.appender.root.layout=BasicLayout
			)";
		}
		PropertyConfigurator::configure("temp.log.prop");

		WrapperLog4Cpp wrapper(log4cpp::Category::getRoot());
		wrapper.LogWarn("%d,%d,%d", 1, 2, 3);
	}

	static CppUnit::Test* CreateTest()
	{
		CppUnit::TestSuite* pSuite = new CppUnit::TestSuite("TestLog4Cpp");

		CppUnit_addTest(pSuite, TestLog4Cpp, testConsoleLog);
		CppUnit_addTest(pSuite, TestLog4Cpp, testRollingFile);
		CppUnit_addTest(pSuite, TestLog4Cpp, testFile);
		CppUnit_addTest(pSuite, TestLog4Cpp, testDebugView);
		CppUnit_addTest(pSuite, TestLog4Cpp, testCategory);
		CppUnit_addTest(pSuite, TestLog4Cpp, testSyslog);
		CppUnit_addTest(pSuite, TestLog4Cpp, testCustomAppender);
		CppUnit_addTest(pSuite, TestLog4Cpp, testWrapper);

		return pSuite;
	}
};


static AddTestCase<TestLog4Cpp> s_Test;
