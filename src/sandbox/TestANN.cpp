#include "stdafx.h"

#include "Fixture.hpp"
#include <string>
#include "opencv/cv.hpp"
#include <math.h>
#include <iostream>
#include <fstream>
#include <list>
#include <Poco/StringTokenizer.h>
#include <opencv2/ml/ml.hpp>


using namespace std;
using namespace Poco;
using namespace cv;

// Artificial Neural Network test
// Ref : An Introduction to Machine Learning, Miroslav Kubat. Chapter5
class ANNTest : public CppUnit::TestCase
{
public:
	ANNTest(const string& name) : CppUnit::TestCase(name) {}

	// X : input
	// HN : hidden layer weight
	// ON : output layer weight
	// HB : hidden layer bias
	// OB : output layer bias
	// H : hidden layer
	Mat ForwardPropagation(const Mat& X, const Mat& HN, const Mat& ON, const Mat& HB, const Mat& OB, Mat& H)
	{
		auto sigmoid = [](double &v, const int* pos) {
			v = 1.0 / (1.0 + exp(-v)); 
		};

		H = HN * X;
		H.forEach<double>([=](double &v, const int* pos) { v += HB.at<double>(pos[0]); });
		H.forEach<double>(sigmoid);
		Mat O = ON * H;
		O.forEach<double>([=](double &v, const int* pos) { v += OB.at<double>(pos[0]); });
		O.forEach<double>(sigmoid);

		return O;
	}

	void testForwardPropagation0()
	{
		Mat X = (Mat_<double>(2, 1) << 
			0.8, 0.1);
		Mat HN = (Mat_<double>(2, 2) << 
			-1.0, 0.5, 
			0.1, 0.7);
		Mat ON = (Mat_<double>(2, 2) << 
			0.9, 0.5,
			0.3, 0.1);
		Mat HB(2, 1, CV_64FC1, Scalar(0.0));
		Mat OB(2, 1, CV_64FC1, Scalar(0.0));

		Mat H;

		Mat Y = ForwardPropagation(X, HN, ON, HB, OB, H);

		assert(abs(Y.at<double>(0, 0) - 0.6359) < 1.0e-3);
		assert(abs(Y.at<double>(1, 0) - 0.5374) < 1.0e-3);
	}

	// In addition to ForwardPropagation, 
	// Y : output of forward propagation
	// T : target output
	// nu : training ratio
	void BackwardPropagation(
		const Mat& X, Mat& HN, Mat& ON, Mat& HB, Mat& OB, const Mat& H, 
		const Mat& Y, const Mat& T, double nu )
	{
		Mat D1 = Y.clone();
 		D1.forEach<double>([=](double &yi, const int* i) {
			double ti = T.at<double>(i);
			yi = yi * (1 - yi) * (ti - yi);
		});

		Mat P = ON.t() * D1;
		Mat D2 = H.clone();
		D2.forEach<double>([=](double &hi, const int*i) {
			double pi = P.at<double>(i);
			hi = hi * (1 - hi) * pi;
		});

		for (int x = 0; x < H.cols; ++x)
		{
			ON += nu * D1.col(x) * H.col(x).t();
			OB += nu * D1.col(x);
			HN += nu * D2.col(x) * X.col(x).t();
			HB += nu * D2.col(x);
		}
	}

	void testBackwardPropagation0()
	{
		Mat X = (Mat_<double>(2, 1) <<
			1, -1);
		Mat HN = (Mat_<double>(2, 2) <<
			-1, 1,
			1, 1);
		Mat ON = (Mat_<double>(2, 2) <<
			1, 1,
			-1, 1);
		Mat H;
		Mat HB(2, 1, CV_64FC1, Scalar(0.0));
		Mat OB(2, 1, CV_64FC1, Scalar(0.0));

		Mat Y = ForwardPropagation(X, HN, ON, HB, OB, H);

		assert(abs(Y.at<double>(0, 0) - 0.65) < 1.0e-3);
		assert(abs(Y.at<double>(1, 0) - 0.5941) < 1.0e-3);

		Mat T = (Mat_<double>(2, 1) <<
			1, 0);

		BackwardPropagation(X, HN, ON, HB, OB, H, Y, T, 0.1);

		assert(abs(HN.at<double>(0, 0) - (-0.9977)) < 1.0e-3);
		assert(abs(HN.at<double>(0, 1) - (0.9977)) < 1.0e-3);
		assert(abs(HN.at<double>(1, 0) - (0.9984)) < 1.0e-3);
		assert(abs(HN.at<double>(1, 1) - (1.0016)) < 1.0e-3);

		assert(abs(ON.at<double>(0, 0) - (1.0009)) < 1.0e-3);
		assert(abs(ON.at<double>(0, 1) - (1.004)) < 1.0e-3);
		assert(abs(ON.at<double>(1, 0) - (-1.0017)) < 1.0e-3);
		assert(abs(ON.at<double>(1, 1) - (0.9928)) < 1.0e-3);

	}

	void testBackwardPropagation1()
	{
		Mat X = (Mat_<double>(2, 1) <<
			1, -1);
		Mat HN = (Mat_<double>(3, 2) <<
			-1, 1,
			1, 1,
			1, -1);
		Mat ON = (Mat_<double>(2, 3) <<
			1, 1, 1,
			-1, 1, 1);
		Mat H;
		Mat HB(3, 1, CV_64FC1, Scalar(0.0));
		Mat OB(2, 1, CV_64FC1, Scalar(0.0));
		Mat Y = ForwardPropagation(X, HN, ON, HB, OB, H);

		Mat T = (Mat_<double>(2, 1) <<
			1, 0);

		BackwardPropagation(X, HN, ON, HB, OB, H, Y, T, 0.1);
	}

	void testBackwardPropagation2()
	{
		Mat X = (Mat_<double>(2, 2) <<
			1, 0.5,
			-1, 0.1 );
		Mat HN = (Mat_<double>(2, 2) <<
			-1, 1,
			1, 1);
		Mat ON = (Mat_<double>(2, 2) <<
			1, 1,
			-1, 1);
		Mat H;
		Mat HB(2, 1, CV_64FC1, Scalar(0.0));
		Mat OB(2, 1, CV_64FC1, Scalar(0.0));

		Mat Y = ForwardPropagation(X, HN, ON, HB, OB, H);

		Mat T = (Mat_<double>(2, 2) <<
			1, 0, 
			0, 1);

		BackwardPropagation(X, HN, ON, HB, OB, H, Y, T, 0.1);
	}

	Mat ReadMat(const string& filename, const string& separators, int resultType = CV_64FC1)
	{
		int rows = 0, cols = 0;

		{
			string line;
			ifstream fs(filename);
			while (getline(fs, line))
			{
				if (rows == 0)
				{
					Poco::StringTokenizer tokens(line, separators);
					cols = (int)tokens.count();
				}
				rows++;
			}
		}

		Mat m(rows, cols, resultType);
		{
			int y = 0;
			string line;
			ifstream fs(filename);
			while (getline(fs, line))
			{
				Poco::StringTokenizer tokens(line, separators);
				for (int x = 0; x < (int)tokens.count(); ++x)
				{
					if (resultType == CV_64FC1)
					{
						m.at<double>(y, x) = atof(tokens[x].c_str());
					}
					else
					{
						m.at<float>(y, x) = (float)atof(tokens[x].c_str());
					}
				}
				++y;
			}
		}

		return m;
	}

	bool areEqual(const cv::Mat& a, const cv::Mat& b) 
	{
		cv::Mat temp;
		cv::bitwise_xor(a, b, temp); 
		return !(cv::countNonZero(temp));
	}

	void testReadMat0()
	{
		{
			ofstream f("temp.txt");
			f << "0 1 2" << endl;
			f << "4 3 2" << endl;
		}

		Mat orig = (Mat_<double>(2, 3) <<
			0, 1, 2,
			4, 3, 2);

		Mat m = ReadMat("temp.txt", " \t");
		assert( areEqual(m, orig) );
	}

	void testReadMat1()
	{
		Mat m = ReadMat("SteelPlateFaults.txt", " \t");
		assert(m.rows == 1941 && m.cols == 34);
	}

	enum class Direction {
		Row = 0,
		Column = 1,
	};

	template< typename T> 
	void Normalize(Mat& m, Direction dir = Direction::Row )
	{
		int count = dir == Direction::Row ? m.rows : m.cols;
		auto getVector = [dir](Mat& m, int i) { return dir == Direction::Row ? m.row(i) : m.col(i); };
		
		for (int i = 0; i < count; ++i)
		{
			Mat X = getVector( m, i );
			auto r = minmax_element(X.begin<T>(), X.end<T>());
			T minv = *r.first, maxv = *r.second;

			X.forEach<T>([=](T &x, const int* pos) { x = (T)(2*(x-minv) / (maxv-minv) - 1.0); });
		}
	}

	double AverageDistance(const Mat& E )
	{
		double sum = 0.0;
		for (int j = 0; j < E.cols; ++j)
		{
			Mat ej = E.col(j);
			sum += sqrt(ej.dot(ej));
		}
		return sum / E.cols;
	}

	// http://archive.ics.uci.edu/ml/datasets/steel+plates+faults
	void testSteelPlateDefects0()
	{
		using namespace cv::ml;

		Mat D = ReadMat("SteelPlateFaults.txt", " \t");
		D = D.rowRange(0, 340);

		Mat X = D.colRange(4, 27).t();
		Normalize<double>(X);
		Mat T = D.colRange(27, 29).t();

		int M = T.rows, N = X.rows;
		Mat HN(N + 1, N, CV_64FC1, Scalar(0.01));
		Mat HB(N + 1, 1, CV_64FC1, Scalar(0.0));
		Mat ON(M, N + 1, CV_64FC1, Scalar(0.01));
		Mat OB(M, 1, CV_64FC1, Scalar(0.0));

		Mat H, Y;
		for (int i = 0; i < 100; ++i)
		{
			Y = ForwardPropagation(X, HN, ON, HB, OB, H);
			BackwardPropagation(X, HN, ON, HB, OB, H, Y, T, 0.1);

			// printf("iteration %d, Error %.3f\n", i, AverageDistance(T-Y));
		}

		printf( "Trained Error %.3f\n", AverageDistance(T - Y));
	}

	int BestIndex(const Mat& x )
	{
		auto mpi = max_element( x.begin<float>(), x.end<float>());
		return (int)distance( x.begin<float>(), mpi);
	}

	void testWithCVML0()
	{
		using namespace cv::ml;

		Mat D = ReadMat("SteelPlateFaults.txt", " \t", CV_32FC1);
		D = D.rowRange(0, 340);

		Mat X = D.colRange(4, 27);
		Normalize<float>(X, Direction::Column);

		Mat T = D.colRange(27, 29).clone();

		Ptr<ANN_MLP> mlp = ANN_MLP::create();

		Mat layer = (Mat_<ushort>(3, 1) << X.cols, 60, T.cols);
		mlp->setLayerSizes(layer);
		mlp->setActivationFunction(ANN_MLP::ActivationFunctions::SIGMOID_SYM);
		mlp->setTermCriteria(TermCriteria(
			TermCriteria::Type::COUNT + TermCriteria::Type::EPS,
			100000, 0.01 ));
		mlp->setTrainMethod(ANN_MLP::TrainingMethods::BACKPROP);

		Ptr<TrainData> trainingData = TrainData::create(
			X, SampleTypes::ROW_SAMPLE, T );

		bool isTrained = mlp->train(trainingData);

		int sum = 0;
		for (int i = 0; i < X.rows; i++) {
			Mat result; mlp->predict( X.row(i), result);
			int pi = BestIndex(result);
			int ti = BestIndex(T.row(i));
			sum += (pi == ti ? 1 : 0);
		}

		printf("%d/%d matched : %.1f accuray\n", sum, T.rows, ((double)sum / T.rows) * 100.0);
	}

	void testWithCVML1()
	{
		using namespace cv::ml;

		Mat D = ReadMat("SteelPlateFaults.txt", " \t", CV_32FC1);

		Mat X = D.colRange(4, 27);
		Normalize<float>(X, Direction::Column);

		Mat T = D.colRange(27, D.cols).clone();

		Ptr<ANN_MLP> mlp = ANN_MLP::create();

		Mat layer = (Mat_<ushort>(3, 1) << X.cols, 60, T.cols);
		mlp->setLayerSizes(layer);
		mlp->setActivationFunction(ANN_MLP::ActivationFunctions::SIGMOID_SYM);
		mlp->setTermCriteria(TermCriteria(
			TermCriteria::Type::COUNT + TermCriteria::Type::EPS,
			100000, 0.00001));
		mlp->setTrainMethod(ANN_MLP::TrainingMethods::BACKPROP);

		Ptr<TrainData> trainingData = TrainData::create(
			X, SampleTypes::ROW_SAMPLE, T);

		bool isTrained = mlp->train(trainingData);

		int sum = 0;
		for (int i = 0; i < X.rows; i++) {
			Mat result;
			mlp->predict(X.row(i), result);
			auto mpi = max_element(result.begin<float>(), result.end<float>());
			int pi = (int)distance(result.begin<float>(), mpi);

			Mat Ti = T.row(i);
			auto mti = max_element(Ti.begin<float>(), Ti.end<float>());
			int ti = (int)distance(Ti.begin<float>(), mti);

			sum += (pi == ti ? 1 : 0);
		}

		printf("%d/%d matched : %.1f accuray\n", sum, T.rows, ((double)sum / T.rows) * 100.0);
	}

	static CppUnit::Test* CreateTest()
	{
		CppUnit::TestSuite* pSuite = new CppUnit::TestSuite("ANNTest");
		
		// CppUnit_addTest(pSuite, ANNTest, testWithCVML0);
		// CppUnit_addTest(pSuite, ANNTest, testWithCVML1);

		// CppUnit_addTest(pSuite, ANNTest, testSteelPlateDefects0); //  takes too long to run

		CppUnit_addTest(pSuite, ANNTest, testBackwardPropagation2);
		
		CppUnit_addTest(pSuite, ANNTest, testForwardPropagation0);
		CppUnit_addTest(pSuite, ANNTest, testBackwardPropagation0);
		CppUnit_addTest(pSuite, ANNTest, testBackwardPropagation1);

		CppUnit_addTest(pSuite, ANNTest, testReadMat0);
		CppUnit_addTest(pSuite, ANNTest, testReadMat1);

		return pSuite;
	}
};


static AddTestCase<ANNTest> s_Test;
