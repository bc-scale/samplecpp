#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <fstream>
#include <cstddef>
#include <set>
#include <map>
#include <random>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <array>

// From C++17 In Detail by Bartlomiej Filipek

using namespace std;

TEST(cpp_17_in_detail_part2, byte)
{
	constexpr std::byte b{1};
	// std::byte c{3535353}; // error: narrowing conversion from int
	constexpr std::byte c{255};
	// shifts:
	constexpr auto b1 = b << 7;
	static_assert(std::to_integer<int>(b) == 0x01);
	static_assert(std::to_integer<int>(b1) == 0x80);
	// various bit operators, like &, |, &, etc
	constexpr auto c1 = b1 ^ c;
	static_assert(std::to_integer<int>(c) == 0xff);
	static_assert(std::to_integer<int>(c1) == 0x7f);
	constexpr auto c2 = ~c1;
	static_assert(std::to_integer<int>(c2) == 0x80);
	static_assert(c2 == b1);
}

struct Foo47 {
	std::string name;
	Foo47(std::string s) : name(std::move(s)) {
		std::cout << "Foo47::Foo47(" << name << ")\n";
	}
	~Foo47() {
		std::cout << "Foo47::~Foo47(" << name << ")\n";
	}
	Foo47(const Foo47& u) : name(u.name) {
		std::cout << "Foo47::Foo47(copy, " << name << ")\n";
	}
	friend bool operator<(const Foo47& u1, const Foo47& u2) {
		return u1.name < u2.name;
	}
};

TEST(cpp_17_in_detail_part2, set_extract)
{
	std::set<Foo47> setNames;
	setNames.emplace("John");
	setNames.emplace("Alex");
	setNames.emplace("Bartek");
	std::set<Foo47> outSet;
	std::cout << "move John...\n";
	// move John to the outSet
	auto handle = setNames.extract(Foo47("John"));
	outSet.insert(std::move(handle));
	for (auto& elem : setNames)
		std::cout << elem.name << '\n';
	std::cout << "cleanup...\n";
}


TEST(cpp_17_in_detail_part2, map)
{
	{
		std::map<std::string, int> m;
		m["hello"] = 1;
		m["world"] = 2;
		// C++11 way:
		if (m.find("great") == std::end(m))
			m["great"] = 3;
		// the lookup is performed twice if "great" is not in the map
		// C++17 way:
		m.try_emplace("super", 4);
		m.try_emplace("hello", 5); // won't emplace, as it's
		// already in the map

		for (const auto& [key, value] : m)
			std::cout << key << " -> " << value << '\n';
	}

	{
		std::map<std::string, std::string> m;
		m["Hello"] = "World";
		std::string s = "C++";
		m.emplace(std::make_pair("Hello", std::move(s)));
		// what happens with the string 's'?
		std::cout << s << '\n';
		std::cout << m["Hello"] << '\n';
		s = "C++";
		m.try_emplace("Hello", std::move(s));
		std::cout << s << '\n';
		std::cout << m["Hello"] << '\n';
	}

	{
		std::map<std::string, Foo47> mapNicks;
		//mapNicks["John"] = Foo47("John Doe"); // error: no default ctor for User()
		auto [iter, inserted] = mapNicks.insert_or_assign("John", Foo47("John Doe"));
		if (inserted)
			std::cout << iter->first << " entry was inserted\n";
		else
			std::cout << iter->first << " entry was updated\n";
	}
}


TEST(cpp_17_in_detail_part2, vector)
{
	std::vector<std::string> stringVector;
	// in C++11/14:
	stringVector.emplace_back("Hello");
	// emplace doesn't return anything, so back() needed
	stringVector.back().append(" World");
	// in C++17:
	stringVector.emplace_back("Hello").append(" World");

	EXPECT_EQ( stringVector[0], "Hello World");
	EXPECT_EQ( stringVector[1], "Hello World");
}

TEST(cpp_17_in_detail_part2, sample)
{
	std::vector<int> v{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	std::vector<int> out;
	std::sample(v.begin(), // range start
		v.end(), // range end
		std::back_inserter(out), // where to put it
		3, // number of elements to sample
		std::mt19937{ std::random_device{}() });

	std::cout << "Sampled values: ";
	for (const auto& i : out)
		std::cout << i << ", ";
}

TEST(cpp_17_in_detail_part2, numeric)
{
	std::cout << std::gcd(24, 60) << '\n';
	std::cout << std::lcm(15, 50) << '\n';

	std::cout << std::clamp(300, 0, 255) << '\n';
	std::cout << std::clamp(-10, 0, 255) << '\n';
}

template <class Container>
void PrintBasicInfo(const Container& cont) {
	std::cout << typeid(cont).name() << '\n';
	std::cout << std::size(cont) << '\n';
	std::cout << std::empty(cont) << '\n';
	if (!std::empty(cont))
		std::cout << *std::data(cont) << '\n';
}

TEST(cpp_17_in_detail_part2, non_member_size_data_empty)
{
	std::vector<int> iv{ 1, 2, 3, 4, 5 };
	PrintBasicInfo(iv);
	float arr[4] = { 1.1f, 2.2f, 3.3f, 4.4f };
	PrintBasicInfo(arr);
}

template<typename Range, typename Func, typename T>
constexpr T SimpleAccumulate(const Range& range, Func func, T init) {
	for (auto&& obj : range) { // begin/end are constexpr
		init += func(obj);
	}
	return init;
}

constexpr int square(int i) { return i*i; }

TEST(cpp_17_in_detail_part2, constexpr)
{
	constexpr std::array arr{ 1, 2, 3 }; // clas deduction...
	// with constexpr lambda
	static_assert(SimpleAccumulate(arr, [](int i) constexpr {
		return i * i;
		}, 0) == 14);
	// with constexpr function
	static_assert(SimpleAccumulate(arr, &square, 0) == 14);
}
