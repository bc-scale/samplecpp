#include "stdafx.h"

#include "Fixture.hpp"
#include "..\include\CoreSystem.h"
#include <future>
#include <thread>
#include <iostream>
#include "Poco\Net\SocketAddress.h"
#include "Poco\DateTimeFormatter.h"
#include "Poco/StreamCopier.h

using namespace std;
using namespace enscape;
using namespace enscape::System;
using namespace Poco;
using namespace Poco::Net;

class DatagramPointTest : public CppUnit::TestCase
{
public:
	DatagramPointTest(const string& name) : CppUnit::TestCase(name) {}

	ISystemFactory* m_Fab;

	void setUp()
	{
		ModuleManager* mm = Fixture::GetGlobal().GetModuleManager();
		mm->Load("enscape.Core.System.dll");
		m_Fab = mm->CreateClass<ISystemFactory>("enscape::System::SystemFactory");
	}

	void testBasic()
	{
		const int REPEAT_COUNT = 10;

		mutex ready;
		condition_variable condition;
		bool readyToReceive = false;

		future<void> p1( async([&](){

			int packetCount = 0;

			IDatagramPoint::Ptr point = m_Fab->CreateDatagramPoint(
				SocketAddress("127.0.0.1:50001"), 
				[&](IDatagramPoint* p)
				{					
					char buffer[MAX_PATH];
					SocketAddress sender;
					int n = p->ReceiveFrom(buffer, sizeof(buffer)-1, sender);
					buffer[n] = '\0';

					cout
						<< sender.toString() << buffer << " -> "
						<< DateTimeFormatter::format(Timestamp(), "received at %s")
						<< endl;

					if (++packetCount >= REPEAT_COUNT)
					{
						p->StopLoop();
					}
				});

			{
				lock_guard<mutex> guard(ready);
				readyToReceive = true;
			}
			condition.notify_one();

			point->RunInLoop();
		}));

		future<void> p2(async([&](){

			IDatagramPoint::Ptr point = m_Fab->CreateDatagramPoint( 
				SocketAddress("127.0.0.1:50002"), 
				[](IDatagramPoint* p){} );

			{
				unique_lock<mutex> lock(ready);
				condition.wait(lock, [&]{return readyToReceive; });
			}

			for (int i = 0; i<REPEAT_COUNT; ++i)
			{
				string msg = DateTimeFormatter::format(Timestamp(), " sent at %s");
				point->SendTo(msg.data(), (int)msg.size(), SocketAddress("127.0.0.1:50001"));
				this_thread::sleep_for(chrono::milliseconds(100));
			}
		}));

		try
		{
			p1.get();
			p2.get();
		}
		catch (Poco::Exception& ex)
		{
			printf("Exception caught : %s", ex.message().c_str());
		}
	}

	void testSocketSize()
	{
		IDatagramPoint::Ptr point = m_Fab->CreateDatagramPoint(
			SocketAddress("127.0.0.1:50001"),
			nullptr);

		cout << format(
			"Default socket receive buffer size = %d, send buffer size = %d",
			point->GetSocket().getReceiveBufferSize(),
			point->GetSocket().getSendBufferSize())
			<< endl;

		point->GetSocket().setReceiveBufferSize(640 * 1024);
		point->GetSocket().setSendBufferSize(640 * 1024);

		assert(point->GetSocket().getReceiveBufferSize() == 640 * 1024);
		assert(point->GetSocket().getSendBufferSize() == 640 * 1024);
	}

	static CppUnit::Test* CreateTest()
	{
		CppUnit::TestSuite* pSuite = new CppUnit::TestSuite("DatagramPointTest");
		
		CppUnit_addTest(pSuite, DatagramPointTest, testSocketSize);
		CppUnit_addTest(pSuite, DatagramPointTest, testBasic);

		return pSuite;
	}
};


static AddTestCase<DatagramPointTest> s_Test;
