#include "stdafx.h"

#include "Fixture.hpp"
#include "Poco/Util/JSONConfiguration.h"
#include <fstream>

using namespace std;
using namespace Poco;
using namespace Poco::Util;

class ConfigurationTest : public CppUnit::TestCase
{
public:
	ConfigurationTest(const string& name) : CppUnit::TestCase(name) {}

	void testLiteral()
	{
		{
			ofstream temp("temp.json", ios_base::out);
			temp << R"(
				{
					"A":"10",
					"B":"3.4567",
					"C":"Hello World!"
				}
			)";
		}
		JSONConfiguration config("temp.json");
		assert(config.getInt("A") == 10);
		assert(config.getDouble("B") == 3.4567);
		assert(config.getString("C") == "Hello World!");
	}

	void testArray()
	{
		{
			ofstream temp("temp.json", ios_base::out);
			temp << R"(
				{
					"A":["1","2","3","4"],
					"B":["3.4567","5.6789"],
					"C":["Hello World!","Good Morning"]
				}
			)";
		}
		JSONConfiguration config("temp.json");		
		// no way to access array
	}

	void testArrayWorkaround()
	{
		{
			ofstream temp("temp.json", ios_base::out);
			temp << R"(
				{
					"B": { 
						"0":"3.4567",
						"1":"5.6789"
					},
					"C": {
						"0":"Hello World!",
						"1":"Good Morning"
					}		
				}
			)";
		}
		JSONConfiguration config("temp.json");

		assert(config.createView("B")->getDouble("0") == 3.4567);
		assert(config.createView("B")->getDouble("1") == 5.6789);
	}

	void testView()
	{
		{
			ofstream temp("temp.json", ios_base::out);
			temp << R"(
				{
					"SFR": { 
						"radius" : "1",
						"algo" : "fast"
					},
					"Blemish": {
						"location": { "x":"10.0", "y":"11.1" },
						"algo": "robust"
					}		
				}
			)";
		}
		JSONConfiguration config("temp.json");

		{
			AbstractConfiguration* SFR = config.createView("SFR");
			assert(SFR->getInt("radius") == 1);
			assert( SFR->getString("algo") == "fast" );
		}

		{
			AbstractConfiguration* Blemish = config.createView("Blemish");
			assert(Blemish->createView("location")->getDouble("x") == 10.0);
			assert(Blemish->createView("location")->getDouble("y") == 11.1);
			assert(Blemish->getString("algo") == "robust");
		}
	}

	void testSave()
	{
		{
			ofstream temp("temp.json", ios_base::out);
			temp << R"(
				{
					"SFR": { 
						"radius" : "1",
						"algo" : "fast"
					},
					"Blemish": {
						"location": { "x":"10.0", "y":"11.1" },
						"algo": "robust"
					}		
				}
			)";
		}
		JSONConfiguration config("temp.json");
		config.createView("SFR")->setString("algo", "very fast");

		{
			ofstream tempsave("tempsave.json", ios_base::out);
			config.save(tempsave);
		}

		{
			JSONConfiguration ts("tempsave.json");
			assert(ts.createView("SFR")->getString("algo") == "very fast");
		}
	}


	static CppUnit::Test* CreateTest()
	{
		CppUnit::TestSuite* pSuite = new CppUnit::TestSuite("ConfigurationTest");
		
		CppUnit_addTest(pSuite, ConfigurationTest, testLiteral);
		CppUnit_addTest(pSuite, ConfigurationTest, testArray);
		CppUnit_addTest(pSuite, ConfigurationTest, testArrayWorkaround);
		CppUnit_addTest(pSuite, ConfigurationTest, testView);
		CppUnit_addTest(pSuite, ConfigurationTest, testSave);

		return pSuite;
	}
};


static AddTestCase<ConfigurationTest> s_Test;
