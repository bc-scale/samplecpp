#include <gtest/gtest.h>
#include <string>
#include <iostream>

// From C++17 In Detail by Bartlomiej Filipek

using namespace std;

std::string_view StartFromWord(std::string_view str, std::string_view word)
{
	return str.substr(str.find(word)); // substr creates only a new view
}

TEST(cpp_17_in_detail_part2, string_view_basic)
{
	std::string str {"Hello Amazing Programming Environment"};
	auto subView = StartFromWord(str, "Programming Environment");
	std::cout << subView << '\n';
}

TEST(cpp_17_in_detail_part2, string_view_creation)
{
	const char* cstr = "Hello World";
	// the whole string:
	std::string_view sv1 { cstr };
	std::cout << sv1 << ", len: " << sv1.size() << '\n';
	// slice
	std::string_view sv2 { cstr, 5 }; // not null-terminated!

	std::cout << sv2 << ", len: " << sv2.size() << '\n';
	// from string:
	std::string str = "Hello String";
	std::string_view sv3 = str;
	std::cout << sv3 << ", len: " << sv3.size() << '\n';
	// ""sv literal
	using namespace std::literals;
	std::string_view sv4 = "Hello\0 Super World"sv;
	std::cout << sv4 << ", len: " << sv4.size() << '\n';
	std::cout << sv4.data() << " - till zero\n";
}

std::vector<int> GenerateVec() { return std::vector<int>(5,1); }

std::string GenerateString() { std::string s("very short live"); return s; }

TEST(cpp_17_in_detail_part2, string_view_risks)
{
	{
		std::string str = "My Super";
		// auto sv = StartFromWord( str + " String", "Super");  // might blow !
	}

	{
		const std::vector<int>& refv = GenerateVec();  // this is safe. 
		// C++ rules say the the lifetime of a temporary object bound to a const reference
		// is prolonged to the lifetime of the reference itself.		
	}

	{
		// but this is not safe.
		//std::string_view sv = GenerateString();
	}
}


class Foo46 {
	std::string name_;
public:
	Foo46( std::string name ) : name_(std::move(name)) {}  // best ! also advised by Scott Meyers. 
	Foo46( const std::string& name ) : name_( name) {}  // well, more typing on argument ? ... verbose ?
};



int string_cut_size()
{
	constexpr auto strv = "Hello Programming World"sv;
	constexpr auto strvCut = strv.substr("Hello "sv.size());
	static_assert(strvCut == "Programming World"sv);

	return strvCut.size();
}


TEST(cpp_17_in_detail_part2, string_view_const_expr)
{
	using namespace std::literals;

	int n = string_cut_size();
	std::cout << n << std::endl;
}


std::vector<std::string_view>
splitSV(std::string_view strv, std::string_view delims = " ")
{
	std::vector<std::string_view> output;
	auto first = strv.begin();
	while (first != strv.end())
	{
		const auto second = std::find_first_of(
			first, std::cend(strv),
			std::cbegin(delims), std::cend(delims));

		if (first != second)
		{
			output.emplace_back(strv.substr(std::distance(strv.begin(), first),
				std::distance(first, second)));

		}
		if (second == strv.end())
			break;
		first = std::next(second);
	}
	return output;
}

TEST(cpp_17_in_detail_part2, string_view_split)
{
	const std::string str{ "Hello Extra,,, Super, Amazing World" };
	for (const auto& word : splitSV(str, " ,"))
		std::cout << word << '\n';
}