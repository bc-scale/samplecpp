#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <map>
#include <array>
#include <set>
#include <mutex>

// From C++17 In Detail by Bartlomiej Filipek

TEST(cpp_17_in_detail_part1, maps)
{
	std::map<std::string, int> mapUsersAge{ { "Alex", 45 }, { "John", 25 } };
	std::map mapCopy{ mapUsersAge };
	if (auto [iter, wasAdded] = mapCopy.insert_or_assign("John", 26); !wasAdded)
		std::cout << iter->first << " reassigned...\n";

	for (const auto& [key, value] : mapCopy)
		std::cout << key << ", " << value << '\n';
}

template<typename T> void linePrinter(const T& x) {
	if constexpr (std::is_integral_v<T>)
		std::cout << "num: " << x << '\n';
	else if constexpr (std::is_floating_point_v<T>) {
		const auto frac = x - static_cast<long>(x);
		std::cout << "flt: " << x << ", frac " << frac << '\n';
	}
	else if constexpr (std::is_pointer_v<T>) {
		std::cout << "ptr, ";
		linePrinter(*x);
	}
	else
		std::cout << x << '\n';
}

template<typename ... Args> void printWithInfo(Args ... args) {
	(linePrinter(args), ...); // fold expression over the comma operator
}

TEST(cpp_17_in_detail_part1, debug_printing)
{
	int i = 10;
	float f = 2.56f;
	printWithInfo(&i, &f, 30);
}

TEST(cpp_17_in_detail_part1, auto_rules)
{
	auto x1 = { 1, 2 }; // decltype(x1) is std::initializer_list<int>
	// auto x2 = { 1, 2.0 }; // error: cannot deduce element type
	// auto x3{ 1, 2 }; // error: not a single element
	auto x4 = { 3 }; // decltype(x4) is std::initializer_list<int>
	auto x5{ 3 }; // decltype(x5) is int
}

struct Foo24
{
	Foo24() { std::cout << "Foo24::Foo24\n"; }
	Foo24(const Foo24&) { std::cout << "Foo24(const Foo24&)\n"; }
	Foo24(Foo24&&) { std::cout << "Foo24(Foo24&&)\n"; }
	~Foo24() { std::cout << "~Foo24\n"; }

	static Foo24 Create() 
	{
		return Foo24();
	}
};

TEST(cpp_17_in_detail_part1, guaranteed_copy_elision)
{
	auto n = Foo24::Create();
}



struct NonMoveable
{
	NonMoveable(int x) : v(x) { }  
	NonMoveable(const NonMoveable&) = delete;
	NonMoveable(NonMoveable&&) = delete;
	std::array<int, 1024> arr;
	int v;
};
NonMoveable make(int val)
{
	if (val > 0)
		return NonMoveable(val);	// in place constructed and no need copy or assignment constructor, 
	return NonMoveable(-val);
}

TEST(cpp_17_in_detail_part1, non_moveable)
{
	auto largeNonMoveableObj = make(90); // construct the object
	std::cout << largeNonMoveableObj.v << std::endl;
}


struct alignas(16) vec4
{
	float x, y, z, w;
};

TEST(cpp_17_in_detail_part1, alloc_aligned_data)
{
	auto pv = std::make_unique<vec4>();
	auto pva = std::make_unique<vec4[]>(100);
}


TEST(cpp_17_in_detail_part1, structured_binding)
{
	{
		std::set<int> mySet;
		auto [iter,inserted] = mySet.insert(10);
	}

	{
		std::pair a(0,1.0f);
		auto [x,y] = a;
	}

	{
		std::pair a(0,1.0f);
		auto& [x,y] = a;
		x = 10;
		y = 42.2f;
	}

	{
		struct Point {
			double x, y;
			static Point GetStartPoint() {  return {0.0, 0.0}; }
		};

		const auto[x,y] = Point::GetStartPoint();
		EXPECT_EQ( x, 0.0);
	}

	{
		const std::map<std::string, int> mapCityPopulation{
			{ "Beijing", 21'707'000 },
			{ "London", 8'787'892 },
			{ "New York", 8'622'698 }
		};
		for (auto& [city, population] : mapCityPopulation)
			std::cout << city << ": " << population << '\n';
	}
}

TEST(cpp_17_in_detail_part1, init_for_if_switch)
{
	const std::string myString = "My Hello World Wow";
	{
		const auto pos = myString.find("Hello");
		if (pos != std::string::npos)
			std::cout << pos << " Hello\n";
		
		const auto pos2 = myString.find("World");
		if (pos2 != std::string::npos)
			std::cout << pos2 << " World\n";
	}

	{
		if (const auto pos = myString.find("Hello"); pos != std::string::npos)
			std::cout << pos << " Hello\n";
		
		if (const auto pos = myString.find("World"); pos != std::string::npos)
			std::cout << pos << " World\n";
	}
}


struct User
{
	int age_ {0};
	std::string name_{ "unknown" };
	inline static const int s_capriciousness = 100;
};

TEST(cpp_17_in_detail_part1, inline_variable)
{
	User u;
	EXPECT_EQ( u.s_capriciousness, 100);
}

TEST(cpp_17_in_detail_part1, constexpr_lambda)
{
	{
		auto SimpleLambda = [](int n) { return n;};
		static_assert( SimpleLambda(3) == 3, "");
	}

	{
		auto SimpleLambda = [] (int n) constexpr { return n; };
		static_assert( SimpleLambda(3) == 3, "");
	}
}

namespace MySuperCompany::SecretProject::SafetySystem {
	class SuperArmor {
	// ...
	};
	class SuperShield {
	// ...
	};
}

TEST(cpp_17_in_detail_part1, nested_namespaces)
{
	MySuperCompany::SecretProject::SafetySystem::SuperArmor sa;
	MySuperCompany::SecretProject::SafetySystem::SuperShield sh;
}

TEST(cpp_17_in_detail_part1, template_argument_deduction)
{
	{
		auto p1 = std::make_pair<int, std::string>(42, "hello world");
		auto p2 = std::make_pair(42, "hello world");

		EXPECT_EQ( p2.first, 42);
		EXPECT_EQ( p2.second, "hello world");
	}	

	{
		using namespace std::string_literals;
		std::pair p3(42, "hello world"s); // deduced automatically!
		
		EXPECT_EQ( p3.second, "hello world");
	}

	std::mutex mut;
	{
		std::lock_guard<std::mutex> lck(mut);
		std::array<int, 3> arr {1, 2, 3};
	}

	{
		std::lock_guard lck(mut);
		std::array arr { 1, 2, 3 };		
	}
}

template <typename T>
struct Foo25 { 
	Foo25(T s) : str(s){} 
	T str; 
};

Foo25(const char *) -> Foo25<std::string>;

TEST(cpp_17_in_detail_part1, deduction_guides)
{
	Foo25 foo( "Hello World");
	EXPECT_EQ( foo.str.size(), strnlen("Hello World", 32 ));
}

// ???
template<class... Ts>
struct overload : Ts... { using Ts::operator()...; };

template<class... Ts>
overload(Ts...) -> overload<Ts...>; // deduction guide
// ???

auto sum_cpp11() { return 0; }
template<typename T1, typename ... T>
auto sum_cpp11( T1 s, T ... ts) {
	return s + sum_cpp11(ts ... );
}

template< typename ...Args > auto fold_sum( Args... args)
{
	return ( args + ... + 0);
}

template< typename ...Args > auto fold_sum2( Args... args)
{
	return ( args + ... );
}

TEST(cpp_17_in_detail_part1, fold_expressions)
{
	{
		auto sum = sum_cpp11( 1, 2, 3, 4, 5);

		EXPECT_EQ( sum, 15);
	}

	{
		auto sum = fold_sum( 1, 2, 3, 4, 5 );
		
		EXPECT_EQ( sum, 15 );
	}
}

template<typename ...Args>
void FoldPrint(Args&&... args)
{
	(std::cout << ... << std::forward<Args>(args)) << '\n';
}

template<typename ...Args>
void FoldSeparateLine(Args&&... args)
{
	auto separateLine = [](const auto& v) {
		std::cout << v << '\n';
	};
	(..., separateLine(std::forward<Args>(args))); // over comma operator
}

TEST(cpp_17_in_detail_part1, fold_print)
{
	FoldPrint( "hello", 10, 20, 30 );
	FoldSeparateLine( "hello", 10, 20, 30 );
}


template<typename T, typename... Args>
void push_back_vec(std::vector<T>& v, Args&&... args)
{
	(v.push_back(std::forward<Args>(args)), ...);
}

TEST(cpp_17_in_detail_part1, fold_push_back)
{
	std::vector<float> vf;
	push_back_vec( vf, 1.2f, 2.3f, 3.4f );
}

template <typename Concrete, typename... Ts>
std::unique_ptr<Concrete> construct_or_nullptr(Ts&&... params)
{
	// if (std::is_constructible_v<Concrete, Ts...>) // compile error 
	if constexpr (std::is_constructible_v<Concrete, Ts...>) 
		return std::make_unique<Concrete>(std::forward<Ts>(params)...);
	else
		return nullptr;
}

class Foo26 {
public:
	Foo26(int, int) {}
};

TEST(cpp_17_in_detail_part1, if_const_expr)
{
	auto p1 = construct_or_nullptr<Foo26>( 10, 20 );
	EXPECT_TRUE( p1 != nullptr );

	auto p2 = construct_or_nullptr<Foo26>( 10, 20, 30 );
	EXPECT_TRUE( p2 == nullptr );
}


class Foo27 {
public:
	int GetA() const { return a; }
	float GetB() const { return b; }

private:
	int a { 42 };
	float b { 3.14f };
};

template< std::size_t I>  auto get( const Foo27& f )
{
	if constexpr ( I == 0 ) return f.GetA();
	else if constexpr ( I == 1 ) return f.GetB();
}

namespace std
{
	template<> struct tuple_size<Foo27> : std::integral_constant<size_t,2> {};
	template<> struct tuple_element<0,Foo27> { using type = int; };
	template<> struct tuple_element<1,Foo27> { using type = float; };
}

TEST(cpp_17_in_detail_part1, if_const_expr_binding)
{
	Foo27 foo27;
	auto [a,b] = foo27;
	EXPECT_EQ( a, 42);
	EXPECT_EQ( b, 3.14f);
}

template < auto ... vs>
struct HeterogenousValueList {};

TEST(cpp_17_in_detail_part1, non_type_template_parameters_with_auto)
{
	using HVL1 = HeterogenousValueList< 'a', 100, 'b' >;
	HVL1 a;
	HVL1 b;

	// then what ?	
}

// [[noreturn]]
// [[carries_dependency]]
// [[fallthrough]]
// [[maybe_unused]]
// [[nodiscard]]

[[deprecated("use AwesomeFunc instead")]] void GoodFunc() { }


// warning on every compile ... 

// namespace [[deprecated("use BetterUtils")]] GoodUtils {
// 	void DoStuff() { }
// }

TEST(cpp_17_in_detail_part1, attributes_deprecated)
{
	// GoodFunc();
	// GoodUtils::DoStuff();
}

enum class [[nodiscard]] ErrorCode {
	OK,
	Fatal,
	System	[[deprecated("use Fatal instead")]],
	FileIssue
};

ErrorCode OpenFile(const char* file) 
{
	return ErrorCode::Fatal;
}

TEST(cpp_17_in_detail_part1, attributes_nodiscard)
{
	// OpenFile("Hello");
	ErrorCode ret = OpenFile("Hello");
}
