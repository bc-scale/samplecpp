#include <gtest/gtest.h>
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <unordered_set>

// From C++ High Performance by Bjorn Andrist, Viktor Sehr


TEST(cpp_high_performance, erase_vector)
{
	auto v = std::vector{ -1, 5, 2, -3, 4, -5, 5};

	// C++ 20
	// std::erase( v, 5);
	// std::erase_if( v, [](auto x) { return x <0; });
}


std::string read_file_content( std::string filename )
{
	auto in = std::ifstream( filename, std::ios::binary | std::ios::ate );
	if( in.is_open())
	{
		auto size = in.tellg();
		auto content = std::string( size, '\0' );
		in.seekg(0);
		in.read( &content[0], size );
		return content;
	}

	throw std::runtime_error( "failed to open file");
}

TEST(cpp_high_performance, read_file)
{
	auto alice_private_key = read_file_content( "private_alice.pem" );
	EXPECT_NE( alice_private_key.find( "-----BEGIN RSA PRIVATE KEY-----"), std::string::npos );
}


class Person {
public:
	Person( std::string name, int age ) : name_(std::move(name)), age_(age) {}
	const std::string& name() const { return name_; }
	int age() const { return age_; }

private:
	std::string name_;
	int age_;
};

template<typename T> void hash_combine(size_t & seed, T const& v)
{	
	seed ^= std::hash<T>()(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

template<> 
struct std::hash<Person>
{
	size_t operator()( Person const& person ) const noexcept 
	{
		auto seed = size_t{0};
		hash_combine( seed, person.name());
		hash_combine( seed, person.age());
		return seed;
	}
};

bool operator==(const Person& lhs, const Person& rhs) {
    return lhs.name() == rhs.name() && lhs.age() == rhs.age();
}

TEST(cpp_high_performance, unordered_set_hash)
{
	std::unordered_set<Person> persons;
	persons.emplace( "Alice", 12 );
	persons.emplace( "Hatter", 47 );
	persons.emplace( "Queen", 36 );
}