#include "stdafx.h"

#include "Fixture.hpp"
#include "kdtree++/kdtree.hpp"
#include "Poco/Stopwatch.h"

using namespace std;

class KDTreeTest : public CppUnit::TestCase
{
public:
	KDTreeTest(const string& name) : CppUnit::TestCase(name) {}

	struct Defect
	{
		char id;
		int x, y;
		short area;
	};

	struct DefectProxy
	{
		typedef double value_type;

		DefectProxy(const Defect& d) : Value(&d) {}
		DefectProxy(const DefectProxy& other) : Value(other.Value) {}
		DefectProxy& operator=(const DefectProxy& other)
		{
			if (this == &other)
				return *this;
			Value = other.Value;
			return *this;
		}

		value_type operator[](size_t const N) const 
		{
			return N == 0 ? Value->x : Value->y;
		}

		const Defect* Value;
	};

	 
	void testFindNearest()
	{
		vector<Defect> cloud = {
			{ 0, 0, 0 },
			{ 1, 1, 0 },
			{ 2, 1, 1 },
			{ 3, 0, 1 }
		};

		typedef KDTree::KDTree<2, DefectProxy > DefectKDTree;
		DefectKDTree tree(cloud.begin(), cloud.end());
		tree.optimize();

		pair<DefectKDTree::const_iterator, double> found = 
			tree.find_nearest(Defect{ 0, 1, 1 });

		assertEqualDelta(0.0, found.second, std::numeric_limits<double>::epsilon());

		const DefectProxy& dp = *found.first;
		assertEqual(2, dp.Value->id);
	}

	void testFindWithinRange()
	{
		vector<Defect> cloud = {
			{ 0, 0, 0 },
			{ 1, 10, 0 },
			{ 2, 10, 10 },
			{ 3, 0, 10 },
			{ 4, 9, 9 },
			{ 5, 7, 7 },
		};

		typedef KDTree::KDTree<2, DefectProxy > DefectKDTree;
		DefectKDTree tree(cloud.begin(), cloud.end());
		tree.optimize();

		vector<DefectProxy> inRange;
		tree.find_within_range( 
			Defect{ 0, 9, 9 }, 
			1.0,
			back_insert_iterator<vector<DefectProxy>>(inRange));

		assertEqual(2, (long)inRange.size());
		assertEqual(4, inRange[0].Value->id);
		assertEqual(2, inRange[1].Value->id);
	}

	void testFindNearestIf()
	{
		vector<Defect> cloud = {
			{ 0, 0, 0, 1 },
			{ 1, 10, 0, 1 },
			{ 2, 10, 10, 1 },
			{ 3, 0, 10, 1 },
			{ 4, 9, 9, 1 },
			{ 5, 7, 7, 3 },
		};

		typedef KDTree::KDTree<2, DefectProxy > DefectKDTree;
		DefectKDTree tree(cloud.begin(), cloud.end());
		tree.optimize();

		pair<DefectKDTree::const_iterator, double> found = 
			tree.find_nearest_if(	
				Defect{ 0, 9, 9 }, 
				3.0,
				[](const DefectProxy& dp) {
					return dp.Value->area >= 2;
				});

		assertEqualDelta(2.828, found.second, 1.0e-3);
		const DefectProxy& dp = *found.first;
		assertEqual(5, dp.Value->id);
	}

	void testKDTime()
	{
		int N = 10000;

		vector<Defect> cloud;	
		for (int i = 0; i < N; ++i)
		{
			cloud.push_back({ i, rand() % 1000, rand() % 1000 });
		}

		typedef KDTree::KDTree<2, DefectProxy > DefectKDTree;

		Poco::Stopwatch watch; watch.start();

		DefectKDTree tree(cloud.begin(), cloud.end());
		tree.optimize();
		
		printf("KDTree generation takes %.3f seconds for %d points\n", 
			(double)watch.elapsed()/watch.resolution(), N );

		watch.restart();
		for (int i = 0; i < N; ++i)
		{
			auto found = tree.find_nearest(Defect{ 0, rand()%1000, rand()%1000});
		}
		printf("KDTree find_nearest takes %.3f seconds for %d points\n",
			(double)watch.elapsed() / watch.resolution(), N);
	}

	void testPlainLoopTime()
	{
		int N = 10000;

		vector<Defect> cloud;
		for (int i = 0; i < N; ++i)
		{
			cloud.push_back({ i, rand() % 1000, rand() % 1000 });
		}

		Poco::Stopwatch watch; watch.start();

		for (int i = 0; i < N; ++i)
		{
			Defect t = { 0, rand() % 1000, rand() % 1000 };
			
			double minDist = 1000;
			int minDistId = 0; 
			for (int j = 0; j < N; ++j)
			{
				const Defect& d = cloud[j];
				double dist = sqrt((t.x - d.x)*(t.x - d.x) + (t.y - d.y)*(t.y - d.y));
				if (dist < minDist)
				{
					minDist = dist;
					minDistId = d.id;
				}
			}
		}
		printf("Plain loop takes %.3f seconds for %d points\n",
			(double)watch.elapsed() / watch.resolution(), N);
	}

	
	static CppUnit::Test* CreateTest()
	{
		CppUnit::TestSuite* pSuite = new CppUnit::TestSuite("KDTreeTest");

		CppUnit_addTest(pSuite, KDTreeTest, testFindNearest);
		CppUnit_addTest(pSuite, KDTreeTest, testFindWithinRange);
		CppUnit_addTest(pSuite, KDTreeTest, testFindNearestIf);
		CppUnit_addTest(pSuite, KDTreeTest, testKDTime);
		CppUnit_addTest(pSuite, KDTreeTest, testPlainLoopTime);
		
		return pSuite;
	}
};


static AddTestCase<KDTreeTest> s_Test;
