#include "stdafx.h"

#include "Fixture.hpp"
#include "CppUnit/TestCase.h"
#include "CppUnit/TestCaller.h"
#include "CppUnit/TestSuite.h"
#include <string>

using namespace std;

class LinkTest : public CppUnit::TestCase
{
public:
	LinkTest(const string& name) : CppUnit::TestCase(name) {}
	~LinkTest() {}

	void setUp() {}

	void tearDown() {}

	void testFoo()
	{
		int i = 0;
		assertEquals(0, i);

		int* a = NULL;
		assertNull(a);

		a = new int;
		assertNotNull(a);

		delete a;
	}

	void testBar()
	{
		int i = 0;

		// warn("Test of warning");
		
		//fail("Test of fail");
	}

	static CppUnit::Test* CreateTest()
	{
		CppUnit::TestSuite* pSuite = new CppUnit::TestSuite("LinkTest");

		CppUnit_addTest(pSuite, LinkTest, testFoo);
		CppUnit_addTest(pSuite, LinkTest, testBar);

		return pSuite;
	}
};


class LinkTestInstance
{
public:
	LinkTestInstance()
	{
		CppUnit::TestSuite* suite = Fixture::GetGlobal().GetTestSuite();
		suite->addTest(LinkTest::CreateTest());
	}
};

LinkTestInstance s_LinkTestInstance;
