#include "stdafx.h"

#include "Fixture.hpp"
#include "../common/threadpool.hpp"

using namespace std;

class BoostThreadPoolTest : public CppUnit::TestCase
{
public:
	BoostThreadPoolTest(const string& name) : BoostThreadPoolTest::TestCase(name) {}
	~BoostThreadPoolTest() {}

	void setUp() {}
	void tearDown() {}

	void testThreadIDSingle()
	{
		boost::threadpool::pool tpool(1);

		vector<DWORD> IDs;
		tpool.schedule([&]()
		{ 
			IDs.push_back(GetCurrentThreadId()); 
			Sleep(100); 
		});
		tpool.wait();
		
		tpool.schedule([&](){ 
			IDs.push_back(GetCurrentThreadId()); 
			Sleep(100); 
		});
		tpool.wait();
		assertEqual(IDs[0], IDs[1]);
	}

	void testThreadIDMultiple()
	{
		boost::threadpool::pool tpool(4);

		vector<DWORD> IDs;
		tpool.schedule([&]()
		{
			IDs.push_back(GetCurrentThreadId());
			Sleep(100);
		});
		tpool.schedule([&](){
			IDs.push_back(GetCurrentThreadId());
			Sleep(100);
		});
		tpool.wait();
		assert(IDs[0] != IDs[1]);
	}

	static CppUnit::Test* CreateTest()
	{
		CppUnit::TestSuite* pSuite = new CppUnit::TestSuite("BoostThreadPoolTest");

		CppUnit_addTest(pSuite, BoostThreadPoolTest, testThreadIDSingle);
//		CppUnit_addTest(pSuite, BoostThreadPoolTest, testThreadIDMultiple); // FIX ME : crash
		
		return pSuite;
	}
};


static AddTestCase<BoostThreadPoolTest> s_Test;
