#include "stdafx.h"

#include "Fixture.hpp"
#include "../include/CoreFixedThreadPool.hpp"
#include <string>
#include <thread>
#include <mutex>
#include "Poco/ThreadPool.h"

void SomeGlobalFn()
{
	Sleep(100);
}

using namespace std;
using namespace Poco;
using namespace enscape;

class PocoThreadPoolTest : public CppUnit::TestCase
{
public:
	PocoThreadPoolTest(const string& name) : PocoThreadPoolTest::TestCase(name) {}
	~PocoThreadPoolTest() {}

	void setUp() {}
	void tearDown() {}

	void testStdMutexInRecurse()
	{
		recursive_mutex m;
		
		m.lock();
		m.lock();
		m.unlock();
		m.unlock();
	}


	void testPocoMutexInRecurse()
	{
		Poco::Mutex m;

		m.lock();
		m.lock();
		m.unlock();
		m.unlock();
	}

	void testThread()
	{
		int count = 0;

		Thread t;
		t.startFunc([&](){count++; });
		t.join();
		assert(count == 1);
	}

	void testThreadPool()
	{
		ThreadPool pool(2, 4);

		assert(pool.allocated() == 2);
		assert(pool.available() == 4);
		assert(pool.used() == 0);

		struct TestRunner : public Runnable
		{
			void run() {
				this_thread::sleep_for(chrono::milliseconds(100));
			}
		};

		TestRunner r0;
		pool.start(r0);
		assert(pool.used() == 1);
		assert(pool.available() == 3);

		TestRunner r1;
		pool.start(r1);
		assert(pool.used() == 2);
		assert(pool.available() == 2);

		TestRunner r2;
		pool.start(r2);
		assert(pool.used() == 3);
		assert(pool.available() == 1);

		TestRunner r3;
		pool.start(r3);
		assert(pool.used() == 4);
		assert(pool.available() == 0);

		while (pool.available() == 0)
		{
			this_thread::sleep_for(chrono::milliseconds(100));
		}

		TestRunner r4;
		pool.start(r4);

		pool.joinAll();
	}

	void testSimpleFixedThreadPool0()
	{
		FixedThreadPool pool(1);

		assert(pool.GetIdleCount() == 1);
		assert(pool.GetRunningCount() == 0);
		assert(pool.IsIdle(0) == true);

		pool.Schedule(0, [](){ Sleep(100); });
		int id0 = pool.GetThreadID(0);

		assert(pool.GetIdleCount() == 0);
		assert(pool.IsIdle(0) == false);

		pool.JoinAll();
	}

	void testSimpleFixedThreadPool1()
	{
		FixedThreadPool pool(1);

		pool.Schedule(0, [](){ Sleep(100); });
		pool.Schedule(0, [](){ Sleep(100); });

		pool.JoinAll();
	}

	void testFixedThreadPool0()
	{
		FixedThreadPool pool(2);

		assert(pool.GetIdleCount() == 2);
		assert(pool.IsIdle(0) == true);
		assert(pool.IsIdle(1) == true);

		pool.Schedule(0, [](){ Sleep(100); });
		pool.Schedule(1, [](){ Sleep(200); });
		int id0 = pool.GetThreadID(0);
		int id1 = pool.GetThreadID(1);
		assert(id0 != id1);

		assert(pool.GetIdleCount() == 0);
		assert(pool.IsIdle(0) == false);
		assert(pool.IsIdle(1) == false);

		pool.JoinAll();
	}

	void testFixedThreadPool1()
	{
		FixedThreadPool pool(2);
	
		assertEqual(2, pool.GetIdleCount());
		assertEqual(true, pool.IsIdle(0) );
		assertEqual(true, pool.IsIdle(1) );
		
		pool.Schedule(pool.ClaimIdle(), [&](){ Sleep(100); });
		pool.Schedule(pool.ClaimIdle(), [&](){ Sleep(100); });
	
		assertEqual(0, pool.GetIdleCount());
		assertEqual(false, pool.IsIdle(0) );
		assertEqual(false, pool.IsIdle(1) );
	
		pool.JoinAll();
		Sleep(10);
		assertEqual(2, pool.GetIdleCount());
	
		pool.Schedule(pool.ClaimIdle(), SomeGlobalFn);
		pool.Schedule(pool.ClaimIdle(), SomeGlobalFn);
	
		pool.Join(0);
		pool.Join(1);
	}
	
	void testFixedThreadPool2()
	{
		FixedThreadPool pool(4);

		pool.Schedule(pool.ClaimIdle(), [](){ Sleep(100); });
		pool.Schedule(pool.ClaimIdle(), [](){ Sleep(200); });
		pool.Schedule(pool.ClaimIdle(), [](){ Sleep(200); });
		pool.Schedule(pool.ClaimIdle(), [](){ Sleep(200); });

		int idleThread;
		assert(pool.TryClaimIdle(idleThread) == false);

		pool.Join(0);
		assert(pool.TryClaimIdle(idleThread) == true);
		assertEqual(idleThread,0);
	}

	void testFixedThreadPool3()
	{
		FixedThreadPool pool(2);

		pool.Schedule(0, [](){ Sleep(100); });
		pool.Schedule(0, [](){ Sleep(100); });
		pool.Schedule(1, [](){ Sleep(100); });
		pool.Schedule(1, [](){ Sleep(100); });

		pool.Join(0);
		pool.Join(1);
	}

	void testFixedThreadPool4()
	{
		FixedThreadPool pool(2);

		pool.Schedule(0, [](){ Sleep(100); });
		assertEqual(pool.ClaimIdle(), 1);

		string msg = CatchEnscapeExceptionMessage<enscape::Exception>([&pool](){
			pool.ClaimIdle();
		});
		assertEqual(msg, "All 2 threads are running");

		pool.Free(1);
		assertEqual(pool.ClaimIdle(), 1);
	}

	void testFixedThreadPool5()
	{
		FixedThreadPool pool(1);
		vector<DWORD> IDs;
		pool.Schedule(0, [&](){ IDs.push_back(GetCurrentThreadId()); Sleep(100); });
		pool.Join(0);
		pool.Schedule(0, [&](){ IDs.push_back(GetCurrentThreadId()); Sleep(100); });
		pool.Join(0);

		assertEqual(IDs[0], IDs[1]);
	}

	void testFixedThreadPool6()
	{
		int N = 100;
		FixedThreadPool pool(N);

		Poco::Mutex lock;
		int sum = 0;
		for (int i = 0; i < pool.Size(); ++i)
		{
			pool.Schedule(i, [&lock,i,&sum]()
			{
				Poco::Mutex::ScopedLock guard(lock);
				sum += (i+1);
			});
		}

		pool.JoinAll();
		assertEqual( (N*(N+1))/2, sum );

		for (int i = 0; i < pool.Size(); ++i)
		{
			pool.Schedule(i, [&lock, i, &sum]()
			{
				Poco::Mutex::ScopedLock guard(lock);
				sum -= (i + 1);
			});
		}

		pool.JoinAll();
		assertEqual(0, sum);
	}

	void testFixedThreadPool7()
	{
		const int LOOP = 100;

		FixedThreadPool pool(1);
		vector<DWORD> IDs;
		for (int i = 0; i < LOOP; ++i)
		{
			pool.Schedule(0, [&](){ IDs.push_back(GetCurrentThreadId()); Sleep(1); });
			pool.Join(0);
		}

		assertEqual(LOOP, IDs.size());
		for (int i = 0; i < LOOP; ++i)
		{
			assertEqual(pool.GetThreadID(0), IDs[i]);
		}
	}

	void testFixedThreadPool8()
	{
		FixedThreadPool pool(4);

		vector<int> indexes(pool.Size());
		for (int i = 0; i < pool.Size(); ++i)
		{
			pool.Schedule(i, [i, &pool, &indexes]()
			{
				indexes[i] = pool.GetCurrentThreadIndex();
			});
		}
		pool.JoinAll();

		for (int i = 0; i < (size_t)indexes.size(); ++i)
		{
			assertEqual(i, indexes[i]);
		}
	}

	static CppUnit::Test* CreateTest()
	{
		CppUnit::TestSuite* pSuite = new CppUnit::TestSuite("PocoThreadPoolTest");
		
		
		CppUnit_addTest(pSuite, PocoThreadPoolTest, testSimpleFixedThreadPool1);
		CppUnit_addTest(pSuite, PocoThreadPoolTest, testSimpleFixedThreadPool0);
		CppUnit_addTest(pSuite, PocoThreadPoolTest, testFixedThreadPool0);
		CppUnit_addTest(pSuite, PocoThreadPoolTest, testFixedThreadPool1);
		CppUnit_addTest(pSuite, PocoThreadPoolTest, testFixedThreadPool2);
		CppUnit_addTest(pSuite, PocoThreadPoolTest, testFixedThreadPool3);
		CppUnit_addTest(pSuite, PocoThreadPoolTest, testFixedThreadPool4);
		// CppUnit_addTest(pSuite, PocoThreadPoolTest, testFixedThreadPool5); // FIX ME : occassional error
		CppUnit_addTest(pSuite, PocoThreadPoolTest, testFixedThreadPool6);
		CppUnit_addTest(pSuite, PocoThreadPoolTest, testFixedThreadPool7);		
		CppUnit_addTest(pSuite, PocoThreadPoolTest, testFixedThreadPool8);
		CppUnit_addTest(pSuite, PocoThreadPoolTest, testStdMutexInRecurse);
		CppUnit_addTest(pSuite, PocoThreadPoolTest, testPocoMutexInRecurse);
		CppUnit_addTest(pSuite, PocoThreadPoolTest, testThreadPool);
		CppUnit_addTest(pSuite, PocoThreadPoolTest, testThread);

		return pSuite;
	}
};


static AddTestCase<PocoThreadPoolTest> s_Test;
