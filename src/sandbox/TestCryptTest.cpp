#include <string>
#include <sstream>
#include <gtest/gtest.h>
#include "Poco/Format.h"
#include "Poco/Crypto/DigestEngine.h"
#include "Poco/Crypto/Cipher.h"
#include "Poco/Crypto/CipherKey.h"
#include "Poco/Crypto/RSAKey.h"
#include "Poco/Crypto/CipherFactory.h"
#include "Poco/Crypto/RSADigestEngine.h"

using namespace std;
using namespace Poco;
using namespace Poco::Crypto;


class SimpleHash
{
public:
	SimpleHash(void){};
	virtual ~SimpleHash(void){};

	long GetHashValue(const char *pKeyStr)
	{
		int    strLength;
		int    i, tmpVal = 0;

		if ((strLength = (int)strlen(pKeyStr)) == 0) return(0);

		for (i = 0; i<strLength; i++) {
			tmpVal = ((tmpVal * 0x00ff) + (0x00ff & toupper(pKeyStr[i]))) % SEED;
		}
		return(tmpVal);
	}

private:
	enum {
		SEED = 7919
	};
};

long GetHashUsingSimpleHash(const string& content)
{
	string salted = content + "enscape key";  // add key 
	SimpleHash hash;
	return hash.GetHashValue(salted.c_str());
}

bool VerifyContentWithRunningPC(const string& content)
{
	return true;
}

string GetHashUsingPocoCrypt(const string& content)
{
	Crypto::DigestEngine engine("MD5");
	engine.update(content);
	engine.update("enscape license");	// add key
	string signature = Crypto::DigestEngine::digestToHex(engine.digest());
	return signature;
}

TEST( CryptTest, testUsageScenario1) 
{
	string licenseFileContent =
		"[ValidUntil]=2018.1.1\n"
		"[MAC]=ab-cd-ef-11-22-33\n"
		;

	string enscapeSign;
	string userSign;

	{
		// enscape sign the license content and let user know
		enscapeSign = GetHashUsingPocoCrypt(licenseFileContent);
		EXPECT_EQ("e57eb4d76bbca710ca00ac9d3fbedb06", enscapeSign);
	}

	{
		// user get sign with the license content
		userSign = GetHashUsingPocoCrypt(licenseFileContent);
	}

	EXPECT_EQ(enscapeSign, userSign);

	if (enscapeSign == userSign)
	{
		VerifyContentWithRunningPC(licenseFileContent);
	}
}

TEST( CryptTest, testUsageScenario2) 
{
	string licenseFileContent =
		"[ValidUntil]=2018.1.1\n"
		"[MAC]=ab-cd-ef-11-22-33\n"
		;

	string enscapeSign;
	string userSign;

	{
		// enscape sign the license content and let user know
		enscapeSign = GetHashUsingPocoCrypt(licenseFileContent);
		EXPECT_EQ("e57eb4d76bbca710ca00ac9d3fbedb06", enscapeSign);
	}

	licenseFileContent =
		"[ValidUntil]=2018.1.1\n"
		"[MAC]=ab-cd-ef-11-22-34\n"	// not 33
		;
	{
		// user get sign with the license content
		userSign = GetHashUsingPocoCrypt(licenseFileContent);
		EXPECT_EQ("c14d7d5dc3e735a816b285b83e1c5fdc", userSign);
	}

	ASSERT_TRUE(enscapeSign != userSign);

	if (enscapeSign == userSign)
	{
		VerifyContentWithRunningPC(licenseFileContent);
	}
}

TEST( CryptTest, testSymmetricEncryption) 
{
	auto& factory = CipherFactory::defaultFactory();
	string transmission;

	{
		Cipher* alice = factory.createCipher(CipherKey("aes-256-ecb", "open sesame"));
		transmission = alice->encryptString("Tall tree, Spy-glass shoulder, ... Skeleton Island ESE and by E");
		EXPECT_TRUE(transmission.find("Skeleton Island") == string::npos);
	}

	{
		Cipher* bob = factory.createCipher(CipherKey("aes-256-ecb", "open sesame"));
		string decrypted = bob->decryptString(transmission);
		EXPECT_TRUE(decrypted == "Tall tree, Spy-glass shoulder, ... Skeleton Island ESE and by E");
	}
}

// >openssl genrsa -des3 -out private_bob.pem 2048
// >openssl rsa -in private_bob.pem -outform PEM -pubout -out public_bob.pem
TEST( CryptTest, testAsymmetricEncryption) 
{
	auto& factory = CipherFactory::defaultFactory();
	string transmission;

	{
		Cipher* alice = factory.createCipher(RSAKey("public_bob.pem"));
		transmission = alice->encryptString("Tall tree, Spy-glass shoulder, ... Skeleton Island ESE and by E");
		EXPECT_TRUE(transmission.find("Skeleton Island") == string::npos);
	}

	{
		Cipher* bob = factory.createCipher(RSAKey( "", "private_bob.pem", "bobbob"));
		string decrypted = bob->decryptString(transmission);
		EXPECT_TRUE(decrypted == "Tall tree, Spy-glass shoulder, ... Skeleton Island ESE and by E");
	}
}

TEST( CryptTest, testDigitalSignature) 
{
	// 	Poco::DigestEngine::Digest is std::vector<unsigned char> 
	tuple<string, Poco::DigestEngine::Digest > transmission;

	{
		string contract = "Alice will give Bob $100 and Bob will give Alice two magic mushrooms";
		RSADigestEngine bob(RSAKey("", "private_bob.pem", "bobbob"));
		bob.update(contract);
		transmission = make_tuple(contract, bob.signature());
	}

	{
		RSADigestEngine alice(RSAKey("public_bob.pem"));
		alice.update(get<0>(transmission));
		bool isValid = alice.verify(get<1>(transmission));
		assert(isValid == true);

		alice.reset();
		alice.update("Alice will give Bob $100 and Bob will give Alice three magic mushrooms");
		assert(false == alice.verify(get<1>(transmission)));
	}
}

TEST( CryptTest, testChainedTransaction) 
{
	struct Transaction
	{
		string Hash;
		vector<unsigned char> Signature;
	};
	
	Transaction queenToAlice;
	{
		// Queen
		RSAKey alicePublic("public_alice.pem");
		stringstream alicePublickey;
		alicePublic.save(&alicePublickey);

		Crypto::DigestEngine hasher("SHA256");
		hasher.update("Queen's own right to create coin");
		hasher.update(alicePublickey.str());
		string hash = Crypto::DigestEngine::digestToHex(hasher.digest());

		RSADigestEngine queen(RSAKey("", "private_queen.pem", "queenqueen"));
		queen.update(hash);

		queenToAlice = Transaction{ hash, queen.signature() };
	}

	{
		// Alice
		RSADigestEngine alice(RSAKey("public_queen.pem"));
		alice.update( queenToAlice.Hash );
		assert( true == alice.verify(queenToAlice.Signature));
	}

	Transaction aliceToBob;
	{
		// Alice
		RSAKey bobPublic("public_bob.pem");
		stringstream bobPublickey;
		bobPublic.save(&bobPublickey);

		Crypto::DigestEngine hasher("SHA256");
		hasher.update(queenToAlice.Hash);
		hasher.update(queenToAlice.Signature.data(), queenToAlice.Signature.size());
		hasher.update(bobPublickey.str());
		string hash = Crypto::DigestEngine::digestToHex(hasher.digest());

		RSADigestEngine alice(RSAKey("", "private_alice.pem", "alicealice"));
		alice.update(hash);

		aliceToBob = Transaction{ hash, alice.signature() };
	}

	{
		// Bob
		RSADigestEngine bob(RSAKey("public_alice.pem"));
		bob.update(aliceToBob.Hash);
		assert(true == bob.verify(aliceToBob.Signature));
	}
}

TEST( CryptTest, testProofOfWork) 
{
	struct Transaction {  string Hash, Signature; };

	struct Block {
		string PrevHash, Nounce;
		vector<Transaction> Transactions;
	};

	Block t0 = {
		"Hello, world!", "4250",  { Transaction{ "", "" } }
	};

	Block t1;
	{
		Crypto::DigestEngine hasher("SHA256");
		
		// verfiy t0 block's transactions and proof-of-work
		hasher.update(t0.PrevHash);
		for (auto tx : t0.Transactions) {
			hasher.update(tx.Hash); hasher.update(tx.Signature);
		}
		hasher.update(t0.Nounce);

		t1.PrevHash = Crypto::DigestEngine::digestToHex(hasher.digest());
		assert(t1.PrevHash.substr(0, 4) == "0000");
		
		// gather transactions
		t1.Transactions = { Transaction{ "ab", "cd" }, Transaction{ "12", "34" }, Transaction{ "56", "78" } };

		// race for the nounce
		for (int nounce = 0;; ++nounce)
		{
			hasher.reset();
			hasher.update(t1.PrevHash);
			for (auto tx : t1.Transactions) {
				hasher.update(tx.Hash); hasher.update(tx.Signature);
			}
			hasher.update(to_string(nounce));

			string hash = Crypto::DigestEngine::digestToHex(hasher.digest());
			if (hash.substr(0, 4) == "0000") {
				t1.Nounce = to_string(nounce);
				break;
			}
		}

		// broadcast t1 block to world 
	}
}

/*
"aes-128-ecb"
"aes-128-cfb"
"aes-128-ofb"
"aes-128-cbc"
"aes-192-ecb"
"aes-192-cfb"
"aes-192-ofb"
"aes-192-cbc"
"aes-256-ecb"
"aes-256-cfb"
"aes-256-ofb"
"aes-256-cbc"
"des-ecb"
"des-cfb"
"des-ofb"
"des-cbc"
*/
