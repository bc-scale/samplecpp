#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <chrono>
#include <regex>

// From C++17 In Detail by Bartlomiej Filipek

using namespace std;

namespace fs = std::filesystem;

void DisplayDirectoryTree(const fs::path& pathToScan, int level = 0) {
	for (const auto& entry : fs::directory_iterator(pathToScan)) {
		const auto filenameStr = entry.path().filename().string();
		if (entry.is_directory()) {
			std::cout << std::setw(level * 3) << "" << filenameStr << '\n';
			DisplayDirectoryTree(entry, level + 1);
		}
		else if (entry.is_regular_file()) {
			std::cout << std::setw(level * 3) << "" << filenameStr
				<< ", size " << fs::file_size(entry) << " bytes\n";
		}
		else
			std::cout << std::setw(level * 3) << "" << " [?]" << filenameStr << '\n';
	}
}

TEST(cpp_17_in_detail_part2, filesystem_demo)
{
	try {
		const fs::path pathToShow{ fs::current_path() };

		std::cout << "listing files in the directory: "
			<< fs::absolute(pathToShow).string() << '\n';

		DisplayDirectoryTree(pathToShow);
	}
	catch (const fs::filesystem_error& err) {
		std::cerr << "filesystem error! " << err.what() << '\n';

	}
	catch (const std::exception& ex) {
		std::cerr << "general exception: " << ex.what() << '\n';
	}
}


TEST(cpp_17_in_detail_part2, filesystem_path)
{
	const filesystem::path testPath{ fs::current_path() };
	if (testPath.has_root_name())
		cout << "root_name() = " << testPath.root_name() << '\n';
	else
		cout << "no root-name\n";
	if (testPath.has_root_directory())
		cout << "root directory() = " << testPath.root_directory() << '\n';
	else
		cout << "no root-directory\n";
	if (testPath.has_root_path())
		cout << "root_path() = " << testPath.root_path() << '\n';
	else
		cout << "no root-path\n";
	if (testPath.has_relative_path())
		cout << "relative_path() = " << testPath.relative_path() << '\n';
	else
		cout << "no relative-path\n";
	if (testPath.has_parent_path())
		cout << "parent_path() = " << testPath.parent_path() << '\n';
	else
		cout << "no parent-path\n";
	if (testPath.has_filename())
		cout << "filename() = " << testPath.filename() << '\n';
	else
		cout << "no filename\n";
	if (testPath.has_stem())
		cout << "stem() = " << testPath.stem() << '\n';
	else
		cout << "no stem\n";
	if (testPath.has_extension())
		cout << "extension() = " << testPath.extension() << '\n';
	else
		cout << "no extension\n";

	int i = 0;
	for (const auto& part : testPath)
		cout << "path part: " << i++ << " = " << part << '\n';	
}

TEST(cpp_17_in_detail_part2, filesystem_path_composition)
{
	filesystem::path p2{ fs::current_path() };

	p2 /= "user";
	p2 += "data";

	std::cout << p2 << "\n";
}

std::ostream& operator<< (std::ostream& stream, fs::perms p)
{
	stream << "owner: "
		<< ((p & fs::perms::owner_read) != fs::perms::none ? "r" : "-")
		<< ((p & fs::perms::owner_write) != fs::perms::none ? "w" : "-")
		<< ((p & fs::perms::owner_exec) != fs::perms::none ? "x" : "-");
	stream << " group: "
		<< ((p & fs::perms::group_read) != fs::perms::none ? "r" : "-")
		<< ((p & fs::perms::group_write) != fs::perms::none ? "w" : "-")
		<< ((p & fs::perms::group_exec) != fs::perms::none ? "x" : "-");
	stream << " others: "
		<< ((p & fs::perms::others_read) != fs::perms::none ? "r" : "-")
		<< ((p & fs::perms::others_write) != fs::perms::none ? "w" : "-")
		<< ((p & fs::perms::others_exec) != fs::perms::none ? "x" : "-");
	return stream;
}

TEST(cpp_17_in_detail_part2, filesystem_directory_iterator)
{
	auto now = fs::file_time_type::clock::now();
	for( auto const &entry : fs::directory_iterator(fs::current_path()))
	{
		auto filetime = fs::last_write_time(entry.path());
		const auto toNow = now - filetime;
		const auto elapsedSec = chrono::duration_cast<chrono::seconds>(toNow).count();
		std::cout << entry.path().filename() << " elasped " << elapsedSec << " seconds \n";

		std::cout << fs::status( entry.path() ).permissions() << "\n";		
	}

	for( auto const &entry : fs::recursive_directory_iterator(fs::current_path().parent_path()))
	{
		// std::cout << entry.path().filename() << "\n";
	}
}

[[nodiscard]] std::string GetFileContents(const fs::path& filePath)
{
	std::ifstream inFile{ filePath, std::ios::in | std::ios::binary };
	if (!inFile)
		throw std::runtime_error("Cannot open " + filePath.string());
	const auto fsize = fs::file_size(filePath);
	if (fsize > std::numeric_limits<size_t>::max())
		throw std::runtime_error("file is too large to fit into size_t! " + filePath.string());

	std::string str(static_cast<size_t>(fsize), 0);
	inFile.read(str.data(), str.size());
	if (!inFile)
		throw std::runtime_error("Could not read the full contents from " + filePath.string());

	return str;
}

TEST(cpp_17_in_detail_part2, filesystem_file_size)
{
	{
		std::ofstream temp("from.bullshit.job", ios_base::out);
		temp << R"(
You cannot be magnificent without an entourage. 
And for the truly magnificent, the very uselessness 
of the uniformed retainers hovering around you is 
the greatest testimony to your greatness.
)";
	}

	std::string content = GetFileContents( fs::path("./from.bullshit.job"));
	EXPECT_TRUE( content.find( "an entourage") != string::npos );
}


struct FileEntry
{
	fs::path mPath;
	uintmax_t mSize{ 0 };
	static FileEntry Create(const fs::path& filePath) {
		return FileEntry{ filePath, fs::file_size(filePath) };
	}
	friend bool operator < (const FileEntry& a, const FileEntry& b) noexcept {
		return a.mSize < b.mSize;
	}
};
std::vector<FileEntry> CollectFiles(const fs::path& inPath)
{
	std::vector<fs::path> paths;
	if (fs::exists(inPath) && fs::is_directory(inPath))
	{
		std::filesystem::recursive_directory_iterator dirpos{ inPath };
		std::copy_if(begin(dirpos), end(dirpos), std::back_inserter(paths),
			[](const fs::directory_entry& entry) {
				return entry.is_regular_file();
			}
		);
	}
	std::vector<FileEntry> files(paths.size());
	std::transform(paths.cbegin(), paths.cend(), files.begin(), FileEntry::Create);
	return files;
}

TEST(cpp_17_in_detail_part2, filesystem_regex_match)
{
	auto files = CollectFiles(fs::current_path());
	std::sort(files.begin(), files.end());

	const std::regex reg("private_.*.pem");
	for (auto& entry : files)
	{
		const auto strFileName = entry.mPath.filename().string();
		if (std::regex_match(strFileName, reg))
			std::cout << strFileName << "\tsize: " << entry.mSize << '\n';
	}
}
