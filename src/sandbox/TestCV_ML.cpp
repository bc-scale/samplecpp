#include "stdafx.h"

#include "Fixture.hpp"
#include <string>
#include "opencv/cv.hpp"
#include <math.h>
#include <iostream>
#include <fstream>
#include <list>
#include <Poco/StringTokenizer.h>
#include <opencv2/ml/ml.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>
#include <iomanip>
using namespace std;
using namespace Poco;
using namespace cv;
using namespace cv::ml;

// Artificial Neural Network test
// Ref : An Introduction to Machine Learning, Miroslav Kubat. Chapter5
class CV_MLTest : public CppUnit::TestCase
{
public:
	CV_MLTest(const string& name) : CppUnit::TestCase(name) {}

	void print(Mat& mat, int prec)
	{
		for (int i = 0; i<mat.size().height; i++)
		{
			cout << "[";
			for (int j = 0; j<mat.size().width; j++)
			{
				cout << fixed << setw(2) << setprecision(prec) << mat.at<float>(i, j);
				if (j != mat.size().width - 1)
					cout << ", ";
				else
					cout << "]" << endl;
			}
		}
	}

	// https://stackoverflow.com/questions/37500713/opencv-image-recognition-setting-up-ann-mlp
	void testTrainXOR()
	{
		const int hiddenLayerSize = 2;
		float inputTrainingDataArray[4][2] = {
			{ 0.0, 0.0 },
			{ 0.0, 1.0 },
			{ 1.0, 0.0 },
			{ 1.0, 1.0 }
		};
		Mat inputTrainingData = Mat(4, 2, CV_32F, inputTrainingDataArray);

		float outputTrainingDataArray[4][1] = {
			{ 0.0 },
			{ 1.0 },
			{ 1.0 },
			{ 0.0 }
		};
		Mat outputTrainingData = Mat(4, 1, CV_32F, outputTrainingDataArray);

		Ptr<ANN_MLP> mlp = ANN_MLP::create();

		Mat layersSize = (Mat_<ushort>(3, 1) << 
			inputTrainingData.cols, 
			hiddenLayerSize, 
			outputTrainingData.cols);
		mlp->setLayerSizes(layersSize);

		mlp->setActivationFunction(ANN_MLP::ActivationFunctions::SIGMOID_SYM);

		TermCriteria termCrit = TermCriteria(
			TermCriteria::Type::COUNT + TermCriteria::Type::EPS,
			100000000,
			0.000000000000000001
			);
		mlp->setTermCriteria(termCrit);

		mlp->setTrainMethod(ANN_MLP::TrainingMethods::BACKPROP);

		Ptr<TrainData> trainingData = TrainData::create(
			inputTrainingData,
			SampleTypes::ROW_SAMPLE,
			outputTrainingData
			);

		mlp->train(trainingData
			/*, ANN_MLP::TrainFlags::UPDATE_WEIGHTS
			+ ANN_MLP::TrainFlags::NO_INPUT_SCALE
			+ ANN_MLP::TrainFlags::NO_OUTPUT_SCALE*/
			);

		for (int i = 0; i < inputTrainingData.rows; i++) {
			Mat sample = Mat(1, inputTrainingData.cols, CV_32F, inputTrainingDataArray[i]);
			Mat result;
			mlp->predict(sample, result);
			cout << sample << " -> ";// << result << endl;
			print(result, 0);
			cout << endl;
		}
	}

	void testScaleFactor0()
	{
		Mat data = (Mat_<float>(6, 4) <<
			1.0, 0.0, 0.0,		1.0,
			2.0, 0.0, 0.0,		2.0,
			3.0, 0.0, 0.0,		3.0,
			4.0, 0.0, 0.0,		4.0,

			1.0, 1.0, 0.0,		1.5,
			1.0, 0.0, 1.0,		0.5
		);

		Mat input = data.colRange(0, 3);
		Mat output = data.colRange(3, 4).clone();

		Ptr<ANN_MLP> mlp = ANN_MLP::create();

		Mat layersSize = (Mat_<ushort>(3, 1) << input.cols, input.cols+1, output.cols);
		mlp->setLayerSizes(layersSize);
		mlp->setActivationFunction(ANN_MLP::ActivationFunctions::SIGMOID_SYM);
		mlp->setTermCriteria(TermCriteria(
			TermCriteria::Type::COUNT + TermCriteria::Type::EPS,
			100000, 0.0000001 ));
		mlp->setTrainMethod(ANN_MLP::TrainingMethods::BACKPROP);
		Ptr<TrainData> trainingData = TrainData::create( input, SampleTypes::ROW_SAMPLE, output );

		mlp->train(trainingData);

		for (int i = 0; i < input.rows; i++) {
			Mat sample = input.row(i);
			Mat result;
			mlp->predict(sample, result);
			cout << sample << " -> ";
			print(result, 2);
		}

		mlp->save("ann.scale.temp");
	}

	void testScaleFactor1()
	{
		Ptr<ANN_MLP> mlp = ANN_MLP::load("ann.scale.temp");

		float step = 0.5f;
		for (float i = 0.f; i < 5.0f; i += step)
		{
			for (float j = 0.f; j < 2.0f; j += step)
			{
				for (float k = 0.f; k <= 1.0f; k += 1.0f)
				{
					Mat sample = (Mat_<float>(1, 3) << i, j, k);
					Mat result;
					mlp->predict(sample, result);
					cout << sample << " -> ";
					print(result, 2);
				}
			}
		}
	}

	static CppUnit::Test* CreateTest()
	{
		CppUnit::TestSuite* pSuite = new CppUnit::TestSuite("CV_MLTest");

		CppUnit_addTest(pSuite, CV_MLTest, testTrainXOR);

		CppUnit_addTest(pSuite, CV_MLTest, testScaleFactor0);
		CppUnit_addTest(pSuite, CV_MLTest, testScaleFactor1);

		return pSuite;
	}
};


static AddTestCase<CV_MLTest> s_Test;
