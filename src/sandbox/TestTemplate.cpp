#include <string>
#include <list>
#include <vector>
#include <memory>
#include <type_traits>  
#include <iostream>
#include <gtest/gtest.h>
#include <Poco/Format.h>

using namespace std;

template<typename T>
class Vector {
private:
	T* elem;
	int sz;

public:
	explicit Vector(int s) {
		elem = new T[s];
		sz = s;
	}

	T& operator[](int i)
	{
		if (i < 0 || i >= size()) 
			throw out_of_range("[i] is out of bound");
		return elem[i]; 
	}
	const T& operator[](int i) const 
	{ 
		if (i < 0 || i >= size()) 
			throw out_of_range("[i] is out of bound");
		
		return elem[i];
	}
	int size() const { return sz; }

	T* begin() const
	{
		return size() ? elem : nullptr;
	}

	T* end() const
	{
		return begin() + size();
	}
};


template<typename Container, typename Value>
Value sum(const Container& c, Value v)
{
	for (auto x : c)
		v += x;
	return v;
}

class Shape
{
public:
	virtual void draw() = 0;
	virtual void rotate( double degree ) = 0;
};

class Rect : public Shape
{
public:
	void draw() { cout << "rect"; }
	void rotate(double degree) { cout << "rotate"; }
};

class Circle : public Shape
{
public:
	void draw() { cout << "circle"; }
	void rotate(double degree) {}
};

template<typename C, typename Oper>
void for_all(C& c, Oper op)
{
	for (auto& x : c)
		op(x);
}


void foo() {
	cout << endl;
}

template<typename T>
void bar(T x)
{
	cout << x << " ";
}

template <typename T, typename ...Tail>
void foo(T head, Tail... tail)
{
	bar(head);
	foo(tail...);
}


void add_name_value(vector<string>& vs )
{
}

void add_name_value_fixed(vector<string>& vs, const string& name, const string& value)
{
	vs.push_back( Poco::format(R"("%s":"%s")", name, value) );
}

void add_name_value_fixed(vector<string>& vs, const string& name, int value)
{
	vs.push_back(Poco::format(R"("%s":%d)", name, value));
}

void add_name_value_fixed(vector<string>& vs, const string& name, double value)
{
	vs.push_back(Poco::format(R"("%s":%f)", name, value));
}

template<typename T, typename ...Tail>
void add_name_value(vector<string>& vs, const string& name, T value, Tail... tail)
{
	add_name_value_fixed(vs, name, value);
	add_name_value(vs, tail...);
}


template <class T>
T MyMax(T a, T b)
{
	return a > b ? a : b;
}

template <>
const char* MyMax(const char* a, const char* b)
{
	return strlen(a) > strlen(b) ? a : b;
}


template<bool> struct CompileTimeAssert;
template<> struct CompileTimeAssert<true> {};


template< typename T >
struct IsVoid {
	static const bool value = false;
};

template<>
struct IsVoid< void >{
	static const bool value = true;
};

template< typename T >
struct IsPointer{
	static const bool value = false;
};

template< typename T >
struct IsPointer< T* >{
	static const bool value = true;
};


struct IO {
	int addr;
	bool state;
};

struct Servo {
	int axis;
	double position;
};

struct Verify {
	template<typename T>
	static void Equal( T arg ) { throw exception("Should specialize"); }

	static void Equal(IO io)
	{
		cout << "check addr " << io.addr << " to be " << io.state << endl;
	}

	static void Equal(Servo s)
	{
		cout << "check axis " << s.axis << " position is at " << s.position << endl;
	}
};

/*
class IplImageReleasePolicy
{
public:
	static void release(IplImage* img)
	{
		cvReleaseImage(&img);
	}
};

using IplImagePtr = Poco::SharedPtr<IplImage, Poco::ReferenceCounter, IplImageReleasePolicy>;


class FILEReleasePolicy
{
public:
	static void release(FILE* fp)
	{
		fclose(fp);
	}
};

using FILEPtr = Poco::SharedPtr<FILE, Poco::ReferenceCounter, FILEReleasePolicy>;
*/

TEST(TemplateTest, testVectorContructor) 
{
	{
		Vector<int> vi(200);
		vi[0] = 123;
		EXPECT_EQ(vi[0],123);
		EXPECT_THROW( vi[200] = 321, out_of_range);
	}

	{
		Vector<string> vs(17);
		vs[3] = "Hello";
		EXPECT_EQ(vs[3] ,"Hello");
		EXPECT_THROW( string x = vs[-1], out_of_range);
	}

	{
		Vector<vector<int>> vli(45);
		vli[4] = vector<int>({ 1, 2, 3 });
		EXPECT_EQ(vli[4][1] , 2);
		EXPECT_THROW( vli[45][3], out_of_range);
	}
}

TEST(TemplateTest, testFunctionTemplate) 
{
	{
		Vector<int> vi(4);
		vi[0] = 0;
		vi[1] = 1;
		vi[2] = 2;
		vi[3] = 3;

		double ds = sum(vi, 0.0);
		EXPECT_EQ(ds , 6.0);

		int is = sum(vi, 0);
		EXPECT_EQ(is , 6);
	}

	{
		list<double> ld;
		ld.push_back(3.0);
		ld.push_back(4.0);

		double ds = sum(ld, 0.0);
		EXPECT_EQ(ds , 7.0);
	}
}

TEST(TemplateTest, testFunctionObject) 
{
	vector<unique_ptr<Shape>> v;
	v.push_back(make_unique<Rect>());
	v.push_back(make_unique<Circle>());

	for_all(v, [](unique_ptr<Shape>& s){ s->draw(); });
	for_all(v, [](unique_ptr<Shape>& s){ s->rotate(45); });
}

TEST(TemplateTest, testVariadicTemplates) 
{
	foo(1, 2, 2, "Hello");

	foo(0.2, 'c', "yuck!", 0, 1, 2);
}


TEST(TemplateTest, testFunctionSepcialization) 
{
	EXPECT_TRUE( MyMax(1, 2) == 2);

	EXPECT_TRUE( MyMax("Morning", "Afternoon") == "Afternoon");
}


TEST(TemplateTest, testCompileTimeAssert) 
{
	// CompileTimeAssert< sizeof(uint32_t) == 2  >();
	CompileTimeAssert< sizeof(uint32_t) == 4 >();

	// CompileTimeAssert< std::is_base_of<Rect, Shape>::value >();
	CompileTimeAssert< std::is_base_of<Shape, Rect>::value >();

	static_assert(std::is_base_of<Shape, Rect>::value, "Rect is a Shape");
}

TEST(TemplateTest, testSpecialization) 
{
	CompileTimeAssert< IsVoid<void>::value >();
	// CompileTimeAssert< IsVoid<int>::value >();

	CompileTimeAssert< IsPointer<Shape*>::value >();
	// CompileTimeAssert< IsPointer<Shape>::value >();

	static_assert(is_pointer<Shape*>::value, "Shape* should be a pointer");
}

TEST(TemplateTest, testVerify) 
{
	Verify::Equal(IO{ 41, true });
	Verify::Equal(Servo{ 3, 3.141592 });
}

TEST(TemplateTest, testAddNameValues) 
{
	vector<string> vs;
	add_name_value(vs, "hello", "world", "good", 42, "bad", 3.14 );
}


#if 0
	IplImagePtr doFoo(IplImagePtr image)
	{
		return IplImagePtr(cvCreateImage(cvGetSize(image), image->depth, image->nChannels));
	}

	void testIplImagePtr()
	{
		{
			IplImagePtr image = cvCreateImage(cvSize(100, 100), 8, 1);
			*image->imageData = 41;
		}

		{
			IplImagePtr image = cvLoadImage("../../../images/Core.Processing/000_IMAGE_L4_F1.bmp", CV_LOAD_IMAGE_UNCHANGED);
			IplImagePtr copied = cvCreateImage(cvGetSize(image), image->depth, image->nChannels);
			cvCopy(image, copied);
		}

		{
			IplImagePtr a = cvCreateImage(cvSize(100, 100), 8, 1);
			IplImagePtr b = a;
			IplImagePtr c = doFoo(b);
		}
	}

	void testFILEPtr()
	{
		{
			FILEPtr fp = fopen("temp.txt", "wt");
			fprintf(fp, "hello world");
		}
	}
#endif