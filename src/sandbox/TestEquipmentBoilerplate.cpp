#include "stdafx.h"

#include "Fixture.hpp"
#include "..\include\CoreSystem.h"
#include <future>
#include <thread>
#include <iostream>
#include "Poco\Net\SocketAddress.h"
#include "Poco\DateTimeFormatter.h"
#include "Poco/JSON/Object.h"
#include "Poco/Any.h"
#include "opencv/cv.hpp"


using namespace std;
using namespace enscape;
using namespace enscape::System;
using namespace Poco;
using namespace Poco::Net;
using namespace cv;

class EquipmentBoilerplateTest : public CppUnit::TestCase
{
public:
	EquipmentBoilerplateTest(const string& name) : CppUnit::TestCase(name) {}

	ISystemFactory* m_Fab;

	void setUp()
	{
		ModuleManager* mm = Fixture::GetGlobal().GetModuleManager();
		mm->Load("enscape.Core.System.dll");
		m_Fab = mm->CreateClass<ISystemFactory>("enscape::System::SystemFactory");
	}

	string IOUpdate2String(int addr, vector<int>& io )
	{
		Poco::JSON::Object j;
		j.set("cmd", "ioUpdate");
		j.set("address", addr);

		Poco::JSON::Array v;
		for (auto i : io) { v.add(i); }
		j.set("value", v);

		std::stringstream ss;
		j.stringify(ss);

		return ss.str();
	}

	string ServoUpdate2String( const vector<double>& pos )
	{
		Poco::JSON::Object j;
		j.set("cmd", "servoUpdate");

		Poco::JSON::Array v;
		for (auto p : pos)  { v.add(p); }
		j.set("value", v);

		std::stringstream ss;
		j.stringify(ss);

		return ss.str();
	}

	struct ImageHeader {
		uint32_t Type;
		uint16_t Length;
		uint16_t Width, Height, BPP;
		uint16_t RegionX, RegionY, RegionWidth, RegionHeight;
	};

	cv::Mat CreateFilledImage( int width, int height, uint32_t fillColor )
	{
		return cv::Mat(height, width, CV_8UC4, 
			cv::Scalar( 
			(fillColor >> 24) & 0xFF,
			(fillColor >> 16) & 0xFF,
			(fillColor >> 8) & 0xFF,
			(fillColor >> 0) & 0xFF
		));
	}

	vector< vector<uint8_t> > GenerateImagePacket( const cv::Mat& m )
	{
		vector < vector<uint8_t> > packets;

		int row = 0;
		while (row < m.rows )
		{
			int l = min(m.cols * (m.rows - row), 64 * 1024 / 4 );
			int r = l / m.cols;

			vector<uint8_t> packet(sizeof(ImageHeader)+ r * m.cols * 4);

			ImageHeader* header = (ImageHeader*)&packet[0];
			*header = ImageHeader {
				0x6182,
				(uint16_t)packet.size(),
				m.cols, m.rows, 32,
				0, row, m.cols, r 
			};

			copy( 
				m.datastart + row * m.cols * 4 , 
				m.datastart + (row + r) * m.cols * 4,
				&packet[sizeof(ImageHeader)]);

			row += r;

			packets.push_back(packet);
		}

		return packets;
	}

	void testEquipmentBoilerplateRemoteLoop()
	{
		const int REPEAT_COUNT = 1000000;

		future<void> p1(async([=](){

			IDatagramPoint::Ptr point = m_Fab->CreateDatagramPoint(
				SocketAddress("127.0.0.1:50000"),
				[=](IDatagramPoint* p) mutable
				{
					char buffer[MAX_PATH];
					SocketAddress sender;
					int n = p->ReceiveFrom(buffer, sizeof(buffer)-1, sender);
					buffer[n] = '\0';

					cout
						<< sender.toString() << buffer << " -> "
						<< DateTimeFormatter::format(Timestamp(), "received at %s")
						<< endl;
				});

			thread t([point]() mutable { point->RunInLoop(); });

			SocketAddress remote("127.0.0.1:41234");
			for (int i = 0; i<REPEAT_COUNT; ++i)
			{
				string msg = IOUpdate2String(0x300, vector<int>{ rand() % 2, rand() % 2, rand() % 2, rand() % 2, rand() % 2 } );
				point->SendTo(msg.data(), (int)msg.size(), remote);

				msg = IOUpdate2String(0x400, vector<int>{ rand() % 2, rand() % 2, rand() % 2, rand() % 2, rand() % 2 });
				point->SendTo(msg.data(), (int)msg.size(), remote);

				msg = ServoUpdate2String(vector<double>{ (double)(rand() % 1000), (double)(rand() % 1000), (double)(rand() % 1000) });
				point->SendTo(msg.data(), (int)msg.size(), remote);

				auto packets = GenerateImagePacket(CreateFilledImage(160, 120, (rand() | 0xFF000000)));
				for (auto packet : packets) {
					point->SendTo(packet.data(), (int)packet.size(), remote);
				}

				this_thread::sleep_for(chrono::milliseconds(1000));
			}

			t.join();
			point->StopLoop();
		}));

		try
		{
			p1.get();
		}
		catch (Poco::Exception& ex)
		{
			printf("Exception caught : %s", ex.message().c_str());
		}
	}

	static CppUnit::Test* CreateTest()
	{
		CppUnit::TestSuite* pSuite = new CppUnit::TestSuite("EquipmentBoilerplateTest");
		
		// CppUnit_addTest(pSuite, EquipmentBoilerplateTest, testEquipmentBoilerplateRemoteLoop);

		return pSuite;
	}
};


static AddTestCase<EquipmentBoilerplateTest> s_Test;
