#include <string>
#include <list>
#include <vector>
#include <memory>
#include <type_traits>  
#include <iostream>
#include <gtest/gtest.h>

using namespace std;

namespace Isolated
{

#pragma region interface

	class ISettings {
	public:
		virtual ~ISettings() {}
		virtual void Set(const string& name ) = 0;
	};

	class IClassifier {
	public:
		virtual ~IClassifier() {}
		virtual void Classify(const string& stuff) = 0;
	};

	class ITrainer {
	public:
		virtual ~ITrainer() {}
		virtual void Train(int count) = 0;
	};

#pragma endregion


#pragma region implementation

	class Settings : public ISettings {
	public:
		void Set(const string& value ) { m_Settings.push_back( value ); }

	protected:
		list<string> m_Settings;
	};

	class Classifier : public Settings, public IClassifier {
	public:
		void Classify(const string& stuff) { printf("classify %s with %s\n", stuff.c_str(), m_Settings.front().c_str()); }
	};

	class Trainer : public Settings, public ITrainer {
	public:
		void Train(int count) { printf("train %s %d times\n", m_Settings.front().c_str(), count); }
	};

#pragma endregion
}


namespace SimpleInheritance
{

#pragma region interface

	class ISettings {
	public:
		virtual ~ISettings() {}
		virtual void Set(const string& name) = 0;
	};

	class IClassifier : public ISettings {
	public:
		virtual void Classify(const string& stuff) = 0;
	};

	class ITrainer : public ISettings {
	public:
		virtual ~ITrainer() {}
		virtual void Train(int count) = 0;
	};

#pragma endregion


#pragma region implementation

	class Settings : public ISettings {
	public:
		void Set(const string& value) { m_Settings.push_back(value); }

	protected:
		list<string> m_Settings;
	};

	class Classifier : public Settings, public IClassifier {
	public:
		void Classify(const string& stuff) { printf("classify %s with %s\n", stuff.c_str(), m_Settings.front().c_str()); }
		void Set(const string& value) { Settings::Set(value); }
	};

	class Trainer : public Settings, public ITrainer {
	public:
		void Train(int count) { printf("train %s %d times\n", m_Settings.front().c_str(), count); }
		void Set(const string& value) { Settings::Set(value); }
	};

#pragma endregion
}


namespace CRTP
{

#pragma region interface

	class ISettings {
	public:
		virtual ~ISettings() {}
		virtual void Set(const string& name) = 0;
	};

	class IClassifier : public ISettings {
	public:
		virtual void Classify(const string& stuff) = 0;
	};

	class ITrainer : public ISettings {
	public:
		virtual ~ITrainer() {}
		virtual void Train(int count) = 0;
	};

#pragma endregion


#pragma region implementation

	template<typename T>
	class Settings : public T {
	public:
		void Set(const string& value) { m_Settings.push_back(value); }

	protected:
		list<string> m_Settings;
	};

	class Classifier : public Settings<IClassifier> {
	public:
		void Classify(const string& stuff) { printf("classify %s with %s\n", stuff.c_str(), m_Settings.front().c_str()); }
	};

	class Trainer : public Settings<ITrainer> {
	public:
		void Train(int count) { printf("train %s %d times\n", m_Settings.front().c_str(), count); }
	};

#pragma endregion
}

// Curiosuly Recurring Template Pattern : https://en.wikipedia.org/wiki/Curiously_recurring_template_pattern

TEST( CRTPTest, testIsolated) 
{
	using namespace Isolated;

	{
		unique_ptr<IClassifier> cls(new Classifier);

		ISettings* clsSetting = dynamic_cast<ISettings*>(cls.get());
		clsSetting->Set("World");

		cls->Classify("Hello");
	}

	{
		unique_ptr<ITrainer> tr(new Trainer);

		ISettings* trSettings = dynamic_cast<ISettings*>(tr.get());
		trSettings->Set("World");
		
		tr->Train(5);
	}
}

TEST( CRTPTest, testSimpleInheritance) 
{
	using namespace SimpleInheritance;

	{
		unique_ptr<IClassifier> cls(new Classifier);
		cls->Set("World");
		cls->Classify("Hello");
	}

	{
		unique_ptr<ITrainer> tr(new Trainer);
		tr->Set("World");
		tr->Train(5);
	}
}


TEST( CRTPTest, testCRTP) 
{
	using namespace CRTP;

	{
		unique_ptr<IClassifier> cls(new Classifier);
		cls->Set("World");
		cls->Classify("Hello");
	}

	{
		unique_ptr<ITrainer> tr(new Trainer);
		tr->Set("World");
		tr->Train(5);
	}
}
