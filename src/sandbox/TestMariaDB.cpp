#include "stdafx.h"

#include "Fixture.hpp"

#include "Poco/Stopwatch.h"
#include "Poco/Data/Session.h"
#include "Poco/Data/Transaction.h"
#include "Poco/Data/MySQL/Connector.h"

using namespace std;
using namespace enscape;
using namespace Poco;
using namespace Poco::Data;


class TestMariaDB : public CppUnit::TestCase
{
public:
	TestMariaDB(const string& name) : CppUnit::TestCase(name) {}
	~TestMariaDB() {}

	void setUp() {}

	void tearDown() {}

	void testDB0()
	{
		using namespace Poco::Data::Keywords;

		Poco::Data::MySQL::Connector::registerConnector();

		try
		{
			Session db("MySQL", "host=127.0.0.1;port=3306;user=root;password=test1234;db=battery");

			string table = "T5000";
			db << format("CREATE TABLE IF NOT EXISTS %s (", table)
				<< "recipe TEXT, lot_id TEXT, tray_id TEXT)", now;

			db << format("DELETE FROM %s", table), now;

			for (int i = 0; i < 25; ++i)
			{
				string recipe = format("R%d", i);
				string lot_id = format("L%d", i);
				string tray_id = format("T%d", i);

				db << format("INSERT INTO %s VALUES(?,?,?)", table)
					, use(recipe), use(lot_id), use(tray_id), now;
			}

			for (int i = 0; i < 25; ++i)
			{
				string recipe = format("R%d", i);
				string lot_id, tray_id;

				db << format("SELECT lot_id, tray_id FROM %s WHERE recipe=?", table),
					into(lot_id), into(tray_id), use(recipe), now;

				assert(lot_id == format("L%d", i));
				assert(tray_id == format("T%d", i));
			}
		}
		catch (Poco::Exception& ex)
		{
			failmsg(ex.message().c_str());
		}

		Poco::Data::MySQL::Connector::unregisterConnector();
	}

	void testDB1()
	{
		using namespace Poco::Data::Keywords;

		Poco::Data::MySQL::Connector::registerConnector();

		try
		{
			Session db("MySQL", "host=127.0.0.1;port=3306;user=root;password=test1234;db=battery");

			string table = "T5000";
			db << format("CREATE TABLE IF NOT EXISTS %s (", table)
				<< "recipe TEXT, lot_id TEXT, tray_id TEXT)", now;

			db << format("DELETE FROM %s", table), now;

			{
				Transaction tx(db);
				tx.transact([&](Session& session)
				{
					for (int i = 0; i < 256; ++i)
					{
						string recipe = format("R%d", i);
						string lot_id = format("L%d", i);
						string tray_id = format("T%d", i);

						session << format("INSERT INTO %s VALUES(?,?,?)", table)
							, use(recipe), use(lot_id), use(tray_id), now;
					}
				});
			}
		}
		catch (Poco::Exception& ex)
		{
			failmsg(ex.message().c_str());
		}

		Poco::Data::MySQL::Connector::unregisterConnector();
	}

	static CppUnit::Test* CreateTest()
	{
		CppUnit::TestSuite* pSuite = new CppUnit::TestSuite("TestMariaDB");

		// CppUnit_addTest(pSuite, TestMariaDB, testDB0);
		// CppUnit_addTest(pSuite, TestMariaDB, testDB1);

		return pSuite;
	}
};

static AddTestCase<TestMariaDB> s_Test;
