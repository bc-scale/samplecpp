#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <thread>
#include <future>
#include <chrono>
#include <mutex>
#include <condition_variable>
#include <queue>

using namespace std;

class TestBench
{
public:
	vector<future<void>> m_InpectionResult;

	void Grab( int grabCount )
	{
		while (grabCount-- > 0)
		{
			this_thread::sleep_for(chrono::milliseconds(30));

			printf("Grabbed %d\n", grabCount);
			m_InpectionResult.emplace_back(async(&TestBench::Inspect, this, grabCount));
		}
	}

	void Inspect( int image )
	{
		this_thread::sleep_for(chrono::milliseconds(80));
		printf("Inspected %d\n", image);
	}

};

TEST(SandBox2, test1) 
{
	GTEST_SKIP_("*** stack smashing detected ***: terminated");
	TestBench tb;
	thread grabber(&TestBench::Grab, &tb, 10);

	grabber.join();
	for (auto& f : tb.m_InpectionResult) { f.get(); }
}
