#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <optional>
#include <vector>
#include <complex>
#include <mutex>

// From C++17 In Detail by Bartlomiej Filipek


class Foo42 {
public:
	std::optional<std::string> FindUserNick()
	{
		if( is_Nick_available_ )
			return std::string( "Nick At Lounge");
		return std::nullopt;
	}

	bool is_Nick_available_ { false };
	void Show( std::string user ) {
		std::cout << user << std::endl;
	}
};

TEST(cpp_17_in_detail_part2, optional_basic)
{
	Foo42 f1;
	std::optional<std::string> nick = f1.FindUserNick();
	if( nick ) {
		f1.Show( *nick );
		f1.Show( nick.value() );
	}

	f1.Show( nick.value_or( "Nick not present") );
	
	f1.is_Nick_available_ = true;
	nick = f1.FindUserNick();
	if( nick ) {
		f1.Show( nick.value() );
	}

	if( auto nick1 = f1.FindUserNick(); nick1 )
	{
		std::cout << *nick << std::endl;
	}
}


TEST(cpp_17_in_detail_part2, optional_creation)
{
	// empty:
	std::optional<int> oEmpty;
	std::optional<float> oFloat = std::nullopt;
	// direct:
	std::optional<int> oInt(10);
	std::optional oIntDeduced(10); // deduction guides
	// make_optional
	auto oDouble = std::make_optional(3.0);
	auto oComplex = std::make_optional<std::complex<double>>(3.0, 4.0);
	// in_place
	std::optional<std::complex<double>> o7{ std::in_place, 3.0, 4.0 };
	// will call vector with direct init of {1, 2, 3}
	std::optional<std::vector<int>> oVec(std::in_place, { 1, 2, 3 });
	// copy from other optional:
	auto oIntCopy = oInt;

	std::optional<std::string> ostr{"Hello World"};
	std::optional<int> oi{10};
}

class Foo43 {
public:
	Foo43() : name_("Default") { std::cout << "Foo43()" << std::endl; }
	~Foo43() { std::cout << "~Foo43()" << std::endl; }
    Foo43(const Foo43& rhs) : name_(rhs.name_) { std::cout << "Foo43(const Foo43&)" << std::endl; }
    Foo43& operator=(const Foo43& rhs) { std::cout << "Foo43& operator=(const Foo43&)" << std::endl; name_=rhs.name_; return *this; }
	std::string name_;
};

TEST(cpp_17_in_detail_part2, optional_default_construction)
{
	std::optional<Foo43> u0; // empty optional
	EXPECT_EQ( u0, std::nullopt );

	std::optional<Foo43> u1{}; // also empty
	EXPECT_EQ( u1, std::nullopt );

	{
		// optional with default constructed object:
		std::optional<Foo43> u2{Foo43()};
		EXPECT_EQ( (*u2).name_, "Default" );		
	}

	{
		auto u2 = std::make_optional<Foo43>();
		EXPECT_EQ( (*u2).name_, "Default" );		
	}

	{
		std::optional<Foo43> u3{std::in_place};
		EXPECT_EQ( (*u3).name_, "Default" );
	}

	{
		std::optional<std::mutex> m1{};
		EXPECT_EQ( m1, std::nullopt );
	}

	{
		// std::optional<std::mutex> m2{ std::mutex() }; // compile error
	}

	{
		std::optional<std::mutex> m3{ std::in_place }; 
		EXPECT_NE( m3, std::nullopt );
	}
}

struct Point {
	Point(int a, int b) : x(a), y(b) {}
	int x, y;
};

TEST(cpp_17_in_detail_part2, optional_with_arguments)
{
	std::optional<Point> opt{ std::in_place, 42, 24 };
	auto opt1 = std::make_optional<Point>( 42, 24 );
}


class UserName
{
public:
	explicit UserName(std::string str) : mName(std::move(str))
	{
		std::cout << "UserName::UserName('" << mName << "')\n";
	}
	~UserName()
	{
		std::cout << "UserName::~UserName('" << mName << "')\n";
	}
private:
	std::string mName;
};

TEST(cpp_17_in_detail_part2, optional_operations)
{
	{
		std::optional<UserName> oEmpty;
		// emplace:
		oEmpty.emplace("Steve");
		// calls ~Steve and creates new Mark:
		oEmpty.emplace("Mark");

		// reset so it's empty again
		oEmpty.reset(); // calls ~Mark
		// same as:
		//oEmpty = std::nullopt;
		// assign a new value:
		oEmpty.emplace("Fred");
		oEmpty = UserName("Joe");
	}

	{
		std::optional<int> oEmpty;
		std::optional<int> oTwo(2);
		std::optional<int> oTen(10);
		std::cout << std::boolalpha;
		std::cout << (oTen > oTwo) << '\n';
		std::cout << (oTen < oTwo) << '\n';
		std::cout << (oEmpty < oTwo) << '\n';
		std::cout << (oEmpty == std::nullopt) << '\n';
		std::cout << (oTen == 10) << '\n';
	}
}

using namespace std;
class UserRecord
{
public:
	UserRecord(string name, optional<string> nick, optional<int> age)
		: mName{ move(name) }, mNick{ move(nick) }, mAge{ age }
	{
	}
	friend ostream& operator << (ostream& stream, const UserRecord& user);
private:
	string mName;
	optional<string> mNick;
	optional<int> mAge;
};

ostream& operator << (ostream& os, const UserRecord& user)
{
	os << user.mName;
	if (user.mNick)
		os << ' ' << *user.mNick;
	if (user.mAge)
		os << ' ' << "age of " << *user.mAge;
	return os;
}

TEST(cpp_17_in_detail_part2, optional_example1)
{
	UserRecord tim{ "Tim", "SuperTim", 16 };
	UserRecord nano{ "Nathan", nullopt, nullopt };
	cout << tim << '\n';
	cout << nano << '\n';
}

// Don't optional<bool>
// Don't optional<int*>