#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <vector>
#include <complex>
#include <mutex>
#include <any>
// From C++17 In Detail by Bartlomiej Filipek

using namespace std;

TEST(cpp_17_in_detail_part2, any_basic)
{
	std::any a(12);
	// set any value:
	a = std::string("Hello!");
	a = 16;
	// reading a value:
	// we can read it as int
	std::cout << std::any_cast<int>(a) << '\n';
	// but not as string:
	try
	{
		std::cout << std::any_cast<std::string>(a) << '\n';
	}
	catch (const std::bad_any_cast& e)
	{
		std::cout << e.what() << '\n';
	}
	// reset and check if it contains any value:
	a.reset();
	if (!a.has_value())
	{
		std::cout << "a is empty!" << '\n';
	}
	// you can use it in a container:
	std::map<std::string, std::any> m;
	m["integer"] = 10;
	m["string"] = std::string("Hello World");
	m["float"] = 1.0f;
	for (auto& [key, val] : m)
	{
		if (val.type() == typeid(int))
			std::cout << "int: " << std::any_cast<int>(val) << '\n';
		else if (val.type() == typeid(std::string))
			std::cout << "string: " << std::any_cast<std::string>(val) << '\n';
		else if (val.type() == typeid(float))
			std::cout << "float: " << std::any_cast<float>(val) << '\n';
	}
}

class Foo45 {
public: 
	Foo45( int a, int b ) : aa(a), bb(b) {}  
	int aa, bb;

	void Print() const { std::cout << aa << ", " << bb << "\n"; }
};

TEST(cpp_17_in_detail_part2, any_creation)
{
	// default initialisation:
	std::any a;
	assert(!a.has_value());
	// initialisation with an object:
	std::any a2{ 10 }; // int
	std::any a3{ Foo45{10, 11} };
	// in_place:
	std::any a4{ std::in_place_type<Foo45>, 10, 11 };
	std::any a5{ std::in_place_type<std::string>, "Hello World" };
	// make_any
	std::any a6 = std::make_any<std::string>( "Hello World" );

	a.emplace<float>(100.5f);
	a.emplace<std::vector<int>>({10, 11, 12, 13});
	a.emplace<Foo45>(10, 11);	

	try
	{
		std::any_cast<Foo45&>(a).Print();
		std::any_cast<Foo45&>(a).aa = 11;
		std::any_cast<Foo45&>(a).Print();
		std::any_cast<int>(a);
	}
	catch(const std::bad_cast& e)
	{
		std::cerr << e.what() << '\n';
	}
	
	int *pa = std::any_cast<int>(&a);
	EXPECT_TRUE( pa == nullptr );

	if( Foo45* pt = std::any_cast<Foo45>(&a); pt )
	{
		pt->aa = 12;
		std::any_cast<Foo45>(a).Print();
	}

	std::cout << "sizeof(std::any)= " << sizeof(std::any) << std::endl;
}

struct property
{
	property();
	property( const std::string& name, const std::any& value) : name(name), value(value) {}
	std::string name;
	std::any value;
};


TEST(cpp_17_in_detail_part2, any_property)
{
	property p1{ "hello", "world" };
	property p2{ "answer", 42 };
	property p3{ "pi", 3.141592 };
	property p4{ "user", Foo45{ 24, 42 } };
}

// std::any is not a template class
// if you knows the types, use std::variant