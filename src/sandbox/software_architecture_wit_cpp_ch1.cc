#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <vector>
#include <memory>
#include <chrono>
#include <variant>

// From Software Architecture with C++ - Adrian Ostrowski, Piotr Gaczkowski

using namespace std;

TEST(software_architecture_wit_cpp_ch1, type_safe_feature)
{
	using namespace std::literals::chrono_literals;

	struct Duration { 
		std::chrono::milliseconds millis_;
	};

	auto d = Duration{};
	d.millis_ = 100ms;

	auto timeout = 1s;
	d.millis_ = timeout;
}

int count_char( std::string_view str, char ch ) {
	return std::count( std::begin(str), std::end(str), ch);
}

TEST(software_architecture_wit_cpp_ch1, algorithms)
{
	auto e_count = count_char( 
		"Modifying and reading a value at the same instant of time is referred to as a simulation race condition.", 'e');

	EXPECT_EQ( e_count, 9);
}

TEST(software_architecture_wit_cpp_ch1, SOLID)
{
	auto SOLID = R"(
Single Responsibility - each code unit should have exactly one responsibility
Open-Closed - open for extension but closed for modification
Liskov Substitution - if a method work for a base, it should work for any of its derived object
Interface segregation - no client should be forced to depend on methods that it does not use
Dependency Injection - high level modules should not depends on lower-level ones. both should depend on abstractions
	)";

	std::cout << SOLID;
}


class Foo1 {
public:
	Foo1( int f, int s ) : first_(f), second_(s) {}	
	int first_;
	int second_;

private:
	int secretThirdMember_ = 42;
	friend std::ostream & operator <<( std::ostream& stream, const Foo1& foo1 );
};

std::ostream & operator <<( std::ostream& stream, const Foo1& foo1 )
{
	stream << foo1.first_ << ", ";	
	stream << foo1.second_ << ", ";	
	stream << foo1.secretThirdMember_;
	return stream;	
}

TEST(software_architecture_wit_cpp_ch1, open_closed)
{
	Foo1 foo1 { 1, 2 };
	std::cout << foo1 <<  std::endl;
}

namespace breaking_Liskov_interface_segregation {

class IFoodProcessor { 
public:
	virtual ~IFoodProcessor() = default;
	virtual void blend() = 0;

	// later on
	virtual void slice() = 0;
	virtual void dice() = 0;
};

class Blender : public IFoodProcessor {
public:
	void blend() override {}
	void slice() override { throw std::logic_error("!!! breaking Liskov, interface"); }
	void dice() override { throw std::logic_error("!!! breaking Liskov, interface"); }
};

class AwasomeFoodProcessor : public IFoodProcessor {
public:
	void blend() override {}
	void slice() override {}
	void dice() override {}
};

}

namespace interface_segregation {

class IBlender { 
public:
	virtual ~IBlender() = default;
	virtual void blend() = 0;
};

class ICutter { 
public:
	virtual ~ICutter() = default;
	virtual void slice() = 0;
	virtual void dice() = 0;
};

class Blender : public IBlender {
public:
	void blend() override {}
};

class AwasomeFoodProcessor : public IBlender, ICutter {
public:
	void blend() override {}
	void slice() override {}
	void dice() override {}
};

}

TEST(software_architecture_wit_cpp_ch1, interface_segregation )
{
	{
		using namespace breaking_Liskov_interface_segregation;

		Blender blender;
		blender.blend();
		// blender.slice();  // crash !!!

		AwasomeFoodProcessor afp;
		afp.dice();
		afp.slice();
		afp.blend();
	}

	{
		using namespace interface_segregation;

		Blender blender;
		blender.blend();

		AwasomeFoodProcessor afp;
		afp.dice();
		afp.slice();
		afp.blend();
	}
}

namespace simple_approach {

class FrontEndDeveloper {
public:
	void developFrontEnd() {}
};

class BackendEndDeveloper {
public:
	void developBackEnd() {}
};

class Project {
public:
	void deliver() {
		fed_.developFrontEnd();
		bed_.developBackEnd();
	}
private:
	FrontEndDeveloper fed_;
	BackendEndDeveloper bed_;
};

}

namespace decouple_using_virtual {

class Developer {
public:
	virtual ~Developer() = default;
	virtual void develop() = 0;
};

class FrontEndDeveloper : public Developer {
public:
	void develop() override { developFrontEnd(); } 
private:
	void developFrontEnd() {}	
};

class BackEndDeveloper : public Developer {
public:
	void develop() override { developBackEnd(); } 
private:
	void developBackEnd() {}	
};

class Project {
public:
	using Developers = std::vector<std::unique_ptr<Developer>>;
	
	explicit Project( Developers developers) : developers_( std::move(developers) ) {}
	
	void deliver() { 
		for( auto &d : developers_ ) { 
			d->develop();
		} 
	}

private:
	Developers developers_;
};

}


namespace variadic_template {

class FrontEndDeveloper {
public:
	void develop() { developFrontEnd(); } 
private:
	void developFrontEnd() {}	
};

class BackEndDeveloper {
public:
	void develop() { developBackEnd(); } 
private:
	void developBackEnd() {}	
};

template<typename... Devs>
class Project {
public:
	using Developers = std::vector<std::variant<Devs...>>;
	
	explicit Project( Developers developers) : developers_( std::move(developers) ) {}
	
	void deliver() { 
		for( auto &dev : developers_ ) { 
			std::visit( [](auto& d) { d.develop(); }, dev ); 
		} 
	}

private:
	Developers developers_;
};

}



TEST(software_architecture_wit_cpp_ch1, dependency_inversion )
{
	{
		using namespace simple_approach;

		Project p1;
		p1.deliver();
	}

	{
		using namespace decouple_using_virtual;

		Project::Developers developers;
		developers.push_back( std::make_unique<FrontEndDeveloper>() );
		developers.push_back( std::make_unique<BackEndDeveloper>() );
		
		Project p1( std::move(developers) );
		p1.deliver();
	}

	{
		using namespace variadic_template;

		using MyProject = Project<FrontEndDeveloper,BackEndDeveloper>;
		auto alice = FrontEndDeveloper();
		auto bob = BackEndDeveloper();
		auto new_project = MyProject{ {alice, bob} }; 
		new_project.deliver();
	}
}


TEST(software_architecture_wit_cpp_ch1, DRY)
{
	std::cout << "Don't Repeat Yourself";
}
