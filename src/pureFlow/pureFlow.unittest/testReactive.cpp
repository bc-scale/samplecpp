#include "pch.h"

#include <string>
#include <future>
#include <algorithm>
#include <vector>
#include <deque>
#include <functional>

using namespace std;

template < typename T>
class values {
public:
	using value_type = T;
	explicit values( std::initializer_list<T> v ) : m_values( v ) {}

	template< typename EmitFunction>
	void set_message_handler(EmitFunction emit)
	{
		m_emit = emit;
        std::for_each(m_values.cbegin(), m_values.cend(), [&](T v) { m_emit(move(v)); });
	}

private:
	std::vector<T> m_values;
	std::function<void(T&&)> m_emit;
};


TEST( TestReactive, testStream) 
{
}


std::pair<size_t, std::string> func1(std::future<std::pair<size_t, std::string>>&& future_input)
{
	const auto input = future_input.get();
	std::this_thread::sleep_for(std::chrono::milliseconds(900));
	std::string output(input.second + " func1");
	return std::make_pair(input.first, output);
}

std::pair<size_t, std::string> func2(std::future<std::pair<size_t, std::string>>&& future_input)
{
	const auto input = future_input.get();
	std::this_thread::sleep_for(std::chrono::milliseconds(950));
	std::string output(input.second + " func2");
	return std::make_pair(input.first, output);
}

void visualize(std::future<std::pair<size_t, std::string>>&& future_input,
    const std::chrono::time_point<std::chrono::high_resolution_clock>& start_time,
    std::atomic<unsigned long>& current_idx)
{
    const auto input = future_input.get();
    size_t this_idx = input.first;
    while (this_idx != current_idx.load())
    {
        std::this_thread::sleep_for(std::chrono::microseconds(1));
    }

    std::cout << "Sample " << this_idx << " output: '" << input.second << "' finished at " <<
        std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::high_resolution_clock::now() - start_time).count() << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    current_idx.store(current_idx.load() + 1);
}


TEST(TestReactive, testAsync)
{
    const auto start_time = std::chrono::high_resolution_clock::now();

    size_t pipeline_depth = 2;
    std::deque<std::future<void>> visualize_futures;
    std::atomic<unsigned long> current_idx{ 0 };
    for (size_t idx = 0; idx < 5; idx++)
    {
        auto input_str = std::string("input_string_") + std::to_string(idx);
        std::promise<std::pair<size_t, std::string> > promise_0;
        auto future_0 = promise_0.get_future();
        promise_0.set_value(std::make_pair(idx, input_str));
        auto future_1 = std::async(std::launch::async, &func1, std::move(future_0));
        auto future_2 = std::async(std::launch::async, &func2, std::move(future_1));
        auto future_vis = std::async(std::launch::async, &visualize, std::move(future_2),
            std::ref(start_time), std::ref(current_idx));
        visualize_futures.push_back(std::move(future_vis));
        if (visualize_futures.size() > pipeline_depth)
        {
            visualize_futures.pop_front();
        }
        std::cout << "Enqueued sample: " << idx << std::endl;
    }

    std::cout << "Waiting to finish..." << std::endl;
    for (auto& fut : visualize_futures)
    {
        fut.get();
    }
    std::cout << "Finished!" << std::endl;
}

// https://stackoverflow.com/questions/46656699/modeling-a-pipeline-in-c-with-replaceable-stages

using namespace std;

template<class...Ts>
struct sink : std::function<void(Ts...)> {
    using std::function<void(Ts...)>::function;
};

template<class...Ts>
using source = sink<sink<Ts...>>;

template<class In, class Out>
using process = sink< source<In>, sink<Out> >;

template<class In, class Out>
sink<In> operator|(process< In, Out > a, sink< Out > b) {
    return [a, b](In in) {
        a([&in](sink<In> s) mutable { s(std::forward<In>(in)); }, b);
    };
}

template<class In, class Out>
source<Out> operator|(source< In > a, process< In, Out > b) {
    return [a, b](sink<Out> out) {
        b(a, out);
    };
}

template<class In, class Mid, class Out>
process<In, Out> operator|(process<In, Mid> a, process<Mid, Out> b) {
    return [a, b](source<In> in, sink<Out> out) {
        a(in, b | out); // or b( in|a, out )
    };
}

template<class...Ts>
sink<> operator|(source<Ts...> a, sink<Ts...> b) {
    return[a, b] { a(b); };
}

process<char, char> to_upper = [](source<char> in, sink<char> out) {
    in([&out](char c) { out(std::toupper(c)); });
};

process<char, char> with_new_line = [](source<char> in, sink<char> out) {
    in([&out](char c) { out(c); out('\n'); });
};

source<char> hello_world = [ptr = "hello world"](sink<char> s){
    for (auto it = ptr; *it; ++it) { s(*it); }
};

sink<char> print = [](char c) {std::cout << c; };

TEST(TestReactive, testPipeline)
{
    auto prog = hello_world | to_upper | with_new_line | print;
    prog();
}