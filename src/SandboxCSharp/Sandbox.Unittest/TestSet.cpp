#include "pch.h"

#include <set>

// https://stackoverflow.com/questions/2620862/using-custom-stdset-comparator

struct Defect
{
	int X, Y;
	int Judge;

	Defect() : Defect(0,0,0) {}

	Defect(int x, int y, int judge)
	{
		X = x; Y = y; Judge = judge;
	}

	bool operator()(const Defect& d1, const Defect& d2) const 
	{
		if (d1.X != d2.X)
			return d1.X < d2.X;
		else if ( d1.Y != d2.Y )
			return d1.Y < d2.Y;
		else
			return d1.Judge < d2.Judge;
	}
};


using namespace std;

TEST(TestSet, TestOldOperator) 
{
	set< Defect, Defect > defects;
	defects.insert(Defect(2, 3, 4));
	defects.insert(Defect(1, 2, 3));
	defects.insert(Defect(1, 3, 1));
	
	vector<Defect> vdefects(defects.begin(), defects.end());
	EXPECT_EQ(vdefects[0].X, 1);
	EXPECT_EQ(vdefects[0].Y, 2);
	EXPECT_EQ(vdefects[1].X, 1);
	EXPECT_EQ(vdefects[1].Y, 3);
	EXPECT_EQ(vdefects[2].X, 2);
	EXPECT_EQ(vdefects[2].Y, 3);
}


TEST(TestSet, TestLambda)
{
	auto cmp = [](const Defect& a, const Defect& b) {
		if (a.X != b.X)
			return a.X < b.X;
		else
			return a.Y < b.Y;
	};
	set< Defect, decltype(cmp) > defects(cmp);
	defects.insert(Defect(2, 3, 4));
	defects.insert(Defect(1, 2, 3));
	defects.insert(Defect(1, 3, 1));

	vector<Defect> vdefects(defects.begin(), defects.end());
	EXPECT_EQ(vdefects[0].X, 1);
	EXPECT_EQ(vdefects[0].Y, 2);
	EXPECT_EQ(vdefects[1].X, 1);
	EXPECT_EQ(vdefects[1].Y, 3);
	EXPECT_EQ(vdefects[2].X, 2);
	EXPECT_EQ(vdefects[2].Y, 3);
}

bool DefectCompare(const Defect& a, const Defect& b) 
{
	if (a.X != b.X)
		return a.X < b.X;
	else
		return a.Y < b.Y;
}

TEST(TestSet, TestGlobalCompare)
{
	set< Defect, decltype(&DefectCompare) > defects(&DefectCompare);
	defects.insert(Defect(2, 3, 4));
	defects.insert(Defect(1, 2, 3));
	defects.insert(Defect(1, 3, 1));

	vector<Defect> vdefects(defects.begin(), defects.end());
	EXPECT_EQ(vdefects[0].X, 1);
	EXPECT_EQ(vdefects[0].Y, 2);
	EXPECT_EQ(vdefects[1].X, 1);
	EXPECT_EQ(vdefects[1].Y, 3);
	EXPECT_EQ(vdefects[2].X, 2);
	EXPECT_EQ(vdefects[2].Y, 3);
}

#include <type_traits>
using Cmp = std::integral_constant<decltype(&DefectCompare), &DefectCompare>;

TEST(TestSet, TestIntegralConstant)
{
	set< Defect, Cmp> defects;
	defects.insert(Defect(2, 3, 4));
	defects.insert(Defect(1, 2, 3));
	defects.insert(Defect(1, 3, 1));

	vector<Defect> vdefects(defects.begin(), defects.end());
	EXPECT_EQ(vdefects[0].X, 1);
	EXPECT_EQ(vdefects[0].Y, 2);
	EXPECT_EQ(vdefects[1].X, 1);
	EXPECT_EQ(vdefects[1].Y, 3);
	EXPECT_EQ(vdefects[2].X, 2);
	EXPECT_EQ(vdefects[2].Y, 3);
}

/*
// Not working at VS2019
TEST(TestSet, TestModern20)
{
	set< Defect, decltype([](const auto& a, const auto& b) {
		if (a.X != b.X)
			return a.X < b.X;
		else
			return a.Y < b.Y;
	}) > defects;
	defects.insert(Defect(2, 3, 4));
	defects.insert(Defect(1, 2, 3));
	defects.insert(Defect(1, 3, 1));

	vector<Defect> vdefects(defects.begin(), defects.end());
	EXPECT_EQ(vdefects[0].X, 1);
	EXPECT_EQ(vdefects[0].Y, 2);
	EXPECT_EQ(vdefects[1].X, 1);
	EXPECT_EQ(vdefects[1].Y, 3);
	EXPECT_EQ(vdefects[2].X, 2);
	EXPECT_EQ(vdefects[2].Y, 3);
}
*/