#ifndef __QVTEST_BENCH_HPP__
#define __QVTEST_BENCH_HPP__

#include <QQuickView>
#include <QQuickWidget>
#include <QQmlEngine>
#include <QQmlContext>
#include "AppRunner.hpp"

template< typename TView>
struct QMLTestBenchBase
{
    QMLTestBenchBase( AppRunner* app, const char *mainqml )
        : app_(app)
    {
        app->wait( [this, mainqml]() {
            view_ = new TView(QUrl::fromLocalFile(mainqml));
            view_->show();
        });
    }

    template< typename TPrefixFn >
    QMLTestBenchBase( AppRunner* app, TPrefixFn&& setupContextFn, const char *mainqml)
        : app_(app)
    {
        app->wait( [this, mainqml, &setupContextFn]() {
            view_ = new TView();
            setupContextFn( view_ );
            view_->setSource(QUrl::fromLocalFile(mainqml));
            view_->show();
        });
    }    

    ~QMLTestBenchBase()
    {
        app_->wait( [this](){ 
            delete view_; 
        }); 
    }

    AppRunner* app_;
    TView *view_ = nullptr;
};

using QMLTestBench = QMLTestBenchBase<QQuickView>;
using QMLTestWidgetBench = QMLTestBenchBase<QQuickWidget>;


#endif