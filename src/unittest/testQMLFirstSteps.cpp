#include "gtest/gtest.h"
#include "Fixture.h"
#include <QQmlApplicationEngine>
#include <QQuickView>
#include <fstream>

using namespace std;

// https://doc.qt.io/qt-5/qmlfirststeps.html

TEST( testQMLFirstSteps, helloWorld ) 
{
    auto app = Fixture::instance()->appRunner();

    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9            
Rectangle {
    width: 200; height: 100; color: "red"
    Text {
        anchors.centerIn: parent
        text: "Hello, World!"
    }
})";
    }

    QQuickView *view; 
    app->wait( [&]() {
        view = new QQuickView(QUrl::fromLocalFile("main.qml"));
        view->show();
    });

    this_thread::sleep_for( 100ms );
    app->wait( [&](){ delete view; }); 
}

TEST( testQMLFirstSteps, scrollview ) 
{
    auto app = Fixture::instance()->appRunner();

    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9       
import QtQuick.Controls 2.2     
ScrollView {
    anchors.fill: parent
    ListView {
        width: parent.width
        model: 20
        delegate: ItemDelegate {
            text: "Item " + (index + 1)
            width: parent.width
        }
    }
})";
    }

    QQuickView *view; 
    app->wait( [&]() {
        view = new QQuickView(QUrl::fromLocalFile("main.qml"));
        view->show();
    });

    this_thread::sleep_for( 100ms );
    app->wait( [&](){ delete view; }); 
}

TEST( testQMLFirstSteps, mouseinput ) 
{
    auto app = Fixture::instance()->appRunner();

    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9       
import QtQuick.Controls 2.2
import Qt.labs.handlers 1.0

Rectangle {
    width: 200; height: 100; color: "red"
    Text {
        anchors.centerIn: parent
        text: "Hello, World!"
    }
    TapHandler { onTapped: parent.color = "blue" }
})";
    }

    QQuickView *view; 
    app->wait( [&]()
    {
        view = new QQuickView(QUrl::fromLocalFile("main.qml"));
        view->show();
    });

    this_thread::sleep_for( 10ms );
    app->wait( [&](){ delete view; }); 
}

TEST( testQMLFirstSteps, keyboradInput ) 
{
    auto app = Fixture::instance()->appRunner();

    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9       
import QtQuick.Controls 2.2
import Qt.labs.handlers 1.0

Rectangle {
    width: 200; height: 100; color: "red"; focus: true
    Text { anchors.centerIn: parent; text: "Hello, World!" }
    Keys.onPressed: {
        if (event.key == Qt.Key_Return) {
            color = "blue"; event.accepted = true;
        }
    }
})";
    }

    QQuickView *view; 
    app->wait( [&]() {
        view = new QQuickView(QUrl::fromLocalFile("main.qml"));
        view->show();
    });

    this_thread::sleep_for( 10ms );
    app->wait( [&](){ delete view; }); 
}

TEST( testQMLFirstSteps, propertyBinding ) 
{
    auto app = Fixture::instance()->appRunner();

    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9       
import QtQuick.Controls 2.2
Rectangle {
    width: 400; height: 200
    Rectangle { width:parent.width/2;  height:parent.height; color:"blue"; }
    Rectangle { width:parent.width/2;  height:parent.height; x:parent.width/2; color:"red"; }
})";
    }

    QQuickView *view; 
    app->wait( [&]() {
        view = new QQuickView(QUrl::fromLocalFile("main.qml"));
        view->show();
    });

    this_thread::sleep_for( 10ms );
    app->wait( [&](){ delete view; }); 
}

TEST( testQMLFirstSteps, animations ) 
{
    GTEST_SKIP_("causing segmentation fault occasionally");

    auto app = Fixture::instance()->appRunner();

    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9       
import QtQuick.Controls 2.2
Rectangle {
    color: "lightgray"; width: 200; height: 200
    property int animatedValue: 0
    SequentialAnimation on animatedValue {
        loops: Animation.Infinite
        PropertyAnimation { to: 150; duration: 1000 }
        PropertyAnimation { to: 0; duration: 1000 }
    }
    Text { anchors.centerIn: parent; text: parent.animatedValue }
})";
    }

    QQuickView *view; 
    app->wait( [&]() {
        view = new QQuickView(QUrl::fromLocalFile("main.qml"));
        view->show();
    });

    this_thread::sleep_for( 10s );
    app->wait( [&](){ delete view; }); 
}

TEST( testQMLFirstSteps, customtypes ) 
{
    auto app = Fixture::instance()->appRunner();

    {
        ofstream f{ "MessageLabel.qml"};
        f << R"(
import QtQuick 2.9       
Rectangle {
    height: 50;     color: "black"
    property string message: "debug message"
    property var msgType: ["debug", "warning", "critical"]
    Column {
        anchors.fill: parent; padding: 5.0; spacing: 2
        Text {
            text: msgType.toString().toUpperCase() + ":"
            font.bold: msgType == "critical"
            font.family: "Terminal Regular"
            color: msgType === "warning" || msgType === "critical" ? "red" : "yellow"
            ColorAnimation on color {
                running: msgType == "critical"
                from: "red"; to: "black"; duration: 1000
                loops: msgType == "critical" ? Animation.Infinite : 1
            }
        }
        Text {
            text: message; font.family: "Terminal Regular"
            color: msgType === "warning" || msgType === "critical" ? "red" : "yellow"
        }
    }
})";
    }

    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9       
Column {
    width: 180; height: 180; 
    padding: 1.5; topPadding: 10.0; bottomPadding: 10.0; spacing: 5
    MessageLabel{ msgType: "debug";     width: parent.width - 2;  }
    MessageLabel{ msgType: "warning";   width: parent.width - 2; message: "This is a warning!";  }
    MessageLabel{ msgType: "critical";  width: parent.width - 2; message: "A critical warning!"; }
})";
    }    

    QQuickView *view; 
    app->wait( [&]() {
        view = new QQuickView(QUrl::fromLocalFile("main.qml"));
        view->show();
    });

    this_thread::sleep_for( 10ms );
    app->wait( [&](){ delete view; }); 
}