#include "gtest/gtest.h"
#include "Fixture.h"
#include <fstream>
#include <chrono>
#include "QMLTestBench.hpp"
#include "ImageScene.h"
#include "useful.hpp"
#include <QTabWidget>
#include <QLabel>


using namespace std;

class MyWidgetLabel : public QLabel
{
    Q_OBJECT
public:
    Q_INVOKABLE void foo()
    {
        show(); // segmentation fault in here !!!
    }
};

TEST( testMixing, noWidgetAtQML ) 
{
    GTEST_SKIP_("crash also at MyWidgetLabel destructor");

    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9;        
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import io.qt.examples 1.0
ColumnLayout {
    Button {
        text: "Will crash"
        onClicked:  { hello.foo() }
    }
    MyWidgetLabel { id : hello }
})";
    }

    auto app = Fixture::instance()->appRunner();    
    app->wait( [&]()
    {
        qmlRegisterType<MyWidgetLabel>("io.qt.examples", 1, 0, "MyWidgetLabel");
    });

    QMLTestBench tb( app,"main.qml");
    std::this_thread::sleep_for(100ms);
}


class Saying : public QObject
{
    Q_OBJECT
public:
    Saying(QObject *parent = nullptr) : QObject(parent) 
    {
        tabs_.show();
    }

    Q_INVOKABLE void addTab( QWidget *s, const QString& tabHead)
    {
        tabs_.addTab( s, tabHead);
    }

    Q_INVOKABLE SpaceShape::ImageScene *imagine( const QString& filePath )
    {
        auto s = new SpaceShape::ImageScene();
        s->setImage( QImage(filePath) );
        return s;
    }

    Q_INVOKABLE void setWidget( const QString& name, QWidget* widget)
    {
        namedWidgets[ name ] = widget;
    }

    Q_INVOKABLE QWidget* getWidget( const QString& name)
    {
        return namedWidgets[ name ];
    }

private:
    QTabWidget tabs_;
    std::map<QString,QWidget*> namedWidgets;
};

TEST( testMixing, sayingImageScene ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9;        
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import io.qt.examples 1.0
ColumnLayout {
    Button { 
        text: "Imagine !";  
        onClicked:  {
            var img = saying.imagine( qsTr("Lenna.png") )
            saying.addTab( img, qsTr("Lenna") )
        } 
    }
    Saying { id : saying }
})";
    }

    auto app = Fixture::instance()->appRunner();    
    app->wait( [&]()
    {
        qmlRegisterType<Saying>("io.qt.examples", 1, 0, "Saying");
        qmlRegisterType<SpaceShape::ImageScene>("io.qt.examples", 1, 0, "ImageScene");
    });

    QMLTestBench tb( app,"main.qml");
    std::this_thread::sleep_for(100ms);
}


TEST( testMixing, sayingInside ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9        
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import io.qt.examples 1.0
ColumnLayout {
    Button { 
        text: "Imagine !";  
        onClicked:  {
            var img = saying.imagine( qsTr("Lenna.png") )
            saying.addTab( img, qsTr("Lenna") )
        } 
    }
    Component.onCompleted : {
        saying.addTab( saying.getWidget( qsTr("root")), qsTr("QuickWidget") )
    }
})";
    }

    auto app = Fixture::instance()->appRunner();    
    Saying* saying;
    app->wait( [&]() { 
        saying = new Saying();
    });

    {
        QMLTestWidgetBench tb( app, 
            [&]( QQuickWidget* qw ){
                qmlRegisterType<SpaceShape::ImageScene>( "io.qt.examples", 1, 0, "ImageScene");
                qmlRegisterType<QWidget>( "io.qt.examples", 1, 0, "QWidget");
                qw->engine()->rootContext()->setContextProperty("saying", saying);
                saying->setWidget( "root", qw );
            },
            "main.qml");
        std::this_thread::sleep_for(100ms);
    }

    app->wait( [&]() { 
        delete saying;
    });
}

#include "testMixing.moc"
