#include "gtest/gtest.h"
#include "Fixture.h"
#include "QMLTestBench.hpp"
#include <fstream>
#include <qqml.h>
#include <QQmlEngine>
#include <QQmlContext>
#include <qqmllist.h>
#include <QAbstractListModel>

using namespace std;

class BackEnd : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString userName READ userName WRITE setUserName NOTIFY userNameChanged)

public:
    explicit BackEnd(QObject *parent = nullptr) : QObject(parent)
    {
        m_userName = "Right Now";
    }

    QString userName() { return m_userName; }
    void setUserName(const QString &userName) 
    {
        if (userName == m_userName)
            return;

        m_userName = userName;
        emit userNameChanged();

        cout << "User name : " << userName.toStdString() << endl;
    }

signals:
    void userNameChanged();

private:
    QString m_userName;
};

// https://doc.qt.io/qt-5/qtqml-cppintegration-topic.html
TEST( testQMLTypes, basic ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9       
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import io.qt.examples 1.0
Item {
    width: 320; height: 480
    BackEnd { id: backend }
    TextField {
        text: backend.userName
        placeholderText: qsTr("Backend name")
        onEditingFinished: { backend.userName = text; }
    }
})";
    }

    Fixture::instance()->appRunner()->wait( [&]() {
        qmlRegisterType<BackEnd>("io.qt.examples", 1, 0, "BackEnd");
    });

    QMLTestBench tb( Fixture::instance()->appRunner(), "main.qml");
    this_thread::sleep_for( 100ms );
}

// https://doc.qt.io/qt-5/qtqml-cppintegration-exposecppattributes.html

class MessageBody : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
public:
    MessageBody()
    {
        m_text = "";
    }

    void setText(const QString &a) {
        if (a != m_text) {
            m_text = a;
            emit textChanged();
        }
    }
    QString text() const { return m_text; }
signals:
    void textChanged();
private:
    QString m_text;
};

class Message : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString author READ author WRITE setAuthor NOTIFY authorChanged)
    Q_PROPERTY(MessageBody* body READ body WRITE setBody NOTIFY bodyChanged)

public:
    Message()
    {
        setBody( new MessageBody{} );
    }

    void setAuthor(const QString &a) {
        if (a != m_author) {
            m_author = a;
            emit authorChanged();
        }
    }
    QString author() const { return m_author; }
    MessageBody* body() const { return m_body.get(); }
    void setBody(MessageBody* body) { m_body.reset(body); }
signals:
    void authorChanged();
    void bodyChanged();
private:
    QString m_author;
    std::unique_ptr<MessageBody> m_body;
};


TEST( testQMLTypes, contextProperty ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9       
Text {
    width: 100; height: 100
    text: msg.author    
    Component.onCompleted: { msg.author = "Jonah" }
})";
    }

    auto app = Fixture::instance()->appRunner();
    Message* msg;
    app->wait([&]() { msg = new Message(); });

    QMLTestBench tb( app, 
        [&]( QQuickView* view ){
            view->engine()->rootContext()->setContextProperty("msg", msg);
        },
        "main.qml");
    this_thread::sleep_for( 100ms );

    app->wait([&]() { delete msg; }); 
}


TEST( testQMLTypes, objectProperty ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9       
import test.qt.types 1.0

Text {
    width: 100; height: 100   
    Message {
        id: msg 
        body: MessageBody { text: "Hello, world!" }        
    }
    text: msg.body.text    
})";
    }

    Fixture::instance()->appRunner()->wait( [&]() {
        qmlRegisterType<Message>("test.qt.types", 1, 0, "Message");
        qmlRegisterType<MessageBody>("test.qt.types", 1, 0, "MessageBody");
    });

    QMLTestBench tb( Fixture::instance()->appRunner(), "main.qml");
    this_thread::sleep_for( 100ms );
}

/*
class MessageBoard : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<Message> messages READ messages)

    static void append_message(QQmlListProperty<Message> *list, Message *msg)
    {
        MessageBoard *msgBoard = qobject_cast<MessageBoard *>(list->object);
        if (msg)
            msgBoard->m_messages.append(msg);
    }    
 
public:
    QQmlListProperty<Message> messages()
    {
        // compile error below : no constructor with below signature 
        return QQmlListProperty<Message>(this, 0, &MessageBoard::append_message); 
    }

private:
    QList<Message *> m_messages;
};

TEST( testQMLTypes, listProperty ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9       
import test.qt.types 1.0
ListView {
    width: 180; height: 400
    model: MessageBoard {}
    delegate: Text {
        text: author + " : " + body.text
    }
})";
    }

    Fixture::instance()->appRunner()->wait( [&]() {
        qmlRegisterType<MessageBoard>("test.qt.types", 1, 0, "MessageBoard");
    });

    QVTestBench tb( Fixture::instance()->appRunner(), "main.qml");
    this_thread::sleep_for( 100s );
}
*/

// http://www.bim-times.com/qt/Qt-5.11.1/qtqml/qtqml-referenceexamples-adding-example.html#

class Person : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(int shoeSize READ shoeSize WRITE setShoeSize NOTIFY shoeSizeChanged)
public:
    Person(QObject *parent = 0) : QObject(parent), m_shoeSize(0) {}
    QString name() const { return m_name; }
    void setName(const QString &s) 
    {
        if (m_name == s) return;
        m_name = s;
        emit nameChanged();
    }
    int shoeSize() const { return m_shoeSize; }
    void setShoeSize(int v)
    {
        if (m_shoeSize == v) return;
        m_shoeSize = v;
        emit shoeSizeChanged();
    }

signals:
    void nameChanged();
    void shoeSizeChanged();

private:
    QString m_name;
    int m_shoeSize;
};

TEST( testQMLTypes, person ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9 
import QtQuick.Controls 2.2
import test.qt.types 1.0
Column {
    width: 320; height: 480; spacing: 20
    Person { id : person; name: "Bob Jones"; shoeSize: 12 }
    TextField {
        text: person.name
        onEditingFinished: { person.name = text; }
    }
    TextField { 
        text: person.shoeSize
        onEditingFinished: { person.shoeSize = text; }
    }
})";
    }

    Fixture::instance()->appRunner()->wait( [&]() {
        qmlRegisterType<Person>("test.qt.types", 1, 0, "Person");
    });

    QMLTestBench tb( Fixture::instance()->appRunner(), "main.qml");
    this_thread::sleep_for( 100ms );
}

class BirthdayParty : public QObject
{
    Q_OBJECT
    Q_PROPERTY(Person *host READ host WRITE setHost NOTIFY hostChanged)
    Q_PROPERTY(QQmlListProperty<Person> guests READ guests)
public:
    BirthdayParty(QObject *parent = 0) : QObject(parent), m_host(nullptr) {}
    Person *host() const { return m_host;}
    void setHost(Person * h)
    {
        if (m_host == h) return;
        m_host = h;
        emit hostChanged();
    }

    QQmlListProperty<Person> guests()
    {
      return QQmlListProperty<Person>(this, this,
               &BirthdayParty::appendGuest,
               &BirthdayParty::guestCount,
               &BirthdayParty::guest,
               &BirthdayParty::clearGuests);        
    }

    void appendGuest(Person* p ){ m_guests.append(p);}
    int guestCount() const { return m_guests.count(); }
    Person *guest(int index) const { return m_guests.at(index); }
    void clearGuests() {  m_guests.clear(); }

signals:
    void hostChanged();

private:
    static void appendGuest(QQmlListProperty<Person>* list , Person* p ) { 
        reinterpret_cast<BirthdayParty*>(list->data)->appendGuest(p);
    }
    static int guestCount(QQmlListProperty<Person>*list) {
        return reinterpret_cast< BirthdayParty* >(list->data)->guestCount();
    }
    static Person* guest(QQmlListProperty<Person>*list, int i) {
        return reinterpret_cast< BirthdayParty* >(list->data)->guest(i);
    }
    static void clearGuests(QQmlListProperty<Person>*list) {
        reinterpret_cast< BirthdayParty* >(list->data)->clearGuests();
    }

    Person *m_host;
    QVector<Person *> m_guests;
};                                                                                                                



TEST( testQMLTypes, birthdayParty ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9 
import QtQuick.Controls 2.2
import test.qt.types 1.0
Column {
    width: 320; height: 480; spacing: 20
    BirthdayParty {
        id : party
        host: Person { name: "Bob Jones";  shoeSize: 12 }
        guests: [
            Person { name: "Leo Hodges" },
            Person { name: "Jack Smith" },
            Person { name: "Anne Brown" }
        ]
    }
    TextField {
        text: party.host.name
        onEditingFinished: { party.host.name = text; }
    }
    TextField { 
        text: party.host.shoeSize
        onEditingFinished: { party.host.shoeSize = text; }
    }
    
    ListView {
        width: 180; height: 200
        model: party.guest
        delegate: Text {
            text: name + ": " + shoeSize
        }
    }
})";
    }

    Fixture::instance()->appRunner()->wait( [&]() {
        qmlRegisterType<BirthdayParty>("test.qt.types", 1, 0, "BirthdayParty");
    });

    QMLTestBench tb( Fixture::instance()->appRunner(), "main.qml");
    this_thread::sleep_for( 100ms );
}


// https://gist.github.com/mitchcurtis/72c745f05b4292660168b1c0cd1ce4d9
class ListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    ListModel() {
        for (int i = 0; i < 10; ++i) {
            mContents.append(QString::fromLatin1("Item %1").arg(i));
        }
    }

    int rowCount(const QModelIndex &parent = QModelIndex()) const override {
        Q_UNUSED(parent);
        return mContents.size();
    }

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override {
        Q_UNUSED(role);
        if (!index.isValid() || index.row() < 0 || index.row() >= rowCount())
            return QVariant();
        return mContents.at(index.row());
    }

    Q_INVOKABLE void removeOne() {
        beginRemoveRows(QModelIndex(), rowCount() - 1, rowCount() - 1);
        mContents.takeLast();
        endRemoveRows();
    }

private:
    QVector<QString> mContents;
};


TEST( testQMLTypes, itemListModel ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9       
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
ColumnLayout {
    ListView {
        id: listView; currentIndex: 9;
        width: 50; height: 200;
        model: listModel;        
        delegate: Rectangle {
            width: 50; height: 50; border.width: 1
            Text { text: index;  anchors.centerIn: parent }
        }
    }
    RowLayout {
        Button {
            text: "Remove from end"
            onClicked: listView.model.removeOne()
        }
        Text { text: "Current index: " + listView.currentIndex }
    }    
})";
    }

    auto app = Fixture::instance()->appRunner();
    ListModel* listModel;
    QMLTestBench tb( app, 
        [&]( QQuickView* view ){
            listModel = new ListModel();
            view->engine()->rootContext()->setContextProperty("listModel", listModel);
        },
        "main.qml");
    this_thread::sleep_for( 100ms );

    app->wait([&]() { delete listModel; }); 
}

#include "testQMLTypes.moc"
