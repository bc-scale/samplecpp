#include "gtest/gtest.h"
#include "Poco/Event.h"
#include <thread>

TEST(HelloTest, BasicAssertions) {
  EXPECT_STRNE("hello", "world");
  EXPECT_EQ(7 * 6, 42);
}

TEST(PocoTest, Event) 
{
  Poco::Event flag{ true };
  std::thread t{ [&]()
  { 
      flag.wait();
  }};

  flag.set();
  t.join();
}