#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "Fixture.h"
#include <fstream>
#include <chrono>
#include "QMLTestBench.hpp"
#include "ImageScene.h"
#include "useful.hpp"
#include "Shaper.h"
#include "ShapeFactory.h"
#include <QAbstractButton>
#include <QTabWidget>

using namespace std;

TEST( testShaper, grid1 ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9;        
import SpaceShape 1.0
Text {
    text: "Hello from QML" 
    ShapeFactory { id : fab }
    Component.onCompleted : {
        shaper
            .beginGrid( "third", [1,1], [1,1] )
                .add( "worth", fab.pushButton( "Hello from QWidget") ).at( Qt.point(0,0) )
                .add( "byQML", qmlWidget ).at( Qt.point(1,0))
                .add( "H", fab.pushButton( "Another from QWidget") ).at( Qt.point(0,1) )
                .add( "B", fab.pushButton( "Yet another Hello from QWidget") ).at( Qt.point(1,1) )
            .end();
    }
})";
    }

    auto app = Fixture::instance()->appRunner();    
    QFrame* frame;
    SpaceShape::Shaper* shaper;
    app->wait( [&]() { 
        frame = new QFrame();
        shaper = new SpaceShape::Shaper( frame );
        frame->show();
    });

    {
        QMLTestWidgetBench tb( app, 
            [&]( QQuickWidget* qw ){
                SpaceShape::Shaper::registerTypes( "SpaceShape", 1, 0 );
                SpaceShape::ShapeFactory::registerTypes( "SpaceShape", 1, 0 );
                qw->engine()->rootContext()->setContextProperty("shaper", shaper);
                qw->engine()->rootContext()->setContextProperty("qmlWidget", qw);
            },
            "main.qml");
        std::this_thread::sleep_for(100ms);
    }

    app->wait( [&]() { 
        delete shaper;
        delete frame;
    });
}

struct ShaperTestBench
{
    ShaperTestBench(AppRunner* a, const char *mainqml) : app(a)
    {
        app->wait( [&]() { 
            frame = new QFrame();
            shaper = new SpaceShape::Shaper( frame );

            quick = new QQuickWidget();
            SpaceShape::Shaper::registerTypes( "SpaceShape", 1, 0 );
            SpaceShape::ShapeFactory::registerTypes( "SpaceShape", 1, 0 );
            quick->engine()->rootContext()->setContextProperty("shaper", shaper);
            quick->engine()->rootContext()->setContextProperty("qmlWidget", quick);
            quick->setSource(QUrl::fromLocalFile(mainqml));

            //frame->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
            frame->show();
        });        
    }

    ~ShaperTestBench()
    {
        app->wait( [&]() { 
            delete quick; 
            delete shaper;
            delete frame;
        });
    }

    AppRunner* app;
    QFrame *frame;
    SpaceShape::Shaper *shaper;
    QQuickWidget* quick;
};


TEST( testShaper, grid2 ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9;        
import SpaceShape 1.0
Text {
    text: "Hello from QML" 
    ShapeFactory { id : fab }
    Component.onCompleted : {
        shaper
            .beginGrid( "nursing", [1,1], [1,1] )
                .add( "requires", fab.pushButton( "exclusive devotion") ).at( Qt.point(0,0) )
                .add( "byQML", qmlWidget ).at( Qt.point(1,0))
                .add( "needs", fab.pushButton( "hard preparation") ).at( Qt.point(0,1) )
                .add( "with", fab.pushButton( "living body") ).at( Qt.point(1,1) )
            .end()
    }
})";
    }

    ShaperTestBench bench(Fixture::instance()->appRunner(), "main.qml");
    bench.app->wait( [&bench]() {
        bench.frame->resize(400,200);

        QAbstractButton* w = (QAbstractButton*)bench.shaper->find( "nursing/requires");
        EXPECT_EQ( "exclusive devotion", w->text());

        w = (QAbstractButton*)bench.shaper->find( "nursing/with");
        EXPECT_EQ( "living body", w->text());
    });
    std::this_thread::sleep_for(100ms);
}


TEST( testShaper, grid3 ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9;        
import SpaceShape 1.0
Text {
    text: "Hello from QML" 
    ShapeFactory { id : fab }
    Component.onCompleted : {
        shaper
            .beginGrid( "nursing", [1,2,3,4], [1] )
                .add( "requires", fab.pushButton( "exclusive devotion") ).at( Qt.point(0,0) )
                .add( "needs", fab.pushButton( "hard preparation") ).at( Qt.point(0,1) )
                .add( "with", fab.pushButton( "living body") ).at( Qt.point(0,2) )
                .add( "byQML", qmlWidget ).at( Qt.point(0,3))
            .end()
    }
})";
    }

    ShaperTestBench bench(Fixture::instance()->appRunner(), "main.qml");
    bench.app->wait( [&bench]() {
        bench.frame->resize(200,400);

        auto* w = bench.shaper->find( "nursing/requires");
        EXPECT_EQ( QRect(9,9,160,34), w->geometry());
        w = bench.shaper->find( "nursing/needs");
        EXPECT_EQ( QRect(9,49,160,69), w->geometry());
        w = bench.shaper->find( "nursing/with");
        EXPECT_EQ( QRect(9,124,160,102), w->geometry());
        w = bench.shaper->find( "nursing/byQML");
        EXPECT_EQ( QRect(9,232,160,137), w->geometry());
    });
    std::this_thread::sleep_for(100ms);
}

TEST( testShaper, grid4 ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9;        
import SpaceShape 1.0
Text {
    text: "Hello from QML" 
    ShapeFactory { id : fab }
    Component.onCompleted : {
        shaper
            .beginGrid( "nursing", [1,2], [2,3] )
                .add( "requires", fab.pushButton( "exclusive devotion") ).at( Qt.point(0,0) )
                .add( "needs", fab.pushButton( "hard preparation") ).at( Qt.point(0,1) )
                .add( "with", fab.pushButton( "living body") ).at( Qt.point(1,0) )
                .add( "byQML", qmlWidget ).at( Qt.point(1,1))
            .end()
    }
})";
    }

    ShaperTestBench bench(Fixture::instance()->appRunner(), "main.qml");
    bench.app->wait( [&bench]() {
        bench.frame->resize(200,400);

        auto* w = bench.shaper->find( "nursing/requires");
        EXPECT_EQ( QRect(9,9,130,118), w->geometry());
        w = bench.shaper->find( "nursing/needs");
        EXPECT_EQ( QRect(9,133,130,236), w->geometry());
        w = bench.shaper->find( "nursing/with");
        EXPECT_EQ( QRect(145,9,81,118), w->geometry());
        w = bench.shaper->find( "nursing/byQML");
        EXPECT_EQ( QRect(145,133,81,236), w->geometry());
    });
    std::this_thread::sleep_for(100ms);
}

TEST( testShaper, grid5 ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9;        
import SpaceShape 1.0
Text {
    text: "Hello from QML" 
    ShapeFactory { id : fab }
    Component.onCompleted : {
        shaper
            .beginGrid( "nursing", [1,1,1], [1,1,1] )
                .add( "requires", fab.pushButton( "exclusive devotion") ).atRect( Qt.rect(0,0,3,1) )
                .add( "needs", fab.pushButton( "hard preparation") ).atRect( Qt.rect(0,1,1,2) )
                .add( "with", fab.pushButton( "living body") ).atRect( Qt.rect(1,1,2,1) )
                .add( "byQML", qmlWidget ).at( Qt.point(2,2))
            .end()
    }
})";
    }

    ShaperTestBench bench(Fixture::instance()->appRunner(), "main.qml");
    bench.app->wait( [&bench]() {
        bench.frame->resize(400,400);

        auto* w = bench.shaper->find( "nursing/requires");
        EXPECT_EQ( QRect(9,9,360,116), w->geometry());
        w = bench.shaper->find( "nursing/needs");
        EXPECT_EQ( QRect(9,131,120,238), w->geometry());
        w = bench.shaper->find( "nursing/with");
        EXPECT_EQ( QRect(135,131,234,116), w->geometry());
        w = bench.shaper->find( "nursing/byQML");
        EXPECT_EQ( QRect(255,253,114,116), w->geometry());
    });
    std::this_thread::sleep_for(100ms);
}


TEST( testShaper, tab1 ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9;        
import SpaceShape 1.0
Text {
    text: "Hello from QML" 
    ShapeFactory { id : fab }
    Component.onCompleted : {
        shaper
            .beginTabs("she is")
                .add( "easiest", fab.pushButton( "to unload") )
                .add( "had", fab.pushButton( "least to eat") )
                .add( "too", fab.pushButton( "young to complain") )
                .add( "byQML", qmlWidget )
            .end()
    }
})";
    }

    ShaperTestBench bench(Fixture::instance()->appRunner(), "main.qml");
    bench.app->wait( [&bench]() {
        bench.frame->resize(400,400);

        auto* tabs = dynamic_cast<QTabWidget*>(bench.shaper->find("she is"));
        EXPECT_NE( tabs, nullptr );

        vector<string> heads;
        for( int i = 0; i < tabs->count(); ++i )
        {
            heads.push_back( tabs->tabText(i).toStdString() );
        }
        EXPECT_THAT( heads, testing::ElementsAre("easiest", "had", "too", "byQML") );
    });
    std::this_thread::sleep_for(100ms);
}

TEST( testShaper, tab2 ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9;        
import SpaceShape 1.0
Text {
    text: "Hello from QML" 
    ShapeFactory { id : fab }
    Component.onCompleted : {
        shaper
            .beginTabs("she is")
                .add( "easiest", fab.pushButton( "to unload") )
                .beginTabs("because")
                    .add( "had", fab.pushButton( "least to eat") )
                    .add( "too", fab.pushButton( "young to complain") )
                .end()
                .add( "byQML", qmlWidget )
            .end()
    }
})";
    }

    ShaperTestBench bench(Fixture::instance()->appRunner(), "main.qml");
    bench.app->wait( [&bench]() {
        bench.frame->resize(400,400);

        auto* tabs = dynamic_cast<QTabWidget*>(bench.shaper->find("she is/because"));
        EXPECT_NE( tabs, nullptr );

        vector<string> heads;
        for( int i = 0; i < tabs->count(); ++i )
        {
            heads.push_back( tabs->tabText(i).toStdString() );
        }
        EXPECT_THAT( heads, testing::ElementsAre("had", "too" ));
    });
    std::this_thread::sleep_for(100ms);
}


TEST( testShaper, tabgrid1 ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9;        
import SpaceShape 1.0
Text {
    text: "Hello from QML" 
    ShapeFactory { id : fab }
    Component.onCompleted : {
        shaper
            .beginTabs("nursing")
                .beginGrid( "is", [1,1], [1,1] )
                    .add( "requires", fab.pushButton( "exclusive devotion") ).at( Qt.point(0,0) )
                    .add( "byQML", qmlWidget ).at( Qt.point(1,0))
                    .add( "needs", fab.pushButton( "hard preparation") ).at( Qt.point(0,1) )
                    .beginTabs("with").at( Qt.point(1,1))
                        .add( "with", fab.pushButton( "living body") )
                    .end()
                .end()
            .end()
    }
})";
    }

    ShaperTestBench bench(Fixture::instance()->appRunner(), "main.qml");
    bench.app->wait( [&bench]() {
        bench.frame->resize(400,400);
        auto* tabs = dynamic_cast<QTabWidget*>(bench.shaper->find("nursing/is/with"));
        EXPECT_NE( tabs, nullptr );
    });
    std::this_thread::sleep_for(100ms);
}

TEST( testShaper, scripting1 ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9;        
import SpaceShape 1.0
Text {
    text: "Hello from QML" 
    ShapeFactory { id : fab }
    Component.onCompleted : {
        shaper
            .beginTabs("she is")
                .add( "easiest", fab.pushButton( "to unload") )
                .add( "had", fab.pushButton( "least to eat") )
                .add( "too", fab.pushButton( "young to complain") )
                .add( "byQML", qmlWidget )
            .end();

        var too = shaper.find("she is/too");
        too.text = "shy to complain";
    }
})";
    }

    ShaperTestBench bench(Fixture::instance()->appRunner(), "main.qml");
    bench.app->wait( [&bench]() {
        bench.frame->resize(400,400);

        auto* too = dynamic_cast<QAbstractButton*>(bench.shaper->find("she is/too"));
        ASSERT_NE( too, nullptr );

        EXPECT_THAT( too->text().toStdString(), testing::Eq("shy to complain"));
    });
    std::this_thread::sleep_for(100ms);
}
