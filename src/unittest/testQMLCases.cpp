#include "gtest/gtest.h"
#include "Fixture.h"
#include "QMLTestBench.hpp"
#include <fstream>

using namespace std;

// https://doc.qt.io/qt-5/qtquick-usecase-visual.html

TEST( testQMLCases, rectangles ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9       
import QtQuick.Controls 2.2
Item {
    width: 320; height: 480
    Rectangle { color: "#272822";  width: 320; height: 480 }
    Rectangle {
        x: 160; y: 20; width: 100; height: 100
        radius: 8 
        gradient: Gradient { 
            GradientStop { position: 0.0; color: "aqua" }
            GradientStop { position: 1.0; color: "teal" }
        }
        border { width: 3; color: "white" }
    }
    Rectangle { x: 40; y: 20; width: 100; height: 100; color: "red" }
})";
    }

    QMLTestBench tb( Fixture::instance()->appRunner(), "main.qml");
    this_thread::sleep_for( 100ms );
}

TEST( testQMLCases, image ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9       
import QtQuick.Controls 2.2
Image {
    x: 40; y: 20; width: 320; height: 240
    // source: "http://codereview.qt-project.org/static/logo_qt.png"
    source: "Lenna.png"
})";
    }

    QMLTestBench tb( Fixture::instance()->appRunner(), "main.qml");
    this_thread::sleep_for( 100ms );
}

TEST( testQMLCases, opacity ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9       
import QtQuick.Controls 2.2
Item {
    width: 320; height: 480
    Rectangle { color: "#272822"; width: 320; height: 480 }
    Item { x: 20; y: 270; width: 200; height: 200; 
        MouseArea {
            anchors.fill: parent
            onClicked: topRect.visible = !topRect.visible
        }
        Rectangle { x: 20; y: 20; width: 100; height: 100; color: "red" }
        Rectangle {
            id: topRect; opacity: 0.5
            x: 100; y: 100; width: 100; height: 100; color: "blue"
        }
    }
})";
    }

    QMLTestBench tb( Fixture::instance()->appRunner(), "main.qml");
    this_thread::sleep_for( 10ms );
}

TEST( testQMLCases, transforms ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9       
import QtQuick.Controls 2.2
Item {
    width: 320; height: 480
    Rectangle { color: "#272822"; width: 320; height: 480 }
    Rectangle {
        rotation: 45
        x: 20; y: 160; width: 100; height: 100; color: "blue"
    }
    Rectangle {
        scale: 0.8
        x: 160; y: 160; width: 100; height: 100; color: "green"
    }
})";
    }

    QMLTestBench tb( Fixture::instance()->appRunner(), "main.qml");
    this_thread::sleep_for( 10ms );
}

// https://doc.qt.io/qt-5/qtquick-usecase-userinput.html
TEST( testQMLCases, mouseevents ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9       
import QtQuick.Controls 2.2
import Qt.labs.handlers 1.0
Item {
    id: root
    width: 320; height: 480
    Rectangle { color: "#272822"; width: 320; height: 480 }
    Rectangle {
        id: rectangle
        x: 40; y: 20; width: 120; height: 120; color: "red"
        TapHandler { onTapped: rectangle.width += 10 }
    }
})";
    }

    QMLTestBench tb( Fixture::instance()->appRunner(), "main.qml");
    this_thread::sleep_for( 10ms );
}

TEST( testQMLCases, texts ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9       
import QtQuick.Controls 2.2
import Qt.labs.handlers 1.0
import QtQuick.Layouts 1.3
ColumnLayout {
    anchors.fill: parent
    TextField {
        id: singleline
        text: "Initial Text"
        Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
        Layout.margins: 5
        background: Rectangle {
            implicitWidth: 200
            implicitHeight: 40
            border.color: singleline.focus ? "#21be2b" : "lightgray"
            color: singleline.focus ? "lightgray" : "transparent"
        }
    }

    TextArea {
        id: multiline
        placeholderText: "Initial text\n...\n...\n"
        Layout.alignment: Qt.AlignLeft
        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.margins: 5
        background: Rectangle {
            implicitWidth: 200
            implicitHeight: 100
            border.color: multiline.focus ? "#21be2b" : "lightgray"
            color: multiline.focus ? "lightgray" : "transparent"
        }
    }
})";
    }

    QMLTestBench tb( Fixture::instance()->appRunner(), "main.qml");
    this_thread::sleep_for( 100ms );
}

//https://doc.qt.io/qt-5/qtquick-usecase-animations.html
TEST( testQMLCases, states ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9       
import QtQuick.Controls 2.2
import Qt.labs.handlers 1.0
Item {
    id: container
    width: 320; height: 120
    Rectangle {
        id: rect
        color: "red"; width: 120; height: 120
        TapHandler {
            onTapped: container.state === '' ? container.state = 'other' : container.state = ''
        }
    }
    states: [
        State { name: "other"; PropertyChanges { target: rect; x: 200 }}
    ]
    transitions: [
        Transition { NumberAnimation { properties: "x,y" } }
    ]
})";
    }

    QMLTestBench tb( Fixture::instance()->appRunner(), "main.qml");
    this_thread::sleep_for( 100ms );
}

TEST( testQMLCases, animating ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9       
import QtQuick.Controls 2.2
import Qt.labs.handlers 1.0
Item {
    width: 320; height: 120
    Rectangle {
        color: "green"; width: 120; height: 120
        Behavior on x {
            NumberAnimation { duration: 600; easing.type: Easing.OutBounce }
        }
        TapHandler { onTapped: parent.x == 0 ? parent.x = 200 : parent.x = 0 }
    }
})";
    }

    QMLTestBench tb( Fixture::instance()->appRunner(), "main.qml");
    this_thread::sleep_for( 100ms );
}

TEST( testQMLCases, moreAnimating ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9       
import QtQuick.Controls 2.2
import Qt.labs.handlers 1.0
import QtQuick.Layouts 1.3

ColumnLayout {
    Item {
        width: 320; height: 120
        Rectangle {
            color: "blue"; width: 120; height: 120
            SequentialAnimation on x {
                id: xAnim
                running: false
                loops: Animation.Infinite 
                NumberAnimation { from: 0; to: 200; duration: 500; easing.type: Easing.InOutQuad }
                NumberAnimation { from: 200; to: 0; duration: 500; easing.type: Easing.InOutQuad }
                PauseAnimation { duration: 250 } // This puts a bit of time between the loop
            }
            TapHandler { onTapped: xAnim.running = true }
        }
    }
    Item {
        width: 320; height: 120
        Rectangle {
            id: rectangle
            color: "yellow"; width: 120; height: 120
            TapHandler { onTapped: anim.running = true }
        }
        SequentialAnimation {
            id: anim
            NumberAnimation { target: rectangle; property: "x"; from: 0; to: 200; duration: 500 }
            NumberAnimation { target: rectangle; property: "x"; from: 200; to: 0; duration: 500 }
        }
    }
})";
    }

    QMLTestBench tb( Fixture::instance()->appRunner(), "main.qml");
    this_thread::sleep_for( 100ms );
}

//https://doc.qt.io/qt-5/qtquick-usecase-text.html

TEST( testQMLCases, displayTexts ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9       
Item {
    id: root; width: 480; height: 320
    Rectangle { color: "#272822"; width: 480; height: 320 }
    Column {
        spacing: 20
        Text { text: 'I am the very model of a modern major general!'; color: "yellow"}
        Text {
            width: root.width
            wrapMode: Text.WordWrap
            text: 'I am the very model of a modern major general. I\'ve information \
                  vegetable, animal and mineral. I know the kings of england and I \
                  quote the fights historical; from Marathon to Waterloo in order categorical.'
            color: "white"
        }
        Text {
            text: 'I am the very model of a modern major general!'
            color: "yellow"
            font { family: 'Courier'; pixelSize: 20; italic: true; capitalization: Font.SmallCaps }
        }
        Text {
            text: '<font color="white">I am the <b>very</b> model of a modern <i>major general</i>!</font>'
            font.pointSize: 14
            textFormat: Text.StyledText
        }
    }
})";
    }

    QMLTestBench tb( Fixture::instance()->appRunner(), "main.qml");
    this_thread::sleep_for( 100ms );
}

// https://doc.qt.io/qt-5/qtquick-usecase-layouts.html
TEST( testQMLCases, anchors ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9       
Item {
    width: 200; height: 200
    Rectangle {
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.margins: 20 
        width: 80; height: 80; color: "orange"
    }
    Rectangle {
        anchors { horizontalCenter: parent.horizontalCenter; top: parent.top; topMargin: 20 }
        width: 80; height: 80; color: "green"
    }
})";
    }

    QMLTestBench tb( Fixture::instance()->appRunner(), "main.qml");
    this_thread::sleep_for( 100ms );
}

TEST( testQMLCases, positioners ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9       
Item {
    width: 300; height: 100
    Row { 
        spacing: 20 
        Rectangle { width: 80; height: 80; color: "red" }
        Rectangle { width: 80; height: 80; color: "green" }
        Rectangle { width: 80; height: 80; color: "blue" }
    }
})";
    }

    QMLTestBench tb( Fixture::instance()->appRunner(), "main.qml");
    this_thread::sleep_for( 100ms );
}

//https://doc.qt.io/qt-5/qtquick-usecase-layouts.html
TEST( testQMLCases, layoutTypes ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9 
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
GroupBox {
    id: gridBox
    title: "Grid layout"
    Layout.fillWidth: true

    GridLayout {
        id: gridLayout
        rows: 3
        flow: GridLayout.TopToBottom
        anchors.fill: parent
        Label { text: "Line 1" }
        Label { text: "Line 2" }
        Label { text: "Line 3" }

        TextField { }
        TextField { }
        TextField { }

        TextArea {
            text: "This widget spans over three rows in the GridLayout.\n"
                  + "All items in the GridLayout are implicitly positioned from top to bottom."
            Layout.rowSpan: 3
            Layout.fillHeight: true
            Layout.fillWidth: true
        }
    }
})";
    }

    QMLTestBench tb( Fixture::instance()->appRunner(), "main.qml");
    this_thread::sleep_for( 100ms );
}

// https://doc.qt.io/qt-5/qtquick-usecase-styling.html
TEST( testQMLCases, styling ) 
{
    GTEST_SKIP_("Need to import another version of QtQuick.Controls");
    // https://stackoverflow.com/questions/49263604/qml-cannot-assign-to-non-existent-property-style
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9 
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
Button {
    text: qsTr("Hello World")
    style: ButtonStyle {
        background: Rectangle {
            implicitWidth: 100
            implicitHeight: 25
            border.width: control.activeFocus ? 2 : 1
            border.color: "#FFF"
            radius: 4
            gradient: Gradient {
                GradientStop { position: 0 ; color: control.pressed ? "#ccc" : "#fff" }
                GradientStop { position: 1 ; color: control.pressed ? "#000" : "#fff" }
            }
        }
    }
})";
    }

    QMLTestBench tb( Fixture::instance()->appRunner(), "main.qml");
    this_thread::sleep_for( 100ms );
}

TEST( testQMLCases, javascript ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9 
import QtQuick.Controls 2.2
import Qt.labs.handlers 1.0

Item {
    id: container
    width: 320; height: 480

    function randomNumber() { return Math.random() * 360; }
    function getNumber() { return container.randomNumber(); }

    TapHandler { onTapped: rectangle.rotation = container.getNumber(); }
    Rectangle { color: "#272822"; width: 320; height: 480 }
    Rectangle {
        id: rectangle
        anchors.centerIn: parent
        width: 160; height: 160; color: "green"
        Behavior on rotation { RotationAnimation { direction: RotationAnimation.Clockwise } }
    }
})";
    }

    QMLTestBench tb( Fixture::instance()->appRunner(), "main.qml");
    this_thread::sleep_for( 100ms );
}

TEST( testQMLCases, javascriptFiles ) 
{
    {
        ofstream f{ "myscript.js"};
        f << R"(
function getRandom(previousValue) {
    return Math.floor(previousValue + Math.random() * 90) % 360;
}
)";
    }

    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9 
import QtQuick.Controls 2.2
import Qt.labs.handlers 1.0
import "myscript.js" as Logic

Item {
    id: container
    width: 320; height: 480
    TapHandler { onTapped: rectangle.rotation = Logic.getRandom(rectangle.rotation); }
    Rectangle { color: "#272822"; width: 320; height: 480 }
    Rectangle {
        id: rectangle
        anchors.centerIn: parent
        width: 160; height: 160; color: "green"
        Behavior on rotation { RotationAnimation { direction: RotationAnimation.Clockwise } }
    }
})";
    }

    QMLTestBench tb( Fixture::instance()->appRunner(), "main.qml");
    this_thread::sleep_for( 100ms );
}