#include "gtest/gtest.h"
#include "Fixture.h"
#include "ImageScene.h"
#include <QImage>
#include <iostream>
#include <chrono>
#include "useful.hpp"

using namespace std;
using namespace SpaceShape;

TEST( testResize, resize ) 
{
    auto app = Fixture::instance()->appRunner();
    
    ImageScene *view; 

    app->wait( [&]()
    {
        view = new ImageScene();
        view->show();
    });

    app->wait( [&]()
    {
        view->setImage( QImage("Lenna.png"));
    });

    std::this_thread::sleep_for(100ms);
    app->wait( [&]() { view->resize(320, 240 ); });
    std::this_thread::sleep_for(100ms);
    app->wait( [&]() { view->resize(160, 120 ); });
    std::this_thread::sleep_for(100ms);
    app->wait( [&]() { view->resize(800, 600 ); });
    std::this_thread::sleep_for(100ms);
    app->wait( [&]() { view->showFullScreen(); });
    std::this_thread::sleep_for(100ms);
    app->wait( [&]() { view->showNormal(); });
    std::this_thread::sleep_for(100ms);

    std::this_thread::sleep_for(1s);

    app->wait( [&]()
    {
        view->close(); 
        delete view;
    }); 
}

TEST( testResize, imageValue ) 
{
    QImage a; 
    ASSERT_EQ( true, a.isNull());

    {
        QImage b("Lenna.png");
        a = b;
    };

    ASSERT_EQ( false, a.isNull());
    ASSERT_EQ( 1048576, a.sizeInBytes());
}

TEST( testResize, wave ) 
{
    auto app = Fixture::instance()->appRunner();
    
    ImageScene *view; 

    app->wait( [&]()
    {
        view = new ImageScene();

        QImage image(640,480, QImage::Format_Grayscale8);        
        image.fill(255);
        view->setImage( image );
        view->show();
    });

    for( int i = 0 ; i < 255; i+=10)
    {
        app->wait( [&,i]()
        {
            auto m = view->image();
            QPainter painter( &m );  // painter will detach image from the one set on view.
            painter.setPen( Qt::darkGray );
            painter.drawEllipse( m.rect().center(), i, i ); 
            painter.end();

            view->setImage( m );
            view->scene()->update();
        });
        std::this_thread::sleep_for(100ms);
    }

    std::this_thread::sleep_for(10ms);

    app->wait( [&]()
    {
        view->close(); 
        delete view;
    }); 
}


TEST( testResize, drawPolygon ) 
{
    auto app = Fixture::instance()->appRunner();
    
    ImageScene *view; 

    app->wait( [&]()
    {
        view = new ImageScene();
        view->setImage( QImage("Lenna.png"));
        view->resize(800, 600 );
        view->show();
    });

    std::this_thread::sleep_for(100ms);

    app->wait( [&]()
    {
        view->close(); 
        delete view;
    }); 
}
