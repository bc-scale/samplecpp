#include "gtest/gtest.h"
#include "Fixture.h"
#include "SpaceShape.h"

using namespace std;
using namespace SpaceShape;

TEST( testSpaceShape, shape ) 
{
    auto ss = new SpaceShapeFoo();
    ASSERT_EQ(42, ss->shape("Hello")); 
}