#include "gtest/gtest.h"
#include "Fixture.h"
#include <QtWidgets>
#include <QImage>
#include <QLabel>
#include <QPixmap>
#include <QGraphicsScene>
#include <QGraphicsView>

using namespace std;

TEST( testImageViewer, byLabel ) 
{
    auto app = Fixture::instance()->appRunner();
    
    QImage *image; 
    QLabel *label; 

    app->wait( [&]()
    {
        image = new QImage(320,240, QImage::Format::Format_Grayscale8);
        label = new QLabel();
        label->setFixedSize( image->size() );
        label->show();
    });

    for( int i = 0 ; i < 255; i+=10)
    {
        app->wait( [&,i]()
        {
            image->fill( i );
            label->setPixmap( QPixmap::fromImage(*image));
            label->update();
        });
        std::this_thread::sleep_for(100ms);
    }

    app->wait( [&]()
    {
        label->close(); 
        delete label;
        delete image;
    }); 
}

TEST( testImageViewer, viaPixmap ) 
{
    auto app = Fixture::instance()->appRunner();
    
    QImage *image; 
    QGraphicsView *view; 
    QGraphicsScene *scene; 
    QGraphicsPixmapItem *imagePixmap;

    app->wait( [&]()
    {
        scene = new QGraphicsScene(-320,-240,640,480);

        image = new QImage(320,240, QImage::Format::Format_Grayscale8);
        imagePixmap = scene->addPixmap(QPixmap::fromImage(*image)); 
        imagePixmap->setOffset( -image->width()/2, -image->height()/2 );

        view = new QGraphicsView( scene );
        view->show();
    });

    for( int i = 0 ; i < 255; i+=10)
    {
        app->wait( [&,i]()
        {
            image->fill( i );
            imagePixmap->setPixmap( QPixmap::fromImage(*image));
        });
        std::this_thread::sleep_for(100ms);
    }

    app->wait( [&]()
    {
        view->close(); 
        delete view;
        delete scene;
        delete image;
    }); 
}


#include <QGraphicsItem>

class ImageItem : public QGraphicsItem
{
public:
    ImageItem( const QImage& m) : image_(m) {}
    ~ImageItem() {}

    QRectF boundingRect() const override
    {
        return scene()->sceneRect();        
    }

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0) override
    {   
        painter->drawImage( scene()->sceneRect(), image_, image_.rect() );
    }

private:
    const QImage& image_;
};


TEST( testImageViewer, byPainter ) 
{
    auto app = Fixture::instance()->appRunner();
    
    QImage *image; 
    ImageItem *imageItem;
    QGraphicsView *view; 
    QGraphicsScene *scene; 

    app->wait( [&]()
    {
        scene = new QGraphicsScene(-320,-240,640,480);

        image = new QImage(320,240, QImage::Format::Format_Grayscale8);
        scene->addItem( imageItem = new ImageItem(*image));

        view = new QGraphicsView( scene );
        view->show();
    });

    for( int i = 0 ; i < 255; i+=10)
    {
        app->wait( [&,i]()
        {
            image->fill( i );
            imageItem->update();
        });
        std::this_thread::sleep_for(100ms);
    }

    app->wait( [&]()
    {
        view->close(); 
        delete view;
        delete scene;
        delete image;
    }); 
}

TEST( testImageViewer, painterOnImage ) 
{
    auto app = Fixture::instance()->appRunner();
    
    QImage *image; 
    ImageItem *imageItem;
    QGraphicsView *view; 
    QGraphicsScene *scene; 

    app->wait( [&]()
    {
        scene = new QGraphicsScene(-320,-240,640,480);

        image = new QImage(320,240, QImage::Format::Format_Grayscale8);
        image->fill(255);
        scene->addItem( imageItem = new ImageItem(*image));

        view = new QGraphicsView( scene );
        view->show();
    });

    for( int i = 0 ; i < 255; i+=10)
    {
        app->wait( [&,i]()
        {
            QPainter painter(image);
            painter.drawRect( QRectF( i,i, 50, 60));
            painter.end();

            imageItem->update();
        });
        std::this_thread::sleep_for(100ms);
    }

    app->wait( [&]()
    {
        view->close(); 
        delete view;
        delete scene;
        delete image;
    }); 
}
