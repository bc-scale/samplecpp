#include "gtest/gtest.h"
#include "Fixture.h"
#include <QtWidgets>
#include <QImage>
#include <QLabel>
#include <QPixmap>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsItem>
#include "FrameGrabber.h"
#include "Poco/Event.h"
#include "useful.hpp"
#include "FramesScene.h"

using namespace std;
using namespace SpaceShape;

class ImageItem : public QGraphicsItem
{
public:
    ImageItem( const QImage& m) : image_(m) {}
    ~ImageItem() {}

    QRectF boundingRect() const override
    {
        return scene()->sceneRect();        
    }

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0) override
    {   
        painter->drawImage( scene()->sceneRect(), image_, image_.rect() );
    }

private:
    const QImage& image_;
};


TEST( testDisplay, copyFrameImage ) 
{
    auto app = Fixture::instance()->appRunner();
    
    QImage *image; 
    ImageItem *imageItem;
    QGraphicsView *view; 
    QGraphicsScene *scene;

    app->wait( [&]()
    {
        scene = new QGraphicsScene(-400,-300,800,600);

        image = new QImage(640,480, QImage::Format::Format_RGB888);
        scene->addItem( imageItem = new ImageItem(*image));

        view = new QGraphicsView( scene );
        view->show();
    });

    FrameGrabber fg{ "/dev/video0" };
    fg.setFormat( 640, 480, 24);
    fg.setBuffers( 4 );

    Poco::Event flag;
    fg.setGrabCallback( [&]( FrameGrabber*sender, Frame& frame){
        std::copy( frame.image, frame.image + frame.imageLength, image->bits() );
        scene->update();

        if( frame.index == 9 ) flag.set();
    });

    fg.grab(10);
    
    ASSERT_TRUE( flag.tryWait(10000) );

    app->wait( [&]() {
        view->close(); 
        delete view;
        delete scene;
        delete image;
    }); 
}

TEST( testDisplay, preloadImages ) 
{
    auto app = Fixture::instance()->appRunner();
    
    FramesScene* view;
    app->wait( [&]()
    {
        view = new FramesScene();
        view->show();
    });

    FrameGrabber fg{ "/dev/video0" };
    fg.setFormat( 640, 480, 24);
    fg.setBuffers( 4 );
    view->setFrames( fg.getBuffers() );

    Poco::Event flag;
    fg.setGrabCallback( [&]( FrameGrabber*sender, Frame& frame){
        view->updateCurrentFrame( frame );

        if( frame.index == 9 ) flag.set();
    });

    fg.grab(10);
    
    ASSERT_TRUE( flag.tryWait(10000) );

    app->wait( [&]() {
        view->close(); 
        delete view;
    }); 
}

TEST( testDisplay, aging ) 
{
    GTEST_SKIP();
    auto app = Fixture::instance()->appRunner();
    
    FramesScene* view;
    app->wait( [&]()
    {
        view = new FramesScene();
        view->show();
    });

    FrameGrabber fg{ "/dev/video0" };
    fg.setFormat( 640, 480, 24);
    fg.setBuffers( 8 );
    view->setFrames( fg.getBuffers() );

    int frameCount = 10000;
    Poco::Event flag;
    fg.setGrabCallback( [&]( FrameGrabber*sender, Frame& frame){
        view->updateCurrentFrame( frame );
        cout << string_format( "%d/%d - %08x\n", frame.index, frame.count, frame.image);
        if( frame.index == frameCount-1  ) flag.set();
    });

    fg.grab(frameCount);
    
    ASSERT_TRUE( flag.tryWait( std::numeric_limits<long>::max()));

    app->wait( [&]() {
        view->close(); 
        delete view;
    }); 
}
