#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "Fixture.h"
#include <fstream>
#include <chrono>
#include "useful.hpp"
#include "Shaper.h"
#include "ShapeFactory.h"
#include "GrabController.h"
#include <QQuickWidget>
#include <QQmlEngine>
#include <QQmlContext>
#include <list>

using namespace std;

struct GrabControllerTestBench
{
    GrabControllerTestBench(AppRunner* a, const char *mainqml) : app(a)
    {
        app->wait( [&]() { 
            frame = new QFrame();
            shaper = new SpaceShape::Shaper( frame );

            quick = new QQuickWidget();
            SpaceShape::GrabController::registerTypes("SpaceShape", 1, 0 );
            SpaceShape::Shaper::registerTypes( "SpaceShape", 1, 0 );
            SpaceShape::ShapeFactory::registerTypes( "SpaceShape", 1, 0 );
            quick->engine()->rootContext()->setContextProperty("shaper", shaper);
            quick->engine()->rootContext()->setContextProperty("qmlWidget", quick);
            quick->setSource(QUrl::fromLocalFile(mainqml));

            //frame->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
            frame->show();
        });        
    }

    ~GrabControllerTestBench()
    {
        app->wait( [&]() { 
            delete quick; 
            delete shaper;
            delete frame;
        });
    }

    AppRunner* app;
    QFrame *frame;
    SpaceShape::Shaper *shaper;
    QQuickWidget* quick;
};


TEST( testGrabController, basic ) 
{
    {
        ofstream f{ "main.qml"};
        f << R"(
import QtQuick 2.9;        
import SpaceShape 1.0
Text {
    text: "Hello from QML" 
    GrabController { 
        id : cam1 
        device : "/dev/video0" 
        frameSize : "320x240"; frameBPP : 24; bufferCount : 8;
    }
    ShapeFactory { id : fab }
    Component.onCompleted : {
        shaper
            .beginGrid( "main", [4,1], [4,1] )
                .add( "viewer", fab.framesScene() ).at( Qt.point(0,0) )
                .add( "byQML", qmlWidget ).at( Qt.point(1,0))
                .add( "grab", fab.pushButton( "Grab") ).at( Qt.point(0,1) )
                .add( "with", fab.framesScene() ).at( Qt.point(1,1) )
            .end();

        cam1.init();
        cam1.transmitTo( shaper.find( "main/viewer" ) );
        cam1.transmitTo( shaper.find( "main/with" ) );

        shaper.find("main/grab").onClicked.connect( grab );
    }

    function grab()
    {
        cam1.grab( 100000 );
    }
})";
    }

    GrabControllerTestBench bench(Fixture::instance()->appRunner(), "main.qml");
    bench.app->wait( [&bench]() {
        bench.frame->resize(400,400);
    });
    std::this_thread::sleep_for(100ms);
}

