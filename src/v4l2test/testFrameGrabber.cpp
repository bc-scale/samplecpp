#include "gtest/gtest.h"
#include "Fixture.h"
#include <iostream>
#include "FrameGrabber.h"
#include "Poco/Event.h"
#include "Poco/Semaphore.h"
#include "useful.hpp"

using namespace std;
using namespace SpaceShape;

TEST( testFrameGrabber, create ) 
{
    FrameGrabber fg{ "/dev/video0" };
}

TEST( testFrameGrabber, setFormat ) 
{
    FrameGrabber fg{ "/dev/video0" };
    fg.setFormat( 640, 480, 24);
    cout << fg.description() << endl;
}

TEST( testFrameGrabber, setBuffers ) 
{
    FrameGrabber fg{ "/dev/video0" };
    fg.setFormat( 640, 480, 24);
    fg.setBuffers( 4 );

    auto bufs = fg.getBuffers();
    ASSERT_EQ( 4, bufs.size() );
    for( auto& b : bufs )
    {
        ASSERT_TRUE( b.width == 640 && b.height == 480 && b.bpp == 24 );
        ASSERT_TRUE( b.image != nullptr && b.imageLength == 640*480*3 );
    }
}

TEST( testFrameGrabber, grab ) 
{
    FrameGrabber fg{ "/dev/video0" };
    fg.setFormat( 640, 480, 24);
    fg.setBuffers( 4 );

    Poco::Event flag;
    fg.setGrabCallback( [&](FrameGrabber*sender, Frame& frame){
        std::cout << string_format( 
            "grabbed %d / %d\n", frame.index, frame.count );
        flag.set(); 
    });

    ASSERT_EQ( FrameGrabber::idle, fg.status() );
    for( int i = 0; i < 10; ++i )
    {
        fg.grab(1);
        ASSERT_EQ( FrameGrabber::grabbing, fg.status() );
        ASSERT_EQ( true, flag.tryWait(10000));
    }
    ASSERT_EQ( false, flag.tryWait(1000));

    fg.stop();
}

TEST( testFrameGrabber, grab10 ) 
{
    FrameGrabber fg{ "/dev/video0" };
    fg.setFormat( 640, 480, 24);
    fg.setBuffers( 4 );

    Poco::Semaphore flag{ 0, 1000 };
    fg.setGrabCallback( [&](FrameGrabber*sender, Frame& f){
        std::cout << string_format( 
            "grabbed %d / %d with %dx%dx%d at %08x(%dKB)\n", 
            f.index, f.count, f.width, f.height, f.bpp, f.image, f.imageLength/1024 );
        flag.set(); 
    });

    fg.grab(10);
    for( int i = 0; i < 10; ++i )
    {
        ASSERT_EQ( true, flag.tryWait(10000));
    }
    ASSERT_EQ( false, flag.tryWait(1000));

    fg.grab(5);
    for( int i = 0; i < 5; ++i )
    {
        ASSERT_EQ( true, flag.tryWait(10000));
    }
    ASSERT_EQ( false, flag.tryWait(1000)); 

    fg.stop();
}

TEST( testFrameGrabber, exceptionAtCallback ) 
{
    FrameGrabber fg{ "/dev/video0" };
    fg.setFormat( 640, 480, 24);
    fg.setBuffers( 4 );
    fg.setGrabCallback( [&](FrameGrabber*sender, Frame& frame){
        throw runtime_error("hello");
    });
    fg.grab(10);
    this_thread::sleep_for( 2s );

    EXPECT_THROW({ fg.stop(); }, std::runtime_error );
}

TEST( testFrameGrabber, noGrabWhenGrabbing ) 
{
    FrameGrabber fg{ "/dev/video0" };
    fg.setFormat( 640, 480, 24);
    fg.setBuffers( 4 );
    fg.grab(10);
    EXPECT_THROW({ fg.grab(1); }, std::runtime_error );
}

TEST( testFrameGrabber, stopMultipleTimes ) 
{
    FrameGrabber fg{ "/dev/video0" };
    fg.setFormat( 640, 480, 24);
    fg.setBuffers( 4 );
    fg.grab(10);
    ASSERT_EQ( FrameGrabber::grabbing, fg.status() );
    
    this_thread::sleep_for( 100ms );
    fg.stop();
    ASSERT_EQ( FrameGrabber::idle, fg.status() );

    fg.stop();
    ASSERT_EQ( FrameGrabber::idle, fg.status() );
}

TEST( testFrameGrabber, video2 ) 
{
    GTEST_SKIP();
    FrameGrabber fg{ "/dev/video2" };
    fg.setFormat( 640, 480, 24);
    cout << fg.description() << endl;
    fg.setBuffers( 4 );

    Poco::Semaphore flag{ 0, 1000 };
    fg.setGrabCallback( [&](FrameGrabber*sender, Frame& f){
        std::cout << string_format( 
            "grabbed %d / %d with %dx%dx%d at %08x(%dKB)\n", 
            f.index, f.count, f.width, f.height, f.bpp, f.image, f.imageLength/1024 );
        flag.set(); 
    });

    fg.grab(10);
    for( int i = 0; i < 10; ++i ) { flag.tryWait(1000);}
}
