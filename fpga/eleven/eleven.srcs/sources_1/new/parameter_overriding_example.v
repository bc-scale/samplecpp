`timescale 1ns / 1ps

module parameter_overriding_example(
    input x,y,z,
    output f
    );
    
    hazard_static #(.DELAY2(4), .DELAY1(6)) example( x,y,z,f);
    
endmodule

module hazard_static 
    #( parameter DELAY1 = 2, DELAY2 = 5 )
    ( input x, y, z, output f );
    
    wire a,b,c;
    and #DELAY2 a1( b, x, y);
    not #DELAY1 n1(a,x);
    and #DELAY2 a2(c,a,z);
    or #DELAY2 o2(f,b,c);
    
endmodule