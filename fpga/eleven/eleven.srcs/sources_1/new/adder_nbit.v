`timescale 1ns / 100ps

module adder_nbit
    #( parameter N = 4 )
    ( input [N-1:0] x, y,
      input c_in,
      output [N-1:0] sum,
      output c_out
    );
    
    assign { c_out, sum } = x + y + c_in;
    
endmodule
