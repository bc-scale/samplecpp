`timescale 1ns / 1ps

module pr_sequence_generator_self_start
    #(  parameter N = 4,
        parameter [N:0] tap = 5'b10011 )
    (   input clk,
        output reg [N-1:0] qout );
        
    wire d = ^(tap[N-1:0] & qout[N-1:0] )
        | (~(|qout[N-1:0]));
    
    always @(posedge clk)
        qout <= { d, qout[N-1:1] };     
       
endmodule
