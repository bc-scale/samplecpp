`timescale 1ns / 1ps

module zero_count_for(
    input [7:0] data,
    output reg [3:0] out
    );
    
    integer i;
    always @(data) begin
        out = 0;
        for( i = 0; i <=7; i = i + 1) 
            if( data[i] == 0) out = out + 1;            
    end
        
endmodule
