`timescale 1ns / 1ps
`include "if_my_int.sv"
`include "if_bottoms.sv"

module if_top(
    input clk,
    input s1,
    input [9:0] d1, d2,
    output equal);
    
    my_int int3(); //instantiation

    if_bottom1 u0 (int3, clk, d1, d2, s1, equal);
    if_bottom2 u1 (int3, clk);

endmodule