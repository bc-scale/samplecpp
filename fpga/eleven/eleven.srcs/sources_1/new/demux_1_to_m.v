`timescale 1ns / 1ps

module demux_1_to_m
    #(  parameter M = 4,
        parameter K = 2 )
    (   input [K-1:0] select,
        input in,
        output reg [M-1:0] y );
    
    integer i;
    always @(*) begin
        for ( i = 0; i < M; i = i + 1 ) begin
            if ( select == i )
                y[i] = in;
            else 
                y[i] = 1'b0;
        end
    end 
    
endmodule
