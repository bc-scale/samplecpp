`timescale 1ns / 1ps

module cascaded_synchronizer
    #(  parameter N = 2 )
    (   input clk, reset_n, d,
        output q_out );
        
    reg [N-1:0] q;
    assign q_out = q[N-1];
    
    always @(posedge clk or negedge reset_n) begin
        if (!reset_n) q <= 0;
        else q <= { q[N-2:0], d };
    end
        
endmodule
