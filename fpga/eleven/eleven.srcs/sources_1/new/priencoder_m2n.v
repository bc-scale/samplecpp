`timescale 1ns / 1ps

module priencoder_m2n
    #(  parameter M = 8,
        parameter N = 3 )
    (   input [M-1:0] x,
        output valid_in,
        output reg [N-1:0] y );
    
    integer i;
    assign valid_in = |x;
    always @(*) begin : check_for_1
        for( i = M-1; i >= 0; i = i - 1)
            if ( x[i] == 1 ) begin 
                y = i;
                disable check_for_1;
            end else y = 0;
    end 
    
endmodule
