`timescale 1ns / 100ps

module mux4_to_1_ifelse(
    input i0, i1, i2, i3,
    input s1, s0,
    output reg out
    );
    
    always @(*)
        if (s1) begin
            if (s0) out = i3; else out = i2; end
        else begin
            if (s0) out = i1; else out = i0; end
            
endmodule
