`timescale 1ns / 1ps

module register_file
    #(  parameter M = 4,    // number of address bits
        parameter N = 16,   // number of words
        parameter W = 8 )   // number of bits in a word
    (     
        input clk, wr_enable,
        input [W-1:0] din,
        input [M-1:0] rd_addra, rd_addrb, wr_addr,
        output [W-1:0] douta, doutb
    );
    
    reg [W-1:0] reg_files[N-1:0];
    assign douta = reg_files[rd_addra], 
        doutb = reg_files[rd_addrb];
        
    always @(posedge clk)
        if ( wr_enable) reg_files[ wr_addr ] <= din;
    
endmodule
