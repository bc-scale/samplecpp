`timescale 1ns / 1ps

module two_counters(
    input clock, clear,
    output [3:0] qout4b,
    output [7:0] qout8b
    );
    
    defparam cnt_4b.N = 4, cnt_8b.N = 8;
    count_nbits cnt_4b( clock, clear, qout4b);
    count_nbits cnt_8b( clock, clear, qout8b);
    
endmodule

module counter_nbits
    #( parameter N = 4 )
    ( input clock, clear, output reg[N-1:0] qout );
    always @(posedge clear or negedge clock )
    begin
        if( clear ) qout <= { N{1'b0} };
        else qout <= (qout + 1);
    end
endmodule