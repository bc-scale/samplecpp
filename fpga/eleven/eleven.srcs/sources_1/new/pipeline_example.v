`timescale 1ns / 1ps

module pipeline_example    
    #(  parameter N = 8 ) 
    (   input clk,
        input [N-1:0] a, b, c,
        output reg [N-1:0] total );
    
    /*
    // incorrect : Can you tell ?
    reg [N-1:0] qa, qb;
    always @(posedge clk) begin
        qa <= a; 
        qd <= qa + b;
        total <= c - qb;        
    end
    */
        
    reg [N-1:0] qa, qb, qc, qd, qe;    
    always @(posedge clk) begin
        qa <= a; qb <= b; qc <= c;
        qd <= qa + qb;
        qe <= qc;
        total <= qe - qd;        
    end
    
endmodule
