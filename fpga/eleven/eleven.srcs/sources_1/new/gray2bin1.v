`timescale 1ns / 1ps

module gray2bin1
    #(  parameter SIZE = 8 )
    (   input [SIZE-1:0] gray,
        output [SIZE-1:0] bin    
    );
    
    genvar i;
    generate 
        for ( i = 0; i < SIZE; i = i + 1 ) 
        begin : bit
            assign bin[i] = ^gray[ SIZE-1 : i ];
        end
    endgenerate     
    
endmodule
