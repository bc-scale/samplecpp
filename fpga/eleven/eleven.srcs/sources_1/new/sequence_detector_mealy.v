`timescale 1ns / 1ps

module sequence_detector_mealy
    (   input clk, reset_n, x,
        output reg z );
        
    reg [1:0] present, next;  
    
    localparam 
        A = 2'b00,
        B = 2'b01,
        C = 2'b10,
        D = 2'b11;
        
    always @(posedge clk or negedge reset_n)
        if (!reset_n) present <= A;
        else present <= next;
  
    always @(present or x) 
        case (present)
            A : if(x) next = A; else next = B;  
            B : if(x) next = C; else next = A;
            C : if(x) next = A; else next = D;
            D : if(x) next = C; else next = B;    
        endcase
      
    always @(present, x )
        case (present)
            A : if(x) z = 1'b0; else z = 1'b0;         
            B : if(x) z = 1'b0; else z = 1'b0;         
            C : if(x) z = 1'b0; else z = 1'b0;         
            D : if(x) z = 1'b1; else z = 1'b0;
        endcase         
        
endmodule
