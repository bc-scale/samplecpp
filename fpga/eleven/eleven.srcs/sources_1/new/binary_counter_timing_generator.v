`timescale 1ns / 1ps

module binary_counter_timing_generator
    #(  parameter N = 9,
        parameter M = 3 )
    (   input clk, reset, enable,
        output reg [N-1:0] qout );
        
    reg [M-1:0] bcnt_out;
    
    always @(posedge clk or posedge reset ) begin
        if (reset) bcnt_out <= {M{1'b0}};
        else begin
            if (enable) bcnt_out <= bcnt_out + 1;
            else bcnt_out <= bcnt_out;
        end
    end
    
    always @(bcnt_out)
        qout <= { {N-1{1'b0}}, 1'b1} << bcnt_out;
        
endmodule
