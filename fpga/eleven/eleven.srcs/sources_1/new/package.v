function automatic [15:0] fact( input [15:0] n );
    if (n == 0) fact = 1;
    else fact = n * fact( n -1 );
endfunction
