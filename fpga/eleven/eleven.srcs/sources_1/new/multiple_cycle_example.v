`timescale 1ns / 1ps

module multiple_cycle_example
    #(  parameter N = 8 ) 
    (   input clk,
        input [N-1:0] a, b, c,
        output reg [N-1:0] total );
        
    reg [N-1:0] qa, qb;
    
    always @(posedge clk) begin
        qa <= a;
        @( posedge clk )
            qb = qa + b;
        @( posedge clk )
            total <= c - qb;
    end
                        
endmodule
