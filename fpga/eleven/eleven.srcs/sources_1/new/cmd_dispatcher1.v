`timescale 1ns / 1ps

module cmd_dispatcher1
    #(  parameter integer DATA_WIDTH = 32 )
    (
        input wire aclk, areset_n, tlast, tvalid,
        input wire [DATA_WIDTH-1 : 0] tdata,
        input wire [(DATA_WIDTH/8)-1 : 0] tstrb,
        
        output wire tready,
        output reg dispatch,
        output reg [1:0] present
    );

    localparam 
        IDLE = 2'b00,
        LOAD = 2'b01,
        DISPATCH = 2'b10,
        NO_WAY = 2'b11;

    reg buffer_full = 0;
    reg [DATA_WIDTH-1:0] cmd;
    // reg [1:0] present;
    reg [1:0] next; 

    assign tready = ~buffer_full;
        
    always @(posedge aclk ) begin
        if (!areset_n) begin 
            present <= IDLE;
            buffer_full <= 0;
        end else 
            present <= next;
    end
  
    always @(*) begin
        case (present)
            IDLE : 
                if(tvalid==1 && tready==1) begin
                    next = LOAD;
                end else 
                    next = IDLE;  
            LOAD : next = DISPATCH;
            DISPATCH : next = IDLE;
            NO_WAY: ;
        endcase
    end
           
    always @(posedge aclk) begin
        case (present)
            IDLE : begin
                if (tvalid==1 && tready==1) begin
                    buffer_full <= 1;
                end    
            end 
                    
            LOAD : begin
                cmd <= { {DATA_WIDTH-8{1'b0}}, tdata[7:0]};
            end         
            
            DISPATCH : begin
                dispatch <= cmd[0];
                buffer_full <= 0;
            end        
            
            NO_WAY : ;
        endcase         
    end
        
endmodule
