`timescale 1ns / 100ps

module tasks_function_sharing2;
    
    reg[15:0] result;
    `include "package.v"
    
    initial begin
        result = fact(5);
        $display( "The result is %d", result );   
    end

endmodule
