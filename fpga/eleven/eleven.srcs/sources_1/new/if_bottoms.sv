`timescale 1ns / 1ps
`include "if_my_int.sv"

module if_bottom1(
    if_my_int int1,
    input clk,
    input [9:0] d1, d2,
    input s1,
    output logic equal);    
    
    always @(*) begin
        if (int1.sel)
            int1.result <= int1.data1;
    end     
endmodule

module if_bottom2(
    if_my_int int1,
    input clk );    
    
    always @(*) begin
        if (int1.sel)
            int1.result <= int1.data1;
    end     
endmodule
