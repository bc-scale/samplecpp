`timescale 1ns / 1ps

module counter( 
    input clock, clear,
    output reg [3:0] qout
    );
    
    always @(negedge clock or posedge clear) begin
        if(clear)
            qout <= 4'd0;
        else
            qout <= (qout+1);
    end
    
endmodule
