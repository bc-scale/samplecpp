`timescale 1ns / 1ps

module booth_style2_step1
    #(  parameter W = 8,        // default word size
        parameter N = 3 )       // n = log2(w)
    (   input clk, start_n, 
        input [W-1:0] multiplier, multiplicand,
        output wire [2*W-1:0] product,
        output reg finish );
    
    reg [W-1:0] mcand, acc;
    reg [N-1:0] cnt;
    reg [W:0] mp;    
    reg [1:0] present, next; 
    
    localparam 
        idle = 2'b00,
        load = 2'b01,
        compute = 2'b10,
        shift_right = 2'b11;
        
    always @(posedge clk or negedge start_n)
        if (!start_n) present <= idle;
        else present <= next;
  
    always @(*) 
        case (present)
            idle : if(!finish) next = load; else next = idle;  
            load : next = compute;
            compute : next = shift_right;
            shift_right : if(cnt==0) next = idle; else next = compute;    
        endcase
      
    always @( posedge clk or negedge start_n ) begin
        if (!start_n) finish <= 0;
        case (present)
            idle : ;         
            load : begin
                mp <= { multiplier, 1'b0 };
                mcand <= multiplicand;
                acc <= 0;
                cnt <= W-1;
            end         
            compute : begin
                case ( mp[1:0] )
                    2'b01 : acc <= acc + mcand;
                    2'b10 : acc <= acc - mcand;
                    default : ;
                endcase
            end        
            shift_right : begin
                {acc, mp} <= { acc[W-1], acc, mp[W:1] };
                cnt <= cnt-1;
                if ( cnt==0 ) finish <= 1; 
            end
        endcase         
    end
    
    assign product = { acc, mp[W:1] };
            
endmodule
