`timescale 1ns / 100ps

// Synthesis : primitive not supported 
/* 
primitive udp_or( output out, input a, b );
    table
        0   0 : 0;
        0   1 : 1;
        1   0 : 1;
        1   1 : 1;
    endtable        
endprimitive
*/