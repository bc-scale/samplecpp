`timescale 1ns / 1ps

module sync_ram
    #(  parameter N = 16,   // number of words 
        parameter A = 4,    // number of address bits
        parameter W = 4     // number of word size in bits
    )
    (   input [A-1:0] addr,
        input [W-1:0] din,
        input cs, wr, clk,
        output reg [W-1:0] dout 
    );
    
    reg [W-1:0] ram[ N-1:0];
    always @(posedge clk)
        if (cs) 
            if (wr)
                ram[addr] <= din;
            else
                dout <= ram[addr];

endmodule
