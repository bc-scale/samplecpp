`timescale 1ns / 1ps

module frequency_divided_synchronizer
    #(  parameter N = 1 )
    (   input clk, reset_n, d,
        output q_out );
     
    reg [1:0] q;
    reg [N-1:0] clk_q;
    wire clk_sync;
    
    always @(posedge clk or negedge reset_n ) begin
        if (!reset_n) clk_q <= 0;
        else clk_q <= clk_q + 1;
    end
    
    assign clk_sync = clk_q[N-1];
    
    always @(posedge clk_sync or negedge reset_n ) begin
        if (!reset_n) q <= 0;
        else q <= { q[0], d };
    end
    
    assign q_out = q[1];    
        
endmodule
