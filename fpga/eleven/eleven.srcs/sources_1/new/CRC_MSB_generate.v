`timescale 1ns / 1ps

module CRC_MSB_generate
    #(  parameter N = 4,
        parameter [N:0] tap = 5'b10011)
    (   input clk, reset_n, data,
        output reg [N-1:0] qout );
    
    wire d = data ^ qout[N-1];
    genvar i;
    generate 
        for( i = 0; i< N; i = i + 1) 
        begin : crc_generator
            if (i == 0 )
                always @(posedge clk or negedge reset_n)
                    if ( !reset_n ) qout[i] <= 1'b0;
                    else qout[i] <= d;
            else
                always @(posedge clk or negedge reset_n)
                    if ( !reset_n ) qout[i] <= 1'b0;
                    else if (tap[i] == 1) qout[i] <= qout[i-1] ^ d;
                    else qout[i] <= qout[i-1];
        end
    endgenerate
       
endmodule
