`timescale 1ns / 1ps

module ring_counter_timing_generator
    #(  parameter N = 4 )
    (   input clk, reset,
        output reg [0:N-1] qout );
        
    always@( posedge clk or posedge reset) 
        if (reset) qout <= { 1'b1, {N-1{1'b0}}};
        else    qout <= { qout[N-1], qout[0:N-2] };
        
endmodule
