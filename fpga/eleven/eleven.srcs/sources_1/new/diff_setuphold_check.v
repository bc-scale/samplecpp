`timescale 1ns / 1ps

module diff_setuphold_check(
    input clk, d, reset_n,
    output reg q
    );
    
    specify 
        specparam tSU = 10, tHD = 2;
        specparam tPLHclk = 4:6:9, tPHLclk = 5:8:11;
        specparam tPLHreset = 3:5:6, tPHLreset = 4:7:9;
        (clk *> q) = (tPLHclk, tPHLclk );
        (reset_n *> q) = (tPLHreset, tPLHreset);
        $setup( d, posedge clk &&& reset_n, tSU );
        $hold( posedge clk, d &&& reset_n, tHD );
    endspecify
          
    always @(posedge clk or negedge reset_n)
        if (!reset_n) q <= 0;
        else q <= d;
    
endmodule
