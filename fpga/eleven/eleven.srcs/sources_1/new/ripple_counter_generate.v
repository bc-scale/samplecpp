`timescale 1ns / 1ps

module ripple_counter_generate
    #(  parameter N = 4 )
    (   input clk, reset_n,
        output reg [N-1:0] qout );
        
    genvar i;
    generate 
        for ( i = 0; i < N; i = i + 1) begin : ripple_counter
            if ( i == 0 )
                always @(negedge clk or negedge reset_n )
                    if ( !reset_n)  qout[0] <= 1'b0;
                    else qout[0] <= ~qout[0];
            else
                always @(negedge qout[i-1] or negedge reset_n )
                    if ( !reset_n ) qout[i] <= 1'b0;
                    else qout[i] = ~qout[i];
        end
    endgenerate
        
endmodule
