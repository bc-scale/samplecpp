`timescale 1ns / 1ps

module decoder_m2n_high
    #(  parameter M = 3,
        parameter N = 8 )
    (   input [M-1:0] x,
        input enable,
        output reg [N-1:0] y );
    
    always @( x or enable )
        if ( !enable ) y = { N{1'b0} };
        else y = { {N-1{1'b0}}, 1'b1} << x;
        
endmodule
