`timescale 1ns / 100ps

module zero_count_while(
    input [7:0] data,
    output reg [3:0] out
    );
    
    integer i;
    always @(data) begin
        out = 0; i = 0;
        while( i <= 7 ) begin
            if( data[i] == 0 ) out = out + 1;
            i = i + 1;
        end
    end    
    
endmodule
