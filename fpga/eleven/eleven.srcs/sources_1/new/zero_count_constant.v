`timescale 1ns / 100ps

module zero_count_constant( out, data );

    parameter SIZE = 8;
    localparam M = count_log_b2(SIZE);

    output reg [M:0] out;
    input [SIZE-1:0] data;

    integer i;

    always @(data) begin
        out = 0;
        for( i = 0; i <= 7; i = i + 1 )
            if( data[i] == 0 ) out = out + 1;
    end
    
    function integer count_log_b2( input integer depth );
    begin
        count_log_b2 = -1;
        while( depth ) begin
            count_log_b2 = count_log_b2 + 1;
            depth = depth >> 1;
        end
    end endfunction

endmodule
