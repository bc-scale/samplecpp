`timescale 1ns / 100ps

module trailing_zero_4b(
    input [3:0] data,
    output reg [2:0] out
    );
    
    always @(data)
        casex(data)
            4'bxxx1 : out = 0;
            4'bxx10 : out = 1;
            4'bx100 : out = 2;
            4'b1000 : out = 3;
            4'b0000 : out = 4;
            default : out = 3'b111;
        endcase
    
endmodule
