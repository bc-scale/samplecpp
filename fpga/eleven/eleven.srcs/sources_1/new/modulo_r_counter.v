`timescale 1ns / 1ps

module modulo_r_counter
    #(  parameter N = 4,
        parameter R = 10 )
    (   input clk, enable, reset,
        output reg [N-1:0] qout,
        output cout );
        
    assign cout = (qout == R-1);
    
    always @(posedge clk) begin
        if (reset) qout <= 0;
        else begin
            if( enable ) begin 
                if ( cout) qout <= 0;
                else qout <= qout + 1;
            end
        end
    end   
          
endmodule
