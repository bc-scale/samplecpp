`timescale 1ns / 1ps

module mux_m_to_1
    #(  parameter M = 4,
        parameter K = 2 )
    (   input [K-1:0] select,
        input [M-1:0] in,
        output reg y );
        
    integer i;
    always @(*) begin
        y = 1'b0;
        for ( i = 0; i < M; i = i + 1) 
            if ( select == i ) y = in[i];
    end
        
endmodule
