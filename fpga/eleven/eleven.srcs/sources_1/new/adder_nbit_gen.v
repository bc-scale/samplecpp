`timescale 1ns / 1ps

module adder_nbit_gen
    #(  parameter N =4)
    (   input [N-1:0] x, y,
        input c_in,
        output [N-1:0] sum,
        output c_out
    );
    
    genvar i;
    wire [N-2:0] c;
    generate
        for ( i = 0; i < N; i = i + 1) 
        begin : adder
            if ( i == 0 )
                full_adder fa( x[i], y[i], c_in, sum[i], c[i] );
            else if ( i == N-1 )
                full_adder fa( x[i], y[i], c[i-1], sum[i], cout );
            else
                full_adder fa( x[i], y[i], c[i-1], sum[i], c[i] );
        end
    endgenerate
       
endmodule

module full_adder
    (   output sum, c_out,
        input x, y, c_in );
        
        assign { c_out, sum } = x + y + c_in;

endmodule
