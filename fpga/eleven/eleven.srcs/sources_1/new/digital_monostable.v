`timescale 1ns / 1ps

module digital_monostable
    #(  parameter M = 3,        // maximum pulse width = 2 ** M
        parameter PW = 5 )      // actual pulse width
    (   input clk, reset_n, start,
        output reg out );
        
    reg [M-1:0] bcnt_out;
    wire d;
    
    always @(posedge clk or negedge reset_n )
        if (!reset_n || (start && ~out))
            bcnt_out <= {M{1'b0}};
        else if ( out) bcnt_out <= bcnt_out + 1;
        
    assign d = (start & ~out) | (~(bcnt_out == (PW-1)) & out);
    
    always @(posedge clk or negedge reset_n)
        if (!reset_n) out <= 1'b0;
        else out <= d;
       
endmodule
