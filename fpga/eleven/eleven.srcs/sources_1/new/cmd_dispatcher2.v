`timescale 1ns / 1ps

module cmd_dispatcher2
    #(  parameter integer DATA_WIDTH = 32 )
    (
        input wire aclk, areset_n, tlast, tvalid,
        input wire [DATA_WIDTH-1 : 0] tdata,
        input wire [(DATA_WIDTH/8)-1 : 0] tstrb,
        
        output wire tready,
        output reg dispatch,
        output reg [1:0] state
    );

    localparam 
        IDLE = 2'b00,
        LOAD = 2'b01,
        DISPATCH = 2'b10
        ;

    reg buffer_full;
    reg [DATA_WIDTH-1:0] cmd;

    assign tready = ~buffer_full;
        
    always @(posedge aclk or negedge areset_n ) begin
         if (!areset_n) begin 
            state <= IDLE;
            buffer_full <= 0;
        end else begin
        
            case ( state ) 
                IDLE : begin
                    if(tvalid==1 && tready==1) begin
                        state <= LOAD;
                        buffer_full <= 1;
                    end                        
                end
                
                LOAD : begin
                    cmd <= { {DATA_WIDTH-8{1'b0}}, tdata[7:0]};
                    state <= DISPATCH;                
                end
                
                DISPATCH : begin
                    dispatch <= cmd[0];
                    buffer_full <= 0;
                    state <= IDLE;
                end
                            
            endcase            
        end        
    end
        
endmodule
