`timescale 1ns / 1ps

module tb_reset;
    parameter clk_period = 10;
    reg clk;
    reg reset;
    
    always begin
        #(clk_period/2) clk <= 1'b0;
        #(clk_period/2) clk <= 1'b1;
    end
    
    initial begin
        reset <= 1'b0;
        wait ( clk !== 1'bx );
        repeat (3) @(negedge clk) reset <= 1'b1;
        reset <= 1'b0;
    end

endmodule
