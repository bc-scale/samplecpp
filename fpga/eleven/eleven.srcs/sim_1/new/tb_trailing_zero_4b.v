`timescale 1ns / 100ps

module tb_trailing_zero_4b;
    reg [3:0] data;
    wire [2:0] out;
    
    trailing_zero_4b uut( .data(data), .out(out));
    
    initial begin
        #10
        
        #10 data = 4'b0000;    
        #10 data = 4'b0010;    
        #10 data = 4'b0011;    
        #10 data = 4'b0100;    
        #10 data = 4'b1010;    
        #10 data = 4'b1000;    
        #10 data = 4'b0000;    
        #10 data = 4'b0110; 
        
        #10 $finish;   
    
    end

endmodule
