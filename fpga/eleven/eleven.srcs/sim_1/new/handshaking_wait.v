`timescale 1ns / 100ps
module handshaking_wait;

    reg [7:0] data, data_output;
    reg ready = 0, ack = 0;
    integer seed = 5;
    
    initial begin
        #400 $finish;
    end
    
    always begin: source
        #5 data <= $random(seed) % 13;
        $display ("The source data is : %d", data );
        #2 ready <= 1;
        wait( ack );
        #12 ready <= 0;
        seed = seed + 3;
    end
    
    always begin : destination
        wait( ready );
        #5 ack <= 1;
        @(negedge ready ) data_output <= data;
        $display( "The output data is %d", data_output );
        #3 ack <= 0;
    end

endmodule
