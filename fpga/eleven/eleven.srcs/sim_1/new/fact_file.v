`timescale 1ns / 100ps

module fact_file;

    integer n, result, fd;
    initial begin
        fd = $fopen( "fact_result.dat", "w" );
        if ( fd == 0 )
            $display( "File open error !" );
            
        for( n = 0; n <= 12; n = n + 1 ) begin
            result = fact( n );
            $fdisplay( fd, "%0d factorial = %0d", n ,result );
        end
        
        $fclose( fd );
    end
    
    function automatic integer fact( input [31:0] n );
        if (n == 0 ) fact = 1;
        else fact = n * fact( n - 1 );
    endfunction
    
endmodule
