`timescale 1ns / 100ps

module tb_counter;

    reg clk, reset;
    wire [3:0] qout;
    
    always begin
        #5 clk = 1'b1;
        #5 clk = 1'b0;
    end 
    
    counter utt( .clock(clk), .clear(reset), .qout(qout) );
    
    initial begin
        reset = 1'b0;        
        #20 reset = 1'b1;
        #20 reset = 1'b0;
        #10 $display( $realtime,, "qout : %x", qout );
        #10 $display( $realtime,, "qout : %x", qout );
        #10 $display( $realtime,, "qout : %x", qout );
        #10 $display( $realtime,, "qout : %x", qout );
        #10 $display( $realtime,, "qout : %x", qout );
        #10 $display( $realtime,, "qout : %x", qout );
        
        #100

        #50 $finish;
    end

endmodule
