`timescale 1ns / 100ps

module tb_random_nbit_adder;
    parameter N = 4;
    reg [N-1:0] x, y;
    reg c_in;
    wire [N-1:0] sum;
    wire c_out;
    
    nbit_adder UUT( 
        .x(x), .y(y), .c_in(c_in),
        .sum(sum), .c_out(c_out));

    integer i;
    reg [N:0] test_sum;
    initial 
        for ( i = 0; i <= 2*N; i = i + 1 ) begin
            x = $random % 2**N;
            y = $random % 2**N;
            c_in = 1'b0;
            test_sum = x + y;
            #15;
            if ( test_sum != { c_out, sum }) 
                $display( "Error iteration %h", i );
            #5;
        end   
    initial
        #200 $finish;
        
    initial
        $monitor( $realtime, "ns %h %h %h %h", x, y, c_in, {c_out, sum} );          
                
endmodule
