`timescale 1ns / 100ps
module static_task;

    reg [3:0] result;
    
    initial begin
        #3 check_counter( 1'b1, result );
        $display( $realtime, , "The value of count is %d", result );
        
        #6 check_counter( 1'b0, result );
        $display( $realtime, , "The value of count is %d", result );

        #1 check_counter( 1'b0, result );
        $display( $realtime, , "The value of count is %d", result );
    end
    
    task check_counter( input reset, output reg[3:0] count );
        begin
            if( reset ) count = 0;
            else begin
                #2 count = count + 1;
                #3 count = count + 1;
                #5 count = count + 1;
            end
        end
    endtask
    

endmodule
