`timescale 1ns / 100ps

module named_event(
    );
    reg [7:0] count;
    event ready;
    
    initial begin
        count = 0;
        #400 $finish;
    end
    
    always begin
        #2 count <= count + 1;
        if ( count %5 == 0 ) -> ready;
    end
    
    always @(ready)
        $display($realtime,,"The count is %d", count );
    
endmodule
