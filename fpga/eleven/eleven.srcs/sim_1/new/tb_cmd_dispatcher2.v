`timescale 1ns / 1ps

module tb_cmd_dispatcher2;

    reg aclk, areset_n, tlast, tvalid;
    reg [31:0] tdata;
    reg [3:0] tstrb;
    wire tready, dispatch;
    wire [1:0] state;    
    
    always begin
        #5 aclk = 1'b1;
        #5 aclk = 1'b0;
    end 
    
    cmd_dispatcher2 utt( 
        .aclk(aclk), .areset_n(areset_n), 
        .tdata(tdata), .tstrb(tstrb), .tlast(tlast), .tvalid(tvalid),
        .tready(tready), .dispatch( dispatch),
        .state(state)
    );

    initial begin
        areset_n = 1;
        tlast = 0;
        tvalid = 0;
        tdata = 0;
        tstrb = 0;
        
        #10 areset_n = 0;
        #100 areset_n = 1;
        
        #100 tdata = 32'hCAFEBEEF; tvalid = 1;
        #10 tvalid = 0;

        #50 tdata = 32'h1234A0A0; tvalid = 1;
        #10 tvalid = 0;
        
        #50 tdata = 32'hBEEF0001; tvalid = 1;
        #10 tdata = 32'hBEEF0002; 
        #10 tdata = 32'hBEEF0003; 
        #10 tdata = 32'hBEEF0004; 
        #10 tdata = 32'hBEEF0005; tvalid = 0;

        #100 $finish;
    end    
    
endmodule
