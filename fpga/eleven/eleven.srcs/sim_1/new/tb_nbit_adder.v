`timescale 1ns / 100ps

module tb_nbit_adder;
    parameter N = 4;
    reg [N-1:0] x, y;
    reg c_in;
    wire [N-1:0] sum;
    wire c_out;
    
    nbit_adder UUT( 
        .x(x), .y(y), .c_in(c_in),
        .sum(sum), .c_out(c_out));
  
    reg [2*N-1:0] i;
    initial 
        for ( i =0; i <= 2**(2*N)-1; i = i + 1 ) begin 
            x[N-1:0] = i[2*N-1:N];
            y[N-1:0] = i[N-1:0];
            c_in = 1'b0;
            #20;
        end
        
    initial
        #1280 $finish;
        
    initial
        $monitor( $realtime, "ns %h %h %h %h", x, y, c_in, {c_out, sum} );          
                
endmodule
