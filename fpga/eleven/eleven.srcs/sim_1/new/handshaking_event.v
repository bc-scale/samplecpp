`timescale 1ns / 100ps

module handshaking_event;
    reg [7:0] data, data_output;
    event ready, ack;
    integer seed = 1;
    
    initial begin
        #5 -> ack;
        #50 $finish;
    end
    
    always @(ack) begin : sender
        data = $random(seed) % 13;
        $display( $realtime,, "The source data is : %d", data );
        seed = seed + 7;
        #5 -> ready;
    end
    
    always @(ready) begin : receiver
        data_output = data;
        $display ($realtime,, "The output data is : %d", data_output );
        #5 -> ack;
    end
    
endmodule
