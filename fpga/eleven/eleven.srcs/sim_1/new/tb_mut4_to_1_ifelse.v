`timescale 1ns / 100ps

module tb_mut4_to_1_ifelse;
    reg i0, i1, i2, i3;
    reg s1, s0;
    wire out;

    initial begin
        #5;
        
        i0 = 1; i1 = 0; i2 = 1; i3 = 0;
        s1 = 0; s0 = 0;
        #10 $display( $realtime,, "out : %x", out );
        
        s1 = 0; s0 = 1;
        #10 $display( $realtime,, "out : %x", out );

        s1 = 1; s0 = 0;
        #10 $display( $realtime,, "out : %x", out );

        s1 = 1; s0 = 1;
        #10 $display( $realtime,, "out : %x", out );

        #10 $finish;
    end

    mux4_to_1_ifelse utt( i0, i1, i2, i3, s1, s0, out );  
        
endmodule
