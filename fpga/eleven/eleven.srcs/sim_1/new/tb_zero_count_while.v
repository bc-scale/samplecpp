`timescale 1ns / 100ps

module tb_zero_count_while;
    reg [7:0] data;
    wire [3:0] out;
    
    zero_count_while uut( .data(data), .out(out));
    
    initial begin
        #10
        
        #10 data = 8'h00;
        #10 data = 8'hAA;
        #10 data = 8'b1101_0101;
        #10 data = 8'b0000_0001;
        #10 data = 8'hFF;
    
        #10 $finish;
    end

endmodule
